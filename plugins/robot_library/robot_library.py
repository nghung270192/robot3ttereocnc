# -*- coding: utf-8 -*-


import os.path

from plugins.robot_library import robot_library_window



def on_configwin_robotsmodel(parent,idx):

	f = parent.object_search_path("robot.library")

	if f == []:
		f = robot_library_window.robot_library_window(parent,idx)
		parent.add_function("robot.library","Robot library",f)
	else:
		f=f[0]

	f.show()

	return