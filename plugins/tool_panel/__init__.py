# -*- coding: utf-8 -*-


import os.path


pluginName = "Tool panel"
version = "0.1"
author = "Olivier Martinez"
tool_icon = None



def register(parent):


	mod = imp.load_source('tool_panel', './plugins/tool_panel/tool_panel.py')

	img = gtk.Image()
	img.set_from_file('./plugins/tool_panel/tool.png')
	img.show()
	tool_icon = gtk.ToggleToolButton ("Tool")
	tool_icon.set_icon_widget(img)
	tool_icon.set_label("Tool")
	tool_icon.connect("toggled", mod.on_tool_toggle, parent)
	tool_icon.set_active(False)
	parent.add_icon(tool_icon)

	return

