# -*- coding: utf-8 -*-


import os.path


pluginName = "Robot panel"
version = "0.1"
author = "Olivier Martinez"
robot_icon = None


def register(parent):

	mod = imp.load_source('tereocnc.plugins.robot_panel', './plugins/robot_panel/robot_panel.py')

	img = gtk.Image()
	img.set_from_file('./plugins/robot_panel/robot.png')
	img.show()
	robot_icon = gtk.ToggleToolButton ("Robot")
	robot_icon.set_label("Robot")
	robot_icon.set_icon_widget(img)
	robot_icon.connect("toggled", mod.on_robot_toggle, parent)
	robot_icon.set_active(False)
	parent.add_icon(robot_icon)



	return

