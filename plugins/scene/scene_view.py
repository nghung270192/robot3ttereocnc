# -*- coding: utf-8 -*-

import pygtk
pygtk.require("2.0")
import gtk
#import gtk.gdkgl as gdkgl
#import gtk.gtkgl as gtkgl
#import tereoGL
import math
import platform
import os
import os.path
import sys

#from pandac.PandaModules          import loadPrcFileData, WindowProperties
#from direct.task                  import Task
#from direct.showbase.DirectObject import DirectObject

#import tereoPanda

#from panda3d.core import Material
#from panda3d.core import VBase4
#from panda3d.core        import DirectionalLight
#from panda3d.core        import AmbientLight
#from pandac.PandaModules import LineSegs
#from pandac.PandaModules import NodePath
#from pandac.PandaModules import GeomVertexData
#from pandac.PandaModules import GeomVertexFormat
#from pandac.PandaModules import Geom
#from pandac.PandaModules import GeomVertexWriter
#from pandac.PandaModules import GeomTriangles
#from pandac.PandaModules import GeomNode
#from pandac.PandaModules import AntialiasAttrib


#from panda3d.core import Filename

# todo in js
#loadPrcFileData("", "window-type none")
#loadPrcFileData("", "framebuffer-multisample 1")




class scene_view(gtk.Frame):

	def __init__(self,tereo):

		gtk.Frame.__init__(self)

		print "new scene view"

		self.tereo = tereo
		self.scene3d = None
		tereo.applog("gui",0,"scene init")

		# ---------------

		self.lat=0
		self.lon=0
		self.mousex = 0
		self.mousey = 0
		self.minlat = -180
		self.maxlat = 180
		self.rotating = False
		self.glarea = None

		#world = Capture()
		#tool = Capture()
		#work = Capture()
		self.model = [] #Collection([model,world])
		self.size = 60.0 #float(size)
		self.distance = self.size * 5.0
		self.near = 0.06 # float(size) * 0.01
		self.far = float(self.size) * 10.0
		#self.tool2view = tool
		#self.world2view = world
		#self.work2view = work

		self.base = None


		vbox = gtk.VBox(False, 0)
		self.add(vbox)

		self.drawing_area = gtk.DrawingArea()
		color = gtk.gdk.color_parse('#234fdb')
		self.drawing_area.modify_bg(gtk.STATE_NORMAL, color)
		#vbox.pack_start(self.drawing_area, False, False, 0)
		vbox.pack_end(self.drawing_area)
		self.drawing_area.show()


		vbox.show()
		self.show()

		return

	def get_widget_id(self,widget) :
		""" Retrieve gtk widget ID to tell the Panda window to draw on it """
		if widget.get_window() == None:
			return
		if platform.system() == "Windows" : return widget.window.handle
		else                              : return widget.window.xid


	def attach_frame(self):

		print "attach frame"
		return
		#import direct.directbase.DirectStart
		props = WindowProperties().getDefault()
		props.setOrigin(0, 0)
		props.setParentWindow(self.get_widget_id(self.drawing_area))
		self.base = base
		base.openDefaultWindow(props=props)
		base.setBackgroundColor(0.49,0.78,0.85)
		self.drawing_area.connect("size_allocate", self.resize_panda_window)

		alight = AmbientLight('alight')
		alight.setColor(VBase4(0.2, 0.2, 0.2, 1))
		alnp = base.render.attachNewNode(alight)
		base.render.setLight(alnp)

		dlight = DirectionalLight('dlight')
		dlight.setColor(VBase4(0.8, 0.8, 0.5, 1))
		# Use a 512x512 resolution shadow map
		dlight.setShadowCaster(True, 512, 512)
		dlnp = base.render.attachNewNode(dlight)

		dlnp.setHpr(0, -60, 0)
		base.render.setLight(dlnp)


		# Enable the shader generator for the receiving nodes
		base.render.setShaderAuto()
		base.render.setAntialias(AntialiasAttrib.MMultisample,1)


		if self.base != None:
			grid = tereoPanda.grid().generate()
			grid.reparentTo(self.base.render)

		#self.draw_world_axis()
		self.build_scene()


		return

	def resize_panda_window(self,widget, request) :

		# Connected to resize event of the widget Panda is draw on so that the Panda window update its size
		props = WindowProperties().getDefault()
		props = WindowProperties(base.win.getProperties())
		props.setOrigin(0, 0)
		props.setSize(request.width, request.height)
		props.setParentWindow(self.get_widget_id(widget))
		base.win.requestProperties(props)
		base.setBackgroundColor(0.49,0.78,0.85)

		return

	def on_scene_activate(widget):

		#if self.container == None:
		#	return


		#self.attach_frame(self.container)
		self.show()

		return



	def build_scene(self):

		print "build scene"

		if self.base == None:
			return

		# ---------------- flow ----------------------

##		for fl in self.env.flow:
##
##			m = fl.get_collection()
##			m = Color(fl.color,[m])
##
##			ww = fl.working_width
##			wl = fl.working_length
##			#za = fl.z_angle
##			#xi = fl.x_inclination
##			px = fl.origin_x
##			pz = fl.origin_z
##			py = fl.origin_y
##			ox = fl.offset_x
##			oy = fl.offset_y
##			oz = fl.offset_z
##			#fo = fl.flow_origin
##
##			if fl.unity == 'mm':
##				pz = pz * 0.03937
##				px = px * 0.03937
##				py = py * 0.03937
##				wl = wl * 0.03937
##				ww = ww * 0.03937
##				ox = ox * 0.03937
##				oy = oy * 0.03937
##				oz = oz * 0.03937
##			#print "a"
##			ori = Box(-5*0.03937,-5*0.03937,0,5*0.03937,5*0.03937,1000*0.03937)
##			#print "a1"
##			#if fo == 0: ori = Translate([ori],ox,oy,oz)
##			#elif fo == 1: ori = Translate([ori],ox+wl,oy,oz)
##			#elif fo == 2: ori = Translate([ori],ox,oy+ww,oz)
##			#elif fo == 3: ori = Translate([ori],ox+wl,oy+ww,oz)
##			#print "a2"
##			ori = Color([1,0.0,0.0,1],[ori])
##			#print "a3"
##			m = Collection([m,ori])
##			#za = za - 90
##			#if za != 0:	m = Rotate([m],za,0,0,1)
##			m = Rotate([m],90,0,0,1)
##			m = Translate([m],px,py,pz)
##			m = Collection([m])
##			#print "b"
##			self.scenevismachframe.addmodel(m)
##			#print "c"
##			fl.vmodel = m



		for r in self.tereo.env.robots:
			print "robot"

			p = None
			for l in r.links:

				print "link"

				if sys.platform == 'win32':
					fname = os.path.expanduser('~') + '\\mes documents\\tereocnc\\robots\\'+r.constructor+'\\'+r.model+'\\'+l.file
				else:
					fname = os.path.expanduser('~') + '/tereocnc/robots/'+r.constructor+'/'+r.model+'/'+l.file

				print "fname:"+fname
				pf = Filename.fromOsSpecific(fname)
				try:
					obj = loader.loadModel(pf)
				except:
					print "could not open "+l.file
				else:

					myMaterial = Material()
					myMaterial.setShininess(50.0) #Make this material shiny
					myMaterial.setAmbient(VBase4(l.color[0],l.color[1],l.color[2],l.color[3]))
					myMaterial.setDiffuse(VBase4(l.color[0],l.color[1],l.color[2],l.color[3]))
					myMaterial.setSpecular(VBase4( (l.color[0]+1)/2 , (l.color[1]+1)/2 , (l.color[2]+1)/2, l.color[3]))
					obj.setMaterial(myMaterial)

					if (l.translate[0] + l.translate[1] + l.translate[2])!=0:
						obj.setPos(l.translate[0]*25.4/1000,-l.translate[2]*25.4/1000,l.translate[1]*25.4/1000)
					#if (l.axis[0] + l.axis[1] + l.axis[2])>0:
					#	obj.setHpr(l.axis[2]*90, l.axis[0]*90, l.axis[1]*90)
					if p == None:
						obj.setScale(10.0)
						obj.setHpr(0, 270, 0)
						obj.reparentTo(self.base.render)
					else:
						obj.reparentTo(p)
					p = obj


			#print "robot translate : %f"%(r.position_x) + " %f"%(r.position_y) + " %f"%(r.position_z)
			#m = Rotate([rob],r.angle_z,0,0,1)
			#m = Translate([m],r.position_x * 0.03937,r.position_y * 0.03937,r.position_z * 0.03937)
			#m = Collection([m])
			#r.vmodel = m
			#self.addmodel(m)


		return


	def clear(self):
		self.model = []
		return

	def add_gtk_shortcuts(self, window, actions) :
		# Create the accel group and add it to the window
		accel_group = gtk.AccelGroup()
		window.add_accel_group(accel_group)
		# Create the action group
		action_group = gtk.ActionGroup('ActionGroup')
		# ==
		# Could give the list of action to add_toggle_actions method all at once and then connect each one
		for action in actions :
			action_group.add_toggle_actions([action])
			gtk_action = action_group.get_action(action[0])
			gtk_action.set_accel_group(accel_group)
			gtk_action.connect_accelerator()
		# ==
		return accel_group, action_group



	def setup_panda_events(self,window) :
		""" Setup mouse events in Panda """
		obj = DirectObject()
##		obj.accept("mouse1"    , print_info, ["Left press"])
##		obj.accept("mouse1-up" , print_info, ["Left release"])
##		obj.accept("mouse2"    , print_info, ["Wheel press"])
##		obj.accept("mouse2-up" , print_info, ["Wheel release"])
##		obj.accept("mouse3"    , print_info, ["Right press"])
##		obj.accept("mouse3-up" , print_info, ["Right release"])
##		obj.accept("wheel_up"  , print_info, ["Scrolling up"])
##		obj.accept("wheel_down", print_info, ["Scrolling down"])
		return obj


	def draw_world_axis(self):

		if self.base == None:
			return

		cnode = self.CreateOpenShadedCone("xaxis",12)
		nodePath = NodePath(cnode)
		nodePath.reparentTo(self.base.render)
		nodePath.setPos(0, 40, -5)

		return


	# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	#
	# Function name : CreateOpenShadedCone
	#
	# Author : Paul H. Leopard, DESE Research, Inc.
	#
	# Description:
	#
	#   Create and return a GeomNode describing an open shaded cone.
	#
	# Input(s):
	#
	#   nodeName - Name of the GeomNode
	#   sliceCount - Number of slices in the cone.
	#
	# Ou	tput(s):
	#
	#   A GeomNode representing the cone
	#
	# Example Useage:
	#
	#   geomNode = CreateOpenShadedCone('ConeWith32Slices',32)
	#   nodePath = NodePath(geomNode)
	#   nodePath.reparentTo(render)
	#
	def CreateOpenShadedCone(self,nodeName,sliceCount):

		# Create points for a circle
		deltaAngle = 360.0/float(sliceCount-2)
		angle = 0
		points = []
		for i in range(0,sliceCount):
			angleRadians = angle*0.01745329251994
			y = math.sin(angleRadians)
			x = math.cos(angleRadians)
			point = (x,y)
			points.append(point)
			angle += deltaAngle

		# Define the vetex data format (GVF).
		gvf = GeomVertexFormat.getV3n3c4t2()

		# Create the vetex data container (GVD) using the GVF from above to
		# describe its contents.
		gvd = GeomVertexData('GridVertices',gvf,Geom.UHStatic)

		# Create writers for the GVD, one for each type of data (column) that
		# we are going to store in it.
		gvwV = GeomVertexWriter(gvd,'vertex')
		gvwT = GeomVertexWriter(gvd,'texcoord')
		gvwC = GeomVertexWriter(gvd,'color')
		gvwN = GeomVertexWriter(gvd,'normal')

		# Use the writers to add vertex data to the GVD.
		T = (0,0,1)
		C = (1,1,1,1)
		n = len(points)

		# Upload Vertex/Normal/Tex/Color for top point
		gvwV.addData3f(T[0],T[1],T[2])
		gvwN.addData3f(0,0,1)
		gvwC.addData4f(C[0],C[1],C[2],C[3])
		gvwT.addData2f(0,0)
		tIndex = 0

		# For each point sample
		for i in range(0,n):

			# Get first point (A)
			p = points[i]
			A = (p[0],p[1],0)

			# Get next point (B)
			j = (i+1) % n
			p = points[j]
			B = (p[0],p[1],0)

 			# Compute surface normal (N0) for Facet(A,B,T)
			x10 = B[0] - A[0]
 			y10 = B[1] - A[1]
			z10 = B[2] - A[2]
 			x12 = B[0] - T[0]
			y12 = B[1] - T[1]
			z12 = B[2] - T[2]
			cpx = (z10*y12) - (y10*z12)
			cpy = (x10*z12) - (z10*x12)
			cpz = (y10*x12) - (x10*y12)
			r = math.sqrt(cpx*cpx + cpy*cpy + cpz*cpz)
			N = (cpx/r, cpy/r, cpz/r)

			# Get Previous point (C)
			# Compute facet normal (N1) for C,A,T
			# Compute average normal N = (N0 + N1)/2
			# @TODO@ Implement these ^^^ later on

			# Compute normalized texture coordinate TX(U,V) on XY plane
			TX = (A[0],A[1])

			# Upload Vertex/Normal/Tex/Color
			gvwV.addData3f(A[0],A[1],A[2])
			gvwN.addData3f(N[0],N[1],N[2])
			gvwC.addData4f(C[0],C[1],C[2],C[3])
			gvwT.addData2f(TX[0],TX[1])

		# Create a GeomPrimitive object to use the vertices in the GVD
		# then fill it with our cone facets data.

		geom = Geom(gvd)

		for i in range(0,n):

			# Get first point (A)
			aIndex = i+1

			# Get next point (B)
			j = (aIndex+1) % n
			bIndex = j

			# Create and upload face F(A,B,T)
			tris = GeomTriangles(Geom.UHStatic)
			tris.setIndexType(Geom.NTUint32)
			tris.addVertex(aIndex)
			tris.addVertex(bIndex)
			tris.addVertex(tIndex)
			tris.closePrimitive()

			geom.addPrimitive(tris)

			str = "\tTris : %d,%d,%d" % (aIndex,bIndex,tIndex)
			print str

		geomNode = GeomNode(nodeName)
		geomNode.addGeom(geom)

		return geomNode

	def resize_window(self,widget, request):

		props = WindowProperties().getDefault()
		props = WindowProperties(base.win.getProperties())
		props.setOrigin(0, 0)
		print "request w:%d"%(request.width)+" h:%d"%(request.height)
		props.setSize(request.width, request.height)
		props.setParentWindow(widget.window.handle)  #xid
		base.win.requestProperties(props)


		return

	#######################################################
	#	3D navigation
	#######################################################

	def gl_startrotate(self, widget, event):

		self.rotating = True
		self.mousex = event.x
		self.mousey = event.y

	def gl_endrotate(self, widget, event):

		self.rotating = False

	def gl_rotate(self, widget, event):

		if self.rotating == False: return

 		s = 0.05

		self.lat = min(self.maxlat, max(self.minlat, self.lat + (event.y - self.mousey) * s))
		self.lon = (self.lon + (event.x - self.mousex) * s) % 360
		self.glarea.queue_draw()
		return

	def gl_zoomwheel(self, widget, event):
		#print "zoomwheel"

		if event.direction == gtk.gdk.SCROLL_UP:
			#zoomin
			self.distance = self.distance / 1.1
		else:
			#zoomout
			self.distance = self.distance * 1.1
		self.glarea.queue_draw()

	def detach2object(self,o):
		#print "detach to object"
		for m in self.model:
			for i in range(len(m.parts)):
				p = m.parts[i]
				#print p #type(p).__name__
				#if type(p).__name__ == "Collection":
				if hasattr(p,"parts") == True:
					r = self.recursive_detach2object(m.parts,i,o)
					if r == True:
						return
				if p == o:
					print "ok found"
					#m = p
					del m.parts[i]
					return
		return

	def recursive_detach2object(self,par,idx,o):
		for i in range(len(par[idx].parts)):
			p = par[idx].parts[i]
			if hasattr(p,"parts") == True:
				ret = self.recursive_detach2object(par[idx].parts,i,o)
				if ret == True:
					return True
			if p == o:
				print "ok found r"
				#par[idx] = p
				del par[idx].parts[i]
				return True
		return False

	def attach2object(self,o,mo):
		#print "attach to object"
		for m in self.model:
			for i in range(len(m.parts)):
				p = m.parts[i]
				#print p #type(p).__name__
				#if type(p).__name__ == "Collection":
				if hasattr(p,"parts") == True:
					r = self.recursive_attach2object(p,o,mo)
					if r == True:
						return
				if p == o:
					#print "ok found"
					m.parts.append(Collection([p,mo]))
					del m.parts[i]
		return

	def recursive_attach2object(self,r,o,m):
		for i in range(len(r.parts)):
			p = r.parts[i]
			if hasattr(p,"parts") == True:
				ret = self.recursive_attach2object(p,o,m)
				if ret == True:
					return True
			if p == o:
				#print "ok found r"
				r.parts.append(Collection([p,m]))
				del r.parts[i]
				return True
		return False


	def addmodel(self,m):
		#print "add model"
		self.model.append(m)
		return

	def removemodel(self,m):
		#print "remove m:"
		#print "range %d"%(len(self.model))

		for i in range(len(self.model)):
			#print "i %d"%(i)
			if self.model[i] == m:
				print "found"
				del self.model[i]

	def basic_lighting(self):
		#self.activate()
		glLightfv(GL_LIGHT0, GL_POSITION, (-8, 10, 5, 0))
		glLightfv(GL_LIGHT0, GL_AMBIENT, (0.3,0.3,0.3,0))
		glLightfv(GL_LIGHT0, GL_DIFFUSE, (.4,.4,.4,0))

		glLightfv(GL_LIGHT0+1, GL_POSITION, (-1, -1, 1.5, 0))
		glLightfv(GL_LIGHT0+1, GL_AMBIENT, (.0,.0,.0,1))
		glLightfv(GL_LIGHT0+1, GL_DIFFUSE, (.0,.0,.4,0))
		#glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, (1.0,1.0,1.0,1.0))

		###glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE, (0.6,0.5,0.4,1.0))

		#glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT, (0.05,0.05,0.05,1.0))
		#glMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, (1.0,0.5,0.8,1.0))
		#glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, (1.0,1.0,1.0,1.0))
		#glMaterialf(GL_FRONT_AND_BACK, GL_SHININESS, (5.0))
		#glMaterialfv(GL_FRONT_AND_BACK, GL_EMISSION, (0.0,0.0,0.0,1.0))
		glEnable(GL_CULL_FACE)
		glCullFace(GL_BACK)
		glEnable(GL_LIGHTING)
		glEnable(GL_LIGHT0)
		#glEnable(GL_LIGHT0+1)
		glShadeModel(GL_SMOOTH)
		glDepthFunc(GL_LESS)
		glEnable(GL_DEPTH_TEST)
		#glMatrixMode(GL_MODELVIEW)
		#glLoadIdentity()
		return

#	def drawWorldAxis(self):
#		# x
#		glColor3f(1.0,0.0,0.0)
#		glBegin (GL_LINES)
#		glVertex ( 0.0, 0.0, 0.0)
#		glVertex ( 0.8, 0.0, 0.0)
#		glVertex ( 1.1, 0.0, 0.1)
#		glVertex ( 1.3, 0.0, -0.1)
#		glVertex ( 1.1, 0.0, -0.1)
#		glVertex ( 1.3, 0.0, 0.1)
#		glEnd ()
#		glBegin (GL_TRIANGLES)
#		glVertex ( 0.8, 0.0, 0.1)
#		glVertex ( 1.0, 0.0, 0.0)
#		glVertex ( 0.8, 0.0, -0.1)
#		glEnd ()
#
#		# y
#		glColor3f(0.0,1.0,0.0)
#		glBegin (GL_LINES)
#		glVertex ( 0.0, 0.0, 0.0)
#		glVertex ( 0.0, 0.8, 0.0)
#		glVertex ( 0.0, 1.1, 0.0)
#		glVertex ( 0.0, 1.2, 0.0)
#		glVertex ( 0.0, 1.2, 0.0)
#		glVertex ( -0.0707, 1.3, 0.0707)
#		glVertex ( 0.0, 1.2, 0.0)
#		glVertex ( 0.0707, 1.3, -0.0707)
#		glEnd ()
#		glBegin (GL_TRIANGLES)
#		glVertex ( 0.0707, 0.8, -0.0707)
#		glVertex ( 0.0, 1.0, 0.0)
#		glVertex ( -0.0707, 0.8, 0.0707)
#		glEnd ()
#
#		# z
#		glColor3f(0.0,0.0,1.0)
#		glBegin (GL_LINES)
#		glVertex ( 0.0, 0.0, 0.0)
#		glVertex ( 0.0, 0.0, 0.8)
#		glVertex ( 0.1, 0.0, 1.1)
#		glVertex ( -0.1, 0.0, 1.1)
#		glVertex ( -0.1, 0.0, 1.3)
#		glVertex ( 0.1, 0.0, 1.1)
#		glVertex ( -0.1, 0.0, 1.3)
#		glVertex ( 0.1, 0.0, 1.3)
#		glEnd ()
#		glBegin (GL_TRIANGLES)
#		glVertex ( 0.0, 0.0, 1.0)
#		glVertex ( 0.1, 0.0, 0.8)
#		glVertex ( -0.1, 0.0, 0.8)
#		glEnd ()
#		return

	def redraw(self,area,_):

		return


		allocation = area.get_allocation ()
		viewport_width = float (allocation.width)
		viewport_height = float (allocation.height)

		#print "viewpor x:%f"%(viewport_width) + "  y:%f"%(viewport_height)

		glViewport (0, 0, int (viewport_width), int (viewport_height))
		glClearColor(1.0, 1.0, 1.0, 0.0)


##		aspect =  viewport_width / viewport_height
##		fovy   =  35.0 # The one which looks to most natural.
##		#z_near =   2.0 # Enough for a moderately sized sample model.
##		#z_far  =  -2.0 # Idem.
##
##		gluPerspective (fovy, aspect, self.near, self.far)
##
##		projection_dx =  0.0
##		projection_dy =  -self.distance/5.0
##		projection_dz = -self.distance*1.2
##
##		glTranslate (projection_dx, projection_dy, projection_dz)
##
##		glRotated(-60.0,  1.0,  0.0,  0.0)
##		glRotated(45.0,  0.0, 0.0,  1.0)

##		for m in self.model:
##
##		    if hasattr(m, "apply"):
##			m.apply()
##		    if hasattr(m, "capture"):
##			m.capture()
##		    if hasattr(m, "draw"):
##			m.draw()
##		    if hasattr(m, "traverse"):
##			m.traverse()
##		    if hasattr(m, "unapply"):
##			m.unapply()


		tereoGL.drawWorldAxis()

		return



#	def on_btn_scene(self, widget, data=None):

		#w = self.get_active_window()
		#w.hide()

		#self.detach_toolbar()

		#self.toolbarhost = "vbox4"
		#vb4 =  self.builder.get_object("vbox4")
		#if vb4 == None:
		#	self.addlog ("gui",1,"vb4 none")
		#if self.toolbar == None:
		#	self.addlog ("gui",1,"tb none")
		#vb4.pack_end(self.toolbar,False,True,0)

		#for w in range(self.toolbar.get_n_items()):
		#	self.toolbar.get_nth_item(w).hide() #set_visible(False)

		#self.builder.get_object(self.TB_IO).show()
		#self.builder.get_object(self.TB_ESTOP).show()
		#self.builder.get_object(self.TB_QUIT).show()
		#self.builder.get_object(self.TB_DRAWING).show()
		#self.builder.get_object(self.TB_TASK).show()
		#self.builder.get_object(self.TB_JOG).show()
		#self.builder.get_object(self.TB_LOG).show()
		#self.builder.get_object(self.TB_NET).show()
		#self.builder.get_object(self.TB_ROBOTS).show()
		#self.builder.get_object(self.TB_FLOWS).show()
		#self.builder.get_object(self.TB_TOOLS).show()
		#self.builder.get_object(self.TB_RUN).show()
		#self.builder.get_object(self.TB_PAUSE).show()
		#self.builder.get_object(self.TB_STOP).show()
		#self.builder.get_object(self.TB_CONFIG).show()

		#fcw = self.builder.get_object("guiWindow")
		#fcw.show()

		#self.scene_init()
		#return










##
##		self.accept("escape", sys.exit)
##		self.accept("arrow_up", self.zoomIn)
##		self.accept("arrow_down", self.zoomOut)
##		self.accept("arrow_left", self.orbitL)
##		self.accept("arrow_left-up", self.orbitoff)
##		self.accept("arrow_right", self.orbitR)
##		self.accept("arrow_right-up", self.orbitoff)
##		self.accept("p", self.getWidgetTransformsF)



##	def orbitR(self):
##		taskMgr.add(self.SpinCameraRTask, "SpinTask")
##	def orbitL(self):
##		taskMgr.add(self.SpinCameraLTask, "SpinTask")
##	def orbitoff(self):
##		taskMgr.remove("SpinTask")
##	def SpinCameraLTask(self, task):
##		base.camera.setHpr(base.camera.getH()-1, 0, 0)
##		self.angleradians = base.camera.getH() * (math.pi / 180.0)
##		base.camera.setPos(self.camDist*math.sin(self.angleradians),-self.camDist*math.cos(self.angleradians),1)
##		return Task.cont
##	def SpinCameraRTask(self, task):
##		base.camera.setHpr(base.camera.getH()+1, 0, 0)
##		self.angleradians = base.camera.getH() * (math.pi / 180.0)
##		base.camera.setPos(self.camDist*math.sin(self.angleradians),-self.camDist*math.cos(self.angleradians),1)
##		return Task.cont
##	def zoomIn(self):
##		radian = base.camera.getH()*math.pi/180.0
##		dx = self.D2M*-math.sin(radian)
##		dy = self.D2M*+math.cos(radian)
##		base.camera.setPos(Vec3(base.camera.getX()+dx,base.camera.getY()+dy,1))
##		self.camDist = self.camDist -1
##	def zoomOut(self):
##		radian = base.camera.getH()*math.pi/180.0
##		dx = self.D2M*+math.sin(radian)
##		dy = self.D2M*-math.cos(radian)
##		base.camera.setPos(Vec3(base.camera.getX()+dx,base.camera.getY()+dy,1))
##		self.camDist = self.camDist +1
##
##












#### ------------------------------------------------------------


class CoordsBase(object):
	def __init__(self, *args):
		self._coords = args
		#self.q = gluNewQuadric()
	def coords(self):
		return self._coords


def AsciiOBJ(filename):

	pandafile = Filename.fromOsSpecific(filename)

	try:
		obj = loader.loadModel(pandafile)
	except:
		print "could not open "+filename
	else:
		obj.reparentTo(render)
		obj.setPos(0, 40, -5)



class Collection(object):
    def __init__(self, parts):
	self.parts = parts
	self.vol = 0

    def traverse(self):
	for p in self.parts:
	    if hasattr(p, "apply"):
		p.apply()
	    if hasattr(p, "capture"):
		p.capture()
	    if hasattr(p, "draw"):
		p.draw()
	    if hasattr(p, "traverse"):
		p.traverse()
	    if hasattr(p, "unapply"):
		p.unapply()

    def volume(self):
	if hasattr(self, "vol") and self.vol != 0:
	    vol = self.vol
	else:
	    vol = sum(part.volume() for part in self.parts)
	#print "Collection.volume", vol
	return vol

    # a collection consisting of overlapping parts will have an incorrect
    # volume, because overlapping volumes will be counted twice.  If the
    # correct volume is known, it can be set using this method
    def set_volume(self,vol):
	self.vol = vol;

class Translate(Collection):
    def __init__(self, parts, x, y, z):
	self.parts = parts
	self.where = x, y, z

    def apply(self):
	glPushMatrix()
	#print "translate : %f"%(self.where[0]) + " %f"%(self.where[1]) + " %f"%(self.where[2])
	glTranslatef(*self.where)

    def unapply(self):
	glPopMatrix()

class Wireframe(Collection):
	def __init__(self, parts):
		self.parts = parts

	def apply(self):
		glPolygonMode( GL_FRONT_AND_BACK, GL_LINE )

	def unapply(self):
		glPolygonMode( GL_FRONT_AND_BACK, GL_FILL )

class Tag(Collection):
	def __init__(self, parts):
		self.parts = parts

class Scale(Collection):
    def __init__(self, parts, x, y, z):
	self.parts = parts
	self.scaleby = x, y, z

    def apply(self):

	glPushMatrix()
	glScalef(*self.scaleby)

    def unapply(self):

	glPopMatrix()

class HalTranslate(Collection):
    def __init__(self, parts, comp, var, x, y, z):
	self.parts = parts
	self.where = x, y, z
	self.comp = comp
	self.var = var

    def apply(self):
	x, y, z = self.where
	v = self.comp[self.var]

	glPushMatrix()
	glTranslatef(x*v, y*v, z*v)

    def unapply(self):

	glPopMatrix()


class HalRotate(Collection):
    def __init__(self, parts, comp, var, th, x, y, z):
	self.parts = parts
	self.where = th, x, y, z
	self.comp = comp
	self.var = var

    def apply(self):
	th, x, y, z = self.where

	glPushMatrix()
	glRotatef(th * self.comp[self.var], x, y, z)

    def unapply(self):

	glPopMatrix()

class Matrix(Collection):
	def __init__(self,part, mat):
		self.parts = part
		self.mat = mat

	def apply(self):
		glPushMatrix()
		glMultMatrixf(self.mat)

	def unapply(self):
		glPopMatrix()

class Rotate(Collection):
    def __init__(self, parts, th, x, y, z):
	self.parts = parts
	self.where = th, x, y, z

    def apply(self):
	th, x, y, z = self.where

	glPushMatrix()
	glRotatef(th, x, y, z)

    def unapply(self):

	glPopMatrix()


# six coordinate version - specify each side of the box
class Box(CoordsBase):
	def draw(self):

		x1, y1, z1, x2, y2, z2 = self.coords()
		if x1 > x2:
			tmp = x1
			x1 = x2
			x2 = tmp
		if y1 > y2:
			tmp = y1
			y1 = y2
			y2 = tmp
 		if z1 > z2:
			tmp = z1
			z1 = z2
			z2 = tmp

		glBegin(GL_QUADS)
		# bottom face
		glNormal3f(0,0,-1)
		glVertex3f(x2, y1, z1)
		glVertex3f(x1, y1, z1)
		glVertex3f(x1, y2, z1)
		glVertex3f(x2, y2, z1)
		# positive X face
		glNormal3f(1,0,0)
		glVertex3f(x2, y1, z1)
		glVertex3f(x2, y2, z1)
		glVertex3f(x2, y2, z2)
		glVertex3f(x2, y1, z2)
		# positive Y face
		glNormal3f(0,1,0)
		glVertex3f(x1, y2, z1)
		glVertex3f(x1, y2, z2)
		glVertex3f(x2, y2, z2)
		glVertex3f(x2, y2, z1)
		# negative Y face
		glNormal3f(0,-1,0)
		glVertex3f(x2, y1, z2)
		glVertex3f(x1, y1, z2)
		glVertex3f(x1, y1, z1)
		glVertex3f(x2, y1, z1)
		# negative X face
		glNormal3f(-1,0,0)
		glVertex3f(x1, y1, z1)
		glVertex3f(x1, y1, z2)
		glVertex3f(x1, y2, z2)
		glVertex3f(x1, y2, z1)
		# top face
		glNormal3f(0,0,1)
		glVertex3f(x1, y2, z2)
		glVertex3f(x1, y1, z2)
		glVertex3f(x2, y1, z2)
		glVertex3f(x2, y2, z2)
		glEnd()

	def volume(self):
		x1, y1, z1, x2, y2, z2 = self.coords()
		vol = abs((x1-x2)*(y1-y2)*(z1-z2))
		#print "Box.volume", vol
		return vol

# capture current transformation matrix
# note that this tranforms from the current coordinate system
# to the viewport system, NOT to the world system
class Capture(object):
    def __init__(self):
		self.t = [0,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0]

    def capture(self):
		a = glGetDoublev(GL_MODELVIEW_MATRIX)
		a = a[:]
		b = []
		for c in a:
			b=b+c[:]
		self.t = b
		return

    def volume(self):
		return 0.0

class Color(Collection):
	def __init__(self, color, parts):
		self.color = color
		Collection.__init__(self, parts)

	def apply(self):
		glPushAttrib(GL_LIGHTING_BIT)
		glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE, self.color)

	def unapply(self):
		glPopAttrib()
		return





# function to invert a transform matrix
# based on http://steve.hollasch.net/cgindex/math/matrix/afforthinv.c
# with simplifications since we don't do scaling

# This function inverts a 4x4 matrix that is affine and orthogonal.  In
# other words, the perspective components are [0 0 0 1], and the basis
# vectors are orthogonal to each other.  In addition, the matrix must
# not do scaling

def invert(src):
	# make a copy
	inv=src[:]
	# The inverse of the upper 3x3 is the transpose (since the basis
	# vectors are orthogonal to each other.
	inv[1],inv[4] = inv[4],inv[1]
	inv[2],inv[8] = inv[8],inv[2]
	inv[6],inv[9] = inv[9],inv[6]
	# The inverse of the translation component is just the negation
	# of the translation after dotting with the new upper3x3 rows. */
	inv[12] = -(src[12]*inv[0] + src[13]*inv[4] + src[14]*inv[8])
	inv[13] = -(src[12]*inv[1] + src[13]*inv[5] + src[14]*inv[9])
	inv[14] = -(src[12]*inv[2] + src[13]*inv[6] + src[14]*inv[10])
	return inv



class Triangle(CoordsBase):
    def draw(self):

		x1, y1, z1, x2, y2, z2, x3, y3, z3, n1, n2, n3 = self.coords()
		glBegin(GL_TRIANGLES)
		glNormal3f(n1,n2,n3)
		glVertex3f(x1, y1, z1)
		glVertex3f(x2, y2, z2)
		glVertex3f(x3, y3, z3)
		glEnd()
		return

class Line(CoordsBase):
	def draw(self):

		x1, y1, z1, x2, y2, z2 = self.coords()
		glBegin(GL_LINES)
		glVertex3f(x1, y1, z1)
		glVertex3f(x2, y2, z2)
		glEnd()
