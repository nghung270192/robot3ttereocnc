# -*- coding: utf-8 -*-

import pygtk
pygtk.require("2.0")
import gtk




class robot_panel_view(gtk.Frame):

	def __init__(self,tereo):

		gtk.Frame.__init__(self)

		self.tereo = tereo

		self.set_label_align(0,0)


		al = gtk.Alignment(0.5, 0.5, 1.0, 1.0)
		al.set_padding(0,0,12,0)

		self.set_label("Robot panel")

		self.add(al)


		al.show()

		sv = gtk.ScrolledWindow()

		al.add(sv)

		sv.show()

		vp = gtk.Viewport()
		sv.add(vp)
		vp.show()


		f = gtk.Frame()
		vp.add(f)

		f.show()

		self.vbox = gtk.VBox(False, 0)
		f.add(self.vbox)
		self.vbox.show()
		self.set_size_request(300,-1)
		self.show()

		self.populate()

		return



	def populate(self):


		if self.tereo.env == None:
			return

		#clear
		children = self.vbox.get_children()
		for iter in children:
			subchildren = iter.get_children()
			for subiter in subchildren:
				subiter.destroy()
			iter.destroy()

		img = gtk.Image()
		img.set_from_file('./media/add.png')
		img.show()
		ln = gtk.Button("New")
		ln.set_image(img)
		ln.show()
		self.vbox.pack_start(ln,False,True,0)
		ln.connect("clicked", self.on_robot_new)

		i = 0

		for r in self.tereo.env.robots:

			# new robot
			frm = gtk.Frame()
			self.vbox.pack_start(frm,False,True,0)
			frm.show()

			tbl = gtk.Table(7,3,False)
			frm.add(tbl)
			tbl.show()

			ln = gtk.Label(str(i))
			ln.set_justify(gtk.JUSTIFY_RIGHT)
			ln.show()
			tbl.attach(ln,0,1,0,1)

			ln = gtk.Label(r.name)
			ln.set_justify(gtk.JUSTIFY_LEFT)
			ln.show()
			tbl.attach(ln,1,2,0,1)

			ln = gtk.Label("Model")
			ln.set_justify(gtk.JUSTIFY_RIGHT)
			ln.show()
			tbl.attach(ln,0,1,1,2)

			ln = gtk.Label(r.model)
			ln.show()
			ln.set_justify(gtk.JUSTIFY_LEFT)
			tbl.attach(ln,1,2,1,2)

			ln = gtk.Label("Constructor")
			ln.set_justify(gtk.JUSTIFY_RIGHT)
			ln.show()
			tbl.attach(ln,0,1,2,3)

			ln = gtk.Label(r.constructor)
			ln.show()
			ln.set_justify(gtk.JUSTIFY_LEFT)
			tbl.attach(ln,1,2,2,3)

			ln = gtk.Label("Status")
			ln.set_justify(gtk.JUSTIFY_RIGHT)
			ln.show()
			tbl.attach(ln,0,1,3,4)

			if r.client == None:
				lbl = "No connector"
			else:
				if r.client.instance == None :
					lbl = "Not initialized"
				else:
					if r.client.instance.connected == True:
						lbl = "Connected"
					else:
						if r.connecting == True:
							lbl = "Connecting..."
						else:
							lbl = "Disconnected"

			ln = gtk.Label(lbl)
			ln.set_justify(gtk.JUSTIFY_LEFT)
			ln.show()
			tbl.attach(ln,1,2,3,4)

			#--------------------

			img = gtk.Image()
			img.set_from_file('./media/configure.png')
			img.show()
			ln = gtk.Button("Info")
			ln.set_image(img)
			ln.show()
			tbl.attach(ln,0,1,4,5)
			ln.connect("clicked", self.on_info, i)

			#---------------------

			lcal = self.tereo.function_search_path("robot.calibration")
			if lcal != []:

				img = gtk.Image()
				img.set_from_file('./media/calibrate.png')
				img.show()
				ln = gtk.Button("Calib.")
				ln.set_image(img)
				ln.show()
				tbl.attach(ln,0,1,5,6)
				#ln.connect("clicked", self.on_robot_calibrate, i)

				slist = []
				defau = ""
				for it in lcal:
					if defau == "":
						defau = it[0]
					slist.append(it[0])

				self.combo1 = gtk.Combo()
				self.combo1.set_popdown_strings(slist)
				self.combo1.entry.set_text(defau)
				tbl.attach(self.combo1, 1, 2, 5, 6)
				self.combo1.show()


			#-----------------------

			img = gtk.Image()
			img.set_from_file('./media/home.png')
			img.show()
			ln = gtk.Button("Home")
			ln.set_image(img)
			ln.show()
			tbl.attach(ln,0,1,6,7)
			ln.connect("clicked", self.on_robot_home, i)

			i+=1


		return

	def on_info(self,widget,i):
		self.tereo.execute_function ("robot.info","Robot info", i)
		return


	def on_robot_home(self,widget,data):

		if self.env == None:
			return
		if len(self.tereo.env.robots)<data :
			return
		if self.tereo.env.robots[data] == None:
			return

		r = self.tereo.env.robots[data]
		if (r.ip != "") and (r.chan != 0):
			if r.proto == "lcnc":
				r.send("command home -1")
			elif r.proto == "cross":
				if r.calculateur != None:
					r.send("command home -1")
				r.setspeed(5)
				r.moveto(r.home)




	def on_sethome(self,widget,i):


		#fo = frame6(1325.943970, 1189.238037, 984.228027, -156.870804, 8.596017, -179.800507)
		#fx = frame6(1434.425049, 23.797190, 1002.853027, -156.870102, 8.595991, -179.800293)
		#fy = frame6(1640.725952, 1205.558960, 985.950378, -156.869598, 8.595720, -179.800095)

		#fo = frame6(1324.321045, 1189.478027, 983.686279, -3.525528, -1.604558, 2.647153)
		#fx = frame6(1428.770020, 75.391869, 1001.255981, -3.525337, -1.604669, 2.647267)
		#fy = frame6(1711.005005, 1210.520020, 984.209229, -3.548363, -1.593004, 2.639205)

		#print "load matrix:"
		#print self.env.flow[0].matrix

		#self.env.flow[0].calibrate_3points(fo,fx,fy,self.env.tools[0])


		#r = self.env.robot[1]
		#r.selected_tool = self.env.tools[0]
		#r.moveto(1000,0,0,-156.87,8.59,-179,0)

		#return



		#print "___ 4 points calibration ________________________________________"

		##fa = frame6(1495.522, 504.9047, 1525.907, -165.2402, 28.81118, -170.4057)
		##fb = frame6(1513.386, 393.9939, 1487.916, 168.9515, 30.31429, 139.7817)
		##fc = frame6(1647.298, 356.1783, 1482.640, -156.4968, 5.572189, 127.5535)
		##fd = frame6(1627.613, 557.0178, 1559.532, -166.1921, -20.37669, -165.2694)
		#fa = frame6(1537, 434, 1433, -170, -81, -123)
		#fb = frame6(1527, 479, 1395, 116, -56, -47)
		#fc = frame6(1595, 536, 1484, 58, -52, 12)
		#fd = frame6(1624, 316, 1489, -53, -49, 140)
		#m = tereoTool.calc_tool_matrix_4points(fa,fb,fc,fd)
		#print m
		#self.env.tools[1].matrix = m

		return

		#print "____ 2 points calibration _______________________________________"

		# tool
		##f1 = frame6(1629.251, 168.4417, 1156.281, -176.9179, 16.18393, 175.3015)
		##f1 = frame6(1268.146, 1.818468, 967.7661, -25.99339, -1.687192, 1.305336)
		##f1 = frame6(321.4, 29.9, 242.6, 0, -10, 25)
		# kuka tag
		##f2 = frame6(1822.499, 229.2046, 1026.493, -176.8476, 20.12885, 175.5276)
		##f2 = frame6(1417.630, 17.39453, 879.4035, -34.93858, -34.44539, 24.17662)
		##f2 = frame6(485.9, 100, 249.6, 0, -30, 40)
		##self.env.tools[0].matrix = tereoTool.calc_tool_matrix_2points(f1,f2)
		#print "tools 0 matrix:"
		#print self.env.tools[0].matrix


		#print "________ flow calibration __________"

		## dummy calibration
		#f = self.env.flow[0]

		##fo = frame6(1203.081,1290.059,975.1141,18.69,-0.47,6.03)
		##fx = frame6(1315.066,371.2379,962.8544,18.57446,-1.001771,4.656073)
		##fy = frame6(1666.011,1325.473, 965.1870, 18.70872, 0.17521, 7.9231)

		#fo = frame6(1054.010, 1539.677, 949.5551, -176.9089, 16.19157, 175.3011)
		#fo = frame6_bymatrix(fo,self.env.tools[0].matrix)

		#fx = frame6(1251.474, -40.88403, 956.3293, -176.9137, 16.19132, 175.3013)
		#fx = frame6_bymatrix(fx,self.env.tools[0].matrix)

		#fy = frame6(1497.534, 1577.288, 939.4255, -176.9096, 16.19387, 175.3054)
		#fy = frame6_bymatrix(fy,self.env.tools[0].matrix)

		#f.calibrate_3points(fo,fx,fy)


		#r = self.env.robot[1]
		#r.selected_tool = self.env.tools[0]
		#rc = self.env.robot[0]

		##r.moveto(1528,0,0,-176.9137,16.19387,175.3013,0)
		#r.moveto(1528,0,0,-169.3,7.22,159.07,0)




		return



	def on_robot_new(self,widget):

		# display robot_library
		self.robotsmodelwinrethandler = self.on_robot_new_ok
		self.robotsmodelwin.maximize()
		self.robotsmodelwin.show()
		self.robotsmodelwin_init()
		return


	def on_robot_new_ok(self):

		self.tmprobot.set_dummy_comp()
		self.tmprobot.db = self.tmpdb
		self.tmprobot.load_mesh()
		self.tereo.env.robots.append(self.tmprobot)
		r = self.tereo.env.robots[len(self.tereo.env.robots)-1]
		self.robotsmodelwinrethandler = None
		if self.builder.get_object("toolbutton12").get_active()==True:
			self.populate_robot_panel()

		rob = r.get_collection()
		m = Rotate([rob],r.angle_z,0,0,1)
		m = Translate([m],r.position_x * 0.03937,r.position_y * 0.03937,r.position_z * 0.03937)
		m = Collection([m])
		rob.vmodel = m
		self.scenevismachframe.addmodel(m)
		return



#---------- robot calibration --------------------

def on_robot_calibrate(widget,r,co):

	# calibration variables
	co = r.tereo.new_calibration_obj()
	co.currentRobot=data
	co.calib_title = "Robot calibration"
	co.calib_nb_question = 1
	co.calib_questions = ['Move the probe at the center top of the calibration box']
	co.calib_robot = r
	co.calib_robot_vars = ['$POS_ACT']
	co.calib_nb_img = 1
	co.calib_imgs = ['robotcalib.png']
	co.calib_handler = on_robot_calibrate_return
	co.calib_tag = [r,co]
	co.calib_win_init()

	return

def on_robot_calibrate_return(r,co):

	if co.calib_ret == None:
		return

	if len(co.calib_ret) == 0:
		return

	center = co.calib_ret[0]

	#print "box center :"
	r.env.robots[co.calib_tag-1].calibrate(center)


	return