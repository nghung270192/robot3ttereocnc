#!/usr/bin/python
import tereoRobot
import tereoToolChanging
import tereoTool
import tereoFlow
import os
import sys


import xml.etree.ElementTree as ET



class environment:
	def __init__(self):

		self.confname = ''
		self.robots = []
		self.toolchanger = tereoToolChanging.toolChangingObj()
		self.geometry = []
		self.tools = []
		self.flow = []
		self.homedir = ''
		self.pdb = None
		self.storing_flow = -1
		self.assembling_flow = -1
		self.woodworking_flow = -1
		return

	def load_env(self,name,homedir):
		self.confname = name
		#self.env.confname = 'tereo1'  #todo : get from inifile path
		self.set_home_dir(homedir)

		inifile=""
		if sys.platform == 'win32':
			inifile = homedir + "\\configs\\"+name+"\\"+name+".ter"
		else:
			inifile = homedir + "/configs/"+name+"/"+name+".ter"
		print "inifile:"+inifile


		try:
			self.pdb = ET.parse(inifile)
			#print ET.tostring(self.pdb.getroot())
			#for E in tree.findall('title'):
			#print(E.text)

			#self.pdb.load_ini_file(inifile)
			self.load_robot()
			self.load_tools()
			self.load_flow()
		except Exception, e:
			print "Error opening file : %s"%(e)
			return False

		return True

	def save_env(self):
		self.save_tools()
		self.save_flow()
		self.save_robot()

		inifile = ""
		if sys.platform == 'win32':
			inifile = self.homedir + "\\configs\\"+self.confname+"\\"+self.confname+".ter"
		else:
			inifile = self.homedir + "/configs/"+self.confname+"/"+self.confname+".ter"

		print "save env"

		try:
			if hasattr(self.pdb,"write"):
				self.pdb.write(inifile)
			else:
				tree = ET.ElementTree(self.pdb)
				tree.write(inifile)
		except Exception, e:
			print "Error saving file : "+inifile
			print e
			print self.pdb.tostring(self.pdb.getroot())

		#self.pdb.save_ini_file(inifile)


	def create_env(self,name,homedir):
		self.confname = name
		self.set_home_dir(homedir)

		inifile=""
		if sys.platform == 'win32':
			inifile = homedir + "\\configs\\"+name+"\\"+name+".ter"
			#makedir
			#os.mkdir(homedir)
			#os.mkdir(homedir + "\\configs")
			os.makedirs(homedir + "\\configs\\"+name)
			open(homedir + "\\configs\\"+name+"\\"+name+".ter", 'a').close()

		else:
			inifile = homedir + "/configs/"+name+"/"+name+".ter"
			#makedir
			#os.mkdir(homedir)
			#os.mkdir(homedir + "/configs")
			os.makedirs(homedir + "/configs/"+name)
			open(homedir + "/configs/"+name+"/"+name+".ter", 'a').close()


	def set_home_dir(self,hd):
		self.homedir = hd
		for r in self.robots:
			r.homedir = hd + '/robots'

		self.toolchanger.homedir = hd + '/toolchanging'

		for t in self.tools:
			t.homedir = hd + '/tools'

		for f in self.flow:
			f.homedir = hd + '/flow'

		return

	def load_robot(self):

		print "load robot"

		self.robots = tereoRobot.get_from_db(self.pdb)

		i = 0
		for r in self.robots:
			r.homedir = self.homedir + '/robots'
			r.env = self
		return

	def load_tools(self):
		self.toolchanger.get_from_db(self.pdb)
		self.tools = tereoTool.get_from_db(self.pdb)
		for t in self.tools:
			t.homedir = self.homedir + '/tools'
		return

	def load_flow(self):
		self.flow = tereoFlow.get_from_db(self.pdb)
		for f in self.flow:
			f.homedir = self.homedir + '/flow'
		return

	def save_robot(self):
		tereoRobot.set_to_db(self.pdb,self.robots)
		return

	def save_tools(self):
		self.toolchanger.set_to_db(self.pdb)
		tereoTool.set_to_db(self.pdb,self.tools)
		return

	def save_flow(self):
		tereoFlow.set_to_db(self.pdb,self.flow)

	def get_property(self,section,param):

		value = None
		if hasattr(self.pdb,"getroot"):
			db = self.pdb.getroot()
		else:
			db = self.pdb

		s = db.find(section)
		if s != None:
			value = s.get(param)
		if value == None: return 'None'
		return value

	def set_property(self,section,param,value):

		if hasattr(self.pdb,"getroot"):
			db = self.pdb.getroot()
		else:
			db = self.pdb

		s = db.find(section)
		if s == None:
			s = ET.SubElement(db,section)
		s.set(param,value)
		return


