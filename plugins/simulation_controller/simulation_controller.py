# -*- coding: utf-8 -*-


import os.path

class simulation_controller:

	def __init__(self):
		self.instance = self
		self.connected = True
		self.connecting = False
		self.lasterror = ""
		comm = []
		self.comm_stack = collections.deque(comm)
		self.waiting_response = False
		self.last_comm = ""
		self.last_response = ""
		self.last_cnc_error = ""
		self.position = {'X':0, 'Y':0, 'Z':0, 'A':0, 'B':0, 'C':0, 'U':0, 'V':0, 'W':0}
		self.joint_position = {'X':0, 'Y':0, 'Z':0, 'A':0, 'B':0, 'C':0, 'U':0, 'V':0, 'W':0}
		self.waiting_job = True
		self.speed = 5
		self.position_updated = True

	def send(self,com):
		self.position_updated = True
		return



def new_simulation_controller():

	return simulation_controller()

