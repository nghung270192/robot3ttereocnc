﻿// 
// - - - BABYLON EDITOR - - - 
//
// © 2013, Muriel Surmely
//
var BABYLON = BABYLON || {};

(function () {
    // constructor
    BABYLON.Manipulator = function (mesh, connectedCanvas, control, locked, pickedPoint) {
        this.mesh = mesh;
        this.scene = mesh.getScene();
        this.connectedCanvas = connectedCanvas;
        this.camera = this.scene.activeCamera;
        this.isActiveCamera = true;
        this.control = control;
        this.scaleLocked = locked;
        this.time = null;
        this.scalingMeshMulti = 400;
        this.positiopnMeshMulti = 1000;
        this.scaleDistanceManipulator = 80;
        if (pickedPoint)
            this.pickedPoint = pickedPoint;
        else
            this.pickedPoint = this.mesh.position;
        this.mouseInfo = {
            downPosX: 0,
            downPosY: 0,
            downRay: null,
            movePosX: 0,
            movePosY: 0,
            upPosX: 0,
            upPosY: 0,

            onDownObject: null,
            onMoveObject: null,
            objectSelector: null
        };
        this.mesh.isPickable = false;
        var that = this;

        this.connectedCanvas.addEventListener("mousemove", function (event) {
            that._mousemove(event);
        });
        this.connectedCanvas.addEventListener("mouseup", function (event) {
            that._mouseup(event);
        });
        this.connectedCanvas.addEventListener("mousedown", function (event) {
            that._mousedown(event);
        });
        this.scene.registerBeforeRender(function () {
            that.vecDistanceCameraMesh = that.pickedPoint.subtract(that.camera.position);
            that.vecDistanceCameraMesh.normalize();
            that.manipulate.position = that.vecDistanceCameraMesh.scale(that.scaleDistanceManipulator).add(that.camera.position);
        });
        this._createParentVector();
    };

    BABYLON.Manipulator.prototype._mouveObjectFromVector = function (vector, evt) {
        var vFromRay = this._getMouseRay(evt.clientX, evt.clientY, this.mouseInfo.onDownObject);
        var vToRay = this._getMouseRay(this.mouseInfo.movePosX, this.mouseInfo.movePosY, this.mouseInfo.onDownObject);
        var iDiff = vToRay.origin.subtract(vFromRay.origin);
        var distance = BABYLON.Vector3.Distance(vFromRay.origin, vToRay.origin);

        if (iDiff[vector] > 0) {
            this.mesh.position[vector] -= distance * this.positiopnMeshMulti;
            this.manipulate.position[vector] -= distance * this.positiopnMeshMulti;
            this.pickedPoint[vector] -= distance * this.positiopnMeshMulti;
        }
        else {
            this.mesh.position[vector] += distance * this.positiopnMeshMulti;
            this.manipulate.position[vector] += distance * this.positiopnMeshMulti;
            this.pickedPoint[vector] += distance * this.positiopnMeshMulti;
        }
    };

    BABYLON.Manipulator.prototype._scaleObjectFromVector = function (vector, evt) {
        var vFromRay = this._getMouseRay(evt.clientX, evt.clientY, this.mouseInfo.onDownObject);
        var vToRay = this._getMouseRay(this.mouseInfo.movePosX, this.mouseInfo.movePosY, this.mouseInfo.onDownObject);
        var iDiff = vToRay.origin.subtract(vFromRay.origin);
        var distance = BABYLON.Vector3.Distance(vFromRay.origin, vToRay.origin);

        if (!this.scaleLocked) {
            if (iDiff[vector] > 0 && this.mesh.scaling[vector] > 0) {
                this.mesh.scaling[vector] -= distance * this.scalingMeshMulti;
            }
            else {
                this.mesh.scaling[vector] += distance * this.scalingMeshMulti;
            }
        }
        else {
            if (iDiff[vector] > 0 && this.mesh.scaling[vector] > 0) {
                this.mesh.scaling.x -= distance * this.scalingMeshMulti;
                this.mesh.scaling.y -= distance * this.scalingMeshMulti;
                this.mesh.scaling.z -= distance * this.scalingMeshMulti;
            }
            else {
                this.mesh.scaling.x += distance * this.scalingMeshMulti;
                this.mesh.scaling.y += distance * this.scalingMeshMulti;
                this.mesh.scaling.z += distance * this.scalingMeshMulti;
            }
        }
    };

    BABYLON.Manipulator.prototype._rotateObjectFromVector = function (vector, evt) {
        var vFromRay = this._getMouseRay(evt.clientX, evt.clientY, this.mouseInfo.onDownObject);
        var vToRay = this._getMouseRay(this.mouseInfo.movePosX, this.mouseInfo.movePosY, this.mouseInfo.onDownObject);
        var distance = BABYLON.Vector3.Distance(vFromRay.origin, vToRay.origin);
        var engine = this.scene._engine;
        var projectMesh = BABYLON.Vector3.Unproject(
                new BABYLON.Vector3(this.mesh.position.x, this.mesh.position.y, 0),
                engine.getRenderWidth() * engine.getHardwareScalingLevel(),
                engine.getRenderHeight() * engine.getHardwareScalingLevel(),
                BABYLON.Matrix.Identity(),
                this.scene._viewMatrix,
                this.scene._projectionMatrix
                );
        var projectMeshX = ((engine.getRenderWidth() * engine.getHardwareScalingLevel() / 2) / projectMesh.x) * projectMesh.x;
        var projectMeshY = ((engine.getRenderHeight() * engine.getHardwareScalingLevel() / 2) / projectMesh.y) * projectMesh.y;

        // Current angle between my mouse and the center point of the object
        var angle1 = (Math.atan2(evt.clientX - projectMeshX, evt.clientY - projectMeshY)) * 180 / Math.PI;
        var angle2 = (Math.atan2(this.mouseInfo.movePosX - projectMeshX, this.mouseInfo.movePosY - projectMeshY)) * 180 / Math.PI;
        var rotationToApply = new BABYLON.Vector3(0, 0, 0);
        distance = distance * 100;
        var newVectorValue = (angle1 - angle2 < 0) ? (this.mesh.rotation[vector] + distance) : (this.mesh.rotation[vector] - distance);

        switch (vector) {
            case "x":
                rotationToApply = BABYLON.Quaternion.RotationYawPitchRoll(0, newVectorValue, 0);
                break;
            case "y":
                rotationToApply = BABYLON.Quaternion.RotationYawPitchRoll(newVectorValue, 0, 0);
                break;
            case "z":
                rotationToApply = BABYLON.Quaternion.RotationYawPitchRoll(0, 0, newVectorValue);
                break;
        }
        if (this.mesh.rotationQuaternion == null) {
            this.mesh.rotationQuaternion = new BABYLON.Quaternion(0, 0, 0, 1);
        }
        this.mesh.rotationQuaternion = rotationToApply.multiply(this.mesh.rotationQuaternion);
        this.mesh.rotation = new BABYLON.Vector3(0, 0, 0);
    };

    BABYLON.Manipulator.prototype._createParentVector = function () {
        var manipulateMaterial = new BABYLON.StandardMaterial("manipulateMaterial", this.scene);
        manipulateMaterial.alpha = 0;
        this.manipulate = new BABYLON.Mesh.CreateBox(this.control, 0, this.scene);
        this.manipulate.material = manipulateMaterial;
       // this.manipulate.position = this.manipulatePosition;
        this.manipulate.renderingGroupId = 1;

        switch (this.control) {
            case BABYLON.Manipulator.CONTROLMODE_POSITION:
                this._createVectorPosition(BABYLON.Manipulator.CONTROLMODE_POSITION);
                break;
            case BABYLON.Manipulator.CONTROLMODE_SCALE:
                this._createVectorPosition(BABYLON.Manipulator.CONTROLMODE_SCALE);
                break;
            case BABYLON.Manipulator.CONTROLMODE_ROTATION:
                this._createVectorRotation();
                break;
        }
    };

    BABYLON.Manipulator.prototype._createVectorPosition = function (type) {
        switch (type) {
            case BABYLON.Manipulator.CONTROLMODE_POSITION:
                this.arrowY = new BABYLON.Mesh.CreateCylinder("y", 3, 1, 0, 15, this.scene);
                this.arrowX = new BABYLON.Mesh.CreateCylinder("x", 3, 0, 1, 15, this.scene);
                this.arrowZ = new BABYLON.Mesh.CreateCylinder("z", 3, 1, 0, 15, this.scene);
                break;
            case BABYLON.Manipulator.CONTROLMODE_SCALE:
                this.arrowY = new BABYLON.Mesh.CreateBox("y", 1, this.scene);
                this.arrowX = new BABYLON.Mesh.CreateBox("x", 1, this.scene);
                this.arrowZ = new BABYLON.Mesh.CreateBox("z", 1, this.scene);
                break;
        }

        /* - - - Construction of the vector Y (red)- - - */
        this.vectorY = new BABYLON.Mesh.CreateCylinder("y", 7, 0.2, 0.2, 15, this.scene);
        this.vectorY.renderingGroupId = 1;
        var vectorYMaterial = new BABYLON.StandardMaterial("vectorYMaterial", this.scene);
        vectorYMaterial.diffuseColor = new BABYLON.Color3(0, 0, 0);
        vectorYMaterial.specularColor = new BABYLON.Color3(0, 0, 0);
        vectorYMaterial.specularPower = 32;
        vectorYMaterial.ambientColor = new BABYLON.Color3(0, 0, 0);
        vectorYMaterial.emissiveColor = new BABYLON.Color3(1, 0, 0);
        this.vectorY.material = vectorYMaterial;
        this.vectorY.position.y = 3.5;
        this.vectorY.parent = this.manipulate;
        this.arrowY.renderingGroupId = 1;
        this.arrowY.position.y = 7.5;
        this.arrowY.scaling.y = 1;
        this.arrowY.material = vectorYMaterial;
        this.arrowY.parent = this.manipulate;

        /* - - - Construction of the vector X (green)- - - */
        this.vectorX = new BABYLON.Mesh.CreateCylinder("x", 7, 0.2, 0.2, 15, this.scene);
        this.vectorX.renderingGroupId = 1;
        var vectorXMaterial = new BABYLON.StandardMaterial("vectorXMaterial", this.scene);
        vectorXMaterial.diffuseColor = new BABYLON.Color3(0, 0, 0);
        vectorXMaterial.specularColor = new BABYLON.Color3(0, 0, 0);
        vectorXMaterial.specularPower = 32;
        vectorXMaterial.ambientColor = new BABYLON.Color3(0, 0, 0);
        vectorXMaterial.emissiveColor = new BABYLON.Color3(0, 1, 0);
        this.vectorX.material = vectorXMaterial;
        this.vectorX.rotation.z = Math.PI / 2;
        this.vectorX.position.x = 3.5;
        this.vectorX.parent = this.manipulate;
        this.arrowX.renderingGroupId = 1;
        this.arrowX.position.x = 7.5;
        this.arrowX.scaling.y = 1;
        this.arrowX.material = vectorXMaterial;
        this.arrowX.rotation.z = Math.PI / 2;
        this.arrowX.parent = this.manipulate;

        /* - - - Construction of the vector Z (blue) - - - */
        this.vectorZ = new BABYLON.Mesh.CreateCylinder("z", 7, 0.2, 0.2, 15, this.scene);
        this.vectorZ.renderingGroupId = 1;
        var vectorZMaterial = new BABYLON.StandardMaterial("vectorXMaterial", this.scene);
        vectorZMaterial.diffuseColor = new BABYLON.Color3(0, 0, 0);
        vectorZMaterial.specularColor = new BABYLON.Color3(0, 0, 0);
        vectorZMaterial.specularPower = 32;
        vectorZMaterial.ambientColor = new BABYLON.Color3(0, 0, 0);
        vectorZMaterial.emissiveColor = new BABYLON.Color3(0, 0, 1);
        this.vectorZ.material = vectorZMaterial;
        this.vectorZ.rotation.x = Math.PI / 2;
        this.vectorZ.position.z = 3.5;
        this.vectorZ.parent = this.manipulate;
        this.arrowZ.renderingGroupId = 1;
        this.arrowZ.position.z = 7.5;
        this.arrowZ.rotation.x = Math.PI / 2;
        this.arrowZ.material = vectorZMaterial;
        this.arrowZ.parent = this.manipulate;
    };

    BABYLON.Manipulator.prototype._createVectorRotation = function () {
        /* - - - Construction of the vector Y (red)- - - */
        this.torusY = BABYLON.Mesh.CreateTorus("y", 18, 0.15, 35, this.scene, false);
        var vectorYMaterial = new BABYLON.StandardMaterial("vectorYMaterial", this.scene);
        this.torusY.renderingGroupId = 1;
        vectorYMaterial.diffuseColor = new BABYLON.Color3(0, 0, 0);
        vectorYMaterial.specularColor = new BABYLON.Color3(0, 0, 0);
        vectorYMaterial.ambientColor = new BABYLON.Color3(0, 0, 0);
        vectorYMaterial.emissiveColor = new BABYLON.Color3(1, 0, 0);
        this.torusY.material = vectorYMaterial;
        this.torusY.parent = this.manipulate;
        var bigTorus = BABYLON.Mesh.CreateTorus("y", 18, 1, 35, this.scene, false);
        var bigTorusMaterial = new BABYLON.StandardMaterial("vectorYMaterial", this.scene);
        bigTorusMaterial.alpha = 0;
        bigTorus.material = bigTorusMaterial;
        bigTorus.parent = this.manipulate;

        /* - - - Construction of the vector X (green)- - - */
        this.torusX = BABYLON.Mesh.CreateTorus("x", 18, 0.15, 35, this.scene, false);
        this.torusX.renderingGroupId = 1;
        var vectorXMaterial = new BABYLON.StandardMaterial("vectorXMaterial", this.scene);
        vectorXMaterial.diffuseColor = new BABYLON.Color3(0, 0, 0);
        vectorXMaterial.specularColor = new BABYLON.Color3(0, 0, 0);
        vectorXMaterial.ambientColor = new BABYLON.Color3(0, 0, 0);
        vectorXMaterial.emissiveColor = new BABYLON.Color3(0, 1, 0);
        this.torusX.material = vectorXMaterial;
        this.torusX.rotation.z = Math.PI / 2;
        this.torusX.parent = this.manipulate;

        var bigTorusX = BABYLON.Mesh.CreateTorus("x", 18, 1, 35, this.scene, false);
        bigTorusX.material = bigTorusMaterial;
        bigTorusX.rotation.z = Math.PI / 2;
        bigTorusX.parent = this.manipulate;

        /* - - - Construction of the vector Z (blue) - - - */
        this.torusZ = BABYLON.Mesh.CreateTorus("z", 18, 0.15, 35, this.scene, false);
        this.torusZ.renderingGroupId = 1;
        var vectorZMaterial = new BABYLON.StandardMaterial("vectorZMaterial", this.scene);
        vectorZMaterial.diffuseColor = new BABYLON.Color3(0, 0, 0);
        vectorZMaterial.specularColor = new BABYLON.Color3(0, 0, 0);
        vectorZMaterial.ambientColor = new BABYLON.Color3(0, 0, 0);
        vectorZMaterial.emissiveColor = new BABYLON.Color3(0, 0, 1);
        this.torusZ.material = vectorZMaterial;
        this.torusZ.rotation.x = Math.PI / 2;
        this.torusZ.parent = this.manipulate;
        var bigTorusZ = BABYLON.Mesh.CreateTorus("z", 18, 1, 35, this.scene, false);
        bigTorusZ.material = bigTorusMaterial;
        bigTorusZ.rotation.x = Math.PI / 2;
        bigTorusZ.parent = this.manipulate;
    };

    BABYLON.Manipulator.prototype._mousemove = function (evt) {
        var onDownObjectName = null;
        var manipulateName = null;

        if (this.mouseInfo.onDownObject != null) {
            onDownObjectName = this.mouseInfo.onDownObject.name;
            manipulateName = this.manipulate.name;
            switch (manipulateName) {
                case BABYLON.Manipulator.CONTROLMODE_POSITION:
                    this._mouveObjectFromVector(this.mouseInfo.onDownObject.name, evt);
                    break;
                case BABYLON.Manipulator.CONTROLMODE_SCALE:
                    this._scaleObjectFromVector(this.mouseInfo.onDownObject.name, evt, false);
                    break;
                case BABYLON.Manipulator.CONTROLMODE_ROTATION:
                    this._rotateObjectFromVector(this.mouseInfo.onDownObject.name, evt);
                    break;
            }
        }
        this.mouseInfo.movePosX = evt.clientX;
        this.mouseInfo.movePosY = evt.clientY;
    };

    BABYLON.Manipulator.prototype._mousedown = function (evt) {
        this.mouseInfo.downPosX = evt.clientX;
        this.mouseInfo.downPosY = evt.clientY;
        this.mouseInfo.movePosX = evt.clientX;
        this.mouseInfo.movePosY = evt.clientY;

        if (BABYLON.Manipulator.isSelected) {
            var pickResult = this.scene.pick(
                                evt.clientX,
                                evt.clientY,
                                function (mesh) {
                                    if (mesh.name == "x" || mesh.name == "y" || mesh.name == "z") {
                                        return true;
                                    }
                                    else {
                                        return false;
                                    }
                                },
                                false
            );
            if (pickResult.hit && pickResult.pickedMesh.parent == this.manipulate) {
                this.pick = true;
                this.mouseInfo.onDownObject = pickResult.pickedMesh;
                this.mouseInfo.downRay = this._getMouseRay(this.mouseInfo.downPosX, this.mouseInfo.downPosY, this.mouseInfo.onDownObject);
                this.camera.detachControl(this.connectedCanvas);
            }
        }
        else {
            this.mouseInfo.onDownObject = null;
        }
        if (this.mouseInfo.onDownObject == null && this.pick != true) {
            this.camera.attachControl(this.connectedCanvas, true);
        }
    };

    BABYLON.Manipulator.prototype._mouseup = function (evt) {
        if (this.mouseInfo.onDownObject != null) {
            this.mouseInfo.onDownObject = null;
            this.pick = false;
            this.camera.attachControl(this.connectedCanvas, true);
        }
    };

    BABYLON.Manipulator.prototype._getMouseRay = function (x, y, mesh) {
        var ray = this.scene.createPickingRay(x, y, BABYLON.Matrix.Identity());
        return ray;
    };

    BABYLON.Manipulator.prototype.reset = function () {
        this.mouseInfo.onDownObject = null;
        this.manipulate.dispose(false);
        this.mesh.isPickable = true;
        return ("reset");
    };

    BABYLON.Manipulator.prototype.updatePosition = function (valueX, valueY, valueZ) {
        if ((!isNaN(parseInt(valueX))) && (!isNaN(parseInt(valueY))) && (!isNaN(parseInt(valueZ)))) {
            this.mesh.position.x = parseInt(valueX);
            this.mesh.position.y = parseInt(valueY);
            this.mesh.position.z = parseInt(valueZ);

            this.manipulate.position.x = parseInt(valueX);
            this.manipulate.position.y = parseInt(valueY);
            this.manipulate.position.z = parseInt(valueZ);
        }
    };

    BABYLON.Manipulator.prototype.updateRotation = function (valueX, valueY, valueZ) {
        var x = 0;
        var y = 0;
        var z = 0;
        if ((!isNaN(parseInt(valueX))) && (!isNaN(parseInt(valueY))) && (!isNaN(parseInt(valueZ)))) {
            x = (parseInt(valueX)) * Math.PI / 180;
            y = (parseInt(valueY)) * Math.PI / 180;
            z = (parseInt(valueZ)) * Math.PI / 180;
            if (this.mesh.rotationQuaternion) {
                this.mesh.rotationQuaternion = null;
            }
            this.mesh.rotation.x = x;
            this.mesh.rotation.y = y;
            this.mesh.rotation.z = z;
        }
    };

    BABYLON.Manipulator.prototype.updateScale = function (valueX, valueY, valueZ) {
        if ((!isNaN(parseInt(valueX))) && (!isNaN(parseInt(valueY))) && (!isNaN(parseInt(valueZ)))) {
            this.mesh.scaling.x = parseInt(valueX);
            this.mesh.scaling.y = parseInt(valueY);
            this.mesh.scaling.z = parseInt(valueZ);
        }
    };

    // Statics
    BABYLON.Manipulator.CONTROLMODE_POSITION = 1;
    BABYLON.Manipulator.CONTROLMODE_ROTATION = 2;
    BABYLON.Manipulator.CONTROLMODE_SCALE = 3;

    BABYLON.Manipulator.OBJECTTYPE_BOX = "CreateBox";
    BABYLON.Manipulator.OBJECTTYPE_SPHERE = "CreateSphere";
    BABYLON.Manipulator.OBJECTTYPE_CYLINDER = "CreateCylinder";
    BABYLON.Manipulator.OBJECTTYPE_TORUS = "CreateTorus";
    BABYLON.Manipulator.OBJECTTYPE_CYLINDERCONE = "CreateCylinderCone";

    BABYLON.Manipulator.isSelected = false;
})();