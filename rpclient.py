#-------------------------------------------------------------------------------
# Name:        module1
# Purpose:
#
# Author:      lambda
#
# Created:     10/04/2013
# Copyright:   (c) lambda 2013
# Licence:     <your licence>
#-------------------------------------------------------------------------------

import rpc
import socket

KUKAPROG = 0x20000002
KUKAVERS = 1

##static struct timeval TIMEOUT = { 25, 0 };


INITIALIZE = 1
UNINITIALIZE = 2
GETVAR = 11
SETVAR = 12
GETCROSSERROR = 31

#kuka type
KUKA_UNKNOWN = 0x0000
KUKA_SIMPLE = 0x0001
KUKA_INT = 0x0011
KUKA_REAL = 0x0021
KUKA_BOOL = 0x0031
KUKA_CHAR = 0x0041
KUKA_STRING = 0x0051
KUKA_STRUCT = 0x0002
KUKA_AXIS = 0x0012
KUKA_FRAME = 0x0022
KUKA_POS = 0x0032
KUKA_ERROR = 0x000a


# kuka error type
KUKA_KUKA_E = 0x001a
KUKA_CROSS_E = 0x002a
KUKA_RPC_E = 0x003a
KUKA_API_E = 0x004a

KUKA_STRING_LEN = 128

KUKA_VARNAME_LEN = 14



class kukaAxis_t():
	def __init__(self):
		self.a1 = 0.0 #kukaReal_t
		self.a2 = 0.0
		self.a3 = 0.0
		self.a4 = 0.0
		self.a5 = 0.0
		self.a6 = 0.0


class kukaFrame_t():
	def __init__(self):
		self.x = 0.0 #kukaReal_t
		self.y = 0.0
		self.z = 0.0
		self.a = 0.0
		self.b = 0.0
		self.c = 0.0


class kukaPos_t():
	def __init__(self):
		self.x = 0.0 #kukaReal_t
		self.y = 0.0
		self.z = 0.0
		self.a = 0.0
		self.b = 0.0
		self.c = 0.0
		self.s = 0 #kukaInt_t
		self.t = 0

class kukaError_t():
	def __init__(self):
		self.type = 0 #kukaErrorType_t
		self.no = 0 #long
		self.desc = [KUKA_STRING_LEN] #kukaString_t



class kukaVal():
	def __init__(self):


		self.type = 0 # kukaType_t();
		self.kukaVal_u = None


# kukaVar_s
class kukaVar_t():
	def __init__(self):
		self.nom = [KUKA_VARNAME_LEN] #char
		self.valeur = kukaVal();




class rpcClient(rpc.Client):
	def __init__(self,host,prog,ver,port):

		self.sock = None #socket.socket()
		#if port == 'tcp':
		#	port = 111

##		self.host = host
##		self.prog = prog
##		self.ver = ver
##		self.port = port
		rpc.Client.__init__(self,host,prog,ver,port)

		return

	def do_call(self):
		print "do call"

		call = self.packer.get_buf()
		print len(call)

		try:
			rpc.sendrecord(self.sock, call)
		except(RuntimeError):
			print "send error"
		pass

		try:
			reply = rpc.recvrecord(self.sock)
		except(RuntimeError):
			print "recv erro"
		pass

		u = self.unpacker
		print reply
		u.reset(reply)

##		xid, verf = u.unpack_replyheader()
##		if xid != self.lastxid:
##			# Can't really happen since this is TCP...
##			raise RuntimeError, 'wrong xid in reply %r instead of %r' % (
##					xid, self.lastxid)

		#print "end do call"

		return

	def makesocket(self):
		print "make socket"
		self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
		return


	def kuka_getVar(self,varInOut):

		#kukaVar_t *result=NULL;
		result = None

		#if( setKukaErrorFromArg(varInOut) ) return 1;

		#result=getvar_1(varInOut, clnt);
		#if( setKukaErrorFromResult(result) ) return 1;

		#// appel rpc reussi : on copie le resultat dans varInOut
		#memcpy(varInOut,result,sizeof(kukaVar_t));

		return 0

	def kuka_setVar(self,varIn):

		#kukaVar_t *result=NULL;
		result = None

		#if( setKukaErrorFromArg(varIn) ) return 1;

		#result = setvar_1(varIn, clnt);
		#if( setKukaErrorFromResult(result) ) return 1;

		return 0


	def kuka_getError(self,kukaErrorIn):

        #kukaVar_t *result=NULL;
		result = None

		#if(kukaErrorIn!=NULL)
		#	*kukaErrorIn=&kukaError;
		#else{
		#	result = getcrosserror_1(NULL, clnt);
		#	setKukaErrorFromResult(result);
		#}
		return

	def pack_struct(self,arg):

		# send ok header

		print "len buf pack : %d"%(len(self.packer.buf))

		# nom ()
		for i in arg.nom:
			self.packer.pack_int(ord(i))

		for i in range(1,KUKA_VARNAME_LEN - len(arg.nom)+1):
			self.packer.pack_int(0)

		print "len buf pack : %d"%(len(self.packer.buf))

		if arg.valeur.type == KUKA_FRAME:
			print "kuka frame"
			self.packer.pack_int(KUKA_FRAME)
			self.packer.pack_float(arg.valeur.kukaVal_u.x)
			self.packer.pack_float(arg.valeur.kukaVal_u.y)
			self.packer.pack_float(arg.valeur.kukaVal_u.z)
			self.packer.pack_float(arg.valeur.kukaVal_u.a)
			self.packer.pack_float(arg.valeur.kukaVal_u.b)
			self.packer.pack_float(arg.valeur.kukaVal_u.c)

			print "len buf pack : %d"%(len(self.packer.buf))

		elif arg.valeur.type == KUKA_BOOL:
			self.packer.pack_int(KUKA_BOOL)
			self.packer.pack_bool(arg.valeur.kukaVal_u)

		elif arg.valeur.type == KUKA_INT:
			self.packer.pack_int(KUKA_INT)
			self.packer.pack_int(arg.valeur.kukaVal_u)

		return

	def unpack_struct(self):

		print "unpack struct"

		kval = kukaVar_t()


		print "buf : %d"%(len(self.unpacker.buf))
		print self.unpacker.buf


		try:

			r = self.unpacker.unpack_replyheader()
			print "retour replyheader :"
			print r

		except EOFError,msg:
			print "nothing"
			print msg
			return
		except RuntimeError,msg:
			print "error"
			print msg
			return
		pass

		print "pos : %d"%(self.unpacker.pos)

		# extract kavvar name
		str =''
		for i in range(1,KUKA_VARNAME_LEN+1):
			asc = self.unpacker.unpack_int()
			if asc > 0:
				str = str + chr(asc)

		kval.nom = str

		print "nom : [" + kval.nom + "]"

		print "pos : %d"%(self.unpacker.pos)

		# type
		kval.valeur.type = self.unpacker.unpack_int()

		print "type : %d"%(kval.valeur.type)

#		print "int:%d"%(self.unpacker.unpack_int())
#		print "int:%d"%(self.unpacker.unpack_int())
#		print "int:%d"%(self.unpacker.unpack_int())
#		print "int:%d"%(self.unpacker.unpack_int())
#		print "int:%d"%(self.unpacker.unpack_int())

		# kukaint
		if kval.valeur.type == KUKA_INT:
			print "kuka int"
			kval.valeur.kukaVal_u = self.unpacker.unpack_int()
		# kukareal
		elif kval.valeur.type == KUKA_REAL:
			print "kuka real"
			kval.valeur.kukaVal_u = self.unpacker.unpack_float()
		# kukabool
		elif kval.valeur.type == KUKA_BOOL:
			print "kuka char"
			kval.valeur.kukaVal_u = self.unpacker.unpack_int()
		# kukachar
		elif kval.valeur.type == KUKA_CHAR:
			print "kuka char"
			kval.valeur.kukaVal_u = self.unpacker.unpack_fstring(1)
		# kukastring
		elif kval.valeur.type == KUKA_STRING:
			print "kuka string"
			kval.valeur.kukaVal_u = self.unpacker.unpack_fstring(KUKA_STRING_LEN)
		# kukaaxis
		elif kval.valeur.type == KUKA_AXIS:
			print "kuka axis"
			kval.valeur.kukaVal_u = kukaAxis_t()
			kval.valeur.kukaVal_u.a1 = self.unpacker.unpack_float()
			kval.valeur.kukaVal_u.a2 = self.unpacker.unpack_float()
			kval.valeur.kukaVal_u.a3 = self.unpacker.unpack_float()
			kval.valeur.kukaVal_u.a4 = self.unpacker.unpack_float()
			kval.valeur.kukaVal_u.a5 = self.unpacker.unpack_float()
			kval.valeur.kukaVal_u.a6 = self.unpacker.unpack_float()

		# kukaframe
		elif kval.valeur.type == KUKA_FRAME:
			print "kuka frame"
			kval.valeur.kukaVal_u = kukaFrame_t()
			kval.valeur.kukaVal_u.x = self.unpacker.unpack_float()
			kval.valeur.kukaVal_u.y = self.unpacker.unpack_float()
			kval.valeur.kukaVal_u.z = self.unpacker.unpack_float()
			kval.valeur.kukaVal_u.a = self.unpacker.unpack_float()
			kval.valeur.kukaVal_u.b = self.unpacker.unpack_float()
			kval.valeur.kukaVal_u.c = self.unpacker.unpack_float()

			k = kval.valeur.kukaVal_u

			print "frame :  x:%f , y:%f , z%f , a:%f ; b:%f , c:%f"%(k.x,k.y,k.z,k.a,k.b,k.c)

		# kukapos
		elif kval.valeur.type == KUKA_POS:
			print "kuka pos"
			kval.valeur.kukaVal_u = kukaPos_t()
			kval.valeur.kukaVal_u.x = self.unpacker.unpack_float()
			kval.valeur.kukaVal_u.y = self.unpacker.unpack_float()
			kval.valeur.kukaVal_u.z = self.unpacker.unpack_float()
			kval.valeur.kukaVal_u.a = self.unpacker.unpack_float()
			kval.valeur.kukaVal_u.b = self.unpacker.unpack_float()
			kval.valeur.kukaVal_u.c = self.unpacker.unpack_float()
			kval.valeur.kukaVal_u.s = self.unpacker.unpack_int()
			kval.valeur.kukaVal_u.t = self.unpacker.unpack_int()

		# kukaerror
		elif kval.valeur.type == KUKA_ERROR:
			print "kuka error"
			kval.valeur.kukaVal_u = kukaError_t()
			kval.valeur.kukaVal_u.type = self.unpacker.unpack_int()
			print "type : "+get_kuka_error_desc(kval.valeur.kukaVal_u.type)
			kval.valeur.kukaVal_u.no = self.unpacker.unpack_int()
			print "no : %d"%(kval.valeur.kukaVal_u.no)
			kval.valeur.kukaVal_u.desc = self.unpacker.unpack_fstring(KUKA_STRING_LEN*4)

		# unknown
		else:
			print "kuka unknown"

		print "pos : %d"%(self.unpacker.pos)
		print "buf : %d"%(len(self.unpacker.buf))
		print self.unpacker.buf

		while self.unpacker.pos < len(self.unpacker.buf)>0:
			i = self.unpacker.unpack_uint()


		print "end extract"

		return kval



##/*************************************************/
##void kuka_displayVar(kukaVar_t *var)
##{
##  if( var==NULL ){
##    printf("Le param?tre de kuka_displayVar ne peut etre NULL.\n");
##    return ;
##  }
##
##  printf("nom    = \"%s\"\n",var->nom);
##  switch(var->valeur.type){
##  case KUKA_INT:
##    printf("type   = KUKA_INT\n");
##    printf("valeur = %d\n",var->valeur.kukaVal_u.kukaInt);
##    break;
##  case KUKA_REAL:
##    printf("type   = KUKA_REAL\n");
##    printf("valeur = %f\n",var->valeur.kukaVal_u.kukaReal);
##    break;
##  case KUKA_BOOL:
##    printf("type   = KUKA_BOOL\n");
##    printf("valeur = %d\n",var->valeur.kukaVal_u.kukaBool);
##    break;
##  case KUKA_CHAR:
##    printf("type   = KUKA_CHAR\n");
##    printf("valeur = '%c'\n",var->valeur.kukaVal_u.kukaChar);
##    break;
##  case KUKA_STRING:
##    printf("type   = KUKA_CHAR\n");
##    printf("valeur = \"%s\"\n",var->valeur.kukaVal_u.kukaString);
##    break;
##  case KUKA_AXIS:
##    printf("type   = KUKA_AXIS\n");
##    printf("valeur = { A1 %f, A2 %f, A3 %f, A4 %f, A5 %f, A6 %f}\n",\
##	   var->valeur.kukaVal_u.kukaAxis.a1,\
##	   var->valeur.kukaVal_u.kukaAxis.a2,\
##	   var->valeur.kukaVal_u.kukaAxis.a3,\
##	   var->valeur.kukaVal_u.kukaAxis.a4,\
##	   var->valeur.kukaVal_u.kukaAxis.a5,\
##	   var->valeur.kukaVal_u.kukaAxis.a6);
##    break;
##  case KUKA_FRAME:
##    printf("type   = KUKA_FRAME\n");
##    printf("valeur = { X %f, Y %f, Z %f, A %f, B %f, C %f}\n",\
##	   var->valeur.kukaVal_u.kukaFrame.x,\
##	   var->valeur.kukaVal_u.kukaFrame.y,\
##	   var->valeur.kukaVal_u.kukaFrame.z,\
##	   var->valeur.kukaVal_u.kukaFrame.a,\
##	   var->valeur.kukaVal_u.kukaFrame.b,\
##	   var->valeur.kukaVal_u.kukaFrame.c);
##    break;
##  case KUKA_POS:
##    printf("type   = KUKA_POS\n");
##    printf("valeur = { X %f, Y %f, Z %f, A %f, B %f, C%f, S %d, T %d}\n",\
##	   var->valeur.kukaVal_u.kukaPos.x,\
##	   var->valeur.kukaVal_u.kukaPos.y,\
##	   var->valeur.kukaVal_u.kukaPos.z,\
##	   var->valeur.kukaVal_u.kukaPos.a,\
##	   var->valeur.kukaVal_u.kukaPos.b,\
##	   var->valeur.kukaVal_u.kukaPos.c,\
##	   var->valeur.kukaVal_u.kukaPos.s,\
##	   var->valeur.kukaVal_u.kukaPos.t);
##    break;
##  case KUKA_ERROR:
##    printf("type   = KUKA_ERROR\n");
##    printf("valeur = \n");
##    printf("\t type = ");
##    switch(var->valeur.kukaVal_u.kukaError.type){
##    case KUKA_API_E:
##      printf("KUKA_API_E ");
##      break;
##    case KUKA_RPC_E:
##      printf("KUKA_RPC_E ");
##      break;
##    case KUKA_CROSS_E:
##      printf("KUKA_CROSS_E ");
##      break;
##    case KUKA_KUKA_E:
##      printf("KUKA_KUKA_E ");
##      break;
##    }
##    printf("\n\t n? = %ld\n\t description = %s\n",
##	   var->valeur.kukaVal_u.kukaError.no,
##	   var->valeur.kukaVal_u.kukaError.desc);
##    break;
##
##  case KUKA_UNKNOWN :
##  default :
##    printf("type   = KUKA_UNKNOWN\n");
##    break;
##  }
##  return;
##}


##/*************************************************/
##/*  FONCTIONS PRIVEES                            */
##/*************************************************/
##/*! Ces fonctions permettent de manager les erreurs
## *  ? partir d'un reultat.
## *  Cette fonction devrait etre la seule ? ecrire
## *  dans kukaError pour simplifier la maintenance
## *  du code.
## */
##int setKukaErrorFromArg(void *arg)
##{
##  if( clnt==NULL ){
##    kukaError.valeur.type=KUKA_ERROR;
##    kukaError.valeur.kukaVal_u.kukaError.type=KUKA_API_E;
##    kukaError.valeur.kukaVal_u.kukaError.no=0;
##    strncpy(kukaError.valeur.kukaVal_u.kukaError.desc,
##	    "The api should be initialized. Call kuka_initialize().",
##	    KUKA_STRING_LEN);
##    return 1;
##  }
##
##  if( arg==NULL ){
##    kukaError.valeur.type=KUKA_ERROR;
##    kukaError.valeur.kukaVal_u.kukaError.type=KUKA_API_E;
##    kukaError.valeur.kukaVal_u.kukaError.no=0;
##    strncpy(kukaError.valeur.kukaVal_u.kukaError.desc,
##	    "Argument can't be NULL.",
##	    KUKA_STRING_LEN);
##    return 1;
##  }
##
##  return 0;
##}
##

def get_kuka_error_desc(num):
	if num == 0x001a:
		return "KUKA_KUKA_E"
	elif num == 0x002a:
		return "KUKA_CROSS_E"
	elif num == 0x003a:
		return "KUKA_RPC_E"
	elif num == 0x004a:
		return "KUKA_API_E"
	else:
		return "unknown error"



def main():

	print "main"

 	pmap = rpc.TCPPortMapperClient('140.80.10.27')
	port = pmap.Getport((KUKAPROG, KUKAVERS, rpc.IPPROTO_TCP, 0))
	pmap.close()

	print "port :%d"%(port)


	clnt = rpcClient("140.80.10.27",KUKAPROG, KUKAVERS, port);  #140.80.10.27


	#setKukaErrorFromResult(clnt)

	#result=initialize_1(&nom, clnt);
	#kukaVar_t * initialize_1(argp, clnt)
	#void *argp;
	#CLIENT *clnt;
	#{
	#	static kukaVar_t res;
	#	bzero((char *)&res, sizeof(res));
	#	if (clnt_call(clnt, INITIALIZE, xdr_void, argp, xdr_kukaVar_t, &res, TIMEOUT) != RPC_SUCCESS) {
	#		return (NULL);
	#	}
	#return (&res);
	#}

	print "init"
	res = clnt.make_call(INITIALIZE, None, None, clnt.unpack_struct)


	print "setspeed"
	speed = kukaVar_t()
	speed.nom = "VITESSE"
	speed.valeur.type = KUKA_INT
	speed.valeur.kukaVal_u  = 3

	#flag = kukaVar_t()
	#flag.nom = "VITESSE"
	#flag.valeur.type = KUKA_INT
	#flag.valeur.kukaVal_u = 0
	#print "flag"
	res = clnt.make_call(GETVAR, speed, clnt.pack_struct, clnt.unpack_struct)

	#print res.valeur.kukaVal_u.desc

	#print "speed"
	#res = clnt.make_call(SETVAR, speed, clnt.pack_struct, clnt.unpack_struct)


	print "setvar"
	#kukavar_t
	cible = kukaVar_t()
	cible.nom = "CIBLE"
	cible.valeur.type = KUKA_FRAME
	cible.valeur.kukaVal_u  = kukaFrame_t()

	#{ X 257.8945, Y -1247.397, Z 1781.805, A -90.000, B 90.000, C 0.0000}

	cible.valeur.kukaVal_u.x = 257.8945
	cible.valeur.kukaVal_u.y = -1247.397
	cible.valeur.kukaVal_u.z = 1881.805
	cible.valeur.kukaVal_u.a = -90
	cible.valeur.kukaVal_u.b = 90.0
	cible.valeur.kukaVal_u.c = 0.0

	res = clnt.make_call(SETVAR, cible, clnt.pack_struct, clnt.unpack_struct)


	print "getvar"
	cible = kukaVar_t()
	cible.nom = "CIBLE"
	cible.valeur.type = KUKA_FRAME
	cible.valeur.kukaVal_u  = kukaFrame_t()

	res = clnt.make_call(GETVAR, cible, clnt.pack_struct, clnt.unpack_struct)

	#if( setKukaErrorFromResult(res) ) : return 1

	#res = kukaVar_t()

#	for n in range(1000):
#		print n
#		res = clnt.make_call(n, None, None, clnt.unpack_struct)




#	if (clnt_call(clnt, INITIALIZE, xdr_void, argp, xdr_kukaVar_t, &res, TIMEOUT) != RPC_SUCCESS) {
#		return (NULL);



	#clnt.close()



	#pass


if __name__ == '__main__':
	main()






##kukaVar_t *
##uninitialize_1(argp, clnt)
##	void *argp;
##	CLIENT *clnt;
##{
##	static kukaVar_t res;
##
##	bzero((char *)&res, sizeof(res));
##	if (clnt_call(clnt, self.UNINITIALIZE, xdr_void, argp, xdr_kukaVar_t, &res, TIMEOUT) != RPC_SUCCESS) {
##		return (NULL);
##	}
##	return (&res);
##}


##kukaVar_t *
##getvar_1(argp, clnt)
##	kukaVar_t *argp;
##	CLIENT *clnt;
##{
##	static kukaVar_t res;
##
##	bzero((char *)&res, sizeof(res));
##	if (clnt_call(clnt, self.GETVAR, xdr_kukaVar_t, argp, xdr_kukaVar_t, &res, TIMEOUT) != RPC_SUCCESS) {
##		return (NULL);
##	}
##	return (&res);
##}


##kukaVar_t *
##setvar_1(argp, clnt)
##	kukaVar_t *argp;
##	CLIENT *clnt;
##{
##	static kukaVar_t res;
##
##	bzero((char *)&res, sizeof(res));
##	if (clnt_call(clnt, self.SETVAR, xdr_kukaVar_t, argp, xdr_kukaVar_t, &res, TIMEOUT) != RPC_SUCCESS) {
##		return (NULL);
##	}
##	return (&res);
##}



##kukaVar_t *
##getcrosserror_1(argp, clnt)
##	void *argp;
##	CLIENT *clnt;
##{
##	static kukaVar_t res;
##
##	bzero((char *)&res, sizeof(res));
##	if (clnt_call(clnt, GETCROSSERROR, xdr_void, argp, xdr_kukaVar_t, &res, TIMEOUT) != RPC_SUCCESS) {
##		return (NULL);
##	}
##	return (&res);
##}
