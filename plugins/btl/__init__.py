# -*- coding: utf-8 -*-

import imp
import os.path


pluginName = "Btl"
version = "0.1"
author = "Olivier Martinez"

def register(parent):


	mod = imp.load_source('btl', './plugins/btl/btl.py')

	parent.add_object('file.importer','Btl importer',mod)



	return


