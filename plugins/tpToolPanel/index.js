var tpToolPanel;

var tpToolPanel = function tpToolPanel() {

	this.pluginName = "ToolPanel";
	this.pluginVersion = "0.1";
	this.pluginAuthor = "Olivier Martinez";
	this.panel;
	
};

tpToolPanel.prototype = {

	"register":function(parent) {
		

		var msg ={};
		msg['msg'] = "ToolPanel";
		parent.add_icon('Tool','plugins/tpToolPanel/tool.png', true, "@tpToolPanel&on_tool_toggle",msg,"tpToolPanel");
		

	},

	"on_tool_toggle":function() {
	
		//console.log("on tool toggle");
		
		var f = $("#tool_panel");
		if(f.length){
		
			//L'élément existe
			if ($("#tpToolPanelTool").hasClass("active")) {
				$("#tpToolPanelTool").removeClass("active");
				$("#tool_panel").hide();
			} else {
				$("#tpToolPanelTool").removeClass("active");
				$("#tool_panel").show();
			}
		  
		} else {
		
			//L'élément n'existe pas
			this.panel = new toolPanelView();
			//parent.add_object("robot_panel", f);
			this.panel.__init__();
			$("#tool_panel").show();
			
		}
		
		this.panel.populate();
		
	
	},

	"unload":function() {
	
	}

};


tereoPlugins['tpToolPanel'] = new tpToolPanel();
		


var toolPanelView = function() {

	this.uid = (((1+Math.random())*0x100000)|0).toString(16).substring(1);
	this.lastmsg ="";
	this.tools = {};
	this.calibrations = {};

}

toolPanelView.prototype = {

	"__init__": function () {

		$( "#tereopanels").append('<div class="tpPanel" id="tool_panel"><div class="tpPanelTitle">Tool panel</div><div><button class="form_button" onclick="tpToolPanel_on_tool_new();"><img src="media/add.png">Add a tool</button></div><div id="toolPanelScroll" class="tpScroll"><div id="tools_list"></div></div></div>');

	},		
		
//f = parent.object_search_path("tool.panel")


	
	"populate":function () {
	
		//console.log("populate");

		// clear
		var myNode = document.getElementById("toolPanelScroll");
		while (myNode.firstChild) {
			myNode.removeChild(myNode.firstChild);
		}

		
		// todo get robot object list		
		// todo get calibration list
		var msg = {}
		msg['msg'] = "get_tool_list";
		msg['uid'] = this.uid+"grl";
		msg['callback'] = "tpToolPanel_on_get_tool_list";
		//console.log (tpRobotPanel_on_get_robot_list);
		//console.log (msg);
		
		tereoMain.call("sendMqttMsg",JSON.stringify(msg));

		msg['msg'] = "get_toolcalibration_list";
		msg['uid'] = this.uid+"gcl";
		msg['callback'] = "tpToolPanel_on_get_toolcalibration_list";
		tereoMain.call("sendMqttMsg",JSON.stringify(msg));
	
	},

	"refresh":function () {
		
		//console.log("refresh");
		
		//clear
		var myNode = document.getElementById("tools_list");
		while (myNode.firstChild) {
			myNode.removeChild(myNode.firstChild);
		}
		
		i = 0;
		var render="<table>";

		for (r in this.tools) { 
		
			if (this.robots.hasOwnProperty(r)) {
			
				rob = this.tools[r];

				render += "<tr><td colspan=2><div class=\"tpPanelChild\"><table><tr><td class=\"tpPanelProps\">"+i+"</td><td>"+too['name']+"</td></tr>";


				render += '<tr><td colspan=2><table><td><button class="form_button" onclick="tpToolPanel_on_tool_calibrate('+too['name']+');"><img src="media/calibrate.png">Calib</button></td>';

				render += '<td><button class="form_button" onclick="tpToolPanel_on_config('+too['name']+');"><img src="media/configure.png">Calib</button></td>';
				
				render += '<td><button class="form_button" onclick="tpToolPanel_on_delete('+too['name']+');"><img src="media/dialog-no.png">Del</button></td></tr></table></td></tr>';


				//-----------------------

				//lcal = self.tereo.function_search_path("tool.calibration")
				
				//lcal = self.tereo.function_search_path("Tool.calibration")
				if (this.calibrations.length >0 ) {
				
					render += '<tr><td><img src="media/calibrate.png"></td><td><button class="form_button" onclick="tpToolPanel_on_tool_calibrate('+r['id']+');">Calib</button></td></tr>';
	//				render += '<tr><td><img src="media/dialog-no.png"></td><td><button onclick="self.on_tool_calibrate, i">Del</button></td></tr>';

					render += '<tr><td colspan=2><select>';

					//slist = []
					//defau = ""
					for (it in this.calibrations) {
						cali = this.calibrations[it];
						//if defau == "":
						//	defau = it[0]
						//slist.append(it[0])
						render += '<option value="'+cali['name']+'">'+cali['name']+'</option>';
					}
					
					render += '</select>';
				}
				
				i+=1;
				render += "</table></div></td></tr>";
			}

		}
		
		render += '</table>';
		//console.log("render:"+render);
		$( "#tools_list").append(render);
		
	}

}


function tpToolPanel_on_get_tool_list(l) {
	//console.log("onplugin toollist:"+l);
	//console.log(l);
	if (l == "get_tool_list") { return; }
	delete cnx_callbacks[this.uid+"grl"];
	
	try {
		l = l.replace(' ','');
		l = l.replace('\n','');
		l = l.replace('\r','');
		tereoPlugins['tpToolPanel'].panel.tools = JSON.parse(l);
	} catch(e) { console.log(e); }
	
	//console.log(tereoPlugins['tpToolPanel'].panel.tools);
	tereoPlugins['tpToolPanel'].panel.refresh();
}

function tpToolPanel_on_get_toolcalibration_list(l) {
	//console.log("onplugin caliblist:"+l);
	if (l == "get_toolcalibration_list") { return; }
	delete cnx_callbacks[this.uid+"gcl"];
	
	try {
		l = l.replace(' ','');
		l = l.replace('\n','');
		l = l.replace('\r','');
		//console.log("after:"+l);
		tereoPlugins['tpToolPanel'].panel.calibrations = JSON.parse(l);
	} catch(e) { console.log(e); }
	//console.log("calib:");
	//console.log(tereoPlugins['tpToolPanel'].panel.calibrations);	
	tereoPlugins['tpToolPanel'].panel.refresh();
}


function tpToolPanel_on_delete(i) {
	//todo
}




function tpToolPanel_on_tool_new() {

	// display robot_library
	/*self.robotsmodelwinrethandler = self.on_robot_new_ok
	self.robotsmodelwin.maximize()
	self.robotsmodelwin.show()
	self.robotsmodelwin_init()*/
		
}


function tpToolPanel_on_tool_new_ok() {

/*
	self.tmprobot.set_dummy_comp()
	self.tmprobot.db = self.tmpdb
	self.tmprobot.load_mesh()
	self.tereo.env.robots.append(self.tmprobot)
	r = self.tereo.env.robots[len(self.tereo.env.robots)-1]
	self.robotsmodelwinrethandler = None
	if self.builder.get_object("toolbutton12").get_active()==True:
		self.populate_robot_panel()

	rob = r.get_collection()
	m = Rotate([rob],r.angle_z,0,0,1)
	m = Translate([m],r.position_x * 0.03937,r.position_y * 0.03937,r.position_z * 0.03937)
	m = Collection([m])
	rob.vmodel = m
	self.scenevismachframe.addmodel(m);
*/	
	
}









/*##################################################
### TOOL CALIBRATION
##################################################*/


function tpToolPanel_on_tool_calibrate(data) {

/*
	// calibration variables
	co = self.tereo.new_calibration_obj();
	co.calib_title = "Tool calibration";
	co.calib_nb_question = 4;
	co.calib_questions = ['Touch the reference object (point 1)','Touch the reference object (point 2)','Touch the reference object (point 3)','Touch the reference object (point 4)'];
	co.calib_robot_vars = ['$POS_ACT','$POS_ACT','$POS_ACT','$POS_ACT'];
	co.calib_nb_img = 1;
	co.calib_imgs = ['toolcalib.png'];
	co.calib_handler = self.on_tool_calibrate_return;
	co.calib_tag = data;

	co.calib_win_init();
*/	
}

function tpToolPanel_on_tool_calibrate_return(self,co) {
/*
	if co.calib_ret == None:
		return

	if len(co.calib_ret) == 0:
		return

	a = co.calib_ret[0];
	b = co.calib_ret[1];
	c = co.calib_ret[2];
	d = co.calib_ret[3];

	fa = frame6(a.x,a.y,a.z,a.a,a.b,a.c);
	fb = frame6(b.x,b.y,b.z,b.a,b.b,b.c);
	fc = frame6(c.x,c.y,c.z,c.a,c.b,c.c);
	fd = frame6(d.x,d.y,d.z,d.a,d.b,d.c);


	self.tereo.env.tool[co.calib_tag-1].calc_tcp(fa,fb,fc,fd);
*/
}




