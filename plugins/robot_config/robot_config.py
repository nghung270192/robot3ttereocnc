# -*- coding: utf-8 -*-


import os.path

from plugins.robot_config import robot_config_window



def on_robotconfig(parent,idx):

	f = parent.object_search_path("robot.config")

	if f == []:
		f = robot_config_window.robot_config_window(parent,idx)
		parent.add_object("robot.config","Robot config",f)
	else:
		f=f[0]

	f.show()


	return
