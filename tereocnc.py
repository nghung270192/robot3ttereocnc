#!/usr/bin/python

import pygtk
pygtk.require("2.0")
import gtk

#from direct.task.TaskManagerGlobal import taskMgr
#from direct.task                  import Task

import collections
import tereoTool
import tereoToolChanging
import thread
from tereoMath import *

import subprocess

import tereoRobot
import tereoUtil
import tereoProcess
import tereoEnvironment
import tereoFlow
import tereoGui
import tereoMain
import linuxcnc_controller


import os
import os.path
import imp
import sys


sys.path += ['.']
MainModule = "__init__"


pluginpath = "./plugins"

splScr = None

#if not pluginpath in sys.path:
#    sys.path.insert(1,pluginpath)




import copy
import gobject
from time import sleep

#import rpclientsaa


import xml.etree.ElementTree as ET


### in server ###


from twisted.internet import gtk2reactor
gtk2reactor.install()
from twisted.internet.task import LoopingCall
from twisted.internet import reactor
#from twisted.internet.protocol import Factory
#from twisted.protocols.basic import LineReceiver
#import tereoNet
### end in server ###

from twisted.protocols.basic import LineReceiver
from twisted.internet.protocol import ReconnectingClientFactory

SETTINGS = gtk.settings_get_default()

if sys.platform == 'win32':
	SETTINGS.set_long_property("gtk-button-images", True, "main")





class calibrationObj:

	def __init__():

		self.calib_title = ""
		self.calib_nb_question = 0
		self.calib_questions = []
		self.calib_robot_vars = []
		self.calib_nb_img = 0
		self.calib_imgs = []
		self.calib_handler = None
		self.calib_tag = None
		self.calib_ret = None
		self.calib_tag = None


		return

	####################################
	### calibration
	# calib_questions[]
	# calibimgs[]
	# calib_title
	# calib_handle  function appel?e en retour
	# calib_ret[] positions retour
	####################################
	def calib_win_init(self):

		if self.calib_questions < 1:
			return

		self.calib_ret = None
		self.calib_index = 0
		self.calib_ret = []

		dlg = self.builder.get_object("calibWindow")
		lbl = self.builder.get_object("caliblabel")
		lbl.set_label(self.calib_questions[0])

		if self.calib_nb_img>0:
			img = self.builder.get_object("calibimg")
			try:
				img.set_from_file(self.calib_imgs[0])
			except:
				self.applog("system",1,"calibration image not found")

		btn = self.builder.get_object("calibnext")
		btn.set_label("Next >")

		if self.calib_title != "":
			dlg.set_title(self.calib_title)

		dlg.show()
		return


	def on_calib_next(self,widget):

		if self.calib_robot <0:
			if self.calib_handler != None:
				self.calib_handler(self)
			return

        # get var
		#self.calib_robot_vars
		r = self.env.robots[self.calib_robot]
		if r.client != None:
			if r.client.instance != None:
				r.client.instance.position_updated = False
				r.send("stat position")
				gobject.timeout_add(100, self.on_calib_next2, r)

		return


	def on_calib_next2(self,r):

		if r.client == None:
			return False
		if r.client.instance == None:
			return False
		if r.client.instance.position_updated == False:
			return True

		#print "avant append"
		f = frame6()
		p = r.client.instance.position
		f.x = p['X']
		f.y = p['Y']
		f.z = p['Z']
		f.a = p['A']
		f.b = p['B']
		f.c = p['C']
		self.calib_ret.append(f)

		# next question or end
		if (self.calib_index+1) == self.calib_nb_question:
			dlg = self.builder.get_object("calibWindow")
			dlg.hide()
			if self.calib_handler != None:
				self.calib_handler(self)
			return

		self.calib_index+=1

		lbl = self.builder.get_object("caliblabel")
		lbl.set_label(self.calib_questions[self.calib_index])

		if self.calib_nb_img>= self.calib_index:
			if self.calib_imgs[self.calib_index] != None:
				img = self.builder.get_object("calibimg")
				try:
					img.set_from_file(self.homedir + '/' + self.calib_imgs[self.calib_index])
				except:
					self.applog("system",1,"calibration image not found")

		if (self.calib_index+1) == self.calib_nb_question:
			btn = self.builder.get_object("calibnext")
			btn.set_label("End")

		return False

	def on_calib_cancel(self,widget):

		dlg = self.builder.get_object("calibWindow")
		dlg.hide()

		if self.calib_handler==None:
			return

		self.calib_handler(self)

		return



#####################################################################
#	MAIN CLASS
#####################################################################

class tereo:

	def new_calibration_obj(self):
		return calibrationObj()


	def get_active_window(self):
		for w in gtk.window_list_toplevels():
			if w.is_active() == True:
				return w



	def check_error(self):

		if hasattr(self,"env") == False:
			return True
		if self.env == None:
			return True
		#for r in self.env.robots:
		#	if r.client != None:
		#		if r.client.last_cnc_error != "":
		#			word_list = r.client.last_cnc_error.split(':')
		#			if len(word_list)>1:
		#				self.applog("network",1,word_list[1] + r.name)
		#			else:
		#				self.applog("network",1,word_list + r.name)
		#			r.client.last_cnc_error = ""
		#		if r.client.instance != None:
		#			if r.client.instance.last_cnc_error != "":
		#				word_list = r.client.instance.last_cnc_error.split(':')
		#				if len(word_list)>1:
		#					self.applog("network",1,word_list[1] + r.name)
		#				else:
		#					self.applog("network",1,r.name)
		#				r.client.instance.last_cnc_error = ""
		#				# todo faire clignoter icone log

		return True

	def function_search_path(self,name):

		funclist = []

		for i in self.functions:
			if i[0] == name:
				funclist.append([i[1],i[2]])

		return funclist

	def object_search_path(self,name):

		objlist = []

		for i in self.objects:
			if i[0] == name:
				# 0 = path, 1 = name, 2 = command
				objlist.append([i[1],i[2]])

		return objlist


	def ok_for_mdi():
##		s.poll()
##		if s.estop == True:
##			print "Can't send mdi emergency stop on"
##			return False
##
##		if s.enabled == False:
##			print "Can't send mdi machine off"
##			return False
##
##		if s.homed == False:
##			print "Can't send mdi machine not homed"
##			return False
##
##		if s.interp_state != linuxcnc.INTERP_IDLE:
##			print "Can't send mdi interpreter is already running"
##			return False

		return True


	def get_robot_list(self):

		l = "{";
		i = 0

		if hasattr(self,"env") :
			if self.env != None:
				for r in self.env.robots:
					if i>0:
						l = l + ","
					l = l + "\""+r.name + "\": {\"name\":\"" + r.name + "\",\"model\":\"" + r.model + "\",\"constructor\":\""+r.constructor+"\",\"ip\":\""+r.ip+"\",\"chan\":\""+str(r.chan)+"\",\"calc\":\""+r.calc+"\",\"calc_ip\":\""+r.calc_ip+"\",\"calc_chan\":\""+str(r.calc_chan)+"\",\"proto\":\""+r.proto+"\"}"
					i=i+1

		l = l + "}"
		#print l

		return l


	def get_network_list(self):

		l = "{";
		i = 0

		if hasattr(self,"env") :
			if self.env != None:
				for r in self.env.robots:
					
					lbl =""
					if r.client == none:
						lbl = "No connector"
					else:
						if r.client.instance == none:
							lbl = "Not initialized"
						else:
							if r.client.instance.connected == True:
								lbl = "connected"
							else:
								if r.connecting == True:
									lbl = "connecting"


					if i>0:
						l = l + ","
					l = l + "\""+r.name + "\": {\"name\":\"" + r.name + "\",\"model\":\"" + r.model + "\",\"status\":\""+lbl+"\",\"ip\":\""+r.ip+"\"}"
					i=i+1

		l = l + "}"
		#print l

		return l

	def get_config_list(self):

		l = "{"
		i = 0

		for filename in os.listdir(self.homedir+'/configs'):
			if i>0:
				l=l+","
			l=l+"\""+str(i)+"\":{\"name\":\""+filename+"\"}"
			i=i+1
		l = l + "}"				
		#print l

		return l

	def load_config(self,c):
		return "ok"
		# load ini file
		self.env = tereoEnvironment.environment()
		ret = self.env.load_env(c, self.homedir)

		if ret == False:
			del self.env
			self.env = None
			return

		self.guiwin = tereoGui.gui_window(self)
		self.guiwin.attach_toolbar(self.toolbar)
		self.update_gui()
		self.connect_robots()
		self.doafter_choice()


		return "ok"

	def get_calibration_list(self):

		l = "{"
		i = 0

		lcal = self.function_search_path("robot.calibration")
		if lcal != []:

			for it in lcal:

				if i>0:
					l=l+","

				l=l+"\""+it[0]+"\":{\"name\":\""+it[0]+"\"}"

				i=i+1
		l = l + "}"				
		#print l

		return l

	def get_robot_kinematic(self, rn):

		l = "{"
		i = 0

		if hasattr(self,"env") :
			if self.env != None:
				for r in self.env.robots:
					
					if r.name == rn:

						for lk in r.links:

							if i>0:
								l = l + ","
							l = l + "\""+str(i)+"\": {\"a\":\"" + str(lk.a) + "\",\"alpha\":\"" + str(lk.alpha) + "\",\"d\":\""+str(lk.d)+"\",\"theta\":\""+str(lk.theta)+"\",\"axislink\":\""+lk.axislink+"\",\"color\":{\"r\":\""+str(lk.color[0])+"\",\"g\":\""+str(lk.color[1])+"\",\"b\":\""+str(lk.color[2])+"\",\"a\":\""+str(lk.color[3])+"\"},\"axis\":{\"x\":\""+str(lk.axis[0])+"\",\"y\":\""+str(lk.axis[1])+"\",\"z\":\""+str(lk.axis[2])+"\"},\"tranlate\":{\"x\":\""+str(lk.translate[0])+"\",\"y\":\""+str(lk.translate[1])+"\",\"z\":\""+str(lk.translate[2])+"\"}}"
							i=i+1

		l = l + "}"
		#print l
	
		return l

	def get_robot_mesh_list(self,rn) :

		print "robot mesh list"
		sp = rn.replace(".","/")
		initfile = "./robots/" + sp + "/robot.mes"
		print "initfile:"+initfile
		meshxml = ET.parse(initfile)

		ll = "{"		

		i = 0

		for l in meshxml.findall("LINK"):
			print "found link"
			if (i>0):
				ll =ll+","
			
			ll = ll + "\""+str(i) + "\":{\"model\":\""+l.get("MODEL")+"\",\"constructor\":\""+l.get("CONSTRUCTOR")+"\",\"min\":\""+l.get("MIN")+"\",\"max\":\""+l.get("MAX")+"\",\"FILE\":\""+l.get("FILE")+"\"}"

			

			i=i+1

		ll = ll + "}"

		return ll
				

	def get_toolcalibration_list(self):

		l = "{"
		i = 0

		lcal = self.function_search_path("tool.calibration")
		if lcal != []:

			for it in lcal:

				if i>0:
					l=l+","

				l=l+"\""+it[0]+"\":{\"name\":\""+it[0]+"\"}"

				i=i+1
		l = l + "}"				
		print l

		return l


	def show_mainwin(self):

		self.mainwindow.show()

		return


	##############################
	##  common interface	    ##
	##############################
	def broadcast_gcode(self,gc):
		if self.env == None:
			return
		for r in self.env.robots:
			r.send_gcode(gc)

	def broadcast(self,com):
		#print "broadcast"
		if self.env == None:
			print "env none"
			return
		for r in self.env.robots:
			#r.execute_action("send",tereoRobot.robotAction(None,-1,-1,com))
			r.execute_action("send",com)

	def listentcp(self,obj,port):
		print "listen"
		reactor.listenTCP(port,obj)
		return

	def on_choose_material(self,widget,data=None):

		# show material choose dialog
		s = widget.get_name()
		self.baseChoice = int(s[-2:-1])
		print self.baseChoice
		dlg = self.builder.get_object("materialDlg")
		dlg.show()

		return

	def on_dlgmat_cancel(self,widget,data=None):

		dlg = self.builder.get_object("materialDlg")
		dlg.hide()

		return

	def on_dlgmat_ok(self,widget,data=None):

		dlg = self.builder.get_object("materialDlg")
		tv = self.builder.get_object("materialtv")

		model, iter = tv.get_selection().get_selected()
		s = model.get_value(iter,0)
		#print s

		self.baseMaterial[self.baseChoice-1] = s

		l = "mat" + str(self.baseChoice)
		lbl = self.builder.get_object(l)
		lbl.set_label(s)

		dlg.hide()

		return


	def applog(self,sec,ico,desc):

		lst = self.object_search_path("log.listener")

		tmp = ""
		self.app_log.append([tmp,ico,sec,desc])

		for ll in lst:
			ll[1](tmp,ico,sec,desc)


		return





	def on_gtk_quit_activate(parent, menuitem):
		#print "quit from menu"
		gtk.main_quit()
		return



	def add_icon(self, ico):

		if self.guiwin == None:
			return

		self.guiwin.add_icon(ico)

		return


	def add_function(self,func_path,name,command):

		self.functions.append([func_path,name,command])
		return


	def get_function(self,func_path,name):

		for i in self.functions:
			if (i[0] == func_path) and (i[1] == name):
				return i[2]
		return None

	def execute_function(self,func_path,name,args=None):
		print "exec function"+func_path
		for i in self.functions:
			if (i[0] == func_path) and (i[1] == name):
				print "found"
				i[2](self,args)
				return
		print "not found"
		return None

	def add_object(self,obj_path,name,command):

		self.objects.append([obj_path,name,command])
		return

	def remove_object(self,obj_path,obj):

		for i in self.objects:
			if (i[0] == obj_path) and (i[2] == obj):
				i = None
				return
		return

	def get_object(self,obj_path,name):

		for i in self.objects:
			if (i[0] == obj_path) and (i[1] == name):
				return i[2]

		return None


	def attach_panel(self,frame):

		if self.guiwin == None:
			return

		self.guiwin.attach_panel(frame)
		return

	def detach_panel(self,frame):

		if self.guiwin == None:
			return
		self.guiwin.remove_panel(frame)

		return


	def get_property(self,section,param):

		value = None
		if hasattr(self.db,"getroot"):
			db = self.db.getroot()
		else:
			db = self.db

		s = db.find(section)
		if s != None:
			value = s.get(param)
		if value == None: return 'None'
		return value

	def set_property(self,section,param,value):

		if hasattr(self.db,"getroot"):
			db = self.db.getroot()
		else:
			db = self.db

		s = db.find(section)
		if s == None:
			s = ET.SubElement(db,section)
		s.set(param,value)
		return

	def update_gui(self):

		self.get_robot_kinematic("ROBOT ONE");

		#print "update gui"

		if self.env == None:
			return

		s = self.env.pdb.find('GUI')
		if s == None:
			return

		div = s.get('division')
		v1 = s.get('view1')
		v2 = s.get('view2')
		v3 = s.get('view3')
		v4 = s.get('view4')

		if (div == None) or (v1 == None) or (v2 == None) or (v3 == None) or (v4 == None):
			return

		div = int(div)
		self.guiwin.remove_children()


		#--------- fullscreen--------------
		if div == 1:
			print "full screen"
			self.guiwin.set_homogeneous(False)
			if v1 == 'None':
				print "v1 none"
				return
			print "get object : "+(v1)
			f1 = self.get_object('gui.frame',v1)
			f = f1(self)
			print "attach"
			self.guiwin.attach_frame(f,0,1,0,1)

		elif div == 2:
			self.guiwin.set_homogeneous(False)
			if (v1 == 'None') or (v2 == 'None'):
				return
			f1 = self.get_object('gui.frame',v1)
			f = f1(self)
			self.guiwin.attach_frame(f,0,1,0,1)

			f2 = self.get_object('gui.frame',v2)
			f = f2(self)
			self.guiwin.attach_frame(f,1,2,0,1)


		elif div == 3:
			self.guiwin.set_homogeneous(False)
			if (v1 == 'None') or (v2 == 'None'):
				return
			f1 = self.get_object('gui.frame',v1)
			self.guiwin.attach_frame(f1(self),0,1,0,1)
			f2 = self.get_object('gui.frame',v2)
			self.guiwin.attach_frame(f2(self),0,1,1,2)

		else:
			self.guiwin.set_homogeneous(True)
			if (v1 != 'None'):
				f1 = self.get_object('gui.frame',v1)
				f=f1(self)
				self.guiwin.attach_frame(f,0,1,0,1)

 			if (v2 != 'None'):
				f2 = self.get_object('gui.frame',v2)
				f=f2(self)
				self.guiwin.attach_frame(f,1,2,0,1)

			if (v3 != 'None'):
				f3 = self.get_object('gui.frame',v3)
				f=f3(self)
				self.guiwin.attach_frame(f,0,1,1,2)

			if (v4 != 'None'):
				f4 = self.get_object('gui.frame',v4)
				f=f4(self)
				self.guiwin.attach_frame(f,1,2,1,2)


		return


	def load_plugins(self):

		print "load plugins"
		plugins = []
		possibleplugins = os.listdir(pluginpath)

		for i in possibleplugins:
			
			try:


				location = os.path.join(pluginpath, i)
				#print location
				if not os.path.isdir(location) or not MainModule + ".py" in os.listdir(location):
					continue
				info = imp.find_module(MainModule, [location])
				plugins.append({"name": i, "info": info})

			except Exception, e:
				print e


		for i in plugins:

			try:
				# run it
				splScr.label.set_label("Loading plugin : " + i["name"])
				gtk.main_iteration()
				pi = imp.load_module(MainModule, *i["info"])
				pi.register(self)

			except Exception, e:
				print e

		print "end load plugins"
		return


	def check_network(self):

		print "check network"

		if hasattr(self,"env") == False:
			#print "no env"
			return True

		if self.env == None:
			#print "env null"
			return True

		for r in self.env.robots:
			#print "proto : " + r.proto
			if r.proto == "cross":
				#print "check cross"
				if r.client != None:
					if r.ip != "" and r.chan > 0:
						#print "ip chan ok"
						if r.client.connected == False:
							#print "not connected"
							if r.client.connecting == False:
								r.client.connecting = True
								self.applog("network",0,"not connected retrying...")
								#r.client.connectTCP(r.ip,r.chan, r.client)
								r.client.initialize()
			if r.client == None:
				
				if (r.ip != "") and (r.chan != 0):
					print "proto : " + r.proto
					if r.proto == "Linuxcnc":
						self.applog("network",0,"init linuxcnc network")
						r.client = linuxcnc_controller.linuxcnc_controller(self)
						
						#reactor.connectTCP(r.ip,r.chan, r.client)
					elif r.proto == "cross":
						self.applog("network",0,"init cross network")
						r.client = rpclientsaa.rpcClient(r.ip)
						if r.calc_ip != "0.0.0.0" and r.calc_chan > 0:
							self.applog("network",0,"init calculator")
							r.calculateur = tereoClient(True, r.calculate)
							reactor.connectTCP(r.calc_ip,r.calc_chan, r.calculateur)
					#r.client.start()
					elif r.proto == "Simulation":
						self.applog("network",0,"init simulator network")
						#todo : activate
						#r.client = tereoRobot.clientSimu()
				else:
					self.applog("network",1,"No ip adress or channel")


			


		# update network
		#netwin = self.builder.get_object("winNet")
		#for w in gtk.window_list_toplevels():
		#	if w == netwin:
		#		self.populate_network()
		#		return w

		return True




	def load_settings(self):

		try:
			self.db = ET.parse('settings.xml')
		except Exception, e:
			self.db = ET.Element('TEREO')

		return

	def save_settings(self):

		if hasattr(self.db,"write"):
			self.db.write('settings.xml')
		else:
			tree = ET.ElementTree(self.db)
			tree.write('settings.xml')

		return


	def add_icon(self,ico):

		self.toolbar.insert(ico,-1)
		ico.show()
		return


	##########
	## init ##
	##########
	def __init__(self):

		print "main app"

		if sys.platform == 'win32':
			self.homedir = os.path.expanduser('~') + '\\mes documents\\tereocnc'
		else:
			self.homedir = os.path.expanduser('~') + '/tereocnc'

		self.env = None
		self.guiwin = None
		self.process_list = None
		#self.tmprobot = None

		self.app_log = []

		# calibration variables
		self.calib_title = ""
		self.calib_nb_question = 0
		self.calib_questions = []
		self.calib_robot_vars = []
		self.calib_nb_img = 0
		self.calib_imgs = []
		self.calib_handler = None
		self.calib_index= 0
		self.calib_tag = -1
		self.calib_robot = -1
		self.calib_ret = None

		self.selectedconfig = ''


		self.functions = []
		self.objects = []

		self.do_work = False
		gcode = []
		self.gcode_stack = collections.deque(gcode)

		self.show_move = True
		self.norecurse = False
		self.workStack = []
		self.matStack = []
		self.baseChoice = 1
		self.baseMaterial = ["","","","","","","",""]

		##self.jogangles = {'X':0.0,'Y':0.0,'Z':0.0,'A':0.0,'B':0.0,'C':0.0,'U':0.0,'V':0.0,'W':0.0}


		self.robotsmodelwinrethandler = None


		self.tmprobot = None
		self.currentRobot = 0

		#--- toolbar ---
		self.toolbar = gtk.Toolbar()
		self.toolbar.show()
		self.toolbar.set_style(gtk.TOOLBAR_BOTH)
		self.toolbar.set_icon_size(4) #gtk.ICON_SIZE_MENU)
		# <property name="icon_size_set">True</property>


		self.load_settings()
		self.load_plugins()

		try:
			self.mainwindow = tereoMain.main_window(self)
		except Exception, e:
			print "error main window"

		

		gobject.timeout_add(1000, self.update)
		gobject.timeout_add(1000, self.check_error)
		# todo activate:
		#gobject.timeout_add(200, self.check_job)
		gobject.timeout_add(10000, self.check_network)

		self.tmpdb = None
		#self.db = tereoDB.db()


		return

	def doafter_choice(self):
		print "loading linuxcnc"
		self.execute_function("command.inout.start","Command inout start",self)
		log = open("/var/log/linuxcnc.log",'a')
	
		self.lcnc_proc = subprocess.Popen('linuxcnc /home/tc/tereocnc/configs/example/example.ini',stdout=log,stderr=log,shell=True)
		return

	def dobefore_quit(self):
		print "kill linuxcnc"
		#if self.lcnc_proc != None:
		#	self.lcnc_proc.terminate()

	def connect_robots(self):
		return
		#get_robot_kinematic("ROBOT ONE");

		if hasattr(self,"env") == False:
			print "no env"
			return 
		if self.env == None:
			print "env null"
			return 
		if hasattr(self.env,"robots") == False:
			print "no robots"
			return
		if self.env.robots == None:
			return

		print "connect robots"
		#todo !!!!!!!!!!!!!!!!!!!!!!!
		for r in self.env.robots:

			if (r.ip != "") and (r.chan != 0):
				#print "proto : " + r.proto
				if r.proto == "Linuxcnc":
					self.applog("network",0,"init linuxcnc network")
					r.client = tereoClient(False, None)
					reactor.connectTCP(r.ip,r.chan, r.client)
				elif r.proto == "cross":
					self.applog("network",0,"init cross network")
					r.client = rpclientsaa.rpcClient(r.ip)
					if r.calc_ip != "0.0.0.0" and r.calc_chan > 0:
						self.applog("network",0,"init calculator")
						r.calculateur = tereoClient(True, r.calculate)
						reactor.connectTCP(r.calc_ip,r.calc_chan, r.calculateur)
					#r.client.start()
				elif r.proto == "Simulation":
					self.applog("network",0,"init simulator network")
					#todo : activate
					#r.client = tereoRobot.clientSimu()
			else:
				self.applog("network",1,"No ip adress or channel")


		return

	def update(self):

		if hasattr(self,"env") == False:
			return True

		if self.env == None:
			return True

		for r in self.env.robots:
			if r.joggeur == None:
				r.set_dummy_comp()
			if r.calculateur == None:
				if r.client != None:
					if r.client.instance != None:
						
						myserver = self.get_object("Command.inout","tcpclient")
						if myserver != None:
							if myserver.s != None:
								myserver.s.sendMessage("@rc@broadcast_position@arg@{\"position\":{\"name\":\""+r.name+"\",\"X\":\""+str(r.client.instance.joint_position['X'])+"\",\"Y\":\""+str(r.client.instance.joint_position['Y'])+"\",\"Z\":\""+str(r.client.instance.joint_position['Z'])+"\",\"A\":\""+str(r.client.instance.joint_position['A'])+"\",\"B\":\""+str(r.client.instance.joint_position['B'])+"\",\"C\":\""+str(r.client.instance.joint_position['C'])+"\"}}")

						#print "--------------------------"
						#print "position"
						#print r.client.instance.position
						#print "axis"
						#print r.client.instance.joint_position
						r.joggeur['X'] = r.client.instance.joint_position['X']
						r.joggeur['Y'] = r.client.instance.joint_position['Y']+90
						r.joggeur['Z'] = r.client.instance.joint_position['Z']
						r.joggeur['A'] = r.client.instance.joint_position['A']
						r.joggeur['B'] = r.client.instance.joint_position['B']
						r.joggeur['C'] = r.client.instance.joint_position['C']
						r.joggeur['U'] = r.client.instance.joint_position['U']
						r.joggeur['V'] = r.client.instance.joint_position['V']
						r.joggeur['W'] = r.client.instance.joint_position['W']
						#print "joggeur"
						#print r.joggeur
			else:
				if r.calculateur.instance != None:
					#print "--------------------------"
					#print r.client.instance.position
					r.joggeur['X'] = r.calculateur.instance.joint_position['X']
					r.joggeur['Y'] = r.calculateur.instance.joint_position['Y']+90
					r.joggeur['Z'] = r.calculateur.instance.joint_position['Z']
					r.joggeur['A'] = r.calculateur.instance.joint_position['A']
					r.joggeur['B'] = r.calculateur.instance.joint_position['B']
					r.joggeur['C'] = r.calculateur.instance.joint_position['C']
					r.joggeur['U'] = r.calculateur.instance.joint_position['U']
					r.joggeur['V'] = r.calculateur.instance.joint_position['V']
					r.joggeur['W'] = r.calculateur.instance.joint_position['W']


		# get robots axis coords
		#print "broadcast"
		if self.show_move:
			self.broadcast("stat joint_position")
		#self.broadcast("stat position")
		#print "exit broadcast"
		return True




#class serverFactory(Factory):
#	def __init__(self):
#		print "serverfactory init"
#		#self.users = {} # maps user names to Chat instances
#		return

#	def buildProtocol(self, addr):
#		print "buildprotocol"
#		return tereoNet.server(c,s,e)





def gtk_iteration(*args, **kw):
    # We handle the gtk events in this task added to Panda TaskManager
    while gtk.events_pending():
        gtk.main_iteration_do(False)
    return Task.cont




if __name__ == "__main__":
	#print "running"
	splScr = tereoUtil.splashScreen('splash.png')
	while gtk.events_pending():
		gtk.main_iteration()
	tereo()
	splScr.window.destroy()
	#taskMgr.add(gtk_iteration, "gtk")
	#LoopingCall(taskMgr.step).start(1 / 10)
	reactor.run()




