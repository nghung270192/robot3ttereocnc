# -*- coding: utf-8 -*-


import os.path
import gobject
import collections
import tereoController
from twisted.protocols.basic import LineReceiver
from twisted.internet.protocol import Protocol,ReconnectingClientFactory
from twisted.internet import reactor

class linuxcnc_controller(tereoController.controller):

	def __init__(self,tereo):

		self.instance = None 
		print "linux controller init2"
		tereoController.controller.__init__(self,tereo)

		self.tereo = tereo
		self.LINUXCNC_STATE_ESTOP = 1
		self.LINUXCNC_STATE_ON = 4
		self.LINUXCNC_STATE_OFF = 3
		self.LINUXCNC_STATE_ESTOP_RESET = 2

		self.LINUXCNC_JOG_STOP = 0
		self.LINUXCNC_JOG_CONTINUOUS = 1
		self.LINUXCNC_JOG_INCREMENT = 2


		self.client = tereoClient(self);
		reactor.connectTCP("127.0.0.1", 8123,tereoClient(self))

		#comm = []
		#self.comm_stack = collections.deque(comm)
		#self.waiting_response = False
		#self.last_comm = ""
		#self.last_response = ""
		#self.last_cnc_error = ""
		#self.position = {'X':0, 'Y':0, 'Z':0, 'A':0, 'B':0, 'C':0, 'U':0, 'V':0, 'W':0}
		self.joint_position = {'X':0, 'Y':0, 'Z':0, 'A':0, 'B':0, 'C':0, 'U':0, 'V':0, 'W':0}
		#self.waiting_job = True
		#self.speed = 5
		#self.position_updated = True


	def set_estop(self,es):
		print "fire estop"
		if es == True:
			self.tereo.applog("Linuxcnc",2,"Emergency stop!")
			self.instance.send("command state %d"%(self.LINUXCNC_STATE_ESTOP))
		else:
			self.tereo.applog("Linuxcnc",1,"Emergency stop reset")
			self.instance.send("command state %d"%(self.LINUXCNC_STATE_ESTOP_RESET))
		tereoController.controller.estop(self,es)
		return

	def stop_motion(self):
		#send to robot
		tereoController.controller.stop_motion(self)
		return

	def set_power(self,p):
		if p == True:
			self.tereo.applog("Linuxcnc",0,"Powering up")
			self.instance.send("command state %d"%(self.LINUXCNC_STATE_ON))
		else:
			self.tereo.applog("Linuxcnc",0,"Powering down")
		        self.instance.send("command state %d"%(self.LINUXCNC_STATE_OFF))
		tereoController.controller.power(self,p)
		return

	def set_new_path(self,points):
		#todo : if current path not complete : stop and start new
		#todo : if power off  : power on
		tereoController.controller.set_new_path(self)
		return


	def get_current_position(self):
		#send request to robot
		tereoController.controller.get_current_position(self)
		self.instance.send("stat joint_position")
		return


	def get_target_position(self):
		#send request to robot
		tereoController.controller.get_target_position(self)
		return

	def get_state_error(self):
		#send request to robot
		tereoController.controller.get_state_error(self)
		return

	def get_state_mode(self):
		#send request to robot
		tereoController.controller.get_state_mode(self)
		return

	def get_state_estop(self):
		#send request to robot
		tereoController.controller.get_state_estop(self)
		return

	def get_state_power(self):
		#send request to robot
		tereoController.controller.get_state_power(self)
		return


	def set_home(self):
		self.instance.send("command home -1")
		return

	def moveto (self,x,y,z):
		self.instance.send("command mode 3")
		self.instance.send("command mdi G21")
		self.instance.send("command mdi G91")
		cmd = "command mdi G00X"+str(x)+"Y"+str(y)+"Z"+str(z)
		#print cmd
		self.instance.send(cmd)
		return

	#---------- linuxcnc ------------------------

	def send(self,com):
		self.instance.send(com)
		self.position_updated = True
		return

	def jog(self,tp,ax,vel,dst):
		#0 = stop
		#1 = cont
		#2 = incr
		if dst == "":
			self.send("command jog "+tp+" "+ax+" "+vel)
		else:
			self.send("command jog "+tp+" "+ax+" "+vel+" "+dst)
		return

	def new_network_error(self,e):
		self.network_error = e
		#tereoController.controller.update_network_status()
		return

	def update(self):
		#print "update"
		#print self.instance.joint_position
		self.instance.send("stat joint_position")		
		self.instance.send("stat position")
		return True


	def get_status(self):
		self.instance.send("stat exec_state")
		return True

	def connection_made(self):
		print "connection made"
		self.network_error = ""
		self.network_connected = True
		self.network_connecting = False
		#tereoController.controller.update_network_status()
		gobject.timeout_add(500,self.update)
		gobject.timeout_add(3000,self.get_status)
		return




#-------------- client linuxcnc --------------------


class tereoProtocol(LineReceiver):

	#def dataReceived(self,data):
	#	print "lcnc_ctrl:"+data

	def __init__(self,ctrl):
		print "init tereoprotocol"
		self.ctrl = ctrl
		ctrl.instance = self
		self.connected = False
		self.connecting = False
		self.lasterror = ""
		comm = []
		self.comm_stack = collections.deque(comm)
		self.waiting_response = False
		self.last_comm = ""
		self.last_response = ""
		self.last_cnc_error = ""
		self.position = {'X':0, 'Y':0, 'Z':0, 'A':0, 'B':0, 'C':0, 'U':0, 'V':0, 'W':0}
		self.joint_position = {'X':0, 'Y':0, 'Z':0, 'A':0, 'B':0, 'C':0, 'U':0, 'V':0, 'W':0}
		self.waiting_job = False
		self.position_updated = True
		self.speed = 0
		self.calculateur = False
		self.ask_result = False
		self.calculateur_function = None

	def send(self,com):
		#print "send:"+com

		if (com == "stat position") or (com == "stat joint_position"):
			self.sendLine(com)
			return

		print "send:"+com

		self.sendLine(com)

		if self.waiting_response == True:
			self.comm_stack.append(com)
		else:
			self.ask_result = True
			#print "ask result true"
			self.sendLine(com)
			self.last_comm = com

	def dataReceived(self, line):
		#print "line received : " + line


		

		line = line.replace("\r","@")
		line = line.replace("\n","@")
		line = line.replace("@@","@")

		lines = line.split('@')

		for line in lines:

			#print "new line:"+line
			if line == "nextjob":

				#print "next job from lcnc"
				if self.calculateur == True:
					if self.ask_result == True:
						#print "send stat position"
						self.ask_result = False
						self.send("stat joint_position")
				else:
					#print "line recevied wj"
					self.waiting_job = True
			else:

				line.strip(' []')
	
				word_list2 = line.split(' ')
				if word_list2[0] == "error":
					t = line.split(' ',1)
					self.last_cnc_error = t[1]
				else:


					#print word_list2
					if len(word_list2) < 2:
						self.last_cnc_error = "unknown response:"+line
					else:
						#print "2"
						#print word_list2
						#print "-2"
						t = word_list2[2].split(':')
						#print "t"
						#print t
						#print "end t"
						#word_list2[1] = t[0]
						#if len(t)>1:
						# 	word_list2.insert(2,t[1])
						#print "3"
						#print word_list2
						#print "end 3"
						if (word_list2[0] == "stat") and (word_list2[1] == 'position'):
							#print t
				
							self.position['X'] = float(t[0])*25.4
							self.position['Y'] = float(t[1])*25.4
							self.position['Z'] = float(t[2])*25.4
							self.position['A'] = float(t[3])*25.4
							self.position['B'] = float(t[4])*25.4
							self.position['C'] = float(t[5])*25.4
							self.position['U'] = float(t[6])*25.4
							self.position['V'] = float(t[7])*25.4
							self.position['W'] = float(t[8])*25.4
							self.position_updated = True
							#print self.position
							
		
						elif (word_list2[0] == "stat") and (word_list2[1] == 'joint_position'):
							#print "joint pos"
							#print word_list2
							#print float(word_list2[2])+"\r\n"
							#print float(word_list2[3])+"\r\n"
							self.joint_position['X'] = float(t[0])
							self.joint_position['Y'] = float(t[1])+90
							self.joint_position['Z'] = float(t[2])
							self.joint_position['A'] = float(t[3])
							self.joint_position['B'] = float(t[4])
							self.joint_position['C'] = float(t[5])
							self.joint_position['U'] = float(t[6])
							self.joint_position['V'] = float(t[7])
							self.joint_position['W'] = float(t[8])
							self.position_updated = True
							#print self.joint_position
							if self.calculateur == True:

								if self.calculateur_function != None:
									self.calculateur_function()
									#print "waiting job after result"
									self.waiting_job = True
							
			
						elif self.last_comm == "":
							self.last_cnc_error = "unexpected response : " + line
						else:
	
							word_list1 = self.last_comm.split(' ')
							#print word_list1
							t = word_list1[1].split(':')
							word_list1[1] = t[0]
	 						if len(t)>1:
							 	word_list2.insert(2,t[1])
				 			#print word_list1
	
							if (word_list1[0] == word_list2[0]) and (word_list2[1] == word_list1[1]):
								#print "comm ok"
								self.last_response = line
								if len(self.comm_stack) > 0:
									self.last_comm = self.comm_stack.popleft()
									self.sendLine(self.last_comm)
	
	
	def connectionMade(self):
		self.ctrl.connection_made()
		return

	def connectionLost(self, reason):
		self.ctrl.new_network_error("Lost connection.  Reason:" + str(reason))
		return



#----------- basic network client --------------



class tereoClient(ReconnectingClientFactory):
	#protocol = tereoProtocol

	def __init__(self, ctrl):
		self.ctrl = ctrl
		self.instance = None

	def buildProtocol(self, addr):
		print "buid protocol"
		self.instance = tereoProtocol(self.ctrl)
		self.resetDelay()
		return self.instance

	def clientConnectionLost(self, connector, reason):
		print "cnx lost"
		self.ctrl.new_network_error("Lost connection.  Reason:" + str(reason))
		ReconnectingClientFactory.clientConnectionLost(self, connector, reason)

	def clientConnectionFailed(self, connector, reason):
		print "cnx failed"
		self.ctrl.new_network_error("Connection failed. Reason:" + str(reason))
		ReconnectingClientFactory.clientConnectionFailed(self, connector, reason)












def new_linuxcnc_controller(tereo):

	return linuxcnc_controller(tereo)












