# -*- coding: utf-8 -*-


	####################################
	######### flow   window ############
	######### pickup        ############
	####################################


	def flowwin_init(self):
		self.tmpdb = tereoDB.db()
		self.tempflow = copy.deepcopy(self.env.flow)

		# show flow mounted
		lv = self.builder.get_object("sceneflowlv")
		for fl in self.tempflow:
			lv.append((None,fl.name))

		da = self.builder.get_object("flowposition")
		da.connect ('expose-event', self.on_flow_draw)

		self.selectedflowname=''
		self.selectedflowgroup=''


		flowlv = self.builder.get_object("flowtv")
		flowlv.clear()
		for groupname in os.listdir(homedir+'/flow'):
			group = flowlv.append(None,(None,groupname))
			for flowname in os.listdir(homedir+'/flow/' + groupname):
				pixbuf = gtk.gdk.pixbuf_new_from_file(homedir + '/flow/' + groupname + '/' + flowname + '/image.png')
				if pixbuf.get_width() > 80:
					f = 80.0/pixbuf.get_width()
					scaled_buf = pixbuf.scale_simple(80,pixbuf.get_height()*f,gtk.gdk.INTERP_BILINEAR)
					cell = flowlv.append(group,(scaled_buf,flowname))
					del scaled_buf
				else:
					cell = flowlv.append(group,(pixbuf,flowname))
				del pixbuf
				# show image
		return

	def on_flowwin_destroy(self,widget):
		# ask save changes
		return

	def on_flowwin_cancel(self,widget):
		self.flowwin.destroy()
		return

	def on_flowwin_ok(self,widget):
		#todo save
		if self.env.flow:
			del self.env.flow
		self.env.flow = copy.deepcopy(self.tempflow)

		#if self.scenevismach:
		#	m = AsciiOBJ(homedir + '/flow/' + self.selectedflowgroup + '/' + self.selectedflowname + '/flow.obj')
		#	m = Color([0.18,0.19,0.2,1],[m])
		#	self.scenevismach.addmodel(m)
		self.flowwin.destroy()
		return

	def on_flowwin_add(self,widget):
		if self.selectedflowname==''or self.selectedflowgroup=='':
			return
		lv = self.builder.get_object("sceneflowlv")
		lv.append((None,self.selectedflowname))
		fl = tereoFlow.flowObj()
		fl.name = self.selectedflowname
		fl.group = self.selectedflowgroup
		fl.homedir = homedir + '/flow'

		# save params
		fl.position_z = float(self.builder.get_object("flowa").get_text())
		fl.position_x = float(self.builder.get_object("flowb").get_text())
		fl.bounding_length = float(self.builder.get_object("boundinglength").get_text())
		fl.bounding_width = float(self.builder.get_object("boundingwidth").get_text())
		fl.bounding_height = float(self.builder.get_object("boundingheight").get_text())

		sens = -1
		fl.bidirectional = 'NO'
		if self.builder.get_object("posflow").get_active() == True:
			sens = 1
			if self.builder.get_object("negflow").get_active() == True:
				fl.bidirectional = 'YES'

		if self.builder.get_object("flowdirectionx").get_active() == True:
			fl.flow_direction_x = sens
		elif self.builder.get_object("flowdirectiony").get_active() == True:
			fl.flow_direction_y = sens
		elif self.builder.get_object("flowdirectionz").get_active() == True:
			fl.flow_direction_z = sens

		if self.builder.get_object("flowpos1").get_active() == True:
			fl.flow_origin = 0
		elif self.builder.get_object("flowpos2").get_active() == True:
			fl.flow_origin = 1
		elif self.builder.get_object("flowpos3").get_active() == True:
			fl.flow_origin = 2
		elif self.builder.get_object("flowpos4").get_active() == True:
			fl.flow_origin = 3

		# inch, in
		if self.builder.get_object("flowunityinch").get_active() == True:
			fl.unity = "inch"
		else:
			fl.unity = 'mm'

		self.tempflow.append(fl)
		return

	def on_flowwin_remove(self,widget):
		return

	def on_flowwin_select_stock(self,widget):
		model, iter = widget.get_selection().get_selected()
		if not iter:
			return
		name = model.get_value(iter,1)
		group = model.get_value(model.iter_parent(iter),1)

		self.selectedflowname = name
		self.selectedflowgroup = group


		tf = tereoFlow.flowObj()
		tf.name = name
		tf.group = group
		tf.homedir = homedir + '/flow'
		tf.get_from_file()

		self.builder.get_object("flowa").set_text(str(1000))
		self.builder.get_object("flowb").set_text(str(1000))

		self.builder.get_object("boundinglength").set_text(str(tf.bounding_length))
		self.builder.get_object("boundingwidth").set_text(str(tf.bounding_width))
		self.builder.get_object("boundingheight").set_text(str(tf.bounding_height))

		if abs(tf.flow_direction_x) > 0:
			self.builder.get_object("flowdirectionx").set_active(True)
		elif abs(tf.flow_direction_y) > 0:
			self.builder.get_object("flowdirectiony").set_active(True)
		elif abs(tf.flow_direction_z) > 0:
			self.builder.get_object("flowdirectionz").set_active(True)

		self.builder.get_object("flowpos%d"%(tf.flow_origin)).set_active(True)


		if tf.bidirectional == 'YES':
			self.builder.get_object("posflow").set_active(True)
			self.builder.get_object("negflow").set_active(True)
		else:
			if (tf.flow_direction_x + tf.flow_direction_y + tf.flow_direction_z) > 0:
				self.builder.get_object("posflow").set_active(True)
				self.builder.get_object("negflow").set_active(False)
			else:
				self.builder.get_object("negflow").set_active(True)
				self.builder.get_object("posflow").set_active(False)

			# inch, in
			if tf.unity == "inch":
				self.builder.get_object("flowunityinch").set_active(True)
			else:
				self.builder.get_object("flowunitymm").set_active(True)

		del tf

		self.flowwin_draw(None)

		return

	def on_flow_draw(self,area, _):

		if self.tempflow:
			self.flowwin_draw(None)
		return

	def flowwin_draw(self,widget):

		area = self.builder.get_object("flowposition")
		drawable = area.window
		style = area.get_style()
		cg = style.fg_gc[gtk.STATE_NORMAL]
		fnt = gtk.gdk.Font("-*-helvetica-bold-r-normal--*-120-*-*-*-*-*")

		dsize = drawable.get_size()
		area_w = dsize[0]
		area_h = dsize[1]

		cg.set_rgb_fg_color(gtk.gdk.Color(65535,65535,65535))
		cg.set_rgb_bg_color(gtk.gdk.Color(65535,65535,65535))
		drawable.draw_rectangle (cg, True, 0,0, area_w, area_h)

		cg.set_rgb_fg_color(gtk.gdk.Color(0,0,0))



		# calculate scale
		minx = 0.0
		maxx = 0.0
		minz = 0.0
		maxz = 0.0
		for fo in self.env.flow:
			bl = fo.bounding_length
			bw = fo.bounding_width
			px = fo.position_x
			pz = fo.position_z
			if fo.unity == 'mm':
				bl = bl * 0.03937
				bw = bw * 0.03937
				px = px * 0.03937
				pz = pz * 0.03937

			if pz > 0:
				if pz < minz: minz = pz
				if (pz + bw) > maxz : maxz = pz + bw
			else:
				if (pz - bw) < minz: minz = pz - bw
				if pz > maxz : maxz = pz
			if px > 0:
				if px < minx: minx = px
				if (px + bl) > maxx : maxx = px + bl
			else:
				if (px - bl) < minx: minx = px - bl
				if px > maxx : maxx = px

		#current flow object
		if self.selectedflowname != '' and self.selectedflowgroup != '':
			a = float(self.builder.get_object("flowa").get_text())
			b = float(self.builder.get_object("flowb").get_text())
			l = float(self.builder.get_object("boundinglength").get_text())
			w = float(self.builder.get_object("boundingwidth").get_text())
			if self.builder.get_object("flowunitymm").get_active() == True:
				l = l * 0.03937
				w = w * 0.03937
				a = a * 0.03937
				b = b * 0.03937

			if a > 0:
				if a < minz: minz = a
				if (a + w) > maxz : maxz = a + w
			else:
				if (a - w) < minz: minz = a - w
				if a > maxz : maxz = a
			if b > 0:
				if b < minx: minx = b
				if (b + l) > maxx : maxx = b + l
			else:
				if (b - l) < minx: minx = b - l
				if b > maxx : maxx = b


		# robot bounding
		if minx > 0 : minx = 0
		if minz > 0 : minz = 0
		if maxx < 0 : maxx = 0
		if maxz < 0 : maxz = 0

		minx = minx
		maxx = maxx
		minz = minz
		maxz = maxz


		scale1 = (area_w-20)/(maxx-minx)
		scale2 = (area_h-20)/(maxz-minz)

		if scale1<scale2:
			scale=scale1
		else:
			scale=scale2


		# (0,0) coords
		cw = (-minx * scale)+10
		ch = (-minz * scale)+10

		#print "minx:%f"%(minx)
		#print "maxx:%f"%(maxx)
		#print "minz:%f"%(minz)
		#print "maxz:%f"%(maxz)
		#print "cw:%f"%(cw)
		#print "ch:%f"%(ch)
		#print "scale:%f"%(scale)
		#print "a:%f"%(a)
		#print "b:%f"%(b)

		# robot position (center)
		s = 10
		drawable.draw_line( cg, int(cw - s), int(ch), int(cw + s), int(ch) )
		drawable.draw_line( cg, int(cw), int(ch - s), int(cw), int(ch + s) )

		# scene flow
		for fo in self.env.flow:
			bl = fo.bounding_length
			bw = fo.bounding_width
			px = fo.position_x
			pz = fo.position_z
			if fo.unity == 'mm':
				bl = bl * 0.03937
				bw = bw * 0.03937
				px = px * 0.03937
				pz = pz * 0.03937
			if pz > 0:
				if px > 0:
					drawable.draw_rectangle (cg, False, int(cw + px*scale), int(ch + pz*scale), int(bl*scale), int(bw*scale) )
				else:
					drawable.draw_rectangle (cg, False, int(cw + px*scale - bl*scale), int(ch + pz*scale), int(bl*scale), int(bw*scale) )
			else:
				if px > 0:
					drawable.draw_rectangle (cg, False, int(cw + px*scale), int(ch + pz*scale - bw*scale), int(bl*scale), int(bw*scale) )
				else:
					drawable.draw_rectangle (cg, False, int(cw + px*scale - bl*scale), int(ch + pz*scale - bw*scale), int(bl*scale), int(bw*scale) )

		if self.selectedflowname == '' or self.selectedflowgroup == '': return

		# current bounding box
		cg.set_rgb_fg_color(gtk.gdk.Color(0,0,65535))
		if a > 0:
			if b > 0:
				drawable.draw_rectangle (cg, False, int(cw + b*scale), int(ch + a*scale), int(l*scale), int(w*scale) )
			else:
				drawable.draw_rectangle (cg, False, int(cw + b*scale - l*scale), int(ch + a*scale), int(l*scale), int(w*scale) )
		else:
			if b > 0:
				drawable.draw_rectangle (cg, False, int(cw + b*scale), int(ch + a*scale - w*scale), int(l*scale), int(w*scale) )
			else:
				drawable.draw_rectangle (cg, False, int(cw + b*scale - l*scale), int(ch + a*scale - w*scale), int(l*scale), int(w*scale) )

		cg.set_rgb_fg_color(gtk.gdk.Color(0,0,0))

		# dashed lines
		cg.set_dashes(0, (4,4))

		## A ##
		cg.set_line_attributes(1,gtk.gdk.LINE_DOUBLE_DASH, gtk.gdk.CAP_BUTT, gtk.gdk.JOIN_MITER)
		if a > 0:
			drawable.draw_line( cg, int(cw + b*scale), int(ch + a*scale), int(cw + b*scale), int(ch) )
			drawable.draw_text( fnt , cg, int(cw + b*scale + 2), int(ch + a*scale/2), "A")
			cg.set_line_attributes(1,gtk.gdk.LINE_SOLID, gtk.gdk.CAP_BUTT, gtk.gdk.JOIN_MITER)
			drawable.draw_polygon(cg, True, [(int(cw + b*scale), int(ch + a*scale)),(int(cw+b*scale-3),int(ch+a*scale-5)),(int(cw+b*scale+3),int(ch+a*scale-5))])
			drawable.draw_polygon(cg, True, [(int(cw + b*scale), int(ch)),(int(cw+b*scale-3),int(ch+5)),(int(cw+b*scale+3),int(ch+5))])
		else:
			drawable.draw_line( cg, int(cw + b*scale), int(ch + a*scale), int(cw + b*scale), int(ch) )
			drawable.draw_text( fnt , cg, int(cw + b*scale + 2), int(ch + a*scale/2), "A")
			cg.set_line_attributes(1,gtk.gdk.LINE_SOLID, gtk.gdk.CAP_BUTT, gtk.gdk.JOIN_MITER)
			drawable.draw_polygon(cg, True, [(int(cw + b*scale), int(ch + a*scale)),(int(cw+b*scale-3),int(ch+a*scale+5)),(int(cw+b*scale+3),int(ch+a*scale+5))])
			drawable.draw_polygon(cg, True, [(int(cw + b*scale), int(ch)),(int(cw+b*scale-3),int(ch-5)),(int(cw+b*scale+3),int(ch-5))])

		## B ##
		cg.set_line_attributes(1,gtk.gdk.LINE_DOUBLE_DASH, gtk.gdk.CAP_BUTT, gtk.gdk.JOIN_MITER)
		if b > 0:
			drawable.draw_line( cg, int(cw + b*scale), int(ch + a*scale), int(cw), int(ch + a*scale) )
			drawable.draw_text( fnt , cg, int(cw + b*scale/2), int(ch + a*scale + 2), "B")
			cg.set_line_attributes(1,gtk.gdk.LINE_SOLID, gtk.gdk.CAP_BUTT, gtk.gdk.JOIN_MITER)
			drawable.draw_polygon(cg, True, [(int(cw + b*scale), int(ch + a*scale)),(int(cw+b*scale-5),int(ch+a*scale-3)),(int(cw+b*scale-5),int(ch+a*scale+3))])
			drawable.draw_polygon(cg, True, [(int(cw), int(ch+a*scale)),(int(cw+5),int(ch+a*scale+3)),(int(cw+5),int(ch+a*scale-3))])
		else:
			drawable.draw_line( cg, int(cw + b*scale), int(ch + a*scale), int(cw), int(ch + a*scale) )
			drawable.draw_text( fnt , cg, int(cw + b*scale/2), int(ch + a*scale + 2), "B")
			cg.set_line_attributes(1,gtk.gdk.LINE_SOLID, gtk.gdk.CAP_BUTT, gtk.gdk.JOIN_MITER)
			drawable.draw_polygon(cg, True, [(int(cw + b*scale), int(ch + a*scale)),(int(cw+b*scale+5),int(ch+a*scale-3)),(int(cw+b*scale+5),int(ch+a*scale+3))])
			drawable.draw_polygon(cg, True, [(int(cw), int(ch+a*scale)),(int(cw-5),int(ch+a*scale+3)),(int(cw-5),int(ch+a*scale-3))])


		#		drawable.draw_arc( cg, False, 5 + i * hole_size + int(hole_size/8), 5 + j * hole_size + int(hole_size/8), hole_size - int(hole_size/4), hole_size - int(hole_size/4), 0, 360*64)


		return


