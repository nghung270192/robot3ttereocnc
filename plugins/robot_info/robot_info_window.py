# -*- coding: utf-8 -*-






import pygtk
pygtk.require("2.0")
import gtk
import gobject

import copy



class robot_info_window(gtk.Window):


	def __init__(self,tereo,args=None):

		gtk.Window.__init__(self)

		self.tereo = tereo
		self.args = args

		self.set_default_size(640, 480)
		self.set_position(gtk.WIN_POS_CENTER)

		self.tmpdb = copy.deepcopy(self.tereo.env.robots[args].db)
		self.tmprobot = copy.deepcopy(self.tereo.env.robots[args])

		vboxrob1 = gtk.VBox()
		vboxrob1.show()
		self.add(vboxrob1)

		hboxrob2 = gtk.HBox()
		hboxrob2.show()
		vboxrob1.pack_start(hboxrob2,True,True,0)

		vboxrob3 = gtk.VBox()
		vboxrob3.show()
		hboxrob2.pack_start(vboxrob3,True,True,0)

		hboxrob3 = gtk.HBox()
		hboxrob3.show()
		vboxrob3.pack_start(hboxrob3,False,False,0)

		labelrob1 = gtk.Label(" Name : ")
		labelrob1.show()
		hboxrob3.pack_start(labelrob1,False,False,0)

		self.entryrob1 = gtk.Entry()
		self.entryrob1.show()
		hboxrob3.pack_start(self.entryrob1,True,True,1)


		winrob3 = gtk.Button("Model / Constructor")
		#winrob3.set_size_request(-1,24)
		winrob3.show()
		winrob3.connect("clicked",self.on_robotsmodel)
		vboxrob3.pack_start(winrob3,False,False,1)

		labelrob2 = gtk.Label("Position :")
		labelrob2.show()
		vboxrob3.pack_start(labelrob2,False,False,2)


		tablerob1 = gtk.Table(3,2)
		tablerob1.show()

		self.entryroby = gtk.Entry()
		self.entryroby.show()
		tablerob1.attach(self.entryroby,1,2,1,2)

		self.entryrobz = gtk.Entry()
		self.entryrobz.show()
		tablerob1.attach(self.entryrobz,1,2,2,3)

		labelrob5 = gtk.Label("Z")
		labelrob5.show()
		tablerob1.attach(labelrob5,0,1,2,3)

		labelrob4 = gtk.Label("Y")
		labelrob4.show()
		tablerob1.attach(labelrob4,0,1,1,2)

		self.entryrobx = gtk.Entry()
		self.entryrobx.show()
		tablerob1.attach(self.entryrobx,1,2,0,1)

		labelrob3 = gtk.Label("X")
		labelrob3.show()
		tablerob1.attach(labelrob3,0,1,0,1)

		vboxrob3.pack_start(tablerob1,False,False,3)


		labelrob6 = gtk.Label("Orientation :")
		labelrob6.show()
		vboxrob3.pack_start(labelrob6,False,False,4)

		hboxrob21 = gtk.HBox()
		hboxrob21.show()

		labelrob21 = gtk.Label(" Angle : ")
		labelrob21.show()
		hboxrob21.pack_start(labelrob21, False, False, 0)

		self.entryrob21 = gtk.Entry()
		self.entryrob21.show()
		hboxrob21.pack_start(self.entryrob21, True, True, 1)

		vboxrob3.pack_start(hboxrob21,False,False,5)

		vboxrob11 = gtk.VBox()
		vboxrob11.show()

		hboxrob2.pack_start(vboxrob11,True,True,1)

		labelrob11 = gtk.Label("Robot Controller :")
		labelrob11.show()
		vboxrob11.pack_start(labelrob11, False, False, 0)


		self.connectorls = [] #gtk.ListStore(gobject.TYPE_STRING,gobject.TYPE_INT)
		self.connectorcb = gtk.Combo()
		self.connectorcb.set_popdown_strings(self.connectorls)
		self.connectorcb.show()
		#cellrenderertext1 = gtk.CellRendererText()
		#"text",0)


		vboxrob11.pack_start(self.connectorcb, False, False, 1)

		self.checkbuttonrob1 = gtk.CheckButton("External kinematics calculator")
		self.checkbuttonrob1.show()

		vboxrob11.pack_start(self.checkbuttonrob1, False, False, 2)

		self.calculatorls = [] #gtk.ListStore(gobject.TYPE_STRING,gobject.TYPE_INT)
		self.calculatorcb = gtk.Combo()
		self.calculatorcb.set_popdown_strings(self.calculatorls)
		self.calculatorcb.show()
		#cellrenderertext2 = gtk.CellRendererText()
		#"text",0)

		vboxrob11.pack_start(self.calculatorcb, False, False, 3)

		#vboxrob3.pack_start(self.checkbuttonrob1, False, False, 1)



        #        <property name="position">1</property>
        #    <property name="position">0</property>


 		hboxrob1 = gtk.HBox()
		hboxrob1.show()

		img = gtk.Image()
		img.set_from_file('./media/dialog-ok.png')
		img.show()
		winrob1 = gtk.Button("OK")
		winrob1.set_image(img)
		winrob1.show()
		#winrob1.set_size_request(-1,24)
		winrob1.connect("clicked",self.on_robotswin_ok)
		hboxrob1.pack_start(winrob1,False,False,0)

		img = gtk.Image()
		img.set_from_file('./media/delete.png')
		img.show()
		winrob2 = gtk.Button("Cancel")
		winrob2.set_image(img)
		winrob2.set_image(img)
		winrob2.show()
		#winrob2.set_size_request(-1,24)
		winrob2.connect("clicked",self.on_robotswin_cancel)
		hboxrob1.pack_start(winrob2,False,False,1)

		vboxrob1.pack_start(hboxrob1, False, False, 1)

		self.show()

		self.populate()


		return


	def on_robotsmodel(self,widget):

		# robot library
		self.tereo.execute_function("robot.library","Robot library",self.args)

		return


	def on_robotswin_cancel(self,widget):
		if self.tmprobot:
			del self.tmprobot
		if self.tmpdb:
			del self.tmpdb
		#todo destroy
		self.tereo.remove_object("robot.info",self)
		self.destroy()
		return

	def on_robotswin_ok(self,widget):

		# save
		if self.tmprobot:
			print "save rob"
			oldrob = None
			if self.tereo.env.robots[self.args]:
				#oldrob = self.tereo.env.robots[self.args]
				#self.tereo.env.robots[self.args] = copy.deepcopy(self.tmprobot)
				#if oldrob != None:
				#del oldrob
				#self.tereo.env.pdb.merge(self.tmpdb)
				self.tereo.env.robots[self.args].proto="lcnc"


			#if self.scenevismach:
			#	del self.scenevismach
			#floor = Collection([Box(-50,-50,-3,50,50,0)])
			#floor = Color([0,1,0,0],[floor])
			#work = Capture()
			#tooltip = Capture()
			#self.env.robot.load_mesh()
			#robot = self.env.robot.get_collection()
			#model = Collection([tooltip,robot,work])
			#self.scenevismach = embedded_vismach(model,tooltip,work,50)
			#self.scenevismachframe.removemodel(self.env.robot[self.currentRobot].model)
		else:
			print "don't save rob"

		self.tereo.remove_object("robot.info",self)
		self.destroy()

		return










	def on_robotsdlg_ok(self,widget):
		dlg = self.builder.get_object("dlgRobots")
		name = self.builder.get_object("dlgRobotsName")
		group = self.builder.get_object("dlgRobotsGroup")
		#name.get_text()
		#group.get_text()
		#todo make dir
		dlg.hide()
		return

	def on_robotsdlg_cancel(self,widget):
		dlg = self.builder.get_object("dlgRobots")
		dlg.hide()
		return




	def populate(self):

		#todo why?
		#self.tereo.env.pdb.extract_section('[TRAJ]', self.tmpdb)
		#for i in range(0,5):
		#	self.tereo.env.pdb.extract_section('[AXIS %d'%(i) + ']', self.tmpdb)

		#concb = self.connectorcb
		#calcb = self.calculatorcb

		#conlv = self.connectorls
		self.connectorls=[]


		self.connectorls.append("None")
		self.connectorcb.entry.set_text("None")
		i = 0
		lc = self.tereo.object_search_path("controller")
		print "controller cb"
		print lc
		for c in lc:
			self.connectorls.append(c[0])
			if self.tmprobot.proto == c[0]:
				self.connectorcb.entry.set_text(c[0])
			i += 1
		self.connectorcb.set_popdown_strings(self.connectorls)

		#self.connectorls.append(("Linuxcnc",0))
		#self.connectorls.append(("Kuka",0))
		#self.connectorls.append(("simulator",0))

		self.calculatorls = []
		self.calculatorls.append("None")
		self.calculatorcb.entry.set_text("None")
		i = 0
		lc = self.tereo.object_search_path("calculator")
		for c in lc:
			self.calculatorls.append(c[0])
			if self.tmprobot.calc == c[0]:
				self.calculatorcb.entry.set_text(c[0])
			i += 1
		self.calculatorcb.set_popdown_strings(self.calculatorls)


		self.entryrob1.set_text(self.tmprobot.name)

		#todo
		#self.entryrobx.set_text("%f"%self.tmprobot.position_x)
		#self.entryroby.set_text("%f"%self.tmprobot.position_y)
		#self.entryrobz.set_text("%f"%self.tmprobot.position_z)
		#self.entryrob21.set_text("%f"%self.tmprobot.angle_z)

		return


	def on_robotswin_destroy(self,widget):

		#todo: ask save changes

		if self.tmprobot:
			del self.tmprobot
		if self.tmpdb:
			del self.tmpdb

		return




