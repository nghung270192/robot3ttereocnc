# -*- coding: utf-8 -*-

from plugins.file.file_view import file_view

####################################################
# file window
####################################################
def on_file_activate(widget, parent):

	f = parent.object_search_path("file")

	if f == []:
		f = file_view(parent)
		parent.add_object("file","File",f)
	else:
		f=f[0] #first
		f=f[1] #1=ref

	f.show()

	return