# -*- coding: utf-8 -*-


import os.path
import tereoProcess


#!/usr/bin/python
###########################################################
#	BTL INTERFACE
###########################################################


class btlMatrix():
	def __init(self):
		self.ox = 0
		self.oy = 0
		self.oz = 0
		self.xx = 0
		self.xy = 0
		self.xz = 0
		self.yx = 0
		self.yy = 0
		self.yz = 0

	def normalize(self):

		maxi = 0
		if abs(self.xx) > maxi: maxi = abs(self.xx)
		if abs(self.xy) > maxi: maxi = abs(self.xy)
		if abs(self.xz) > maxi: maxi = abs(self.xz)

		if maxi != 0:
			self.xx = self.xx / maxi
			self.xy = self.xy / maxi
			self.xz = self.xz / maxi

		maxi = 0
		if abs(self.yx) > maxi: maxi = abs(self.yx)
		if abs(self.yy) > maxi: maxi = abs(self.yy)
		if abs(self.yz) > maxi: maxi = abs(self.yz)

		if maxi == 0: return
		self.yx = self.yx / maxi
		self.yy = self.yy / maxi
		self.yz = self.yz / maxi

		return





class btlProcessObject():
	def __init__(self):
		self.parent=None
		self.interface=None
		self.processKey = ''
		self.referencePlane = btlMatrix()
		self.referencePlane.xx = 1 #default
		self.referencePlane.yy = 1 #default
		self.processParameters = {}
		self.processIdent = 0
		self.processingQuality = ''
		self.comment = ''
		self.priority = 0
		self.recess = ''
		self.process = ''
		self.tereoproc=None

	def makeproc(self):
		print "makeproc"
		word_list = self.processKey.split(' ',1)
		pkey = word_list[0]
		g = pkey[0]
		s = pkey[6]
		key = pkey[2:5]

		print "g-key-s:" + g + "-" + key + "-" + s
		#self.interface.application.drawSideTriangle(s,self.parent)

		print "a"

		if 'P01' in self.processParameters.keys(): p1 = self.processParameters["P01"]
		else: p1 = ''
		if 'P02' in self.processParameters.keys(): p2 = self.processParameters["P02"]
		else: p2 = ''
		if 'P03' in self.processParameters.keys(): p3 = self.processParameters["P03"]
		else: p3 = ''
		if 'P04' in self.processParameters.keys(): p4 = self.processParameters["P04"]
		else: p4 = ''
		if 'P05' in self.processParameters.keys(): p5 = self.processParameters["P05"]
		else: p5 = ''
		if 'P06' in self.processParameters.keys(): p6 = self.processParameters["P06"]
		else: p6 = ''
		if 'P07' in self.processParameters.keys(): p7 = self.processParameters["P07"]
		else: p7 = ''
		if 'P08' in self.processParameters.keys(): p8 = self.processParameters["P08"]
		else: p8 = ''
		if 'P09' in self.processParameters.keys(): p9 = self.processParameters["P09"]
		else: p9 = ''
		if 'P10' in self.processParameters.keys(): p10 = self.processParameters["P10"]
		else: p10 = ''
		if 'P11' in self.processParameters.keys(): p11 = self.processParameters["P11"]
		else: p11 = ''
		if 'P12' in self.processParameters.keys(): p12 = self.processParameters["P12"]
		else: p12 = ''
		if 'P13' in self.processParameters.keys(): p13 = self.processParameters["P13"]
		else: p13 = ''
		if 'P14' in self.processParameters.keys(): p14 = self.processParameters["P14"]
		else: p14 = ''
		if 'P15' in self.processParameters.keys(): p15 = self.processParameters["P15"]
		else: p15 = ''
		if 'P16' in self.processParameters.keys(): p16 = self.processParameters["P16"]
		else: p16 = ''
		if 'P17' in self.processParameters.keys(): p17 = self.processParameters["P17"]
		else: p17 = ''
		if 'P18' in self.processParameters.keys(): p18 = self.processParameters["P18"]
		else: p18 = ''
		if 'P19' in self.processParameters.keys(): p19 = self.processParameters["P19"]
		else: p19 = ''
		if 'P20' in self.processParameters.keys(): p20 = self.processParameters["P20"]
		else: p20 = ''
		if 'P21' in self.processParameters.keys(): p21 = self.processParameters["P21"]
		else: p21 = ''
		if 'P22' in self.processParameters.keys(): p22 = self.processParameters["P22"]
		else: p22 = ''

		print "b"

		# cut
		if key == "010":
			# cut
			if g == '1' or g == '2':

				p1 = self.interface.scale2unit(p1)
				p2 = self.interface.scale2unit(p2)
				p3 = self.interface.scale2unit(p3)
				#angle
				p6 = self.interface.scale2unit(p6)
				p7 = self.interface.scale2unit(p7)
				self.tereoproc = tereoProcess.Cut(s,self.parent,g, p1, p2, p3, p6, p7)
				self.tereoproc.name ="cut"
				self.tereoproc.uid = self.parent.singleMemberNumber + "-" + self.processIdent
				return

			# longitudinal cut
			if g == '0' or g == '3' or g == '4':

				p1 = self.interface.scale2unit(p1)
				p2 = self.interface.scale2unit(p2)
				p4 = self.interface.scale2unit(p4)
				p11 = self.interface.scale2unit(p11)
				p12 = self.interface.scale2unit(p12)
				#angle
				p7 = self.interface.scale2unit(p7)
				p13 = self.interface.scale2unit(p13)
				p14 = self.interface.scale2unit(p14)

				self.tereoproc = tereoProcess.LongitudinalCut(s,self.parent,g, p1, p2, p4, p7, p11, p12, p13, p14)
				self.tereoproc.name ="longitudinal cut"
				self.tereoproc.uid = self.parent.singleMemberNumber + "-" + self.processIdent
				#not implemented
				return
			return

		elif key == '011':
			# double cut
			if g == '1' or g == '2':

				p1 = self.interface.scale2unit(p1)
				p2 = self.interface.scale2unit(p2)
				#angle
				p6 = self.interface.scale2unit(p6)
				p7 = self.interface.scale2unit(p7)
				p8 = self.interface.scale2unit(p8)
				p9 = self.interface.scale2unit(p9)

				self.tereoproc = tereoProcess.DoubleCut(s,self.parent,g, p1, p2, p6, p7, p8, p9)
				self.tereoproc.name ="double cut"
				self.tereoproc.uid = self.parent.singleMemberNumber + "-" + self.processIdent
				#not implemented
				return
			return

		elif key == '012':
			# Ridge or Valley Cut
			if g == '0':

				p1 = self.interface.scale2unit(p1)
				p2 = self.interface.scale2unit(p2)
				p4 = self.interface.scale2unit(p4)
				p11 = self.interface.scale2unit(p11)
				p12 = self.interface.scale2unit(p12)
				#angle
				p7 = self.interface.scale2unit(p7)
				p9 = self.interface.scale2unit(p9)
				p13 = self.interface.scale2unit(p13)
				p14 = self.interface.scale2unit(p14)
				p15 = self.interface.scale2unit(p15)
				p16 = self.interface.scale2unit(p16)

				self.tereoproc = tereoProcess.RidgeValleyCut(s,self.parent,g,p1,p2,p4,p7,p9,p11,p12,p13,p14,p15,p16)
				self.tereoproc.name ="ridge or valley cut"
				self.tereoproc.uid = self.parent.singleMemberNumber + "-" + self.processIdent
				#not implemented
				return
			return

		elif key == '013':
			# Saw Cut
			if g == '0' or g == '3' or g == '4':

				p1 = self.interface.scale2unit(p1)
				p2 = self.interface.scale2unit(p2)
				p3 = self.interface.scale2unit(p3)
				p11 = self.interface.scale2unit(p11)
				p12 = self.interface.scale2unit(p12)
				#angle
				p6 = self.interface.scale2unit(p6)
				p7 = self.interface.scale2unit(p7)
				p8 = self.interface.scale2unit(p8)

				self.tereoproc = tereoProcess.SawCut(s,self.parent,g,p1,p2,p3,p6,p7,p8,p11,p12)
				self.tereoproc.name ="saw cut"
				self.tereoproc.uid = self.parent.singleMemberNumber + "-" + self.processIdent
				#not implemented
				return
			return

		elif key == '016':
			# Slot
			if g == '3' or g == '4':

				p1 = self.interface.scale2unit(p1)
				p2 = self.interface.scale2unit(p2)
				p3 = self.interface.scale2unit(p3)
				p11 = self.interface.scale2unit(p11)
				p12 = self.interface.scale2unit(p12)
				p13 = self.interface.scale2unit(p13)
				p14 = self.interface.scale2unit(p14)
				p15 = self.interface.scale2unit(p15)
				#angle
				p6 = self.interface.scale2unit(p6)
				p7 = self.interface.scale2unit(p7)
				p8 = self.interface.scale2unit(p8)
				p9 = self.interface.scale2unit(p9)

				self.tereoproc = tereoProcess.Slot(s,self.parent,g,p1,p2,p3,p4,p6,p7,p8,p9,p10,p11,p12,p13,p14,p15)
				self.tereoproc.name ="slot"
				self.tereoproc.uid = self.parent.singleMemberNumber + "-" + self.processIdent
				#not implemented
				return
			return

		elif key == '017':
			# Front Slot (rainure d extremite)
			if g == '3' or g == '4':
				print "front slot"

				p1 = self.interface.scale2unit(p1)
				p2 = self.interface.scale2unit(p2)
				p3 = self.interface.scale2unit(p3)
				p11 = self.interface.scale2unit(p11)
				p12 = self.interface.scale2unit(p12)
				p13 = self.interface.scale2unit(p13)
				#angle
				p6 = self.interface.scale2unit(p6)
				p7 = self.interface.scale2unit(p7)
				p8 = self.interface.scale2unit(p8)

				self.tereoproc = tereoProcess.FrontSlot(s,self.parent,g,p1,p2,p3,p6,p7,p8,p11,p12,p13)
				self.tereoproc.name ="front slot"
				self.tereoproc.uid = self.parent.singleMemberNumber + "-" + self.processIdent
				return

			return

		elif key == '020':
			# Birds Mouth
			if g == '3' or g == '4':

				p1 = self.interface.scale2unit(p1)
				p2 = self.interface.scale2unit(p2)
				p11 = self.interface.scale2unit(p11)
				p12 = self.interface.scale2unit(p12)
				p13 = self.interface.scale2unit(p13)
				p14 = self.interface.scale2unit(p14)
				p15 = self.interface.scale2unit(p15)
				#angle
				p6 = self.interface.scale2unit(p6)
				p7 = self.interface.scale2unit(p7)
				p8 = self.interface.scale2unit(p8)
				p9 = self.interface.scale2unit(p9)
				p10 = self.interface.scale2unit(p10)

				self.tereoproc = tereoProcess.BirdsMouth(s,self.parent,g,p1,p2,p5,p6,p7,p8,p9,p10,p11,p12,p13,p14,p15)
				self.tereoproc.name ="birds mouth"
				self.tereoproc.uid = self.parent.singleMemberNumber + "-" + self.processIdent
				#not implemented
				return
			return

		elif key == '030':
			# Ridge Lap
			if g == '1' or g == '2':

				p1 = self.interface.scale2unit(p1)
				p2 = self.interface.scale2unit(p2)
				p11 = self.interface.scale2unit(p11)
				p12 = self.interface.scale2unit(p12)
				p13 = self.interface.scale2unit(p13)
				#angle
				p6 = self.interface.scale2unit(p6)

				self.tereoproc = tereoProcess.RidgeLap(s,self.parent,g,p1,p2,p6,p11,p12,p13)
				self.tereoproc.name ="ridge lap"
				self.tereoproc.uid = self.parent.singleMemberNumber + "-" + self.processIdent
				#not implemented
				return

			# Lap Joint
			elif g == '3' or g == '4':
				print "lap joint"

				p1 = self.interface.scale2unit(p1)
				p2 = self.interface.scale2unit(p2)
				p3 = self.interface.scale2unit(p3)
				p11 = self.interface.scale2unit(p11)
				p12 = self.interface.scale2unit(p12)
				p14 = self.interface.scale2unit(p14)
				#angle
				p6 = self.interface.scale2unit(p6)
				p7 = self.interface.scale2unit(p7)
				p8 = self.interface.scale2unit(p8)
				p9 = self.interface.scale2unit(p9)
				p10 = self.interface.scale2unit(p10)
				p13 = self.interface.scale2unit(p13)

				self.tereoproc = tereoProcess.LapJoint(s,self.parent,g,p1,p2,p3,p4,p6,p7,p8,p9,p10,p11,p12,p13,p14)
				self.tereoproc.name ="lap joint"
				self.tereoproc.uid = self.parent.singleMemberNumber + "-" + self.processIdent
				return
			return

		elif key == '032':
			# Notch / Rabbet
			if g == '3' or g == '4':

				p1 = self.interface.scale2unit(p1)
				p2 = self.interface.scale2unit(p2)
				p11 = self.interface.scale2unit(p11)
				p12 = self.interface.scale2unit(p12)
				p13 = self.interface.scale2unit(p13)


				self.tereoproc = tereoProcess.NotchRabbet(s,self.parent,g,p1,p2,p4,p11,p12,p13)
				self.tereoproc.name ="notch rabbet"
				self.tereoproc.uid = self.parent.singleMemberNumber + "-" + self.processIdent
				#not implemented
				return
			return

		elif key == '033':
			# Block House Half Lap, Stair Riser Dado
			if g == '3' or g == '4':

				p1 = self.interface.scale2unit(p1)
				p11 = self.interface.scale2unit(p11)
				p12 = self.interface.scale2unit(p12)
				p13 = self.interface.scale2unit(p13)
				#angle
				p6 = self.interface.scale2unit(p6)

				self.tereoproc = tereoProcess.BlockHouse(s,self.parent,g,p1,p6,p11,p12,p13)

				#not implemented
				self.tereoproc.name ="block house half lap"
				self.tereoproc.uid = self.parent.singleMemberNumber + "-" + self.processIdent
				return
			return

		elif key == '034':
			# Seathing Cut
			if g == '3' or g == '4':

				p1 = self.interface.scale2unit(p1)
				p11 = self.interface.scale2unit(p11)
				p12 = self.interface.scale2unit(p12)

				self.tereoproc = tereoProcess.SeathingCut(s,self.parent,g,p1,p11,p12)
				self.tereoproc.name ="seathing cut"
				self.tereoproc.uid = self.parent.singleMemberNumber + "-" + self.processIdent
				#not implemented
				return
			return

		elif key == '035':
			# French Ridge Lap
			if g == '1' or g == '2':

				p1 = self.interface.scale2unit(p1)
				p13 = self.interface.scale2unit(p13)
				#angle
				p6 = self.interface.scale2unit(p6)

				self.tereoproc = tereoProcess.FrenchRidgeLap(s,self.parent,g,p1,p2,p6,p13)
				self.tereoproc.name ="french ridge lap"
				self.tereoproc.uid = self.parent.singleMemberNumber + "-" + self.processIdent
				#not implemented
				return
			return

		elif key == '036':
			# Chamfer
			if g == '3' or g == '4':

				p1 = self.interface.scale2unit(p1)
				p11 = self.interface.scale2unit(p11)
				p12 = self.interface.scale2unit(p12)

				self.tereoproc = tereoProcess.Chamfer(s,self.parent,g,p1,p4,p11,p12,p15)
				self.tereoproc.name ="chamfer"
				self.tereoproc.uid = self.parent.singleMemberNumber + "-" + self.processIdent
				#not implemented
				return
			return

		elif key == '037':
			# Block House Half Lap
			if g == '4':

				p1 = self.interface.scale2unit(p1)
				p3 = self.interface.scale2unit(p3)
				p8 = self.interface.scale2unit(p8)
				p9 = self.interface.scale2unit(p9)
				p10 = self.interface.scale2unit(p10)
				p11 = self.interface.scale2unit(p11)
				p12 = self.interface.scale2unit(p12)
				p13 = self.interface.scale2unit(p13)
				p14 = self.interface.scale2unit(p14)
				p15 = self.interface.scale2unit(p15)
				p16 = self.interface.scale2unit(p16)
				p17 = self.interface.scale2unit(p17)
				p18 = self.interface.scale2unit(p18)

				self.tereoproc = tereoProcess.BlockHouseHalfLap(s,self.parent,g,p1,p3,p4,p5,p8,p9,p10,p11,p12,p13,p14,p15,p16,p17,p18,p19)
				self.tereoproc.name ="block house half lap"
				self.tereoproc.uid = self.parent.singleMemberNumber + "-" + self.processIdent
				#not implemented
				return
			return

		elif key == '038':
			# Block House Front
			if g == '3' or g == '4':

				p1 = self.interface.scale2unit(p1)
				p11 = self.interface.scale2unit(p11)
				p12 = self.interface.scale2unit(p12)
				p13 = self.interface.scale2unit(p13)
				p15 = self.interface.scale2unit(p15)
				#angle
				p6 = self.interface.scale2unit(p6)

				self.tereoproc = tereoProcess.BlockHouseFront(s,self.parent,g,p1,p4,p6,p11,p12,p13,p15)
				self.tereoproc.name ="block house front"
				self.tereoproc.uid = self.parent.singleMemberNumber + "-" + self.processIdent
				#not implemented
				return
			return

		elif key == '039':
			# Pocket
			if g == '4':

				p1 = self.interface.scale2unit(p1)
				p2 = self.interface.scale2unit(p2)
				p11 = self.interface.scale2unit(p11)
				p12 = self.interface.scale2unit(p12)
				p13 = self.interface.scale2unit(p13)
				#angle
				p6 = self.interface.scale2unit(p6)
				p7 = self.interface.scale2unit(p7)
				p8 = self.interface.scale2unit(p8)
				p10 = self.interface.scale2unit(p10)

				self.tereoproc = tereoProcess.Pocket(s,self.parent,g,p1,p2,p4,p6,p7,p8,p10,p11,p12,p13)
				self.tereoproc.name ="pocket"
				self.tereoproc.uid = self.parent.singleMemberNumber + "-" + self.processIdent
				#not implemented
				return
			return

		elif key == '040':
			# Drilling
			print "drilling"
			if g == '3' or g == '4':

				p1 = self.interface.scale2unit(p1)
				p2 = self.interface.scale2unit(p2)
				p3 = self.interface.scale2unit(p3)
				p11 = self.interface.scale2unit(p11)
				p12 = self.interface.scale2unit(p12)
				#angle
				p6 = self.interface.scale2unit(p6)
				p7 = self.interface.scale2unit(p7)

				self.tereoproc = tereoProcess.Drilling(s,self.parent,g,p1,p2,p3,p6,p7,p11,p12)
				self.tereoproc.name ="drilling"
				self.tereoproc.uid = self.parent.singleMemberNumber + "-" + self.processIdent
				#not implemented
				return
			return

		elif key == '050':
			# Tenon
			print "tenon"
			if g == '1' or g == '2':

				p1 = self.interface.scale2unit(p1)
				p2 = self.interface.scale2unit(p2)
				p10 = self.interface.scale2unit(p10)
				p11 = self.interface.scale2unit(p11)
				p12 = self.interface.scale2unit(p12)
				p13 = self.interface.scale2unit(p13)
				p14 = self.interface.scale2unit(p14)
				p15 = self.interface.scale2unit(p15)
				#angle
				p6 = self.interface.scale2unit(p6)
				p7 = self.interface.scale2unit(p7)
				p8 = self.interface.scale2unit(p8)

				self.tereoproc = tereoProcess.Tenon(s,self.parent,g,p1,p2,p4,p5,p6,p7,p8,p10,p11,p12,p14,p15)

				self.tereoproc.name ="tenon"
				self.tereoproc.uid = self.parent.singleMemberNumber + "-" + self.processIdent
				#not implemented
				return
			# Mortise
			elif g == '3' or g == '4':

				p1 = self.interface.scale2unit(p1)
				p2 = self.interface.scale2unit(p2)
				p3 = self.interface.scale2unit(p3)
				p10 = self.interface.scale2unit(p10)
				p11 = self.interface.scale2unit(p11)
				p12 = self.interface.scale2unit(p12)
				p13 = self.interface.scale2unit(p13)
				p14 = self.interface.scale2unit(p14)
				p15 = self.interface.scale2unit(p15)
				#angle
				p6 = self.interface.scale2unit(p6)
				p7 = self.interface.scale2unit(p7)
				p8 = self.interface.scale2unit(p8)
				p16 = self.interface.scale2unit(p16)

				self.tereoproc = tereoProcess.Mortise(s,self.parent,g,p1,p2,p3,p4,p6,p7,p8,p10,p11,p12,p13,p14,p15,p16)
				self.tereoproc.name ="mortise"
				self.tereoproc.uid = self.parent.singleMemberNumber + "-" + self.processIdent
				#not implemented
				return
			return

		elif key == '051':
			# Mortise Front
			if g == '3' or g == '4':

				p1 = self.interface.scale2unit(p1)
				p2 = self.interface.scale2unit(p2)
				p10 = self.interface.scale2unit(p10)
				p11 = self.interface.scale2unit(p11)
				p12 = self.interface.scale2unit(p12)
				p13 = self.interface.scale2unit(p13)
				p14 = self.interface.scale2unit(p14)
				p15 = self.interface.scale2unit(p15)
				#angle
				p6 = self.interface.scale2unit(p6)
				p7 = self.interface.scale2unit(p7)
				p8 = self.interface.scale2unit(p8)

				self.tereoproc = tereoProcess.MortiseFront(s,self.parent,g,p1,p2,p4,p6,p7,p8,p10,p11,p12,p14,p15)
				self.tereoproc.name ="mortise front"
				self.tereoproc.uid = self.parent.singleMemberNumber + "-" + self.processIdent

				#not implemented
				return
			return

		elif key == '052':
			# House
			if g == '3' or g == '4':

				self.tereoproc = tereoProcess.House(s,self.parent,g,p5,p9)
				self.tereoproc.name ="house"
				self.tereoproc.uid = self.parent.singleMemberNumber + "-" + self.processIdent
				#not implemented
				return
			return

		elif key == '053':
			# House Mortise
			if g == '3' or g == '4':

				self.tereoproc = tereoProcess.HouseMortise(s,self.parent,g,p9)
				self.tereoproc.name ="house mortise"
				self.tereoproc.uid = self.parent.singleMemberNumber + "-" + self.processIdent
				#not implemented
				return
			return

		elif key == '055':
			# Dovetail Tenon
			if g == '1' or g == '2':

				p1 = self.interface.scale2unit(p1)
				p2 = self.interface.scale2unit(p2)
				p9 = self.interface.scale2unit(p9)
				p11 = self.interface.scale2unit(p11)
				p12 = self.interface.scale2unit(p12)
				p14 = self.interface.scale2unit(p14)
				p15 = self.interface.scale2unit(p15)
				#angle
				p6 = self.interface.scale2unit(p6)
				p7 = self.interface.scale2unit(p7)
				p8 = self.interface.scale2unit(p8)
				p10 = self.interface.scale2unit(p10)

				self.tereoproc = tereoProcess.DovetailTenon(s,self.parent,g,p1,p2,p4,p6,p7,p8,p9,p10,p11,p12,p14,p15)
				self.tereoproc.name ="dovetail tenon"
				self.tereoproc.uid = self.parent.singleMemberNumber + "-" + self.processIdent
				#not implemented
				return

			# Dovetail Mortise
			elif g == '3' or g == '4':

				p1 = self.interface.scale2unit(p1)
				p2 = self.interface.scale2unit(p2)
				p3 = self.interface.scale2unit(p3)
				p9 = self.interface.scale2unit(p9)
				p11 = self.interface.scale2unit(p11)
				p12 = self.interface.scale2unit(p12)
				p13 = self.interface.scale2unit(p13)
				p14 = self.interface.scale2unit(p14)
				p15 = self.interface.scale2unit(p15)
				#angle
				p6 = self.interface.scale2unit(p6)
				p7 = self.interface.scale2unit(p7)

				self.tereoproc = tereoProcess.DovetailMortise(s,self.parent,g,p1,p2,p3,p4,p5,p6,p7,p9,p10,p11,p12,p13,p14,p15)
				self.tereoproc.name ="dovetail mortise"
				self.tereoproc.uid = self.parent.singleMemberNumber + "-" + self.processIdent
				#not implemeted
				return
			return

		elif key == '056':
			# Dovetail Mortise Front
			if g == '3' or g == '4':

				p1 = self.interface.scale2unit(p1)
				p2 = self.interface.scale2unit(p2)
				p3 = self.interface.scale2unit(p3)
				p9 = self.interface.scale2unit(p9)
				p11 = self.interface.scale2unit(p11)
				p12 = self.interface.scale2unit(p12)
				p14 = self.interface.scale2unit(p14)
				p15 = self.interface.scale2unit(p15)
				#angle
				p6 = self.interface.scale2unit(p6)
				p7 = self.interface.scale2unit(p7)
				p8 = self.interface.scale2unit(p8)
				p10 = self.interface.scale2unit(p10)

				self.tereoproc = tereoProcess.DovetailMortiseFront(s,self.parent,g,p1,p2,p3,p4,p5,p6,p7,p8,p9,p10,p11,p12,p14,p15)
				self.tereoproc.name ="dovetail mortise front"
				self.tereoproc.uid = self.parent.singleMemberNumber + "-" + self.processIdent
				#not implemented
				return
			return

		elif key == '060':
			# Marking / Labeling
			if g == '3' or g == '4':

				p1 = self.interface.scale2unit(p1)
				p2 = self.interface.scale2unit(p2)
				p11 = self.interface.scale2unit(p11)
				p12 = self.interface.scale2unit(p12)
				p13 = self.interface.scale2unit(p13)
				p15 = self.interface.scale2unit(p15)
				#angle
				p6 = self.interface.scale2unit(p6)
				p7 = self.interface.scale2unit(p7)

				self.tereoproc = tereoProcess.MarkingLabeling(s,self.parent,g,p1,p2,p4,p6,p7,p11,p12,p13,p15)
				self.tereoproc.name ="marking labeling"
				self.tereoproc.uid = self.parent.singleMemberNumber + "-" + self.processIdent
				#not implemented
				return
			return

		elif key == '061':
			# Text
			if g == '4':

				p1 = self.interface.scale2unit(p1)
				p2 = self.interface.scale2unit(p2)
				p13 = self.interface.scale2unit(p13)
				#angle
				p6 = self.interface.scale2unit(p6)

				self.tereoproc = tereoProcess.Text(s,self.parent,g,p1,p2,p6,p9,p10,p11,p13,p15)
				self.tereoproc.name ="text"
				self.tereoproc.uid = self.parent.singleMemberNumber + "-" + self.processIdent
				#not implemented
				return
			return

		elif key == '070':
			# Simple Scarf
			if g == '1' or g == '2':

				p1 = self.interface.scale2unit(p1)
				p11 = self.interface.scale2unit(p11)
				p12 = self.interface.scale2unit(p12)
				p13 = self.interface.scale2unit(p13)
				p14 = self.interface.scale2unit(p14)
				p15 = self.interface.scale2unit(p15)

				self.tereoproc = tereoProcess.SimpleScarf(s,self.parent,g,p1,p11,p12,p13,p14,p15)
				self.tereoproc.name ="simple scarf"
				self.tereoproc.uid = self.parent.singleMemberNumber + "-" + self.processIdent
				#not implemented
				return
			return

		elif key == '071':
			# Scarf Joint
			if g == '1' or g == '2':

				p1 = self.interface.scale2unit(p1)
				p10 = self.interface.scale2unit(p10)
				p11 = self.interface.scale2unit(p11)
				p12 = self.interface.scale2unit(p12)
				p13 = self.interface.scale2unit(p13)
				p14 = self.interface.scale2unit(p14)
				p15 = self.interface.scale2unit(p15)
				#angle
				p7 = self.interface.scale2unit(p7)

				self.tereoproc = tereoProcess.ScarfJoint(s,self.parent,g,p1,p7,p9,p10,p11,p12,p13,p14,p15)
				self.tereoproc.name ="scarf joint"
				self.tereoproc.uid = self.parent.singleMemberNumber + "-" + self.processIdent
				#not implemented
				return
			return

		elif key == '080':
			# Step Joint
			if g == '1' or g == '2':

				p1 = self.interface.scale2unit(p1)
				p11 = self.interface.scale2unit(p11)
				p12 = self.interface.scale2unit(p12)
				p13 = self.interface.scale2unit(p13)
				p14 = self.interface.scale2unit(p14)
				p15 = self.interface.scale2unit(p15)
				#angle
				p7 = self.interface.scale2unit(p7)

				self.tereoproc = tereoProcess.StepJoint(s,self.parent,g,p1,p4,p7,p11,p12,p14,p15)
				self.tereoproc.name ="step joint"
				self.tereoproc.uid = self.parent.singleMemberNumber + "-" + self.processIdent
				#not implemented
				return

			# Step Joint Notch
			elif g == '3' or g == '4':

				p1 = self.interface.scale2unit(p1)
				p2 = self.interface.scale2unit(p2)
				p10 = self.interface.scale2unit(p10)
				p11 = self.interface.scale2unit(p11)
				p12 = self.interface.scale2unit(p12)
				p13 = self.interface.scale2unit(p13)
				p14 = self.interface.scale2unit(p14)
				p15 = self.interface.scale2unit(p15)
				#angle
				p7 = self.interface.scale2unit(p7)

				self.tereoproc = tereoProcess.StepJointNotch(s,self.parent,g,p1,p2,p4,p7,p10,p11,p12,p13,p14,p15)
				self.tereoproc.name ="step joint notch"
				self.tereoproc.uid = self.parent.singleMemberNumber + "-" + self.processIdent
				#not implemented
				return

		elif key == '090':
			# Planing
			if g == '3' or g == '4':

				p1 = self.interface.scale2unit(p1)
				p11 = self.interface.scale2unit(p11)
				p12 = self.interface.scale2unit(p12)

				self.tereoproc = tereoProcess.Planing(s,self.parent,g,p1,p4,p11,p12)
				self.tereoproc.name ="planing"
				self.tereoproc.uid = self.parent.singleMemberNumber + "-" + self.processIdent
				#not implemented
				return
			return

		elif key == '100':
			# Profile Front
			if g == '3' or g == '4':

				p1 = self.interface.scale2unit(p1)
				p3 = self.interface.scale2unit(p3)
				p11 = self.interface.scale2unit(p11)
				p12 = self.interface.scale2unit(p12)
				#angle
				p6 = self.interface.scale2unit(p6)
				p7 = self.interface.scale2unit(p7)
				p8 = self.interface.scale2unit(p8)

				self.tereoproc = tereoProcess.ProfileFront(s,self.parent,g,p1,p3,p6,p7,p8,p11,p12)
				self.tereoproc.name ="profile front"
				self.tereoproc.uid = self.parent.singleMemberNumber + "-" + self.processIdent
				#not implemented
				return
			return

		elif key == '101':
			# Profile Head concave
			if g == '3' or g == '4':

				p1 = self.interface.scale2unit(p1)
				p11 = self.interface.scale2unit(p11)
				p12 = self.interface.scale2unit(p12)
				p13 = self.interface.scale2unit(p13)
				p14 = self.interface.scale2unit(p14)
				p15 = self.interface.scale2unit(p15)

				self.tereoproc = tereoProcess.ProfileHeadConcave(s,self.parent,g,p1,p11,p12,p13,p14,p15)
				self.tereoproc.name ="profile head concave"
				self.tereoproc.uid = self.parent.singleMemberNumber + "-" + self.processIdent
				#not implemented
				return
			return

		elif key == '102':
			# Profile Head convex
			if g == '3' or g == '4':

				p1 = self.interface.scale2unit(p1)
				p11 = self.interface.scale2unit(p11)
				p12 = self.interface.scale2unit(p12)
				p13 = self.interface.scale2unit(p13)
				p14 = self.interface.scale2unit(p14)
				p15 = self.interface.scale2unit(p15)

				self.tereoproc = tereoProcess.ProfileHeadConvex(s,self.parent,g,p1,p11,p12,p13,p14,p15)
				self.tereoproc.name ="profile head convex"
				self.tereoproc.uid = self.parent.singleMemberNumber + "-" + self.processIdent
				#not implemented
				return
			return

		elif key == '103':
			# Profile Head cambered
			if g == '3' or g == '4':

				p1 = self.interface.scale2unit(p1)
				p10 = self.interface.scale2unit(p10)
				p11 = self.interface.scale2unit(p11)
				p12 = self.interface.scale2unit(p12)
				p13 = self.interface.scale2unit(p13)
				p14 = self.interface.scale2unit(p14)

				self.tereoproc = tereoProcess.ProfileHeadCambered(s,self.parent,g,p1,p10,p11,p12,p13,p14,p15)
				self.tereoproc.name ="profile head cambered"
				self.tereoproc.uid = self.parent.singleMemberNumber + "-" + self.processIdent
				#not implemented
				return
			return

		elif key == '104':
			# Round Arch
			if g == '4':

				p1 = self.interface.scale2unit(p1)
				p11 = self.interface.scale2unit(p11)
				p12 = self.interface.scale2unit(p12)

				self.tereoproc = tereoProcess.RoundArch(s,self.parent,g,p1,p11,p12)
				self.tereoproc.name ="round arch"
				self.tereoproc.uid = self.parent.singleMemberNumber + "-" + self.processIdent
				#not implemented
				return
			return

		elif key == '106':
			# Profile Head
			if g == '3' or g == '4':

				p1 = self.interface.scale2unit(p1)
				p9 = self.interface.scale2unit(p9)
				p10 = self.interface.scale2unit(p10)
				p11 = self.interface.scale2unit(p11)
				p12 = self.interface.scale2unit(p12)
				p13 = self.interface.scale2unit(p13)
				p14 = self.interface.scale2unit(p14)
				p15 = self.interface.scale2unit(p15)
				p16 = self.interface.scale2unit(p16)
				p17 = self.interface.scale2unit(p17)
				p18 = self.interface.scale2unit(p18)
				p19 = self.interface.scale2unit(p19)
				p20 = self.interface.scale2unit(p20)
				p21 = self.interface.scale2unit(p21)
				p22 = self.interface.scale2unit(p22)

				self.tereoproc = tereoProcess.ProfileHead(s,self.parent,g,p1,p4,p9,p10,p11,p12,p13,p14,p15,p16,p17,p18,p19,p20,p21,p22)
				self.tereoproc.name ="profile head"
				self.tereoproc.uid = self.parent.singleMemberNumber + "-" + self.processIdent
				#not implemented
				return
			return

		elif key == '107':
			# Sphere
			if g == '3' or g == '4':

				p1 = self.interface.scale2unit(p1)
				p2 = self.interface.scale2unit(p2)
				p3 = self.interface.scale2unit(p3)
				p11 = self.interface.scale2unit(p11)
				p12 = self.interface.scale2unit(p12)
				p13 = self.interface.scale2unit(p13)

				self.tereoproc = tereoProcess.Sphere(s,self.parent,g,p1,p2,p3,p11,p12,p13)
				self.tereoproc.name ="sphere"
				self.tereoproc.uid = self.parent.singleMemberNumber + "-" + self.processIdent
				#not implemented
				return
			return

		elif key == '120':
			# Triangle Cut
			if g == '4':

				p1 = self.interface.scale2unit(p1)
				p2 = self.interface.scale2unit(p2)
				p3 = self.interface.scale2unit(p3)
				p10 = self.interface.scale2unit(p10)
				p11 = self.interface.scale2unit(p11)
				p12 = self.interface.scale2unit(p12)
				p13 = self.interface.scale2unit(p13)
				p14 = self.interface.scale2unit(p14)
				p15 = self.interface.scale2unit(p15)

				self.tereoproc = tereoProcess.TriangleCut(s,self.parent,g,p1,p2,p3,p10,p11,p12,p13,p14,p15)
				self.tereoproc.name ="triangle cut"
				self.tereoproc.uid = self.parent.singleMemberNumber + "-" + self.processIdent
				#not implemented
				return
			return

		elif key == '136':
			# Tyrolean Dovetail
			if g == '1' or g == '2' or g == '3' or g == '4':

				p1 = self.interface.scale2unit(p1)
				p2 = self.interface.scale2unit(p2)
				p3 = self.interface.scale2unit(p3)
				p5 = self.interface.scale2unit(p5)
				p7 = self.interface.scale2unit(p7)
				p8 = self.interface.scale2unit(p8)
				p11 = self.interface.scale2unit(p11)
				p12 = self.interface.scale2unit(p12)
				p13 = self.interface.scale2unit(p13)
				p15 = self.interface.scale2unit(p15)
				#angle
				p6 = self.interface.scale2unit(p6)
				p9 = self.interface.scale2unit(p9)

				self.tereoproc = tereoProcess.TyroleanDovetail(s,self.parent,g,p1,p2,p3,p4,p5,p6,p7,p8,p9,p11,p12,p13,p14,p15,p16)
				self.tereoproc.name ="tyrolean dovetail"
				self.tereoproc.uid = self.parent.singleMemberNumber + "-" + self.processIdent
				#not implemented
				return
			return

		elif key == '138':
			# Dovetail
			if g == '1' or g == '2' or g == '3' or g == '4':

				p1 = self.interface.scale2unit(p1)
				p2 = self.interface.scale2unit(p2)
				p3 = self.interface.scale2unit(p3)
				p5 = self.interface.scale2unit(p5)
				p11 = self.interface.scale2unit(p11)
				p12 = self.interface.scale2unit(p12)
				p15 = self.interface.scale2unit(p15)
				#angle
				p9 = self.interface.scale2unit(p9)

				self.tereoproc = tereoProcess.Dovetail(s,self.parent,g,p1,p2,p3,p4,p5,p9,p11,p12,p14,p15,p16)
				self.tereoproc.name ="dovetail"
				self.tereoproc.uid = self.parent.singleMemberNumber + "-" + self.processIdent
				#not implemented
				return
			return

		elif key == '250':
			# Free Contour
			if g == '0' or g == '3' or g == '4':

				p1 = self.interface.scale2unit(p1)
				p2 = self.interface.scale2unit(p2)
				p3 = self.interface.scale2unit(p3)
				p5 = self.interface.scale2unit(p5)
				p6 = self.interface.scale2unit(p6)
				p8 = self.interface.scale2unit(p8)
				p9 = self.interface.scale2unit(p9)
				p13 = self.interface.scale2unit(p13)
				p15 = self.interface.scale2unit(p15)

				self.tereoproc = tereoProcess.FreeContour(s,self.parent,g,p1,p2,p3,p5,p6,p8,p9,p13,p15)
				self.tereoproc.name ="free contour"
				self.tereoproc.uid = self.parent.singleMemberNumber + "-" + self.processIdent
				#not implemented
				return
			return

		elif key == '900':
			# Variant
			if g == '0' or g == '1' or g == '2' or g == '3' or g == '4':
				self.tereoproc = tereoProcess.Variant(s,self.parent,g)
				self.tereoproc.name ="variant"
				self.tereoproc.uid = self.parent.singleMemberNumber + "-" + self.processIdent
				#not implemented
				return
			return


		return




class btlInterface():

	def __init__(self):

		self.application = None
		self.btlGeneral = False
		self.btlPart = False
		self.btlRawPart = False
		#self.btlUid = False
		self.btlProcess = False
		self.currentPart = None
		self.currentUid = 0
		self.currentProcess = None
		self.genePart = tereoProcess.process_main_element()
		self.version = ''
		self.build = ''
		self.edition = ''
		self.filename = ''
		self.btlError = False
		self.parsing_dict = {	'VERSION:' :			self.parse_version,
								'BUILD:' :				self.parse_build,
								'EDITION:' :			self.parse_edition,
								'[GENERAL]' :			self.parse_general,
								'PROJECTNUMBER:' :		self.parse_projectnumber,
								'PROJECTNAME:' :		self.parse_projectname,
								'PROJECTPART:' :		self.parse_projectpart,
								'PROJECTGUID:' :		self.parse_projectguid,
								'LISTNAME:' :			self.parse_listname,
								'CUSTOMER:' :			self.parse_customer,
								'ARCHITECT:' :			self.parse_architect,
								'EDITOR:' :				self.parse_editor,
								'DELIVERYDATE:' :		self.parse_deliverydate,
								'EXPORTDATE:' :			self.parse_exportdate,
								'EXPORTTIME:' :			self.parse_exporttime,
								'EXPORTRELEASE:' :		self.parse_exportrelease,
								'LANGUAGE:' :			self.parse_language,
								'RANGE:' :				self.parse_range,
								'SCALEUNIT:' :			self.parse_scaleunit,
								'COMPUTERNAME:' :		self.parse_computername,
								'USER:' :				self.parse_user,
								'SOURCEFILE:' :			self.parse_sourcefile,
								'EXPORTFILE:' :			self.parse_exportfile,
								'RECESS:' :				self.parse_recess,
								'COMMENT:' :			self.parse_comment,
								'[RAWPART]' :			self.parse_rawpart,
								'PROCESSKEY:' :			self.parse_processkey,
								'REFERENCEPLANE:' :		self.parse_referenceplane,
								'PROCESSPARAMETERS:' :	self.parse_processparameters,
								'[PART]' :				self.parse_part,
								'SINGLEMEMBERNUMBER:' :	self.parse_singlemembernumber,
								'ASSEMBLYNUMBER:' :		self.parse_assemblynumber,
								'ORDERNUMBER:' :		self.parse_ordernumber,
								'DESIGNATION:' :		self.parse_designation,
								'ANNOTATION:' :			self.parse_annotation,
								'STOREY:' :				self.parse_storey,
								'GROUP:' :				self.parse_group,
								'PACKAGE:' :			self.parse_package,
								'MATERIAL:' :			self.parse_material,
								'TIMBERGRADE:' :		self.parse_timbergrade,
								'QUALITYGRADE:' :		self.parse_qualitygrade,
								'COUNT:' :				self.parse_count,
								'LENGTH:' :				self.parse_length,
								'HEIGHT:' :				self.parse_height,
								'WIDTH:' :				self.parse_width,
								'COLOUR:' :				self.parse_colour,
								'PLANINGLENGTH:' :		self.parse_planninglength,
								'STARTOFFSET:' :		self.parse_startoffset,
								'ENDOFFSET:' :			self.parse_endoffset,
								'UID:' :				self.parse_uid,
								'TRANSFORMATION:' :		self.parse_transformation,
								'CAMBER:' :				self.parse_camber,
								'PARTOFFSET:' :			self.parse_partoffset,
								'PROCESSINGQUALITY:' :	self.parse_processingquality,
								'OUTLINE:' :			self.parse_outline,
								'APERTURE:' :			self.parse_aperture,
								'STOREYTYPE:' :			self.parse_storeytype,
								'ELEMENTNUMBER:' :		self.parse_elementnumber,
								'LAYER:' :				self.parse_layer,
								'MODULENUMBER:' :		self.parse_modulenumber,
								'GRAINDIRECTION:' :		self.parse_graindirection,
								'REFERENCESIDE:' :		self.parse_refenreceside,
								'PROCESSIDENT:' :		self.parse_processident,
								'PRIORITY:' :			self.parse_priority,
								'PROCESS:' :			self.parse_process,
								'[COMPOSITE]' :			self.parse_composite,
								'TYPE:' :				self.parse_type }

		self.general_properties = {
                    'Project number' : 'projectNumber',
                    'Project name' : 'projectName',
                    'Project part' : 'projectPart',
                    'Project GUID' : 'projectGuid',
                    'List name' : 'listName',
                    'Customer' : 'customer',

                    'Architect' : 'architect',
                    'Editor' : 'editor',
                    'Delivery date' : 'deliveryDate',
                    'Export date' : 'exportDate',
                    'Export time' : 'exportTime',
                    'Export release' : 'exportRelease',
                    'Language' : 'language',
                    'Range' : 'geneRange',

                    'Scale unit' : 'scaleUnit',
                    'Processing quality' : 'processingQuality',
                    'Computer name' : 'computerName',
                    'User' : 'user',
                    'Source file' : 'sourceFile',
                    'Export file' : 'exportFile',
                    'Recess' : 'recess',
                    'Comment' : 'comment' }

		self.part_properties = { 'Single member number' : 'singleMemberNumber',
                    'Assembly number' : 'assemblyNumber',
                    'Order number' : 'orderNumber',
                    'Designation' : 'designation',
                    'Annotation' : 'annotation',
                    'Storey' : 'storey',
                    'Group' : 'group',
                    'Package' : 'package',
                    'Material' : 'material',
                    'Timber grade' : 'timberGrade',
                    'Quality grade' : 'qualityGrade',
                    'Count' : 'count',
                    'Length' : 'length',
                    'Height' : 'height',
                    'Width' : 'width',
                    'Colour' : 'colour',
                    'Planing length' : 'planingLength',
                    'Start offset' : 'startOffset',
                    'End offset' : 'endOffset',

                    'Camber' : 'camber',
                    'Part offset' : 'partOffset',
                    'Processing quality' : 'processingQuality',
                    'Outline' : 'outline',
                    'Aperture' : 'aperture',
                    'Recess' : 'recess',
                    'Storey type' : 'storeyType',
                    'Element number' : 'elementNumber',
                    'Layer' : 'layer',
                    'Module number' : 'moduleNumber',
                    'Comment' : 'comment',
                    'Grain direction' : 'grainDirection',
                    'Refrence side' : 'referenceSide' }


		self.process_properties = { 'Process key' : 'processKey',
                       'Reference plane' : 'referencePlane',
                       'Process parameters' : 'processParameters',
                       'Process ident' : 'processIdent',
                       'Processing quality' : 'processingQuality',
                       'Comment' : 'comment',
                       'Priority' : 'priority',
                       'Recess' : 'recess',
                       'Process' : 'process' }



		return

	#### MATH FUNCTIONS ####




	def scale2unit(self,value):
		if value == '' : return 0
		value = float(value)
		#self.genePart.scaleUnit = 4
		if self.genePart.scaleUnit == 1:
			value=value/10.0
		elif self.genePart.scaleUnit == 2:
			value=value/100.0
		elif self.genePart.scaleUnit == 3:
			value=value/1000.0
		elif self.genePart.scaleUnit == 4:
			value=value/10000.0
		return value

	#### PARSE FUNCTIONS ####
	def parse_version(self, args):
		self.version = args.strip('"')
		return

	def parse_build(self, args):
		self.build = args.strip('"')
		return

	def parse_edition(self, args):
		self.edition = args #STANDARD or PREFABRICATION
		return

	def parse_general(self, args):
		if self.btlGeneral:
			self.btlError = True
			return
		self.btlGeneral = True
		return

	def parse_projectnumber(self, args):
		self.genePart.projectNumber = args.strip('"')
		return

	def parse_projectname(self, args):
		self.genePart.projectName = args.strip('"')
		return

	def parse_projectpart(self, args):
		self.genePart.projectPart = args.strip('"')
		return

	def parse_projectguid(self, args):
		self.genePart.projectGuid = args.strip('"')
		return

	def parse_listname(self, args):
		self.genePart.listName = args.strip('"')
		return

	def parse_customer(self, args):
		self.genePart.customer = args.strip('"')
		return

	def parse_architect(self, args):
		self.genePart.architect = args.strip('"')
		return

	def parse_editor(self, args):
		self.genePart.editor = args.strip('"')
		return

	def parse_deliverydate(self, args):
		self.genePart.deliveryDate = args.strip('"')
		return

	def parse_exportdate(self, args):
		self.genePart.exportDate = args.strip('"')
		return

	def parse_exporttime(self, args):
		self.genePart.exportTime = args.strip('"')
		return

	def parse_exportrelease(self, args):
		self.genePart.exportRelease = args.strip('"')
		return

	def parse_language(self, args):
		self.genePart.language = args.strip('"')
		return

	def parse_range(self, args):
		self.genePart.geneRange = args.strip('"') #STANDARD or EXTENDED
		return

	def parse_scaleunit(self, args):
		# precision
		self.genePart.scaleUnit = int(args) # integer 1 = 1/10, 2 = 1/100
		return

	def parse_computername(self, args):
		self.genePart.computerName = args.strip('"')
		return

	def parse_user(self, args):
		self.genePart.user = args.strip('"')
		return

	def parse_sourcefile(self, args):
		self.genePart.sourceFile = args.strip('"')
		return

	def parse_exportfile(self, args):
		self.genePart.exportFile = args.strip('"')
		return

	def parse_recess(self, args):
		# AUTOMATIC, MANUAL
		if self.btlProcess:
			self.currentProcess.recess = args
			return

		if self.btlPart:
			if self.btlUid:
				self.currentPart.uid.append(self.currentUid)
				self.btlUid = False
				self.currentPart.recess = args
				return

		self.genePart.recess = args.strip('"')
		return

	def parse_comment(self, args):
		if self.btlProcess:
			self.currentProcess.comment = self.currentProcess.comment + args.strip('"') + '\n'
        	return

		if self.btlGeneral:
			self.genePart.comment = self.genePart.comment + args.strip('"') + '\n'
			return

		if self.btlPart:
			if btlUid:
				self.currentPart.uid.append(self.currentUid)
				self.btlUid = False
			self.currentPart.comment = self.currentPart.comment + args.strip('"') + '\n'

		return

	def parse_rawpart(self, args):
		# not implemented
		self.btlGeneral = False
		self.btlPart = False
		self.btlUid = False
		self.btlRawPart = True
		return

	def parse_processkey(self, args):
		print "parse processkey : "+args
		if self.btlRawPart:
			# rawpart not implemented
			return

		if self.btlPart:
			if self.btlUid:
				self.currentPart.uid.append(self.currentUid)
				self.btlUid = False
			if self.btlProcess:
				self.currentProcess.makeproc()
				self.currentPart.process.append(self.currentProcess)
				self.btlProcess = False
			self.btlProcess = True
			self.currentProcess = btlProcessObject()
			self.currentProcess.processKey = args
			self.currentProcess.parent=self.currentPart
			self.currentProcess.interface=self
		return

	def parse_referenceplane(self, args):
		if self.btlRawPart:
			# rawpart not implemented
			return

		if self.btlProcess:
			self.currentProcess.referencePlane = args
		return

	def parse_processparameters(self, args):
		if self.btlRawPart:
			# rawpart not implemented
			return

		if self.btlProcess:
			param_list = args.split(' ')
			pp_list={}
			for pl in param_list:
				pl = pl.strip()
				if pl != '':
					val1 = pl[4:]
					val1 = val1.strip('"')
					val1 = val1.lstrip('0')
					if val1 != '':
						val = float(val1)
						#val = self.scale2unit(val)
						pp_list[pl[:3]]=val
					else:
						pp_list[pl[:3]]=0.0

			self.currentProcess.processParameters = pp_list

		return

	def parse_part(self, args):

		if self.btlPart:
			if self.btlUid:
				self.currentPart.uid.append(self.currentUid)
			if self.btlProcess:
				self.currentProcess.makeproc()
				self.currentPart.process.append(self.currentProcess)
				self.btlProcess = False
			self.genePart.parts.append(self.currentPart)

		self.btlGeneral = False
		self.btlRawPart = False
		self.btlUid = False
		self.btlPart = True

		self.currentPart = tereoProcess.process_element()
		self.currentPart.interface = self
		return

	def parse_singlemembernumber(self, args):
		# integer
		if self.btlPart:
			self.currentPart.singleMemberNumber = args.strip('"')
		return

	def parse_assemblynumber(self, args):
		if self.btlPart:
			self.currentPart.assemblyNumber = args.strip('"')
		return

	def parse_ordernumber(self, args):
		if self.btlPart:
			self.currentPart.orderNumber = args.strip('"')
		return

	def parse_designation(self, args):
		if self.btlPart:
			self.currentPart.designation = args.strip('"')
		return

	def parse_annotation(self, args):
		if self.btlPart:
			self.currentPart.annotation = args.strip('"')
		return

	def parse_storey(self, args):
		if self.btlPart:
			self.currentPart.storey = args.strip('"')
		return

	def parse_group(self, args):
		if self.btlPart:
			self.currentPart.group = args.strip('"')
		return

	def parse_package(self, args):
		if self.btlPart:
			self.currentPart.package = args.strip('"')
		return

	def parse_material(self, args):
		if self.btlPart:
			mt = args.strip('"')
			self.currentPart.material = mt
			for mat in self.genePart.material:
				if mat == mt:
					return
			self.genePart.material.append(mt)

		return

	def parse_timbergrade(self, args):
		if self.btlPart:
			self.currentPart.timberGrade = args.strip('"')
		return

	def parse_qualitygrade(self, args):
		if self.btlPart:
			self.currentPart.qualityGrade = args.strip('"')
		return

	def parse_count(self, args):
		if self.btlPart:
			val = args.strip('"')
			val = val.lstrip('0')
			self.currentPart.count = int(val) # integer
		return

	def parse_length(self, args):
		if self.btlPart:
			val = float(args)
			val = self.scale2unit(val)
			self.currentPart.length = val
		return

	def parse_height(self, args):
		if self.btlPart:
			val = float(args)
			val = self.scale2unit(val)
			self.currentPart.height = val
		return

	def parse_width(self, args):
		if self.btlPart:
			val = float(args)
			val = self.scale2unit(val)
			self.currentPart.width = val
		return

	def parse_colour(self, args):
		if self.btlPart:
			self.currentPart.colour = args # colour
		return

	def parse_planninglength(self, args):
		if self.btlPart:
			self.currentPart.planingLength = args
		return

	def parse_startoffset(self, args):
		if self.btlPart:
			self.currentPart.startOffset = args
		return

	def parse_endoffset(self, args):
		if self.btlPart:
			self.currentPart.endOffset = args
		return

	def parse_uid(self, args):
		if self.btlUid:
			self.currentPart.uid.append(self.currentUid)
		self.btlUid = True
		#self.currentUid = btlUidObject()
		self.currentUid = int(args)
		return

	def parse_transformation(self, args):

		param_list = args.split(' ')
		pp_list={}
		for pl in param_list:
			pl = pl.strip()
			if pl != '':
				val1 = pl[3:]
				val1 = val1.strip('"')
				val1 = val1.lstrip('0')
				if val1 != '':
					val = float(val1)
					val = self.scale2unit(val)
					pp_list[pl[:2]]=val
				else:
					pp_list[pl[:2]]=0.0

		t = btlMatrix()
		t.ox = pp_list["OX"]
		t.oy = pp_list["OY"]
		t.oz = pp_list["OZ"]
		t.xx = pp_list["XX"]
		t.xy = pp_list["XY"]
		t.xz = pp_list["XZ"]
		t.yx = pp_list["YX"]
		t.yy = pp_list["YY"]
		t.yz = pp_list["YZ"]
		t.normalize()
		self.btlUid = False
		self.currentPart.transformations.append(t)
		self.currentPart.glmatrix.append(None)

		return

	def parse_camber(self, args):
		if self.btlPart:
			if self.btlUid:
				self.currentPart.uid.append(self.currentUid)
				self.btlUid = False
			self.currentPart.camber = args
		return

	def parse_partoffset(self, args):
		if self.btlPart:
			if self.btlUid:
				self.currentPart.uid.append(self.currentUid)
				self.btlUid = False
			self.currentPart.partOffset = args
		return

	def parse_processingquality(self, args):
		# AUTOMATIC,VISIBLE,FAST
		if self.btlProcess:
			self.currentProcess.processingQuality = args
			return


		if self.btlPart:
			if self.btlUid:
				self.currentPart.uid.append(self.currentUid)
				self.btlUid = False
			self.currentPart.processingQuality = args
        	return

		self.genePart.processingQuality = args
		return

	def parse_outline(self, args):
		if self.btlPart:
			if self.btlUid:
				self.currentPart.uid.append(self.currentUid)
				self.btlUid = False
			self.currentPart.outline = args
		return

	def parse_aperture(self, args):
		if self.btlPart:
			if self.btlUid:
				self.currentPart.uid.append(self.currentUid)
				self.btlUid = False
			self.currentPart.aperture = args
		return

	def parse_storeytype(self, args):
		if self.btlPart:
			if self.btlUid:
				self.currentPart.uid.append(self.currentUid)
				self.btlUid = False
			self.currentPart.storeyType = args # CEILING,ROOF, WALL
		return

	def parse_elementnumber(self, args):
		if self.btlPart:
			if self.btlUid:
				self.currentPart.uid.append(self.currentUid)
				self.btlUid = False
			self.currentPart.elementNumber = args.strip('"')
		return

	def parse_layer(self, args):
		if self.btlPart:
			if self.btlUid:
				self.currentPart.uid.append(self.currentUid)
				self.btlUid = False
			self.currentPart.layer = args #integer
		return

	def parse_modulenumber(self, args):
		if self.btlPart:
			if self.btlUid:
				self.currentPart.uid.append(self.currentUid)
				self.btlUid = False
			self.currentPart.moduleNumber = args.strip('"')
		return

	def parse_graindirection(self, args):
		if self.btlPart:
			if self.btlUid:
				self.currentPart.uid.append(self.currentUid)
				self.btlUid = False
			self.currentPart.grainDirection = args
		return

	def parse_refenreceside(self, args):
		if self.btlPart:
			if self.btlUid:
				self.currentPart.uid.append(self.currentUid)
				self.btlUid = False
			self.currentPart.referenceSide = args
		return

	def parse_processident(self, args):
		if self.btlRawPart:
			# rawpart not implemented
			return

		if self.btlProcess:
			self.currentProcess.processIdent = args
		return

	def parse_priority(self, args):
		if self.btlProcess:
			self.currentProcess.priority = args
		return

	def parse_process(self, args):
		# YES or NO
		if self.btlProcess:
			self.currentProcess.process = args
		return

	def parse_composite(self, args):
		# not implemented
		return

	def parse_type(self, args):
		# not implemented
		return

	def parse_end(self):
		print "the end"

		if self.btlPart:
			if self.btlProcess:
				self.currentProcess.makeproc()
				self.currentPart.process.append(self.currentProcess)
			if self.btlUid:
				self.currentPart.uid.append(self.currentUid)
			self.genePart.parts.append(self.currentPart)
			print "additem"

		return


def get_extension():
	return 'btl'


def open_file(filename,tereo):

	tereo.applog("Btl",0,"Opening "+filename)

	try:

		inter = btlInterface()

		with open(filename, 'r') as f:

			btlfile = f.readlines()


			for line in btlfile:

            	#print "--------"
				line = line.rstrip('\r')
				line = line.rstrip('\n')
				line = line.rstrip('\r')
				if len(line)>0 :
					#print line
					word_list = line.split(' ',1)
					#print word_list
					word = word_list[0]

					if len(word_list) > 1:
						arg = word_list[1]
						arg = arg.strip()
					else:
						arg = ''

					inter.parsing_dict[word](arg)

					if inter.btlError:
						tereo.applog("Btl",2,"Error parsing file "+filename)
						return

			f.close()

		inter.parse_end()
		inter.filename = filename
		tereo.applog("Btl",0,"File "+filename+" loaded successfully")
		tereo.process_list = inter.genePart
		tereo.execute_function("process","Update")

		return

	except (UnicodeError, LookupError, IOError):
		tereo.applog("Btl",2,"Coult not decode file "+filename)
	pass

	return

