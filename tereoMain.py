
import pygtk
pygtk.require("2.0")
import gtk
import os
import gobject
import tereoGui
import tereoEnvironment


class main_window(gtk.Window):

	def __init__(self, tereo):

		print "init"

		gtk.Window.__init__(self)

		self.set_position(gtk.WIN_POS_CENTER)
		self.set_default_size(640, 480)

		#self.set_resizable(False)
		self.set_decorated(False)

		self.tereo = tereo

		self.connect_object("destroy", self.on_mainwin_destroy, None)

		vbox1b = gtk.VBox(False, 0)
		self.add(vbox1b)
		vbox1b.show()
		titleFrame = gtk.Frame()
		vbox1b.pack_start(titleFrame,False,False,0)
		titleFrame.set_size_request(-1,52)
		titleFrame.show()


		configlv = gtk.ListStore(gobject.TYPE_STRING,gobject.TYPE_STRING)

		for filename in os.listdir(self.tereo.homedir+'/configs'):
			configlv.append(('',filename))

		imgCartouche = gtk.Image()
		imgCartouche.set_from_file('./media/cartouche.png')
		imgCartouche.set_size_request(-1,52)
		imgCartouche.show()
		titleFrame.add(imgCartouche)




		hpaned1 = gtk.HPaned()
		hpaned1.show()
		#<property name="can_focus">True</property>
		hpaned1.set_position(320)
		#<property name="position_set">True</property>
		vbox1b.pack_start(hpaned1,True,True,0)

		vbox2b = gtk.VBox()
		vbox2b.show()
		hpaned1.pack1(vbox2b,False,False)

		swConfigList = gtk.ScrolledWindow()
		swConfigList.show()

		vbox2b.pack_start(swConfigList,True,True,0)

		lvConfigList = gtk.TreeView(configlv)
		lvConfigList.show()
		lvConfigList.set_headers_visible(False)
		lvConfigList.connect("cursor-changed", self.on_mainwin_select)
		swConfigList.add(lvConfigList)

		configlstico = gtk.TreeViewColumn("Image")
		configlstcellpix = gtk.CellRendererPixbuf()
		#configlstcellpixname = gtk.CellRendererText()
		configlstico.pack_start(configlstcellpix,True)
		#todo
		#configlstico.add_attribute(configlstcellpix,"text",0)

		configlstname = gtk.TreeViewColumn("Name")
		configlstcellname = gtk.CellRendererText()
		configlstname.pack_start(configlstcellname,True)
		configlstname.add_attribute(configlstcellname,"text",1)

		lvConfigList.append_column(configlstico)
		lvConfigList.append_column(configlstname)



		table1 = gtk.Table(2,2)
		table1.show()
		table1.set_homogeneous(True)
		#<property name="column_spacing">4</property>
		#<property name="row_spacing">4</property>

		vbox2b.pack_start(table1,False,False,0)

		img = gtk.Image()
		img.set_from_file('./media/yes.png')
		img.show()
		btnConfig = gtk.Button("Open")
		btnConfig.set_image(img)
		btnConfig.set_size_request(-1,40)
		btnConfig.show()
		btnConfig.connect("clicked",self.on_mainwin_config)
		table1.attach(btnConfig,0,1,0,1)


		img = gtk.Image()
		img.set_from_file('./media/add.png')
		img.show()
		btnNew = gtk.Button("New")
		btnNew.set_image(img)
		btnNew.set_size_request(-1,40)
		btnNew.show()
		btnNew.connect("clicked",self.on_mainwin_new)
		table1.attach(btnNew,1,2,0,1)

		img = gtk.Image()
		img.set_from_file('./media/delete.png')
		img.show()
		btnDel = gtk.Button("Delete")
		btnDel.set_image(img)
		btnDel.set_size_request(-1,40)
		btnDel.show()
		btnDel.connect("clicked",self.on_mainwin_delete)
		table1.attach(btnDel,0,1,1,2)

		img = gtk.Image()
		img.set_from_file('./media/quit.png')
		img.show()
		btnQuit3 = gtk.Button("Quit")
		btnQuit3.set_image(img)
		btnQuit3.set_size_request(-1,40)
		btnQuit3.show()
		btnQuit3.connect("clicked",self.on_mainwin_quit)
		table1.attach(btnQuit3,1,2,1,2)


		vbox3b = gtk.VBox()
		vbox3b.show()
		#<property name="border_width">2</property>

		hpaned1.pack2(vbox3b,False,False)

		frmPreview = gtk.Frame()
		frmPreview.show()
		vbox3b.pack_start(frmPreview,False,False,0)

		self.imgscene = gtk.Image()
		self.imgscene.set_size_request(-1,200)
		self.imgscene.show()
		frmPreview.add(self.imgscene)

		swText = gtk.ScrolledWindow()
		swText.show()
		vbox3b.pack_start(swText)
		self.mainwintext = gtk.TextBuffer()
		tvText = gtk.TextView(self.mainwintext)
		swText.add(tvText)
		tvText.show()

		self.selectedconfig = ""

		

		self.show()

		print "end init"


		return


	def on_mainwin_destroy(self,widget):
		#print "destroy-----------------------------------------------"
		#gtk.main_quit()
		return



	def on_mainwin_config(self,widget):

		if self.selectedconfig == '':
			return

		# load ini file
		self.tereo.env = tereoEnvironment.environment()
		ret = self.tereo.env.load_env(self.selectedconfig, self.tereo.homedir)


		if ret == False:
			del self.tereo.env
			self.tereo.env = None
			return

		# connect robots
		#print "connect robots"
		#todo !!!!!!!!!!!!!!!!!!!!!!!
##		for r in self.env.robot:
##
##			if (r.ip != "") and (r.chan != 0):
##				#print "proto : " + r.proto
##				if r.proto == "lcnc":
##					self.applog("network",0,"init linuxcnc network")
##					r.client = tereoClient(False, None)
##					reactor.connectTCP(r.ip,r.chan, r.client)
##				elif r.proto == "cross":
##					self.applog("network",0,"init cross network")
##					r.client = rpclientsaa.rpcClient(r.ip)
##					if r.calc_ip != "0.0.0.0" and r.calc_chan > 0:
##						self.applog("network",0,"init calculator")
##						r.calculateur = tereoClient(True, r.calculate)
##						reactor.connectTCP(r.calc_ip,r.calc_chan, r.calculateur)
##					#r.client.start()
##				elif r.proto == "simu":
##					self.applog("network",0,"init simulator network")
##					#todo : activate
##					#r.client = tereoRobot.clientSimu()
##			else:
##				self.applog("network",1,"No ip adress or channel")

		self.hide()
		self.tereo.guiwin = tereoGui.gui_window(self.tereo)
		self.tereo.guiwin.attach_toolbar(self.tereo.toolbar)
		self.tereo.update_gui()
		self.tereo.connect_robots()
		self.tereo.doafter_choice()

		return

	def on_mainwin_quit(self,widget):
		self.tereo.save_settings()
		self.tereo.dobefore_quit()
		self.destroy()
		gtk.main_quit()

		return

	def responseToDialog(self,entry, dialog, response):
		dialog.response(response)

	def on_mainwin_new(self,widget):

		#base this on a message dialog
		dialog = gtk.MessageDialog(
			None,
			gtk.DIALOG_MODAL | gtk.DIALOG_DESTROY_WITH_PARENT,
			gtk.MESSAGE_QUESTION,
			gtk.BUTTONS_OK,
			None)

		dialog.set_markup('Please enter a configuration <b>name</b>:')
		dialog.set_position(gtk.WIN_POS_CENTER)
		#create the text input field
		entry = gtk.Entry()
		#allow the user to press enter to do ok
		entry.connect("activate", self.responseToDialog, dialog, gtk.RESPONSE_OK)
		#create a horizontal box to pack the entry and a label
		hbox = gtk.HBox()
		hbox.pack_start(gtk.Label("Name:"), False, 5, 5)
		hbox.pack_end(entry)
		#some secondary text
		dialog.format_secondary_markup("This will be the name of your working environment")
		#add it and show it
		dialog.vbox.pack_end(hbox, True, True, 0)
		dialog.show_all()
		#w = gtk.Window(gtk.WINDOW_TOPLEVEL)

		#go go go
		dialog.run()
		text = entry.get_text()
		if text != "":
			#todo find existing name
			self.tereo.env = tereoEnvironment.environment()
			self.tereo.env.create_env(text, self.tereo.homedir)

			dialog.destroy()
			#self.scene_init()

			while gtk.events_pending():
				gtk.main_iteration(False)

			self.hide()

			return

		dialog.destroy()


		return

	def on_mainwin_delete(self,widget):

		if self.selectedconfig == '':
			return


		dialog = gtk.Dialog('Are you sure you want to delete :'+self.selectedconfig+' ?', None, 0,
		(gtk.STOCK_CANCEL, gtk.RESPONSE_CANCEL,
		gtk.STOCK_NO, gtk.RESPONSE_NO,
		gtk.STOCK_YES, gtk.RESPONSE_YES))

		response = dialog.run()

		dialog.destroy()
		while gtk.events_pending():
			gtk.main_iteration(False)

		if response == gtk.RESPONSE_YES:

			# delete folder content
			folder = ""
			if sys.platform == 'win32':
				folder = self.tereo.homedir + "\\configs\\"+self.selectedconfig
			else:
				inifile = self.tereo.homedir + "/configs/"+self.selectedconfig

			for the_file in os.listdir(folder):
				file_path = os.path.join(folder, the_file)
				try:
					if os.path.isfile(file_path):
						os.unlink(file_path)
				except Exception, e:
					self.applog("file",2,e)
			try:
				os.rmdir(folder)
			except Exception, e:
				self.tereo.applog("File",2,e)
		return



	def on_mainwin_select(self,widget):

		model, iter = widget.get_selection().get_selected()
		conf = model.get_value(iter,1)
		self.selectedconfig = conf

		# readme
		readme = ''
		try:
			with open(self.tereo.homedir + '/configs/' + conf + '/README', 'r') as f:
				readmefile = f.readlines()
			for line in readmefile:
				readme = readme + line + '\r'
		except:
			self.tereo.applog("File",0,"No configuration readme file")

		self.mainwintext.set_text(readme)

		# thumbnail
		pixbuf = None
		try:
			pixbuf = gtk.gdk.pixbuf_new_from_file(self.tereo.homedir + '/configs/' + conf + '/thumbnail.png')
			if pixbuf != None:
				scaled_buf = pixbuf.scale_simple(306,200,gtk.gdk.INTERP_BILINEAR)
				self.imgscene.set_from_pixbuf(scaled_buf)
				del scaled_buf
			del pixbuf
		except:
			self.tereo.applog("File",0,"No configuration thumbnail")

		return



