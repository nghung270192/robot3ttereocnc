#!/usr/bin/python

import pygtk
pygtk.require("2.0")
import gtk
#import gtk.gdkgl as gdkgl
#import gtk.gtkgl as gtkgl
#from OpenGL.GL import *
#from OpenGL.GLU import *
#import tereoGL


class CoordsBase(object):
	def __init__(self, *args):
		self._coords = args
		#self.q = gluNewQuadric()
	def coords(self):
		return self._coords

class AsciiOBJ:
    def __init__(self, filename=None, data=None):
        if data is None:
            data = open(filename, "r")
        elif isinstance(data, str):
            data = data.split("\n")

        self.v = v = []
        self.vn = vn = []
        self.f = f = []
        for line in data:
            if line.startswith("#"): continue
            if line.startswith("vn"):
                vn.append([float(w) for w in line.split()[1:]])
            elif line.startswith("v"):
                v.append([float(w) for w in line.split()[1:]])
            elif line.startswith("f"):
                f.append(self.parse_face(line))

#        print v[:5]
#        print vn[:5]
#        print f[:5]

        self.list = None


    def parse_int(self, i):
        if i == '': return None
        return int(i)

    def parse_slash(self, word):
        return [self.parse_int(i) for i in word.split("/")]

    def parse_face(self, line):
        return [self.parse_slash(w) for w in line.split()[1:]]

    def draw(self):
        if self.list is None:
            # OpenGL isn't ready yet in __init__ so the display list
            # is created during the first draw
            self.list = glGenLists(1)
            glNewList(self.list, GL_COMPILE)
            #glEnable(GL_CULL_FACE)
            #glCullFace(GL_BACK)
            glBegin(GL_TRIANGLES)
            #print "obj", len(self.f)
            for f in self.f:
                for v, t, n in f:
                    if n:
                        glNormal3f(*self.vn[n-1])
                    t = self.v[v-1]
                    #glVertex3f(*self.v[v-1])
                    glVertex3f(t[0],t[1],t[2])
            glEnd()
            glEndList()
            del self.v
            del self.vn
            del self.f
        glCallList(self.list)


class Collection(object):
    def __init__(self, parts):
	self.parts = parts
	self.vol = 0

    def traverse(self):
	for p in self.parts:
	    if hasattr(p, "apply"):
		p.apply()
	    if hasattr(p, "capture"):
		p.capture()
	    if hasattr(p, "draw"):
		p.draw()
	    if hasattr(p, "traverse"):
		p.traverse()
	    if hasattr(p, "unapply"):
		p.unapply()

    def volume(self):
	if hasattr(self, "vol") and self.vol != 0:
	    vol = self.vol
	else:
	    vol = sum(part.volume() for part in self.parts)
	#print "Collection.volume", vol
	return vol

    # a collection consisting of overlapping parts will have an incorrect
    # volume, because overlapping volumes will be counted twice.  If the
    # correct volume is known, it can be set using this method
    def set_volume(self,vol):
	self.vol = vol;

class Translate(Collection):
    def __init__(self, parts, x, y, z):
	self.parts = parts
	self.where = x, y, z

    def apply(self):
	glPushMatrix()
	#print "translate : %f"%(self.where[0]) + " %f"%(self.where[1]) + " %f"%(self.where[2])
	glTranslatef(*self.where)

    def unapply(self):
	glPopMatrix()

class Wireframe(Collection):
	def __init__(self, parts):
		self.parts = parts

	def apply(self):
		glPolygonMode( GL_FRONT_AND_BACK, GL_LINE )

	def unapply(self):
		glPolygonMode( GL_FRONT_AND_BACK, GL_FILL )

class Tag(Collection):
	def __init__(self, parts):
		self.parts = parts

class Scale(Collection):
    def __init__(self, parts, x, y, z):
	self.parts = parts
	self.scaleby = x, y, z

    def apply(self):

	glPushMatrix()
	glScalef(*self.scaleby)

    def unapply(self):

	glPopMatrix()

class HalTranslate(Collection):
    def __init__(self, parts, comp, var, x, y, z):
	self.parts = parts
	self.where = x, y, z
	self.comp = comp
	self.var = var

    def apply(self):
	x, y, z = self.where
	v = self.comp[self.var]

	glPushMatrix()
	glTranslatef(x*v, y*v, z*v)

    def unapply(self):

	glPopMatrix()


class HalRotate(Collection):
    def __init__(self, parts, comp, var, th, x, y, z):
	self.parts = parts
	self.where = th, x, y, z
	self.comp = comp
	self.var = var

    def apply(self):
	th, x, y, z = self.where

	glPushMatrix()
	glRotatef(th * self.comp[self.var], x, y, z)
	#print "halrotate : "
	#print self.comp
	#print self.var
	#print self.comp[self.var]

    def unapply(self):

	glPopMatrix()

class Matrix(Collection):
	def __init__(self,part, mat):
		self.parts = part
		self.mat = mat

	def apply(self):
		glPushMatrix()
		glMultMatrixf(self.mat)

	def unapply(self):
		glPopMatrix()

class Rotate(Collection):
    def __init__(self, parts, th, x, y, z):
	self.parts = parts
	self.where = th, x, y, z

    def apply(self):
	th, x, y, z = self.where

	glPushMatrix()
	glRotatef(th, x, y, z)

    def unapply(self):

	glPopMatrix()


# six coordinate version - specify each side of the box
class Box(CoordsBase):
	def draw(self):

		x1, y1, z1, x2, y2, z2 = self.coords()
		if x1 > x2:
			tmp = x1
			x1 = x2
			x2 = tmp
		if y1 > y2:
			tmp = y1
			y1 = y2
			y2 = tmp
 		if z1 > z2:
			tmp = z1
			z1 = z2
			z2 = tmp

		glBegin(GL_QUADS)
		# bottom face
		glNormal3f(0,0,-1)
		glVertex3f(x2, y1, z1)
		glVertex3f(x1, y1, z1)
		glVertex3f(x1, y2, z1)
		glVertex3f(x2, y2, z1)
		# positive X face
		glNormal3f(1,0,0)
		glVertex3f(x2, y1, z1)
		glVertex3f(x2, y2, z1)
		glVertex3f(x2, y2, z2)
		glVertex3f(x2, y1, z2)
		# positive Y face
		glNormal3f(0,1,0)
		glVertex3f(x1, y2, z1)
		glVertex3f(x1, y2, z2)
		glVertex3f(x2, y2, z2)
		glVertex3f(x2, y2, z1)
		# negative Y face
		glNormal3f(0,-1,0)
		glVertex3f(x2, y1, z2)
		glVertex3f(x1, y1, z2)
		glVertex3f(x1, y1, z1)
		glVertex3f(x2, y1, z1)
		# negative X face
		glNormal3f(-1,0,0)
		glVertex3f(x1, y1, z1)
		glVertex3f(x1, y1, z2)
		glVertex3f(x1, y2, z2)
		glVertex3f(x1, y2, z1)
		# top face
		glNormal3f(0,0,1)
		glVertex3f(x1, y2, z2)
		glVertex3f(x1, y1, z2)
		glVertex3f(x2, y1, z2)
		glVertex3f(x2, y2, z2)
		glEnd()

	def volume(self):
		x1, y1, z1, x2, y2, z2 = self.coords()
		vol = abs((x1-x2)*(y1-y2)*(z1-z2))
		#print "Box.volume", vol
		return vol

# capture current transformation matrix
# note that this tranforms from the current coordinate system
# to the viewport system, NOT to the world system
class Capture(object):
    def __init__(self):
		self.t = [0,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0]

    def capture(self):
		a = glGetDoublev(GL_MODELVIEW_MATRIX)
		a = a[:]
		b = []
		for c in a:
			b=b+c[:]
		self.t = b
		return

    def volume(self):
		return 0.0

class Color(Collection):
	def __init__(self, color, parts):
		self.color = color
		Collection.__init__(self, parts)

	def apply(self):
		glPushAttrib(GL_LIGHTING_BIT)
		glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE, self.color)

	def unapply(self):
		glPopAttrib()
		return



class frame():

	def __init__(self): #, model, tool, work, size=10, hud=0, rotation_vectors=None, lat=0, lon=0):
		self.r_back = self.g_back = self.b_back = 0
		self.plotdata = []
		self.plotlen = 4000
		##self.hud = Hud()

		self.lat=0
		self.lon=0
		self.mousex = 0
		self.mousey = 0
		self.minlat = -180
		self.maxlat = 180
		self.rotating = False
		self.glarea = None


		#if rotation_vectors: self.rotation_vectors = rotation_vectors
		##self.set_latitudelimits(-180,180)
		##self.after(100, lambda: self.set_viewangle(lat,lon))
		##global HUD
		##HUD = 0
		##if(hud != 0 and hasattr(hud,"app")):
		##	HUD = hud
				#point ou app at the global
		##self.hud = HUD
		##self.hud.app = self
		world = Capture()
		tool = Capture()
		work = Capture()
		self.model = [] #Collection([model,world])
		self.size = 60.0 #float(size)
		self.distance = self.size * 5.0
		self.near = 0.06 # float(size) * 0.01
		self.far = float(self.size) * 10.0
		self.tool2view = tool
		self.world2view = world
		self.work2view = work
		##self.pack(fill="both", expand=1)

		return

	def clear(self):
		self.model = []
		return

	def attach_frame(self,widget):

		glscene_box = gtk.VBox (homogeneous=False, spacing=0)
		widget.pack_start (glscene_box,True,True,0)
		widget.reorder_child(glscene_box,0)
		glscene_box.show ()
		glconfig = gdkgl.Config(mode = (gtk.gdkgl.MODE_RGBA | gtk.gdkgl.MODE_DOUBLE | gtk.gdkgl.MODE_DEPTH))
		self.glarea = gtkgl.DrawingArea (glconfig)
		self.glarea.add_events(gtk.gdk.ALL_EVENTS_MASK)
		self.glarea.connect ('expose-event', self.redraw)
		self.glarea.connect ('scroll-event', self.gl_zoomwheel)
		self.glarea.connect ('button-press-event', self.gl_startrotate)
		self.glarea.connect ('button-release-event', self.gl_endrotate)
		self.glarea.connect ('motion-notify-event', self.gl_rotate)
		glscene_box.pack_start (child=self.glarea, expand=True)
		self.glarea.show ()

		return

	#######################################################
	#	3D navigation
	#######################################################

	def gl_startrotate(self, widget, event):

		self.rotating = True
		self.mousex = event.x
		self.mousey = event.y

	def gl_endrotate(self, widget, event):

		self.rotating = False

	def gl_rotate(self, widget, event):

		if self.rotating == False: return

 		s = 0.05

		self.lat = min(self.maxlat, max(self.minlat, self.lat + (event.y - self.mousey) * s))
		self.lon = (self.lon + (event.x - self.mousex) * s) % 360
		self.glarea.queue_draw()
		return

	def gl_zoomwheel(self, widget, event):
		#print "zoomwheel"

		if event.direction == gtk.gdk.SCROLL_UP:
			#zoomin
			self.distance = self.distance / 1.1
		else:
			#zoomout
			self.distance = self.distance * 1.1
		self.glarea.queue_draw()

	def detach2object(self,o):
		#print "detach to object"
		for m in self.model:
			for i in range(len(m.parts)):
				p = m.parts[i]
				#print p #type(p).__name__
				#if type(p).__name__ == "Collection":
				if hasattr(p,"parts") == True:
					r = self.recursive_detach2object(m.parts,i,o)
					if r == True:
						return
				if p == o:
					print "ok found"
					#m = p
					del m.parts[i]
					return
		return

	def recursive_detach2object(self,par,idx,o):
		for i in range(len(par[idx].parts)):
			p = par[idx].parts[i]
			if hasattr(p,"parts") == True:
				ret = self.recursive_detach2object(par[idx].parts,i,o)
				if ret == True:
					return True
			if p == o:
				print "ok found r"
				#par[idx] = p
				del par[idx].parts[i]
				return True
		return False

	def attach2object(self,o,mo):
		#print "attach to object"
		for m in self.model:
			for i in range(len(m.parts)):
				p = m.parts[i]
				#print p #type(p).__name__
				#if type(p).__name__ == "Collection":
				if hasattr(p,"parts") == True:
					r = self.recursive_attach2object(p,o,mo)
					if r == True:
						return
				if p == o:
					#print "ok found"
					m.parts.append(Collection([p,mo]))
					del m.parts[i]
		return

	def recursive_attach2object(self,r,o,m):
		for i in range(len(r.parts)):
			p = r.parts[i]
			if hasattr(p,"parts") == True:
				ret = self.recursive_attach2object(p,o,m)
				if ret == True:
					return True
			if p == o:
				#print "ok found r"
				r.parts.append(Collection([p,m]))
				del r.parts[i]
				return True
		return False


	def addmodel(self,m):
		#print "add model"
		self.model.append(m)
		return

	def removemodel(self,m):
		#print "remove m:"
		#print "range %d"%(len(self.model))

		for i in range(len(self.model)):
			#print "i %d"%(i)
			if self.model[i] == m:
				print "found"
				del self.model[i]

	def basic_lighting(self):
		#self.activate()
		glLightfv(GL_LIGHT0, GL_POSITION, (-8, 10, 5, 0))
		glLightfv(GL_LIGHT0, GL_AMBIENT, (0.3,0.3,0.3,0))
		glLightfv(GL_LIGHT0, GL_DIFFUSE, (.4,.4,.4,0))

		glLightfv(GL_LIGHT0+1, GL_POSITION, (-1, -1, 1.5, 0))
		glLightfv(GL_LIGHT0+1, GL_AMBIENT, (.0,.0,.0,1))
		glLightfv(GL_LIGHT0+1, GL_DIFFUSE, (.0,.0,.4,0))
		#glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, (1.0,1.0,1.0,1.0))

		###glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE, (0.6,0.5,0.4,1.0))

		#glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT, (0.05,0.05,0.05,1.0))
		#glMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, (1.0,0.5,0.8,1.0))
		#glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, (1.0,1.0,1.0,1.0))
		#glMaterialf(GL_FRONT_AND_BACK, GL_SHININESS, (5.0))
		#glMaterialfv(GL_FRONT_AND_BACK, GL_EMISSION, (0.0,0.0,0.0,1.0))
		glEnable(GL_CULL_FACE)
		glCullFace(GL_BACK)
		glEnable(GL_LIGHTING)
		glEnable(GL_LIGHT0)
		#glEnable(GL_LIGHT0+1)


		#glMatrixMode(GL_MODELVIEW)
		#glLoadIdentity()
		return

#	def drawWorldAxis(self):
#		# x
#		glColor3f(1.0,0.0,0.0)
#		glBegin (GL_LINES)
#		glVertex ( 0.0, 0.0, 0.0)
#		glVertex ( 0.8, 0.0, 0.0)
#		glVertex ( 1.1, 0.0, 0.1)
#		glVertex ( 1.3, 0.0, -0.1)
#		glVertex ( 1.1, 0.0, -0.1)
#		glVertex ( 1.3, 0.0, 0.1)
#		glEnd ()
#		glBegin (GL_TRIANGLES)
#		glVertex ( 0.8, 0.0, 0.1)
#		glVertex ( 1.0, 0.0, 0.0)
#		glVertex ( 0.8, 0.0, -0.1)
#		glEnd ()
#
#		# y
#		glColor3f(0.0,1.0,0.0)
#		glBegin (GL_LINES)
#		glVertex ( 0.0, 0.0, 0.0)
#		glVertex ( 0.0, 0.8, 0.0)
#		glVertex ( 0.0, 1.1, 0.0)
#		glVertex ( 0.0, 1.2, 0.0)
#		glVertex ( 0.0, 1.2, 0.0)
#		glVertex ( -0.0707, 1.3, 0.0707)
#		glVertex ( 0.0, 1.2, 0.0)
#		glVertex ( 0.0707, 1.3, -0.0707)
#		glEnd ()
#		glBegin (GL_TRIANGLES)
#		glVertex ( 0.0707, 0.8, -0.0707)
#		glVertex ( 0.0, 1.0, 0.0)
#		glVertex ( -0.0707, 0.8, 0.0707)
#		glEnd ()
#
#		# z
#		glColor3f(0.0,0.0,1.0)
#		glBegin (GL_LINES)
#		glVertex ( 0.0, 0.0, 0.0)
#		glVertex ( 0.0, 0.0, 0.8)
#		glVertex ( 0.1, 0.0, 1.1)
#		glVertex ( -0.1, 0.0, 1.1)
#		glVertex ( -0.1, 0.0, 1.3)
#		glVertex ( 0.1, 0.0, 1.1)
#		glVertex ( -0.1, 0.0, 1.3)
#		glVertex ( 0.1, 0.0, 1.3)
#		glEnd ()
#		glBegin (GL_TRIANGLES)
#		glVertex ( 0.0, 0.0, 1.0)
#		glVertex ( 0.1, 0.0, 0.8)
#		glVertex ( -0.1, 0.0, 0.8)
#		glEnd ()
#		return

	def redraw(self,area,_):

		drawable = area.get_gl_drawable ()
		context = area.get_gl_context ()

		if not drawable.gl_begin (context):
			return


		glClearColor(1.0, 1.0, 1.0, 0.0)
		glClearDepth(1.0)
		glDepthFunc(GL_LESS)
		glEnable(GL_DEPTH_TEST)
		glShadeModel(GL_SMOOTH)



		# Projection using perspective and a few units backward.
		self.basic_lighting()


		glMatrixMode (GL_PROJECTION)
		glLoadIdentity ()


		allocation = area.get_allocation ()
		viewport_width = float (allocation.width)
		viewport_height = float (allocation.height)
		aspect =  viewport_width / viewport_height
		fovy   =  35.0 # The one which looks to most natural.
		#z_near =   2.0 # Enough for a moderately sized sample model.
		#z_far  =  -2.0 # Idem.
		gluPerspective (fovy, aspect, self.near, self.far)

		# Model defined in default coordinates.
		glMatrixMode (GL_MODELVIEW)


		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);



		glViewport (0, 0, int (viewport_width), int (viewport_height))

		glClear (GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)

		# Model defined in default coordinates.
#		glMatrixMode (GL_MODELVIEW)
#		glLoadIdentity ()


		glLoadIdentity ()




		#print "lat:%f"%(self.lat)
		#print "lon:%f"%(self.lon)
		glRotatef(tereoGL.snap(self.lat), 1.0, 0.0, 0.0)
		glRotatef(tereoGL.snap(self.lon), 0.0, 1.0, 0.0)


		projection_dx =  0.0
		projection_dy =  -self.distance/5.0
		projection_dz = -self.distance*1.2

		glTranslate (projection_dx, projection_dy, projection_dz)
		#glRotated((0.0 - 35.0),  -1.0,  0.0,  0.0)
		#glRotated((0.0 + 45.0),  0.0, -1.0,  0.0)
		glRotated(-60.0,  1.0,  0.0,  0.0)
		glRotated(45.0,  0.0, 0.0,  1.0)

		#self.size=50.0
		#ratio = 2.0
		#glScalef(1/ratio,1/ratio,1/ratio)

		##if self.winfo_width() == 1: return
		#print "size:%d"%(len(self.model))
		for m in self.model:

		    if hasattr(m, "apply"):
			m.apply()
		    if hasattr(m, "capture"):
			m.capture()
		    if hasattr(m, "draw"):
			m.draw()
		    if hasattr(m, "traverse"):
			m.traverse()
		    if hasattr(m, "unapply"):
			m.unapply()



		view2world = invert(self.world2view.t)
		view2work = invert(self.work2view.t)
		tx, ty, tz = self.tool2view.t[12:15]
		wx = tx*view2work[0]+ty*view2work[4]+tz*view2work[8]+view2work[12]
		wy = tx*view2work[1]+ty*view2work[5]+tz*view2work[9]+view2work[13]
		wz = tx*view2work[2]+ty*view2work[6]+tz*view2work[10]+view2work[14]
		#if len(self.plotdata) == self.plotlen:
		#	del self.plotdata[:self.plotlen / 10]
		#point = [ wx, wy, wz ]
		#if not self.plotdata or point != self.plotdata[-1]:
		#	self.plotdata.append(point)


		glPushMatrix()
		glMultMatrixd(view2world)
		glMultMatrixd(self.work2view.t)
		##if(hasattr(self.hud, "draw")):
		##	self.hud.draw()

##		glDisable(GL_LIGHTING)
##		glLineWidth(2)
##		glColor3f(1.0,0.5,0.5)
##		glBegin(GL_LINE_STRIP)
##		for p in self.plotdata:
##			glVertex3f(*p)
##		glEnd()
		glEnable(GL_LIGHTING)


		glColor3f(1,1,1)
		glLineWidth(1)
		glDisable(GL_BLEND)
		glDepthFunc(GL_LESS)

		# back to world again

		glPopMatrix()

		glDisable(GL_LIGHTING)

		glPushMatrix()
		glScalef(self.size*2,self.size*2,self.size*2)
		tereoGL.drawWorldAxis()

		glPopMatrix()


		# floor grid
		glColor3f(0.5,0.5,0.5)

		for x in range(-int(self.size/4.0),int(self.size/4.0)):
			glBegin (GL_LINES)
			glVertex3f ( -self.size*2, x * 8.0, 0.0)
			glVertex3f ( self.size*2, x * 8.0, 0.0)
			glVertex3f ( x * 8.0, -self.size*2, 0.0)
			glVertex3f ( x * 8.0, self.size*2, 0.0)
			glEnd ()

		glEnable(GL_LIGHTING)

		drawable.swap_buffers ()
		drawable.gl_end ()

		return

# function to invert a transform matrix
# based on http://steve.hollasch.net/cgindex/math/matrix/afforthinv.c
# with simplifications since we don't do scaling

# This function inverts a 4x4 matrix that is affine and orthogonal.  In
# other words, the perspective components are [0 0 0 1], and the basis
# vectors are orthogonal to each other.  In addition, the matrix must
# not do scaling

def invert(src):
	# make a copy
	inv=src[:]
	# The inverse of the upper 3x3 is the transpose (since the basis
	# vectors are orthogonal to each other.
	inv[1],inv[4] = inv[4],inv[1]
	inv[2],inv[8] = inv[8],inv[2]
	inv[6],inv[9] = inv[9],inv[6]
	# The inverse of the translation component is just the negation
	# of the translation after dotting with the new upper3x3 rows. */
	inv[12] = -(src[12]*inv[0] + src[13]*inv[4] + src[14]*inv[8])
	inv[13] = -(src[12]*inv[1] + src[13]*inv[5] + src[14]*inv[9])
	inv[14] = -(src[12]*inv[2] + src[13]*inv[6] + src[14]*inv[10])
	return inv



class Triangle(CoordsBase):
    def draw(self):

		x1, y1, z1, x2, y2, z2, x3, y3, z3, n1, n2, n3 = self.coords()
		glBegin(GL_TRIANGLES)
		glNormal3f(n1,n2,n3)
		glVertex3f(x1, y1, z1)
		glVertex3f(x2, y2, z2)
		glVertex3f(x3, y3, z3)
		glEnd()
		return

class Line(CoordsBase):
	def draw(self):

		x1, y1, z1, x2, y2, z2 = self.coords()
		glBegin(GL_LINES)
		glVertex3f(x1, y1, z1)
		glVertex3f(x2, y2, z2)
		glEnd()
