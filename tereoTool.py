#!/usr/bin/python

import pygtk
pygtk.require('2.0')
import gtk
#import gtk.gdkgl as gdkgl
#import gtk.gtkgl as gtkgl
from tereoMath import *
import math

import xml.etree.ElementTree as ET

############## TOOL ###############


def get_from_db(db):

	col = []

	for t in db.findall('TOOL'):

		to = toolObj()

		to.name = t.findtext('NAME')
		to.group = t.findtext('GROUP')
		p = t.findtext('NUMBER')
		if p:
			to.index = int(p)

		p = t.findtext('MATRIX')
		if p:
			wo = p.split(' ')
			for w in wo:
				w.strip()
			to.matrix = matrix44()
			to.matrix.from_array([ float(wo[0]) , float(wo[1]) ,float(wo[2]) , float(wo[3]) , float(wo[4]) , float(wo[5]),float(wo[6]),float(wo[7]),float(wo[8]),float(wo[9]),float(wo[10]),float(wo[11]),float(wo[12]),float(wo[13]),float(wo[14]),float(wo[15]) ])

		col.append(to)

	return col

def set_to_db(db,tools):



	# delete all tools
	if hasattr(db,"getroot"):
		ter = db.getroot()
	else:
		ter = db

	t=ter.find("TOOL")
	while t != None :
		ter.remove(t)
		t = ter.find("TOOL")


	for to in tools:

		t = ET.SubElement(ter,'TOOL')
		if t != None:
			t.set('NUMBER',str(to.index))
			t.set('NAME',str(to.name))
			t.set('GROUP',str(to.group))
			t.set('MATRIX',to.matrix.dump2())


	return


class toolObj():
	def __init__(self):
		self.TYPE_DRILL = 0
		self.TYPE_MILL = 1
		self.type = self.TYPE_DRILL
		self.diameter = 0.0
		self.overall_length = 0.0
		self.usable_length = 0.0
		self.point_length = 0.0
		self.cutting_diameter = 0.0
		self.pocket_number = 0
		self.group = ''
		self.name = ''
		self.homedir = ''
		self.index = 0
		self.frame = frame6()   #X 55.75440, Y -256.4340, Z 506.4290, A 0.0, B 0.0, C 0.0
		self.matrix = matrix44()

		return

	def get_from_file(self):

		try:
			with open(self.homedir + '/' + self.group + '/' + self.name, 'r') as f:
				toolfile = f.readlines()
				for line in toolfile:
					line = line.rstrip('\r')
					line = line.rstrip('\n')
					line = line.rstrip('\r')
					word_list = line.split(' ',1)
					#print word_list
					word = word_list[0]

					if len(word_list) > 1:
						arg = word_list[1]
						arg = arg.strip()
					else:
						arg = ''

					if word == 'DIAMETER':
						self.diameter = float(arg)
					elif word == 'OVERALL_LENGTH':
						self.overall_length = float(arg)
					elif word == 'POINT_LENGTH':
						self.point_length = float(arg)
					elif word == 'USABLE_LENGTH':
						self.usable_length = float(arg)
					elif word == 'CUTTING_DIAMETER':
						self.cutting_diameter = float(arg)
					elif word == 'TYPE_DRILL':
						self.type = self.TYPE_DRILL
					elif word == 'TYPE_MILL':
						self.type = self.TYPE_MILL

			return;
		except (UnicodeError, LookupError, IOError):
			print "coult not decode file"
		pass

		return


	def showparams(self,widget):

		widget.clear()
		if self.type == self.TYPE_DRILL:
			widget.append(('1',"%g"%round(self.cutting_diameter/2.54,2),self.cutting_diameter))
			widget.append(('2',"%g"%round(self.overall_length/2.54,2),self.overall_length))
			widget.append(('3',"%g"%round(self.usable_length/2.54,2),self.usable_length))
			widget.append(('4',"%g"%round(self.point_length/2.54,2),self.point_length))
		elif self.type == self.TYPE_MILL:
			widget.append(('1',"%g"%round(self.cutting_diameter/2.54,2),self.cutting_diameter))
			widget.append(('2',"%g"%round(self.usable_length/2.54,2),self.usable_length))
			widget.append(('3',"%g"%round(self.overall_length/2.54,2),self.overall_length))
			widget.append(('4',"%g"%round(self.diameter/2.54,2),self.diameter))
		return

