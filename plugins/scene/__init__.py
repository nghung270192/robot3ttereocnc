# -*- coding: utf-8 -*-


import imp
import os.path


pluginName = "Process"
version = "0.1"
author = "Olivier Martinez"

scene_icon = None


def register(parent):

	mod = imp.load_source('scene', './plugins/scene/scene.py')

	parent.add_object('gui.frame','Scene 3D',mod.new_scene_frame)

	return
