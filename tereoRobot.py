#!/usr/bin/python

import tereoEnvironment
from tereoProcess import *
import collections
import Queue
import math
import tereoMath
import gobject

import xml.etree.ElementTree as ET


class robotAction():
	def __init__(self,pos = None, base = -1, speed = -1, command = "", param = None):
		self.pos = pos
		self.base = base
		self.speed = speed
		self.command = command
		self.param = param



class robotLink():
	def __init__(self):
		self.index = 0
		self.file = ""
		self.type = ""
		self.translate = [0.0,0.0,0.0]
		self.axis = [0,0,0]
		self.axislink = ""
		self.color = [0.0,0.0,0.0,1.0]
		self.matrixz = tereoMath.matrix44()
		self.a = 0.0 # distance Zi ? Zi+1
		self.alpha = 0.0 #angle de torsion autour de X entre Zi et Zi+1
		self.d = 0.0 # distance de Xi-1 ? Xi  le long de Z
		self.theta = 0.0 # angle de rotation entre Xi-1 et Xi autour de Z
		self.forward = tereoMath.matrix44()

class robotobj():
	def __init__(self):
		self.index = 0
		self.axes = 0
		#for i in range(0,7):
		#	self.axis.append(robotAxis())
		self.model = ''
		self.constructor = ''
		self.name = ''
		self.ip = "0.0.0.0"
		self.chan = 8123
		self.calc = ""
		self.calc_ip = "0.0.0.0"
		self.calc_chan = 8123
		self.proto = "simu"
		self.home = tereoMath.frame6()
		self.dir = ''
		self.position_matrix = tereoMath.matrix44()
		self.mesh = None
		self.halcomp = None
		self.joggeur = None
		self.tool = None
		self.attached_object = None
		self.db = None
		self.client = None
		self.calculateur = None
		self.toolname = ''
		self.last_coords = tereoMath.frame6()
		self.last_speed = 0.0
		self.links = []
		self.number = 0
		self.env = None
		self.selected_tool = None
		self.vmodel = None
		self.calib_state = 0
		self.calib_count = 0
		self.calib_x = 0.0
		self.calib_y = 0.0
		self.calib_z = 0.0
		self.calib_haut = []
		self.calib_gauche = []
		self.calib_droite = []
		self.calib_loin = []
		self.calib_proche = []

		self.FUNC_NONE = 0
		self.FUNC_MOVE = 1
		self.FUNC_ROTATE = 2
		self.FUNC_MOVELIN = 3
		self.FUNC_PROBE = 4
		self.FUNC_CONTACT = 5
		self.FUNC_PREHEN = 6
		self.FUNC_NAIL = 7
		self.FUNC_DEPREHEN = 8


		self.last_func = self.FUNC_NONE
		self.last_base_dir = 0

		self.joggeur = {'X':0, 'Y':0, 'Z':0, 'A':0, 'B':0, 'C':0, 'U':0, 'V':0, 'W':0}

		return

	def get_db_value(self,section,prop,prop_condition="",val_condition=""):

		if hasattr(self.db,"getroot"):
			db = self.db.getroot()
		else:
			db = self.db

		for s in db.findall(section):
			if s != None:
				if prop_condition != "":
					c = s.get(prop_condition)
					if c == val_condition:
						p = s.get(prop)
						if p != None:
							return p
						else:
							return ""
				else:
					p = s.get(prop)
					if p != None:
						return p
					else:
						return ""

		return ""

	def execute_action(self,func,action):
		#print "funct:"+str(func)
		#print "action:"+str(action)
		if func=="send":
			if action=="command state 2":
				print "envoi estop"
				
				self.client.set_estop(True)
			if action=="command state 1":
				self.client.set_estop(False)
		return

	def transform(self,f,base):

		return f
		#return [x,y,z,a,b,c]

		frm = tereoMath.frame()

		if base>-1:

			t = self.env.flow[base].matrix
			print "compensation matrix"
			print t

			frm.x = f.x*t[0]+f.y*t[4]+f.z*t[8]+t[12]
			frm.y = f.x*t[1]+f.y*t[5]+f.z*t[9]+t[13]
			frm.z = f.x*t[2]+f.y*t[6]+f.z*t[10]+t[14]

			print "frm avant tool matrix"
			print frm

			#f=tereoMath.frame6(frm[0], frm[1], frm[2], a, b, c)
			#print "tool matrix"
			f.x = frm[0]
			f.y = frm[1]
			f.z = frm[2]

		print "f avant tool matrix"
		print f.dump()

		if self.selected_tool != None:
			tm = self.selected_tool.matrix
			print tm
			#ftm = tereoMath.matrix_to_frame6(tm)
			#ftm.dump()

			f.dump()
			m6 = tereoMath.frame6_to_matrix(f)
			print m6
			mul = tereoMath.matrix_mult_matrix2(m6,tm)
			print mul
			f2 = tereoMath.matrix_to_frame6(mul)

			frm.x = f2.x
			frm.y = f2.y
			frm.z = f2.z
			frm.a = f.a
			frm.b = f.b
			frm.c = f.c

			print "frm apres tool matrix"
			print frm

			return frm

		else:
			print "selected tool = none"
			return f

	def end_process(self):
		# end of the current process for kuka cabinet
		# to send all mouvements in one shot
		print "end process"
		if self.proto=="cross":
			self.client.instance.end_process()
		elif self.proto == "simu":
			self.client.waiting_job = True
		return

	def attach_object(self,ao,vm):
		print "robot attach object"
		if ao == None:
			print "model null"
			return

		self.attached_object = ao

		v = self.last_coords

##		# cancel tool compensation
		w = [0,-6.88975,3.838575]

		if v[3] != 0:
			myRotate(w,v[3],1,0,0)
		if v[4] != 0:
			myRotate(w,v[4],0,1,0)
		if v[5] != 0:
			myRotate(w,v[5],0,0,1)


		dum = Box(-1.0,-1.0,0.0,1.0,1.0,50.0)
		dum = Color([1.0,0.0,0.0,1.0],[dum])

##		vm.addmodel(dum)

		lumber = Box(0,0,0,ao.length,ao.width,ao.height)
		lumber = Wireframe([lumber])
		lumber = Color([0.0,0.0,0.0,1.0],[lumber])
		#lumber = Collection([dum,lumber])

		ao.topmodel = lumber


##		# tool compensation
##		if self.toolname == '/tool.obj':
##			print "tool"
##			lumber = Translate([lumber],35.433,0,-6.6929)
##			lumber = Rotate([lumber],90,1,0,0)
##		elif self.toolname == '/scieclou.obj':
##			print "scieclou"
##			lumber = Translate([lumber],2.95275,0,2.95275)
##			lumber = Rotate([lumber],-90,1,0,0)



		ao.x_angle = ao.x_angle -90 #+ v[3]
		ao.y_angle = ao.y_angle #homing + v[4]
		ao.z_angle = ao.z_angle #+ v[5]

		if ao.x_angle != 0:
			lumber = Rotate([lumber],ao.x_angle,1,0,0) # inclination

		if ao.y_angle != 0:
			lumber = Rotate([lumber],ao.y_angle,0,1,0) # rotation

		if ao.z_angle != 0:
			lumber = Rotate([lumber],ao.z_angle,0,0,1) # rotation


		# move object relative to robot
		a = [ao.x - v[0] + w[0], ao.y - v[1] + w[1], ao.z - v[2] + w[2] ]

		# tool compensation
#		ao.z = ao.z + 6.88975
#		ao.y = ao.y + 3.838575
		a[2] = a[2] + 6.88975
		a[1] = a[1] + 3.838575

		print "ao2 x:%f"%(ao.x)+" y:%f"%(ao.y)+" z:%f"%(ao.z)

#		lumber = Translate([lumber],ao.x,ao.y,ao.z) # position relative to the scene


		ao.x_angle = ao.x_angle + v[3] + 90
		ao.y_angle = ao.y_angle + v[4]
		ao.z_angle = ao.z_angle + v[5]


		if v[3] != 0:
			lumber = Rotate([lumber],v[3],1,0,0) # inclination
			myRotate(a,v[3],1,0,0)

		if v[4] != 0:
			lumber = Rotate([lumber],v[4],0,1,0) # rotation
			myRotate(a,v[4],0,1,0)

		if v[5] != 0:
			lumber = Rotate([lumber],v[5],0,0,1) # rotation
			myRotate(a,v[5],0,0,1)


		print "ao1 x:%f"%(ao.x)+" y:%f"%(ao.y)+" z:%f"%(ao.z)
		print "v x:%f"%(v[0])+" y:%f"%(v[1])+" z:%f"%(v[2])+" A:%f"%(v[3])+" B:%f"%(v[4])+" C:%f"%(v[5])


		lumber = Translate([lumber],a[0],a[1],a[2]) # position relative to the scene

		ao.x = a[0]
		ao.y = a[1]
		ao.z = a[2]

		ao.model = lumber
		self.attached_object = ao
		vm.attach2object(self.tool,ao.model)


		return

	def detach_object(self,vm):
		vm.detach2object(self.attached_object.model)
		del self.attached_object
		self.attached_object = None
		return

	def robot_matrix(self,f):

		# tool compensation
		w = [0,6.88975,-3.838575]

		if f.a != 0:
			myRotate(w,a,1,0,0)
		if f.b != 0:
			myRotate(w,b,0,1,0)
		if f.c != 0:
			myRotate(w,c,0,0,1)

		print "compensation:"
		print "wx:%f"%(w[0])+" wy:%f"%(w[1])+" wz:%f"%(w[2])

		f.x = f.x + w[0]
		f.y = f.y + w[1]
		f.z = f.z + w[2]

		v = [f.x - (self.position_x * 0.03937), f.y - (self.position_y * 0.03937), f.z - (self.position_z * 0.03937), f.a, f.b, f.c]

		if self.angle_z != 0:
			myRotate(v,-self.angle_z,0,0,1)

		return v


	def compensate_error(self,f,base):

		print "compensate error x:%d"%(f.x)+" y:%d"%(f.y)+" z:%d"%(f.z)+" base:%d"%(base)

		if base == -1:
			return

		angles = [0.0,0.0,0.0]

		angles[0] = (f.x * self.env.flow[base].compensX[0]/1000.0) + (f.y * self.env.flow[base].compensY[0]/1000.0) + self.env.flow[base].compensO[0]
		angles[1] = (f.x * self.env.flow[base].compensX[1]/1000.0) + (f.y * self.env.flow[base].compensY[1]/1000.0) + self.env.flow[base].compensO[1]
		angles[2] = (f.x * self.env.flow[base].compensX[2]/1000.0) + (f.y * self.env.flow[base].compensY[2]/1000.0) + self.env.flow[base].compensO[2]

		return angles


	def get_collection(self):
		return self.mesh


	def load_mesh(self):
		if self.mesh:
			del self.mesh

		self.get_links_from_db()

		#osb = AsciiOBJ(self.homedir + '/' + self.constructor + '/' + self.model + '/osb.obj') #Capture()
		##osb = Rotate([osb],-90,1,0,0)
		##osb = Translate([osb],-35.433,0,6.6929)
		#osb = Rotate([osb],90,0,1,0)
		#osb = Translate([osb],0,-3,0)
		#osb = Color([0.89,0.69,0.4,1.0],[osb])

		if self.toolname != '':
			tool = AsciiOBJ(self.homedir + '/' + self.constructor + '/' + self.model + '/' + self.toolname) #Capture()
			#tool = Box(-1,-1,0,1,1,2)
			#tool = Translate([tool],-2.95,0.0,-2.95)
			t = [0.0,0.0,0.0]
			to = self.links[7].translate

			if self.toolname == 'tool.obj':
				tool = Rotate([tool],-90,1,0,0)
				t = [-35.433,0,6.6929]

			elif self.toolname == 'scieclou.obj':
				tool = Rotate([tool],90,1,0,0)
				t = [-2.95275,0,-2.95275]
			elif self.toolname == 'palpeur.obj':
				tool = Rotate([tool],90,1,0,0)
				tool = Rotate([tool],90,0,1,0)

			#t[2] = t[2] - to[2]

			tool = Translate([tool],t[0],t[1],t[2])
			tool = Color(self.links[7].color,[tool])
			#tool = Collection([tool, osb])


		link6 = AsciiOBJ(self.homedir + '/' + self.constructor + '/' + self.model + '/' + self.links[6].file)
		link6 = Color(self.links[6].color,[link6])

		if self.toolname != '':
			tool = Translate([tool],to[0],to[1],to[2])

		t = self.links[6].translate

		if self.toolname != '':
			tool = Translate([tool],-t[0],-t[1],-t[2])
			self.tool = tool
			link6 = Collection([link6, self.tool])

		# ----------------------------
		to = t
		r = self.links[6].axis
		if self.joggeur: link6 = HalRotate([link6],self.joggeur,"C",1,r[0],r[1],r[2])

		link5 = AsciiOBJ(self.homedir + '/' + self.constructor + '/' + self.model + '/' + self.links[5].file)
		link5 = Color(self.links[5].color,[link5])
		link6 = Translate([link6],to[0],to[1],to[2])
		t = self.links[5].translate
		link6 = Translate([link6],-t[0],-t[1],-t[2])
		link5 = Collection([link6, link5])
		# ----------------------------
		to = t
		r = self.links[5].axis
		if self.joggeur: link5 = HalRotate([link5],self.joggeur,"B",1,r[0],r[1],r[2])

		link4 = AsciiOBJ(self.homedir + '/' + self.constructor + '/' + self.model + '/' + self.links[4].file)
		link4 = Color(self.links[4].color,[link4])

		link5 = Translate([link5],to[0],to[1],to[2])
		t = self.links[4].translate
		link5 = Translate([link5],-t[0],-t[1],-t[2])
		link4 = Collection([link5, link4])
		# ----------------------------
		to = t
		r = self.links[4].axis
		if self.joggeur: link4 = HalRotate([link4],self.joggeur,"A",1,r[0],r[1],r[2])

		link3 = AsciiOBJ(self.homedir + '/' + self.constructor + '/' + self.model + '/' + self.links[3].file)
		link3 = Color(self.links[3].color,[link3])
		link4 = Translate([link4],to[0],to[1],to[2])
		t = self.links[3].translate
		link4 = Translate([link4],-t[0],-t[1],-t[2])
		link3 = Collection([link4, link3])
		# ----------------------------
		to = t
		r = self.links[3].axis
		if self.joggeur: link3 = HalRotate([link3],self.joggeur,"Z",1,r[0],r[1],r[2])

		link2 = AsciiOBJ(self.homedir + '/' + self.constructor + '/' + self.model + '/' + self.links[2].file)
		link2 = Color(self.links[2].color,[link2])
		link3 = Translate([link3],to[0],to[1],to[2])
		t = self.links[2].translate
		print "l2 trans : %f"%(t[0])+" %f"%(t[1])+" %f"%(t[2])
		link3 = Translate([link3],-t[0],-t[1],-t[2])
		link2 = Collection([link3, link2])
		# ----------------------------
		to = t
		r = self.links[2].axis
		if self.joggeur: link2 = HalRotate([link2],self.joggeur,"Y",1,r[0],r[1],r[2])

		link1 = AsciiOBJ(self.homedir + '/' + self.constructor + '/' + self.model + '/' + self.links[1].file)
		link1 = Color(self.links[1].color,[link1])
		link2 = Translate([link2],to[0],to[1],to[2])
		link1 = Collection([link2, link1])
		# ----------------------------
		r = self.links[1].axis
		if self.joggeur: link1 = HalRotate([link1],self.joggeur,"X",1,r[0],r[1],r[2])

		base = AsciiOBJ(self.homedir + '/' + self.constructor + '/' + self.model + '/' + self.links[0].file)
		base = Color(self.links[0].color,[base])
		base = Collection([link1,base])
		# ----------------------------

		base = Rotate([base],90,0,0,1)

		self.mesh = Collection([base])

		return



	def calculate(self):
		print "calc finished"
		if self.calculateur == None:
			return

		if self.calculateur.instance == None:
			return

		jp = self.calculateur.instance.joint_position
		print "result : x:%f"%(jp['X']) + " y:%f"%(jp['Y']) + " z:%f"%(jp['Z']) + " a:%f"%(jp['A']) + " b:%f"%(jp['B']) + " c:%f"%(jp['C'])

		if self.proto == "cross":
			print "envoi du calcul au cross"
			if self.last_func == self.FUNC_MOVE:
				self.send_move(-jp['X'],jp['Y'],jp['Z']+90.0,jp['A'],jp['B'],jp['C'],-1)
			elif self.last_func == self.FUNC_ROTATE:
				self.send_rotate(-jp['X'],jp['Y'],jp['Z']+90.0,jp['A'],jp['B'],jp['C'],-1)
			elif self.last_func == self.FUNC_MOVELIN:
				self.send_move_lin(-jp['X'],jp['Y'],jp['Z']+90.0,jp['A'],jp['B'],jp['C'],-1)
			elif self.last_func == self.FUNC_PROBE:
				self.send_probe_axis(-jp['X'],jp['Y'],jp['Z']+90.0,jp['A'],jp['B'],jp['C'],self.last_base_dir,-1)
			elif self.last_func == self.FUNC_CONTACT:
				self.send_move_contact(-jp['X'],jp['Y'],jp['Z']+90.0,jp['A'],jp['B'],jp['C'],-1)
			elif self.last_func == self.FUNC_PREHEN:
				self.send_prehension(-jp['X'],jp['Y'],jp['Z']+90.0,jp['A'],jp['B'],jp['C'],-1)
			elif self.last_func == self.FUNC_NAIL:
				self.send_nail(-jp['X'],jp['Y'],jp['Z']+90.0,jp['A'],jp['B'],jp['C'],-1)
			elif self.last_func == self.FUNC_DEPREHEN:
				self.send_deprehension(-jp['X'],jp['Y'],jp['Z']+90.0,jp['A'],jp['B'],jp['C'],-1)

			return




def get_from_db(db):
	#print "robot get from db"
	col = []

	for t in db.findall('ROBOT'):

		ro = robotobj()

		p = t.get('IP')
		if p != None and p != "None":
			ro.ip = p
		p = t.get('CALC_IP')
		if p != None and p != "None":
			ro.calc_ip = p
		ro.name = t.get('NAME')
		ro.model = t.get('MODEL')
		ro.constructor = t.get('CONSTRUCTOR')
		p = t.get('NUMBER')
		if p: ro.index = int(p)
		p = t.get('CHAN')
		if p: ro.chan = int(p)
		p = t.get('CALC_CHAN')
		if p: ro.calc_chan = int(p)
		p = t.get('PROTOCOL')
		if p =='' or p == None: p ='None'
		ro.proto = p
		p = t.get('CALC')
		if p == None or p =='': p ='None'
		ro.calc = p
		p = t.get('POSITION_MATRIX')
		if p:
			wo = p.split(' ')
			for w in wo:
				w.strip()
			ro.position_matrix = tereoMath.matrix44()
			ro.position_matrix.from_array([ float(wo[0]) , float(wo[1]) ,float(wo[2]) , float(wo[3]) , float(wo[4]) , float(wo[5]),float(wo[6]),float(wo[7]),float(wo[8]),float(wo[9]),float(wo[10]),float(wo[11]),float(wo[12]),float(wo[13]),float(wo[14]),float(wo[15]) ])

		p = t.get('HOME')
		if p:
			wo = p.split(' ')
			for w in wo:
				w.strip()
			ro.home = tereoMath.frame6(float(wo[0]),float(wo[1]),float(wo[2]),float(wo[3]),float(wo[4]),float(wo[5]))


		# ----------get model informations----------------
		ro.links = []

		m = t.find('MESH')
		if m:

			# ---------------------------------

			def get_link_info(l,rl):

				p = l.get('DH_A')

				if p:
					rl.a = float(p)
					#print "dha"
				p = l.get('DH_ALPHA')
				if p:
					rl.alpha = float(p)
					#print "alpha"
				p = l.get('DH_D')
				if p: rl.d = float(p)
				p = l.get('DH_THETA')
				if p: rl.theta = float(p)

				p = l.get('NUMBER')
				if p: rl.index = int(p)

				p = l.get('AXISLINK')
				if p: rl.axislink = p

				rl.file = l.get('FILE')
				p = l.get('TRANSLATE')
				if p:
					wo = p.split(' ')
					for w in wo:
						w.strip()
					rl.translate = [float(wo[0]),float(wo[1]),float(wo[2])]

				p = l.get('AXIS')
				if p:
					wo = p.split(' ')
					for w in wo:
						w.strip()
					rl.axis = [int(wo[0]),int(wo[1]),int(wo[2])]

				p = l.get('COLOR')
				if p:
					wo = p.split(' ')
					for w in wo:
						w.strip()
					rl.color = [float(wo[0]),float(wo[1]),float(wo[2]),1.0]
				return

			# base
			for l in m.findall('BASE'):
				rl = robotLink()
				rl.type = "BASE"
				ro.links.append(rl)
				get_link_info(l,rl)

			# links
			i = 0
			for l in m.findall('LINK'):
				#print "link found"
				rl = robotLink()
				rl.type = "LINK"
				ro.links.append(rl)
				get_link_info(l,rl)
				i += 1
			ro.axes = i

			# tool
			for l in m.findall('HAND'):
				rl = robotLink()
				rl.type = "HAND"
				ro.links.append(rl)
				get_link_info(l,rl)

			#------------------------

		ro.db = ET.Element('XML')

		#--------get extra informations-----------
		for c in t.getchildren():
			if c.tag != 'MESH' and c.tag != "":
				ro.db.append(c)

		col.append(ro)

	return col



def set_to_db(db,robots):



	# delete all robots
	if hasattr(db,"getroot"):
		ter = db.getroot()
	else:
		ter = db

	t=ter.find("ROBOT")
	while t != None :
		ter.remove(t)
		t = ter.find("ROBOT")



	for ro in robots:

		t = ET.SubElement(ter,'ROBOT')
		if t != None:
			t.set('NUMBER',str(ro.index))
			t.set('NAME',ro.name)
			t.set('IP',str(ro.ip))
			t.set('CHAN',str(ro.chan))
			t.set('PROTOCOL',str(ro.proto))
			t.set('CALC',str(ro.calc))
			t.set('CONSTRUCTOR',ro.constructor)
			t.set('MODEL',ro.model)
			t.set('CALC_IP',str(ro.calc_ip))
			t.set('CALC_CHAN',str(ro.calc_chan))
			t.set('POSITION_MATRIX', ro.position_matrix.dump2())
			t.set('HOME', ro.home.dump2())

			# --- model ---
			m = ET.SubElement(t,'MESH')
			for l in ro.links:
				#print "link ok"
				if l.type == "BASE":
					#print "link base"
					li = ET.SubElement(m,"BASE")
				if l.type == "LINK":
					#print "link link"
					li = ET.SubElement(m,"LINK")
					li.set('NUMBER',str(l.index))
				if l.type == "HAND":
					#print "link hand"
					li = ET.SubElement(m,"HAND")
				li.set('FILE',l.file)
				li.set('TRANSLATE',"%f"%(l.translate[0])+" %f"%(l.translate[1])+" %f"%(l.translate[2]))
				li.set('AXIS',"%d"%(l.axis[0])+" %d"%(l.axis[1])+" %d"%(l.axis[2]))
				li.set('COLOR',"%f"%(l.color[0])+" %f"%(l.color[1])+" %f"%(l.color[2]))
				li.set('DH_A',"%f"%(l.a))
				li.set('DH_ALPHA',"%f"%(l.alpha))
				li.set('DH_D',"%f"%(l.d))
				li.set('DH_THETA',"%f"%(l.theta))


			# --- extra ---
			for c in ro.db.getchildren():
				t.append(c)



	return


