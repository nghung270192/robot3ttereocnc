#!/usr/bin/python

from twisted.internet.protocol import Factory
from twisted.protocols.basic import LineReceiver
#from twisted.internet import reactor
#import linuxcnc

class server(LineReceiver):

	def __init__(self,c,s,e):
		print "init tereonet server"
		self.c = c # command
		self.s = s # stat
		self.e = e # error channel
		#self.name = None
		#self.state = "GETNAME"

	def connectionMade(self):
		print "client connected"
		self.sendLine("Welcome to tereocnc")

	def connectionLost(self,reason):
		print "connection lost"
		#if self.users.has_key(self.name):
		#	del self.users[self.name]

	def lineReceived(self,line):
		print "linereceived:["+line+"]"
		word_list = line.split(' ')
		line = line + " "
		if word_list[0] == "stat":
			self.s.poll()
			co = word_list[1]
			if co=="acceleration": o=line+":%f" % (self.s.acceleration)
			elif co=="active_queue": o=line+":%d" % (self.s.active_queue)
			elif co=="actual_position": o=line+":".join(map(str,self.s.actual_position))
			elif co=="adaptive_feed_enabled": o=line+":%d" % (self.s.adaptive_feed_enabled)
			elif co=="ain": o=line+":".join(self.s.ain)
			elif co=="angular_units": o=line+":"+self.s.angular_units
			elif co=="aout": o=line+":".join(map(str,self.s.aout))
			elif co=="axes": o=line+":"+ self.s.axes
			#elif co=="axis": s=line+":%f" % (s.acceleration)

			elif co=="axis_mask": o=line+":%d" % (self.s.axis_mask)
			elif co=="block_delete": o=line+":%d" % (self.s.block_delete)
			elif co=="command": o=line+":" + self.s.command
			elif co=="current_line": o=line+":%d" % (self.s.current_line)
			elif co=="current_vel": o=line+":%f" % (self.s.current_vel)
			elif co=="cycle_time": o=line+":" + self.s.cycle_time
			elif co=="debug": o=line+":%d" % (self.s.debug)
			elif co=="delay_left": o=line+":%f" % (self.s.delay_left)
			elif co=="din": o=line+":".join(map(str,self.s.din))
			elif co=="distance_to_go": o=line+":%f" % (self.s.distance_to_go)
			elif co=="dout": o=line+":".join(map(str,self.s.dout))
			elif co=="dtg": o=line+":".join(map(str,self.s.dtg))
			elif co=="echo_serial_number": o=line+":%d" % (self.s.echo_serial_number)
			elif co=="enabled": o=line+":%d" % (self.s.enabled)
			elif co=="estop": o=line+":%d" % (self.s.estop)
			elif co=="exec_state": o=line+":%d" % (self.s.exec_state)
			elif co=="feed_hold_enabled": o=line+":%d" % (self.s.feed_hold_enabled)
			elif co=="feed_override_enabled": o=line+":%d" % (self.s.feed_override_enabled)
			elif co=="feed_rate": o=line+":%f" % (self.s.feed_rate)
			elif co=="file": o=line+":"+self.s.file
			elif co=="flood": o=line+":%d" % (self.s.flood)
			elif co=="g5x_index": o=line+":"+self.s.g5x_index
			elif co=="g5x_offset": o=line+":".join(map(str,self.s.g5x_offset))
			elif co=="g92_offset": o=line+":".join(map(str,self.s.g92_offset))
			elif co=="gcodes": o=line+":".join(map(str,self.s.gcodes))
			elif co=="homed": o=line+":%d" % (self.s.homed)
			elif co=="id": o=line+":%d" % (self.s.id)
			elif co=="inpos": o=line+":%d" % (self.s.inpos)
			elif co=="input_timeout": o=line+":%d" % (self.s.input_timeout)
			elif co=="interp_state": o=line+":%d" % (self.s.interp_state)
			elif co=="interpreter_errcode": o=line+":%d" % (self.s.interpreter_errcode)
			elif co=="joint_actual_position": o=line+":".join(map(str,self.s.joint_actual_position))
			elif co=="joint_position": o=line+":".join(map(str,self.s.joint_position))
			elif co=="kinematics_type": o=line+":%d" % (self.s.kinematics_type)
			elif co=="limit": o=line+":".join(map(str,self.s.limit))
			elif co=="linear_units": o=line+":"+self.s.linear_units
			elif co=="lube": o=line+":%d" % (self.s.lube)
			elif co=="lube_level": o=line+":%d" % (self.s.lube_level)
			elif co=="max_acceleration": o=line+":%f" % (self.s.max_acceleration)
			elif co=="max_velocity": o=line+":%f" % (self.s.max_velocity)
			elif co=="mcodes": o=line+":".join(map(str,self.s.mcodes))
			elif co=="mist": o=line+":%d" % (self.s.mist)
			elif co=="motion_line": o=line+":%d" % (self.s.motion_line)
			elif co=="motion_mode": o=line+":%d" % (self.s.motion_mode)
			elif co=="motion_type": o=line+":%d" % (self.s.motion_type)
			elif co=="optional_stop": o=line+":%d" % (self.s.optional_stop)
			elif co=="paused": o=line+":%d" % (self.s.paused)
			elif co=="pocket_prepped": o=line+":%d" % (self.s.pocket_prepped)
			elif co=="position": o =line+":".join(map(str,self.s.position))
			elif co=="probe_tripped": o=line+":%d" % (self.s.probe_tripped)
			elif co=="probe_val": o=line+":%d" % (self.s.probe_val)
			elif co=="probed_position": o=line+":".join(map(str,self.s.probed_position))
			elif co=="probing": o=line+":%d" % (self.s.probing)
			elif co=="program_units": o=line+":%d" % (self.s.program_units)
			elif co=="queue": o=line+":%d" % (self.s.queue)
			elif co=="queue_full": o=line+":%d" % (self.s.queue_full)
			elif co=="read_line": o=line+":%d" % (self.s.read_line)
			elif co=="rotation_xy": o=line+":%f" % (self.s.rotation_xy)
			elif co=="settings": o=line+":".join(map(str,self.s.settings))
			elif co=="spindle_brake": o=line+":%d" % (self.s.spindle_brake)
			elif co=="spindle_direction": o=line+":%d" % (self.s.spindle_direction)
			elif co=="spindle_enabled": o=line+":%d" % (self.s.spindle_enabled)
			elif co=="spindle_increasing": o=line+":%d" % (self.s.spindle_increasing)
			elif co=="spindle_override_enabled": o=line+":%d" % (self.s.spindle_override_enabled)
			elif co=="spindle_speed": o=line+":%f" % (self.s.spindle_speed)
			elif co=="spindlerate": o=line+":%f" % (self.s.spindlerate)
			elif co=="state": o=line+":%d" % (self.s.state)
			elif co=="task_mode": o=line+":%d" % (self.s.task_mode)
			elif co=="task_paused": o=line+":%d" % (self.s.task_paused)
			elif co=="task_state": o=line+":%d" % (self.s.task_state)
			elif co=="tool_in_spindle": o=line+":%d" % (self.s.tool_in_spindle)
			elif co=="tool_offset": o=line+":".join(map(str,self.s.tool_offset))
			#if co=="tool_table": o=line+":%f" % (self.s.tool_table)
			elif co=="velocity": o=line+":%f" % (self.s.velocity)
			else:
				o = "unknown command"
				return
			self.sendLine(o)

		elif word_list[0] == "command":
			co = word_list[1]
			if co=="abort": self.c.abort()
			elif co=="auto":
				ar = int(word_list[2])
				self.c.auto(ar)
			elif co=="brake":
				ar = int(word_list[2])
				self.c.brake(ar)
			elif co=="debug":
				ar = int(word_list[2])
				self.c.debug(ar)
			elif co=="feedrate":
				ar = float(word_list[2])
				self.c.feedrate(ar)
			elif co=="flood":
				ar = int(word_list[2])
				self.c.flood(ar)
			elif co=="home":
				ar = int(word_list[2])
				self.c.home(ar)
			elif co=="jog":
				com = int(word_list[2])
				ax = int(word_list[3])
				if len(word_list)==6:
					vel = int(word_list[4])
					dist = int(word_list[5])
					self.c.jog(com,ax,vel,dist)
				elif len(word_list)==5:
					vel = int(word_list[4])
					self.c.jog(com,ax,vel)
				else:
					self.c.jog(com,ax)
			elif co=="load_tool_table":
				self.c.load_tool_table()
			elif co=="maxvel":
				ar = float(word_list[2])
				self.c.maxvel(ar)
			elif co=="mdi":
				self.c.mdi(word_list[2])
			elif co=="mist":
				ar = int(word_list[2])
				self.c.mist(ar)
			elif co=="mode":
				ar = int(word_list[2])
				self.c.mode(ar)
			elif co=="override_limits":
				self.c.override_limits()
			elif co=="program_open":
				self.c.program_open(word_list[2])
			elif co=="reset_interpreter":
				self.c.reset_interpreter()
			elif co=="set_adaptative_feed":
				ar = int(word_list[2])
				self.c.set_adaptative_feed(ar)
			elif co=="set_analog_output":
				pin = int(word_list[2])
				val = float(word_list[3])
				self.c.set_analog_output(pin,val)
			elif co=="set_block_delete":
				ar = int(word_list[2])
				self.c.set_block_delete(ar)
			elif co=="set_digital_output":
				pin = int(word_list[2])
				val = int(word_list[3])
				self.c.set_digital_output(pin,val)
			elif co=="set_feed_hold":
				ar = int(word_list[2])
				self.c.set_feed_hold(ar)
			elif co=="set_feed_override":
				ar = int(word_list[2])
				self.c.set_feed_override(ar)
			elif co=="set_max_limit":
				ax = int(word_list[2])
				val = float(word_list[3])
				self.c.set_max_limit(ax,val)
			elif co=="set_min_limit":
				self.c.set_min_limit()
			elif co=="set_optional_stop":
				ar = int(word_list[2])
				self.c.set_optional_stop(ar)
			elif co=="set_spindle_override":
				ar = int(word_list[2])
				self.c.set_spindle_override(ar)
			elif co=="spindle":
				ar = int(word_list[2])
				self.c.spindle(ar)
			elif co=="spindleoverride":
				fac = float(word_list[2])
				self.c.spindleoverride(fac)
			elif co=="state":
				ar = int(word_list[2])
				self.c.state(ar)
			elif co=="teleop_enable":
				ar = int(word_list[2])
				self.c.teleop_enable(ar)
			elif co=="teleop_vector":
				if len(word_list) == 8:
					self.c.teleop_vector(float(word_list[2]),float(word_list[3]),float(word_list[4]),float(word_list[5]),float(word_list[6]),float(word_list[7]))
				else:
					self.c.teleop_vector(float(word_list[2]),float(word_list[3]),float(word_list[4]))
			elif co=="teleop_offset":
				self.c.teleop_offset(int(word_list[2]),float(word_list[3]),float(word_list[4]),float(word_list[5]),float(word_list[6]),float(word_list[7]),int(word_list[8]))
			elif co=="traj_mode":
				ar = int(word_list[2])
				self.c.traj_mode(ar)
			elif co=="unhome":
				ar = int(word_list[2])
				self.c.unhome(ar)
			elif co=="wait_complete":
				if len(word_list)==3:
					val = float(word_list[2])
					self.c.wait_complete(val)
				else:
					self.c.wait_complete()
			else:
				self.sendLine("unknown command")
				return
			self.sendLine("command "+co+" OK")

		#if self.state == "GETNAME":
		#	self.handle_GETNAME(line)
		#else:
		#	self.handle_CHAT(line)

	#def handle_GETNAME(self, name):
		#if self.users.has_key(name):
		#	self.sendLine("Name taken , please choose another.")
		#	return
	#	self.sendLine("welcome, %s!" % (name,))
	#	self.name = name
	#	self.users[name] = self
	#	self.state = "CHAT"

	#def handle_CHAT(self,message):
		#message = "<%s> %s" % (self.name, message)
		#for name, protocol in self.users.iteritems():
		#	if protocol != self:
		#		protocol.sendLine(message)



