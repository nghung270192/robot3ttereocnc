# -*- coding: utf-8 -*-

import pygtk
pygtk.require("2.0")
import gtk


class config_window(gtk.Window):


	def __init__(self,tereo):

		gtk.Window.__init__(self)
		self.set_title("General preferences")
		self.set_position(gtk.WIN_POS_CENTER)
		self.set_default_size(320, 480)

		self.tereo = tereo

		self.choix = 1

		hbox1 = gtk.HBox(False,0)
		hbox1.show()
		self.add(hbox1)

		box1 = gtk.VBox(False, 0)
		box1.set_border_width(5)
		hbox1.pack_start(box1,False,False,0)

		# desktop division
		label = gtk.Label("Desktop division :")
		label.set_alignment(0, 0)
		box1.pack_start(label, False, False, 0)
		label.show()

		# radio button
		self.button1 = gtk.RadioButton(None, "full screen")
		self.button1.connect("toggled", self.callback, "1")
		box1.pack_start(self.button1, False, False, 1)
   		self.button1.set_active(True)
		self.button1.show()

		self.button2 = gtk.RadioButton(self.button1, "2 horizontal")
   		self.button2.connect("toggled", self.callback, "2")
   		box1.pack_start(self.button2, False, False, 2)
   		self.button2.show()

		self.button3 = gtk.RadioButton(self.button1, "2 vertical")
		self.button3.connect("toggled", self.callback, "3")
		box1.pack_start(self.button3, False, False, 3)
		self.button3.show()

		self.button4 = gtk.RadioButton(self.button1, "4 mosaic")
		self.button4.connect("toggled", self.callback, "4")
		box1.pack_start(self.button4, False, False, 4)
		self.button4.show()


		# view selection


		separator = gtk.HSeparator()
		separator.show()
		box1.pack_start(separator, False, False, 5)


		slist = []
		slist.append('None')
		for i in self.tereo.objects:
			if i[0] == 'gui.frame':
				slist.append(i[1])

		self.label1 = gtk.Label("View 1 :")
		self.label1.set_alignment(0, 0)
		box1.pack_start(self.label1, False, False, 6)
		self.label1.show()

		self.combo1 = gtk.Combo()
		self.combo1.set_popdown_strings(slist)
		self.combo1.entry.set_text('None')
		box1.pack_start(self.combo1, False, False, 7)
		self.combo1.show()

		self.label2 = gtk.Label("View 2 :")
		self.label2.set_alignment(0, 0)
		box1.pack_start(self.label2, False, False, 8)
		self.label2.hide()

		self.combo2 = gtk.Combo()
		self.combo2.set_popdown_strings(slist)
		self.combo2.entry.set_text('None')
		box1.pack_start(self.combo2, False, False, 9)
		self.combo2.hide()

		self.label3 = gtk.Label("View 3 :")
		self.label3.set_alignment(0, 0)
		box1.pack_start(self.label3, False, False, 10)
		self.label3.hide()

		self.combo3 = gtk.Combo()
		self.combo3.set_popdown_strings(slist)
		self.combo3.entry.set_text('None')
		box1.pack_start(self.combo3, False, False, 11)
		self.combo3.hide()

		self.label4 = gtk.Label("View 4 :")
		self.label4.set_alignment(0, 0)
		box1.pack_start(self.label4, False, False, 12)
		self.label4.hide()

		self.combo4 = gtk.Combo()
		self.combo4.set_popdown_strings(slist)
		self.combo4.entry.set_text('None')
		box1.pack_start(self.combo4, False, False, 13)
		self.combo4.hide()


		#apply
		img = gtk.Image()
		img.set_from_file('./media/dialog-ok.png')
		img.show()
		button = gtk.Button("Apply")
		button.set_image(img)
		button.connect_object("clicked", self.close_window, self, None)
		box1.pack_start(button, False, False, 14)
		button.set_flags(gtk.CAN_DEFAULT)
		button.grab_default()
		button.show()

		box1.show()



		return


	def callback(self, widget, data=None):
		j = (data, ("ON")[widget.get_active()])
		print "radio"
		i = int(j[0])
		print i


		if i == 1:
			self.label2.hide()
			self.combo2.hide()
			self.combo2.entry.set_text('None')
			self.label3.hide()
			self.combo3.hide()
			self.combo3.entry.set_text('None')
			self.label4.hide()
			self.combo4.hide()
			self.combo4.entry.set_text('None')
			self.choix= 1
		elif i == 2:
			self.label2.show()
			self.combo2.show()
			self.label3.hide()
			self.combo3.hide()
			self.combo3.entry.set_text('None')
			self.label4.hide()
			self.combo4.hide()
			self.combo4.entry.set_text('None')
			self.choix= 2
		elif i == 3:
			self.label2.show()
			self.combo2.show()
			self.label3.hide()
			self.combo3.hide()
			self.combo3.entry.set_text('None')
			self.label4.hide()
			self.combo4.hide()
			self.combo4.entry.set_text('None')
			self.choix= 3
		else:
			self.label2.show()
			self.combo2.show()
			self.label3.show()
			self.combo3.show()
			self.label4.show()
			self.combo4.show()
			self.choix= 4

		return


	def close_window(self, widget, event, data=None):

		view1 = self.combo1.entry.get_text()
		view2 = self.combo2.entry.get_text()
		view3 = self.combo3.entry.get_text()
		view4 = self.combo4.entry.get_text()

		self.tereo.set_property('gui','division',self.choix)
		self.tereo.set_property('gui','view1',view1)
		self.tereo.set_property('gui','view2',view2)
		self.tereo.set_property('gui','view3',view3)
		self.tereo.set_property('gui','view4',view4)
		self.tereo.update_gui()

		self.hide()

		return False

