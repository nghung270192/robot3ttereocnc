# -*- coding: utf-8 -*-

###########################################################################
#    Copyright (C) 2003 by Wido Depping
#    <widod@users.sourceforge.net>
#
# Copyright: See COPYING file that comes with this distribution
#
###########################################################################

#from qt import *
#from base.utils.gui.SearchForm import SearchForm
#from base.utils.gui.SearchResultView import SearchResultView

class geometry_view(QWidget):

    def __init__(self, parent=None, name=None, fl=0):

        QWidget.__init__(self, parent, name, fl)

        self.vLayout = QVBoxLayout(self)

        self.searchForm = SearchForm(self)
        self.resultView = SearchResultView(self)

        self.vLayout.addWidget(self.searchForm)
        self.vLayout.addWidget(self.resultView)

        self.connect(self.searchForm, PYSIGNAL("ldap_result"), \
                self.resultView.setResult)

#--------------------


	####################################
	######### geometry window ##########
	######### pickup          ##########
	####################################
	def geometrywin_init(self):
		return

	def on_geometrywin_destroy(self,widget):
		# ask save changes
		return

	def on_geometrywin_cancel(self,widget):
		self.geometrywin.destroy()
		return

	def on_configwin_geometry(self,widget):
		self.geometrywin.show()
		screen = self.geometrywin.get_screen()
		self.geometrywin.resize(screen.get_width(),screen.get_height())
		return
