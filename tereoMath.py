#!/usr/bin/python

import math
import tereoRobot
#import scipy.optimize
import numpy


class matrix44:
	def __init__(self,m11=0,m12=0,m13=0,m14=0, m21=0,m22=0,m23=0,m24=0, m31=0,m32=0,m33=0,m34=0, m41=0,m42=0,m43=0,m44=1):
		self.m11 = m11 #0
		self.m12 = m12 #1
		self.m13 = m13 #2
		self.m14 = m14 #3
		self.m21 = m21 #4
		self.m22 = m22 #5
		self.m23 = m23 #6
		self.m24 = m24 #7
		self.m31 = m31 #8
		self.m32 = m32 #9
		self.m33 = m33 #10
		self.m34 = m34 #11
		self.m41 = m41 #12
		self.m42 = m42 #13
		self.m43 = m43 #14
		self.m44 = m44 #15

		return

	def dump(self):

		print "[%f"%(self.m11)+" %f"%(self.m12)+" %f"%(self.m13)+" %f"%(self.m14)
		print " %f"%(self.m21)+" %f"%(self.m22)+" %f"%(self.m23)+" %f"%(self.m24)
		print " %f"%(self.m31)+" %f"%(self.m32)+" %f"%(self.m33)+" %f"%(self.m34)
		print " %f"%(self.m41)+" %f"%(self.m42)+" %f"%(self.m43)+" %f"%(self.m44)+"]"

		return

	def dump2(self):

		return "%f"%(self.m11)+" %f"%(self.m12)+" %f"%(self.m13)+" %f"%(self.m14)+" %f"%(self.m21)+" %f"%(self.m22)+" %f"%(self.m23)+" %f"%(self.m24)+" %f"%(self.m31)+" %f"%(self.m32)+" %f"%(self.m33)+" %f"%(self.m34)+" %f"%(self.m41)+" %f"%(self.m42)+" %f"%(self.m43)+" %f"%(self.m44)

	def clone(self):
		m = matrix44()
		m.m11 = self.m11
		m.m12 = self.m12
		m.m13 = self.m13
		m.m14 = self.m14
		m.m21 = self.m21
		m.m22 = self.m22
		m.m23 = self.m23
		m.m24 = self.m24
		m.m31 = self.m31
		m.m32 = self.m32
		m.m33 = self.m33
		m.m34 = self.m34
		m.m41 = self.m41
		m.m42 = self.m42
		m.m43 = self.m43
		m.m44 = self.m44
		return m

	def determinant(self):

		d = self.m11 * (self.m22*self.m33-self.m23*self.m32) - self.m12 * (self.m21*self.m33-self.m23*self.m31) + self.m13 * (self.m21*self.m32-self.m22*self.m31)
		return d


	def minor(self):

		min = matrix44()

		min.m11 = self.m22*self.m33 - self.m32*self.m23
		min.m12 = self.m21*self.m33 - self.m31*self.m23
		min.m13 = self.m21*self.m32 - self.m31*self.m22

		min.m21 = self.m12*self.m33 - self.m32*self.m13
		min.m22 = self.m11*self.m33 - self.m31*self.m13
		min.m23 = self.m11*self.m32 - self.m31*self.m12

		min.m31 = self.m12*self.m23 - self.m22*self.m13
		min.m32 = self.m11*self.m23 - self.m21*self.m13
		min.m33 = self.m11*self.m22 - self.m21*self.m12

		return min


	def cofactor(self):

		cofa = matrix44()

		cofa.m11 = self.m11
		cofa.m12 = -self.m12
		cofa.m13 = self.m13

		cofa.m21 = -self.m21
		cofa.m22 = self.m22
		cofa.m23 = -self.m23

		cofa.m31 = self.m31
		cofa.m32 = -self.m32
		cofa.m33 = self.m33

		return cofa


	def adjoint(self):

		adj = matrix44()

		min = matrix_minor(m)
		cofa = matrix_cofactor(min)

		adj.m11 = cofa.m11
		adj.m12 = cofa.m21
		adj.m13 = cofa.m31

		adj.m21 = cofa.m12
		adj.m22 = cofa.m22
		adj.m23 = cofa.m32

		adj.m31 = cofa.m13
		adj.m32 = cofa.m23
		adj.m33 = cofa.m33

		return adj


	def mult_vector(self,v):

		r = vector3()
		r.x = self.m11 * v.x + self.m12 * v.y + self.m13 * v.z + self.m14
		r.y = self.m21 * v.x + self.m22 * v.y + self.m23 * v.z + self.m24
		r.z = self.m31 * v.x + self.m32 * v.y + self.m33 * v.z + self.m34

		return r



	def invert(self):

		i = matrix44()

		d = self.determinant()
		if d ==0:
			print "matrix determinant zero : exit"
			return

		adj = self.adjoint()

		i.m11 = adj.m11 / d
		i.m12 = adj.m12 / d
		i.m13 = adj.m13 / d

		i.m21 = adj.m21 / d
		i.m22 = adj.m22 / d
		i.m23 = adj.m23 / d

		i.m31 = adj.m31 / d
		i.m32 = adj.m32 / d
		i.m33 = adj.m33 / d

		v = vector3( -self.m14, -self.m24, -self.m34 )
		v = i.mult_vector(v)

		i.m14 = v.x
		i.m24 = v.y
		i.m34 = v.z

		return i

	def to_array(self):
		return [self.m11,self.m12,self.m13,self.m14,self.m21,self.m22,self.m23,self.m24,self.m31,self.m32,self.m33,self.m34,self.m41,self.m42,self.m43,self.m44]


	def from_array(self,a):

		self.m11 = a[0]
		self.m12 = a[1]
		self.m13 = a[2]
		self.m14 = a[3]
		self.m21 = a[4]
		self.m22 = a[5]
		self.m23 = a[6]
		self.m24 = a[7]
		self.m31 = a[8]
		self.m32 = a[9]
		self.m33 = a[10]
		self.m34 = a[11]
		self.m41 = a[12]
		self.m42 = a[13]
		self.m43 = a[14]
		self.m44 = a[15]

		return


	def mult_matrix(self,mb):

		mc = matrix44()
		c = mc.to_array()

		a = self.to_array()
		b = mb.to_array()

		for row in range(0,4):
			for column in range(0,4):
				c[row*4+column] = 0;
				for n in range(0,4):
					c[row*4+column] += a[row*4+n] * b[n*4+column]


		mc.from_array(c)

		return mc



	def to_frame6(self):

		f = frame6()

		#   [m00 m01 m02]
		#   [m10 m11 m12]
		#   [m20 m21 m22]


		f.x = m.m14
		f.y = m.m24
		f.z = m.m34

		if m.m21 > 0.998:
			# singularity at north pole
			f.b = math.degrees(math.atan2(m.m13,m.m33))
			f.c = 90
			f.a = 0
			return f

		if m.m21 < -0.998:
			# singularity at south pole
			f.b = math.degrees(math.atan2(m.m13,m.m33))
			f.c = -90
			f.a = 0
			return f

		f.b = math.degrees(math.atan2(-m.m31,m.m11))
		f.c = math.degrees(math.atan2(-m.m23,m.m22))
		f.a = math.degrees(math.asin(m.m21))


		return f


	def scale(self,f):
		self.m11 = self.m11 * f
		self.m12 = self.m12 * f
		self.m13 = self.m13 * f
		self.m14 = self.m14 * f
		self.m21 = self.m21 * f
		self.m22 = self.m22 * f
		self.m23 = self.m23 * f
		self.m24 = self.m24 * f
		self.m31 = self.m31 * f
		self.m32 = self.m32 * f
		self.m33 = self.m33 * f
		self.m34 = self.m34 * f
		return


#------------------------------------------------------------------------------------







class vector3:
	def __init__(self,x=0.0,y=0.0,z=0.0):
		self.x = x
		self.y = y
		self.z = z
		return

	def vector_getunit(self):
		n = math.sqrt(self.x*self.x + self.y*self.y + self.z*self.z)
		u = vector3(0.0,0.0,0.0)
		u.x = v.x / n
		u.y = v.y / n
		u.z = v.z / n
		return u

	def dump(self):
		print "x : %f"%(self.x)+" y : %f"%(self.y)+" z : %f"%(self.z)
		return

#------------------------------------------------------------------------------------


class frame6:
	def __init__(self,x=0.0,y=0.0,z=0.0,a=0.0,b=0.0,c=0.0):
		self.x = x
		self.y = y
		self.z = z
		self.a = a
		self.b = b
		self.c = c

	def dump(self):

		print "x:%f"%(self.x)+" y:%f"%(self.y)+" z:%f"%(self.z)+" a:%f"%(self.a)+" b:%f"%(self.b)+" c:%f"%(self.c)
		return

	def dump2(self):

		return "%f"%(self.x)+" %f"%(self.y)+" %f"%(self.z)+" %f"%(self.a)+" %f"%(self.b)+" %f"%(self.c)


	def clone(self):
		f = frame6()
		f.x = self.x
		f.y = self.y
		f.z = self.z
		f.a = self.a
		f.b = self.b
		f.c = self.c
		return f

	def to_matrix(self):

		m = matrix44()

		c3 = math.cos(math.radians(self.a))
		s3 = math.sin(math.radians(self.a))
		c4 = math.cos(math.radians(self.b))
		s4 = math.sin(math.radians(self.b))
		c5 = math.cos(math.radians(self.c))
		s5 = math.sin(math.radians(self.c))


		m.m11 = c3*c4
		m.m12 = s4*s5*c3-c5*s3
		m.m13 = c3*c5*s4+s5*s3

		m.m21 = c4*s3
		m.m22 = c5*c3+s5*s4*s3
		m.m23 = c5*s4*s3-s5*c3

		m.m31 = -s4
		m.m32 = s5*c4
		m.m33 = c5*c4

		m.m14 = self.x
		m.m24 = self.y
		m.m34 = self.z

		return m


	def frame6_bymatrix(self,m):

		print "frame6 bymatrix---------"
		print "f"
		self.dump()

		frm = vector3()

		frm.x = self.x
		frm.y = self.y
		frm.z = self.z

		i = m.invert()

		frm = i.mult_vector(frm)

		m6 = self.to_matrix(f)
		mul = i.mult_matrix2(i,m6)
		f6 = mul.to_frame6()

		return



#------------------------------------------------------------------------------------



class matrix33:

	def __init__(m11=0,m12=0,m13=0, m21=0,m22=0,m23=0, m31=0,m32=0,m33=0):
		self.m11 = m11 #0
		self.m12 = m12 #1
		self.m13 = m13 #2
		self.m21 = m21 #3
		self.m22 = m22 #4
		self.m23 = m23 #5
		self.m31 = m31 #6
		self.m32 = m32 #7
		self.m33 = m33 #8
		return

	def determinant(m):
		d = self.m11 * (self.m22*self.m33-self.m23*self.m32) - self.m12 * (self.m21*self.m33-self.m23*self.m31) + self.m13 * (self.m21*self.m32-self.m22*self.m31)
		return d


	def mult_vector(self,v):

		r = vector3()
		r.x = self.m11 * v.x + self.m12 * v.y + self.m13 * v.z
		r.y = self.m21 * v.x + self.m22 * v.y + self.m23 * v.z
		r.z = self.m31 * v.y + self.m32 * v.y + self.m33 * v.z

		return r


	def invert():

		i = matrix33()

		d = self.determinant()
		if d ==0:
			print "matrix determinant zero : exit"
			return

		adj = self.adjoint()

		i.m11 = adj.m11 / d
		i.m12 = adj.m12 / d
		i.m13 = adj.m13 / d

		i.m21 = adj.m21 / d
		i.m22 = adj.m22 / d
		i.m23 = adj.m23 / d

		i.m31 = adj.m31 / d
		i.m32 = adj.m32 / d
		i.m33 = adj.m33 / d

		return i


	def mult_matrix(self,mb):

		mc = matrix33()
		c = mc.to_array()

		a = self.to_array()
		b = mb.to_array()

		for row in range(0,3):
			for column in range(0,3):
				c[row*3+column] = 0;
				for n in range(0,3):
					c[row*3+column] += a[row*3+n] * b[n*3+column]


		mc.from_array(c)

		return mc


	def to_array(self):
		return [self.m11,self.m12,self.m13,self.m14,self.m21,self.m22,self.m23,self.m24,self.m31,self.m32,self.m33,self.m34,self.m41,self.m42,self.m43,self.m44]


	def from_array(self,a):
		m.m11 = a[0]
		m.m12 = a[1]
		m.m13 = a[2]
		m.m21 = a[3]
		m.m22 = a[4]
		m.m23 = a[5]
		m.m31 = a[6]
		m.m32 = a[7]
		m.m33 = a[8]
		m.m41 = a[9]
		m.m42 = a[10]
		m.m43 = a[11]
		return


	# this conversion uses conventions as described on page:
	# http://www.euclideanspace.com/maths/geometry/rotations/euler/index.htm
	# Coordinate System: right hand
	# Positive angle: right hand
	# Order of euler angles: heading first, then attitude, then bank
	# matrix row column ordering:
	# [m00 m01 m02]    [m0 m1 m2]
	# [m10 m11 m12]    [m3 m4 m5]
	# [m20 m21 m22]    [m6 m7 m8]
	def to_euler(self):

		# Assuming the angles are in degrees
		if self.m21 > 0.998: # singularity at north pole
			heading = math.degrees(math.atan2(self.m13,self.m33))
			attitude = 90
			bank = 0
			return [heading,bank,attitude]

		if self.m21 < -0.998: # singularity at south pole
			heading = math.degrees(math.atan2(self.m13,self.m33))
			attitude = -90
			bank = 0
			return [heading,bank,attitude]

		heading = math.degrees(math.atan2(-self.m31,self.m11))
		bank = math.degrees(math.atan2(-self.m23,self.m22))
		attitude = math.degrees(math.asin(self.m21))

		return [heading,bank,attitude]


	def to_euler2(self):


		angle_y = -math.asin(mat[2]) # Calcul de l'Angle Y
		C = math.cos(angle_y)

		if math.fabs(C) > 0.005:	# Gimbal lock ?

			rx = mat[10] / C # Non, donc calcul de l'angle X
			ry = -mat[6] / C

			angle_x = math.degrees(math.atan2(ry, rx))

			rx = mat[0] / C            # Calcul de l'angle Z
			ry = -mat[1] / C

			angle_z  = math.degrees(math.atan2(ry, rx))

		else: # Gimbal lock

			angle_x  = 0   # Angle X ? 0
			rx = mat[5] # Calcul de l'angle Z
			ry = mat[4]

			angle_z  = math.degrees(math.atan2(ry, rx))


		#angle_x = clamp(angle_x, 0, 360);  /* Modulo ;) */
		#angle_y = clamp(angle_y, 0, 360);
		#angle_z = clamp(angle_z, 0, 360);

		return [angle_x, angle_y, angle_z]



	def scale(self,f):
		self.m11 = self.m11 * f
		self.m12 = self.m12 * f
		self.m13 = self.m13 * f
		self.m21 = self.m21 * f
		self.m22 = self.m22 * f
		self.m23 = self.m23 * f
		self.m31 = self.m31 * f
		self.m32 = self.m32 * f
		self.m33 = self.m33 * f
		return



#------------------------------------------------------------------------------------




def matrix_mult_matrix_bad(a,b):

	m = [0.0,0.0,0.0,0.0,  0.0,0.0,0.0,0.0,  0.0,0.0,0.0,0.0,  0.0,0.0,0.0,0.0]

	m[0] = a[0] * b[0] + a[1] * b[4] + a[2] * b[8]
	m[1] = a[0] * b[1] + a[1] * b[5] + a[2] * b[9]
	m[2] = a[0] * b[2] + a[1] * b[6] + a[2] * b[10]

	m[4] = a[4] * b[0] + a[5] * b[4] + a[6] * b[8]
	m[5] = a[4] * b[1] + a[5] * b[5] + a[6] * b[9]
	m[6] = a[4] * b[2] + a[5] * b[6] + a[6] * b[10]

	m[8] = a[8] * b[0] + a[9] * b[4] + a[10] * b[8]
	m[9] = a[8] * b[1] + a[9] * b[5] + a[10] * b[9]
	m[10] = a[8] * b[2] + a[9] * b[6] + a[10] * b[10]

	v = [m[3], m[7], m[11]]

	v = matrix_mult_vector(b,v)

	m[3] = v[0]
	m[7] = v[1]
	m[11] = v[2]

	return m






def normal_to_euler(n):

	e = [0.0,0.0,0.0]

	e[0] = math.degrees(math.atan2(n[1],n[0]))
	e[1] = math.degrees(math.acos(n[2]))
	e[2] = 0.0

	return e





def vector_distance_squared(a,b):

	dx = a[0] - b[0]
	dy = a[1] - b[1]
	dz = a[2] - b[2]

	return [dx*dx,dy*dy,dz*dz]




def matrix44_calc_ldhm_matrix(a,alpha,d,theta):

	#print "a: %f"%(a)+" alpha:%f"%(alpha)+" d : %f"%(d) + " theta:%f"%(theta)

	#m[0] = math.cos(math.radians(theta))
	#m[1] = -math.sin(math.radians(theta)) * math.cos(math.radians(alpha))
	#m[2] = math.sin(math.radians(theta)) * math.sin(math.radians(alpha))
	#m[3] = a * math.cos(math.radians(theta))

	#m[4] = math.sin(math.radians(theta))
	#m[5] = math.cos(math.radians(theta)) * math.cos(math.radians(alpha))
	#m[6] = -math.cos(math.radians(theta)) * math.sin(math.radians(alpha))
	#m[7] = a * math.sin(math.radians(theta))

	#m[8] = 0
	#m[9] = math.sin(math.radians(alpha))
	#m[10] = math.cos(math.radians(alpha))
	#m[11] = d

	alpha = math.radians(alpha)
	theta = math.radians(theta)
	c = math.cos(theta)
	s = math.sin(theta)
	ca = math.cos(alpha)
	sa = math.sin(alpha)

	m = matrix44(c, -s, 0, a,    s*ca, c*ca, -sa, -sa*d, s*sa, c*sa, ca, ca*d, 0, 0, 0, 1)

	return m


def axis_to_points_matrix(axis,m):

	#print "matrix"
	#print m

	l = int(len(m) / 4)
	#print "len : %i"%(l)

	if l < 1:
		print "need a least 1 line"
		return None

	#print "len axis : %i"%(len(axis))

	points = []

	for j in range(0,len(axis)):
		#print "j : %i"%(j)
		v = [0.0,0.0,0.0]

		dhmatrix = None
		for i in range(0,l):
			#print "i : %i"%(i)
			if i == l-1:
				ma = matrix44_calc_ldhm_matrix(m[i*4], m[i*4+1], m[i*4+2], m[i*4+3])
			else:
				ma = matrix44_calc_ldhm_matrix(m[i*4], m[i*4+1], m[i*4+2], m[i*4+3] + axis[j][i])

			if i == 0:
				dhmatrix = ma
			else:
				dhmatrix = dhmatrix.mult_matrix2(ma)

			#print "matrix:"
			#print dhmatrix

		#print axis[j]

		v = dhmatrix.mult_vector(v)
		#print "x: %f"%(v[0])+" Y: %f"%(v[1])+" Z: %f"%(v[2])


		# euler angles
		#f = matrix33_to_euler2(dhmatrix)
		#print "euler  a:%f"%(f[0])+ " b:%f"%(f[1])+ " c:%f"%(f[2])

		points.append(v)

	return points



def axis_to_point_robot (axis,robot):

	print "axis_to_point"

	print "axis:"
	print axis
	print "robot axes : %d"%(robot.axes)

	v = vector3(0.0,0.0,0.0)

	dhmatrix = None
	i = 0
	for l in robot.links:

		if l.type == "LINK":

			if i == robot.axes-1:
				m = matrix44_calc_ldhm_matrix(l.a, l.alpha, l.d, l.theta)
			else:
				print "i=%d"%(i)
				m = matrix44_calc_ldhm_matrix(l.a, l.alpha, l.d, l.theta+axis[i])

			if i == 0:
				dhmatrix = m
			else:
				dhmatrix = dhmatrix.mult_matrix(m)

			i += 1

		#print "matrix:"
		#print dhmatrix

	v = dhmatrix.mult_vector(v)
	#print "x: %f"%(v[0])+" Y: %f"%(v[1])+" Z: %f"%(v[2])

	return v



def axis_to_points_robot(axis,robot):

	if len(axis) == 0:
		print "need a least 1 axis"
		return None

	points = []

	for j in range(0,len(axis)):

		v = [0.0,0.0,0.0]

		dhmatrix = None
		i = 0
		for l in robot.links:

			if l.type == "LINK":

				if i == robot.axes-1:
					m = matrix44_calc_ldhm_matrix(l.a, l.alpha, l.d, l.theta)
				else:
					m = matrix44_calc_ldhm_matrix(l.a, l.alpha, l.d, l.theta+axis[j][i])

				if i == 0:
					dhmatrix = m
				else:
					dhmatrix = dhmatrix.mult_matrix(m)

				i += 1

			#print "matrix:"
			#print dhmatrix

		v = dhmatrix.mult_vector(v)
		#print "x: %f"%(v[0])+" Y: %f"%(v[1])+" Z: %f"%(v[2])

		points.append(v)

	return points



# p = points, n = normal du plan, c = center
def dist_plan_normale(p,n,c):

	#print "dist plan normale"
	#print "point"
	#print p
	#print "normale"
	#print n
	#print "center"
	#print c


	nn = math.sqrt(n[0]*n[0] + n[1]*n[1] + n[2]*n[2])
	#print "nn : %f"%(nn)

	delta = n[0]*c[0] + n[1]*c[1] + n[2]*c[2]
	delta = -delta
	#print "delta : %f"%(delta)

	d = abs(n[0] * p[0] + n[1] * p[1] + n[2] * p[2] + delta) / nn
	#print "distance: %f"%(d)

	return d


# protection orthogonale point sur plan(normale,point sur plan)
def proj_point_plan(pts,n,pp):

	#print "proj point plan"

	#print "point"
	#print pts
	#print "normale"
	#print n
	#print "centre"
	#print pp

	A = n[0]
	B = n[1]
	C = n[2]

	D = -A*pp[0] - B*pp[1] - C*pp[2]

	t = -(A * pts[0] + B * pts[1] + C * pts[2] + D)/(A*A + B*B + C*C)
	p = [pts[0] + A*t, pts[1] + B*t, pts[2] + C*t]
	return p


def find_plane(points):

	xyz0=numpy.mean(points, axis=0)
	M=points-xyz0
	u,s,vh = numpy.linalg.svd(M)
	v = vh.conj().transpose()
	return [v[:,-1],xyz0]


# main error function to minimize
# aa = robot+tool dhm table
# ah = axis_haut
# ag = axis_gauche,
# agp = axis_gauche_plan
# ad = axis_droite
# adp = axis_droite_plan
# ap = axis_proche
# app = axis_proche_plan
# al = axis_loin
# alp = axis_loin_plan
def error_func(axis_array,ah,ag,agp,ad,adp,ap,app,al,alp):

##	axis_array = [None] * (len(aa)+4)
##	axis_array[0] = 0
##	axis_array[1] = 0
##	axis_array[2] = 486.5
##	axis_array[3] = 0
##
##	for i in range(0,len(aa)):
##		axis_array[4+i] = aa[i]

	#print axis_array

	####################### plans ######################""

	# haut
	pts_haut = axis_to_points_matrix(ah,axis_array)
	print "---------------------------------------"
	thaut = find_plane(pts_haut)
	#print "haut"
	#print thaut

	#for i in range(0,len(pts_haut)):
	#	print "v %f"%(pts_haut[i][0])+" %f"%(pts_haut[i][1])+" %f"%(pts_haut[i][0])

	# gauche
	pts_gauche = axis_to_points_matrix(ag,axis_array)
	pts_gauche_plan = axis_to_points_matrix(agp,axis_array)
	tgauche = find_plane(pts_gauche)
	#print "gauche"
	#print tgauche

	# droite
	pts_droite = axis_to_points_matrix(ad,axis_array)
	pts_droite_plan = axis_to_points_matrix(adp,axis_array)
	tdroite = find_plane(pts_droite)
	#print "droite"
	#print tdroite

	# loin
	pts_loin = axis_to_points_matrix(al,axis_array)
	pts_loin_plan = axis_to_points_matrix(alp,axis_array)
	tloin = find_plane(pts_loin)
	#print "loin"
	#print tloin

	# proche
	pts_proche = axis_to_points_matrix(ap,axis_array)
	pts_proche_plan = axis_to_points_matrix(app,axis_array)
	tproche = find_plane(pts_proche)
	#print "proche"
	#print tproche



	################### erreur residuelle moyenne ####################"
	erm = 0.0
	for i in range(len(pts_haut)):
		d = dist_plan_normale(pts_haut[i],thaut[0],thaut[1])
		erm += d
	erm /= len(pts_haut)
	erm = abs(erm)

	############### erreur de distances ##################"
	#print "gauche droite"
	# gauche / droite
	dgauche = 0.0
	for i in range (0,len(pts_gauche)):
		# projeter points sur plan gauche
		#print pts_gauche[i]
		pp = proj_point_plan(pts_gauche[i],tgauche[0],tgauche[1])
		#print "proj point plan:"
		#print pp
		# calculer distance projete sur plan droite
		d = dist_plan_normale(pp,tdroite[0],tdroite[1])
		dgauche += (d - 230) #12x 25.4
		#print "d : %f"%(d)
		#print "dgauche : %f"%(dgauche)

	dgauche = abs(dgauche / len(pts_gauche))
	#print "erreur moyenne distance gauche droite : %f"%(dgauche)


	# proche / loin
	dproche = 0.0
	for i in range (0,len(pts_proche)):
		# projeter points sur plan proche
		pp = proj_point_plan(pts_proche[i],tproche[0],tproche[1])
		#print "proj point plan:"
		#print pp
		# calculer distance projete sur plan droite
		d = dist_plan_normale(pp,tloin[0],tloin[1])
		dproche += (d - 215)
		#print d

	dproche = abs(dproche / len(pts_proche))
	#print "erreur moyenne distance proche loin : %f"%(dproche)

	total_erreur_distance = (dproche+dgauche)/2
	#print "total erreurs  distance = :%f"%(total_erreur_distance)

	total_erreur = total_erreur_distance+erm
	print "err. resid. moy. : %f"%(erm)+" err. dist. : %f"%(total_erreur_distance)+ " erreur globale : %f"%(total_erreur)
	return abs(total_erreur)


def calc_robot_model_error(aa,axis_haut,axis_gauche,axis_gauche_plan,
					axis_droite,axis_droite_plan,axis_proche,axis_proche_plan,
					axis_loin,axis_loin_plan):

	r = scipy.optimize.fmin(error_func,aa,args=(axis_haut,axis_gauche,axis_gauche_plan,
					axis_droite,axis_droite_plan,axis_proche,axis_proche_plan,
					axis_loin,axis_loin_plan))

	print r

	return
