# -*- coding: utf-8 -*-


import os.path



pluginName = "Linuxcnc controller"
version = "0.1"
author = "Olivier Martinez"


def register(parent):

	mod = imp.load_source('linuxcnc_controller', './plugins/linuxcnc_controller/linuxcnc_controller.py')
	mod2 = imp.load_source('linuxcnc_controller', './plugins/linuxcnc_controller/linuxcnc_controller_panel.py')

	parent.add_object('robot.config.panel.general','Linuxcnc panel general',mod2.new_linuxcnc_controller_panel_general)
	parent.add_object('robot.config.panel.axis','Linuxcnc panel axis',mod2.new_linuxcnc_controller_panel_axis)

	parent.add_object('controller','Linuxcnc',mod.new_linuxcnc_controller)


	return
