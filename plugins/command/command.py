# -*- coding: utf-8 -*-

LINUXCNC_STATE_ESTOP_RESET = 2
LINUXCNC_STATE_ESTOP = 1
LINUXCNC_STATE_ON = 4
LINUXCNC_STATE_OFF = 3



def on_estop_toggled(widget,parent):

	print "estop toggled"

	if parent == None:
		return

	if widget.get_active() == False:
		parent.applog("Command",0,"emergency stop reset")
		#todo
		parent.broadcast("command state %d"%(LINUXCNC_STATE_ESTOP_RESET))
	else:
		parent.applog("Command",2,"emergency stop")
		#todo
		parent.broadcast("command state %d"%(LINUXCNC_STATE_ESTOP))

	return


def on_start_toggled(widget, parent):

	print "start toggled"

	if parent == None:
		return

	if widget.get_active() == False:
		parent.applog("system",0,"powering down")
		# linuxcnc.STATE_OFF
		parent.broadcast("command state %d"%(LINUXCNC_STATE_OFF))
	else:
		parent.applog("system",0,"powering up")
		parent.broadcast("command state %d"%(LINUXCNC_STATE_ON))

	return


