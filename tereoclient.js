// =================== MQTT =========================

var http     = require('http')
  , httpServ = http.createServer()
  , mosca    = require('mosca')
  , mqttServ = new mosca.Server({});

mqttServ.attachHttpServer(httpServ);



// ================= client robot ==========================

var ex = require('express');
var bp = require('body-parser');
var fs = require('fs');
var os = require('os');
//var io = require('socket.io');
var path = require('path');
var EventEmitter = require('events').EventEmitter;
var w = require("http");
var engine = require('ejs').__express;
var re = require("request");
var sys = require('sys');
var exec = require('child_process').exec;

var parentDir = process.cwd();

var tereo_proc = new Array();

//======================== nodered process =======================

var noderedproc;
var asknodered_restart = 0;
var nodered_replacejson = 0;
var nodered_json = "";
var nodered_error = 0;
var nodered_uid = "";

console.log("starting nodered");
try {
	process.chdir(parentDir+"\\nodered");
	noderedproc = exec('node red.js', function (error, stdout, stderr) {
		console.log(error);
		console.log(stdout);
		console.log(stderr);
	});
} catch(e) { console.log(e); }

process.chdir(parentDir);

try { 
	noderedproc.on('exit', function (code) {

		console.log('nodered exited : ' + code);
		if (nodered_replacejson == 1) {
  
			// remove script
			try {
			fs.unlink(parentDir+'\\nodered\\flows_birdofprey.json', function(err) {
				if (err) {
					console.log(err);
					nodered_error = 1;
				}
			});
			} catch(e) { console.log(e); }
			// save script on temp directory
			fs.appendFile(parentDir+'\\nodered\\flows_birdofprey.json',nodered_json, function(err) {
				
				if (err) {					
					console.log('script creation error:'+err);
					nodered_error = 1;
				}
			
				// trying to restart nodered
				if (asknodered_restart == 1) {
					console.log('restarting nodered');
					asknodered_restart = 0;
					noderedproc = exec('node '+parentDir+'\\nodered\red.js', function (error, stdout, stderr) {
					});
				}
			
				if (nodered_error == 0) {			
					publish_cloud(nodered_uid+"@rc@logic deployed");
				}	
			
			});
  
		}

	});
} catch(e) { console.log(e); }

//======================= buid in events =========================

var cp = os.cpus().toString();
var child;





// fired when a message is published
mqttServ.on('published', function(packet, client) {

	if (client) {
		console.log('Published', packet['payload'].toString());
		//console.log('Client', client);

		msg = packet['payload'].toString();
		
		uid = "";
		
		// get uid
		var pos = msg.indexOf("@rc@");
			
		if (pos>0) {
			uid = msg.substr(0,pos);
			msg = msg.substr(pos+4,msg.length-pos);
		}
		
		pos = msg.indexOf(' ');
		if (pos>0) {
			tereocom = msg.substr(0,pos);
			tereodata = msg.substr(pos+1,msg.length-pos-1);
		} else {
			tereocom = msg;
			tereodata = "";
		}
		

				
		// module linux uninstallation -----------------------------------------------------------------
		if (tereocom == 'tcr_linux_module_uninstall') {
			console.log('linux module uninstall');	
					
			// sudo apt-get install <package>
			var child;
			child = exec('apt-get remove '+tereodata, function (error, stdout, stderr) {
				//listener.sockets.emit( 'tcr_moduleuninstall', {'stdout':stdout, 'stderr':stderr});
				if (error !== null) {
					console.log('exec error: '+error);
					publish_cloud(uid+"@rc@error "+error+"\r\n"+stderr);
				} else {
					publish_cloud(uid+"@rc@"+stdout);
				}
			});
		}

		// module linux installation -----------------------------------------------------------------
		if (tereocom == 'tcr_linux_module_install') {
			console.log('linux module install');	

			// remove script
			fs.unlink('/tmp/tcr_script.sh', function(err) {
				//console.log('unlink error:'+err);
			});
			// save script on temp directory
			fs.appendFile('/tmp/tcr_script.sh',script, function(err) {
				
				if (err) {					
					publish_cloud(uid+"@rc@error "+stderr);
					console.log('script creation error:'+err);
					//listener.sockets.emit( 'tcr_moduleinstall', '{"stderr":"'+err+'"}');
				} else {
					// run script
				
					var child;
					child = exec('sh /tmp/tcr_script.sh', function (error, stdout, stderr) {
						if (error !== null) {
							//console.log('exec error: '+error);
							//listener.sockets.emit( 'tcr_moduleinstall', {'stdout':stdout, 'stderr':stderr});
							publish_cloud(uid+"@rc@error "+stderr);
							return;
						}
						

					});


				}
			});

			publish_cloud(uid+"@rc@"+stdout);

		}

		
		// module linux update ----------------------------------------------------------------------
		if (tereocom == 'tcr_linux_module_update') {
			console.log('linux module update');
		
		}
		
		// module linux info -------------------------------------------------------------------------
		if (tereocom == 'tcr_linux_module_info') {
			console.log('linux module info');	
			// aptitude show <package> or dpkg --status <package>
			var child;
			child = exec('aptitude show '+tereodata, function (error, stdout, stderr) {
				if (error !== null) {
					//console.log('exec error: '+error);
					publish_cloud(uid+"@rc@error "+error+"\r\n"+stderr);
				} else {
					publish_cloud(uid+"@rc@"+stdout);
				}
			});
		}
		
		// launch script -------------------------------------------------------------------------
		if (tereocom == 'tcr_launch') {
			console.log('launch');	
			
			process.chdir(parentDir+"\\sandbox");
			try {
				tereo_proc[tereodata] = exec('node '+tereodata, function (error, stdout, stderr) {
					if (error !== null) {
						//console.log('exec error: '+error);
						publish_cloud(uid+"@rc@error "+error+"\r\n"+stderr);
					} 
				});
			} catch(e) { console.log(e); }
			publish_cloud(uid+"@rc@OK");
			process.chdir(parentDir);
		}
		
		// stop script -------------------------------------------------------------------------
		if (tereocom == 'tcr_stop') {
			console.log('stop');	
			// aptitude show <package> or dpkg --status <package>
						
			try {
				console.log('kill '+tereodata);
				tereo_proc[tereodata].kill('SIGTERM');
			} catch(e) { console.log(e); }
		}
		
		// system info -------------------------------------------------------------------------
		if (tereocom == 'tcr_system_info') {
			console.log('system info');
			publish_cloud(uid+"@rc@"+'tmpdir:'+os.tmpdir()+' hostname:'+os.hostname()+' type:'+os.type()+' platform:'+os.platform()+' arch:'+os.arch()+' release:'+os.release()+' uptime:'+os.uptime()+' totalmem:'+os.totalmem()+' freemem:'+ os.freemem());
		}
		
		// move -------------------------------------------------------------------------
		if (tereocom == 'tcr_move') {
			console.log('move');
			process.chdir(parentDir+"\\sandbox");
			try {
				var child;
				child = exec('move /Y '+tereodata, function (error, stdout, stderr) {
					if (error !== null) {
						//console.log('exec error: '+error);
						publish_cloud(uid+"@rc@error "+error+"\r\n"+stderr);
					} else {
						publish_cloud(uid+"@rc@"+stdout);
					}
				});
			} catch(e) { console.log(e); }
			process.chdir(parentDir);
		}
		
		// wget -------------------------------------------------------------------------
		if (tereocom == 'tcr_wget') {
			console.log('wget install');
			
			console.log(parentDir+'\\wget "'+tereodata+'"');
			
			var child;
			
			process.chdir(parentDir+"\\sandbox");
			
			child = exec(parentDir+'\\wget "'+tereodata+'"',  function (error, stdout, stderr) {
				console.log(error);
				console.log(stdout);
				console.log(stderr);
				if (error !== null) {
					console.log('exec error: '+error);
					publish_cloud(uid+"@rc@error "+error+"\r\n"+stderr);
				} else {
					console.log("ok");
					if (stdout.indexOf('saved')) { publish_cloud(uid+"@rc@"+stdout); }
				}
				console.log("end");
				process.chdir(parentDir);
			});
		}
		
		// unzip -------------------------------------------------------------------------
		if (tereocom == 'tcr_unzip') {
			console.log('unzip');
			
			var child;
			
			
			child = exec('unzip.exe sandbox\\'+tereodata+' -d sandbox',  function (error, stdout, stderr) {
				console.log(error);
				console.log(stdout);
				console.log(stderr);
				if (error !== null) {
					console.log('exec error: '+error);
					publish_cloud(uid+"@rc@error "+error+"\r\n"+stderr);
				} else {
					console.log("ok");
					publish_cloud(uid+"@rc@"+stdout);
				}
				console.log("end");
				//process.chdir(parentDir);
			});
		}
		
		// untar -------------------------------------------------------------------------
		if (tereocom == 'tcr_untar') {
			console.log('untar');
			
			var child;
			
			
			child = exec('tartool sandbox\\'+tereodata+' sandbox',  function (error, stdout, stderr) {
				console.log(error);
				console.log(stdout);
				console.log(stderr);
				if (error !== null) {
					console.log('exec error: '+error);
					publish_cloud(uid+"@rc@error "+error+"\r\n"+stderr);
				} else {
					console.log("ok");
					publish_cloud(uid+"@rc@"+stdout);
				}
				console.log("end");

			});
		}
		

		
		// npm install -------------------------------------------------------------------------
		if (tereocom == 'tcr_nodejs_module_install') {
			console.log('nodejs module install');
			console.log('npm install '+tereodata);
			var child;
			
			process.chdir(parentDir+"\\sandbox");
			
			child = exec('npm install '+tereodata,  function (error, stdout, stderr) {
				console.log(error);
				console.log(stdout);
				console.log(stderr);
				if (error !== null) {
					console.log('exec error: '+error);
					publish_cloud(uid+"@rc@error "+error+"\r\n"+stderr);
				} else {
					console.log("ok");
					publish_cloud(uid+"@rc@"+stdout);
				}
				console.log("end");
				process.chdir(parentDir);
			});
		}

		// npm uninstall -------------------------------------------------------------------------
		if (tereocom == 'tcr_nodejs_module_uninstall') {
			console.log('nodejs module uninstall');
			console.log('npm uninstall '+tereodata);
			var child;
			
			process.chdir(parentDir+"\\sandbox");
			child = exec('npm uninstall '+tereodata, function (error, stdout, stderr) {
				if (error !== null) {
					console.log('exec error: '+error);
					publish_cloud(uid+"@rc@error "+error+"\r\n"+stderr);
				} else {
					publish_cloud(uid+"@rc@"+stdout);
				}
				console.log("end");
				process.chdir(parentDir);
			});
		}	

		// npm update -------------------------------------------------------------------------
		if (tereocom == 'tcr_nodejs_module_update') {
			console.log('nodejs module update');
			var child;

			process.chdir(parentDir+"\\sandbox");
			child = exec('npm update '+tereodata, function (error, stdout, stderr) {
				if (error !== null) {
					console.log('exec error: '+error);
					publish_cloud(uid+"@rc@error "+error+"\r\n"+stderr);
				} else {
					publish_cloud(uid+"@rc@"+stdout);
				}
				console.log("end");
				process.chdir(parentDir);
			});
		}
		
		// npm info -------------------------------------------------------------------------
		if (tereocom == 'tcr_nodejs_module_info') {
			console.log('nodejs module info');
			var child;

			process.chdir(parentDir+"\\sandbox");
			child = exec('npm list '+tereodata, function (error, stdout, stderr) {
				if (error !== null) {
					console.log('exec error: '+error);
					publish_cloud(uid+"@rc@error "+error+"\r\n"+stderr);
				} else {
					publish_cloud(uid+"@rc@"+stdout);
				}
				console.log("end");
				process.chdir(parentDir);
			});
		}
		
		// nodered injection -------------------------------------------------------------------
		if (tereocom == 'tcr_deploy_nodered') {
		
			console.log('nodejs injection');

			nodered_replacejson = 1;
			nodered_json = tereodata;
			nodered_error = 0;
			nodered_uid = uid;
			
			// stop nodered
			asknodered_restart = 1;
			try {
				console.log('kill noderedproc');
				noderedproc.kill('SIGTERM');
			} catch(e) { console.log(e); }
			
		}		
	
	
	}
	
});
// fired when a client connects
mqttServ.on('clientConnected', function(client) {
	if (client) {
		console.log('Client Connected:', client.id);
	}
});

// fired when a client disconnects
mqttServ.on('clientDisconnected', function(client) {
	if (client) {
		console.log('Client Disconnected:', client.id);
	}
});

function publish_cloud(msg) {

	console.log("publish cloud");
	
	try {
		var messageObj = {
			'topic': "/RoboticCloud",
			'retained': false,
			'qos': 0,
			'payload': msg,
			//'timestamp': moment(),
			//'subscriptionId': subscription.id,
			//'color': websocketclient.getColorForSubscription(subscription.id)
		};			
		mqttServ.publish(messageObj);
		
	} catch(e) { console.log(e); };

}



/*
var listener = io.listen(server);

listener.set('log level', 1);

var globalEventLoop = new EventEmitter();



function getFilter(ext) {
    return function(filename) {
        return filename.match(new RegExp('\\.' + ext + '$', 'i'));
    };
}


var nb_clients = 0;

listener.sockets.on('connection', function (socket) {

	console.log('connected');
	//console.log(socket);

	nb_clients += 1;

	//socket.send('initialize');  // opens socket with client
     
	socket.on('disconnect', function(){
		nb_clients -= 1;
		console.log('client disconnected');

	});

	// module uninstallation -----------------------------------------------------------------
	socket.on('tcr_moduleuninstall', function(script) {
		//console.log('module uninstall');	
		// sudo apt-get install <package>
		var child;
		child = exec('apt-get remove '+data, function (error, stdout, stderr) {
			listener.sockets.emit( 'tcr_moduleuninstall', {'stdout':stdout, 'stderr':stderr});
			if (error !== null) {
				console.log('exec error: '+error);
			}
		});
	});

	// module installation -----------------------------------------------------------------
	socket.on('tcr_moduleinstall', function(script) {
		console.log('module install');	

		// remove script
		fs.unlink('/tmp/tcr_script.sh', function(err) {
			//console.log('unlink error:'+err);
		});
		// save script on temp directory
		fs.appendFile('/tmp/tcr_script.sh',script, function(err) {
			
			if (err) {
				console.log('script creation error:'+err);
				listener.sockets.emit( 'tcr_moduleinstall', '{"stderr":"'+err+'"}');
			} else {
				// run script
			
				var child;
				child = exec('sh /tmp/tcr_script.sh', function (error, stdout, stderr) {
					if (error !== null) {
						console.log('exec error: '+error);
						listener.sockets.emit( 'tcr_moduleinstall', {'stdout':stdout, 'stderr':stderr});
					}

				});


			}
		});

		listener.sockets.emit( 'tcr_moduleinstall', '{"stdout":"ok"}');
	});
	
	
	
	// module uninstallation
	socket.on('tcr_moduleuninstall', function() {
	
	
	});
	
	// module update
	socket.on('tcr_moduleupdate', function() {
	
	
	});
	
	// module info -------------------------------------------------------------------------
	socket.on('tcr_moduleinfo', function(data) {
		console.log('module info');	
		// aptitude show <package> or dpkg --status <package>
		var child;
		child = exec('aptitude show '+data, function (error, stdout, stderr) {
			listener.sockets.emit( 'tcr_moduleinfo', {'stdout':stdout, 'stderr':stderr});
			if (error !== null) {
				console.log('exec error: '+error);
			}
		});
	});
	
	// system info -------------------------------------------------------------------------
	socket.on('tcr_systeminfo', function(data) {
		console.log('system info');
		listener.sockets.emit( 'tcr_systeminfo', {'tmpdir':os.tmpdir(), 'hostname':os.hostname(), 'type':os.type(),
													'platform':os.platform(), 'arch': os.arch(), 'release': os.release(),
													'uptime': os.uptime(), 'totalmem': os.totalmem(), 'freemem': os.freemem() }
							);	
	});
	
	// run
	socket.on('tcr_run', function() {
	
	
	});
	
	// stop
	socket.on('tcr_stop', function() {
	
	
	});
	
	
	var interval = setInterval( 
		function sendstats() {
			listener.sockets.emit( 'cpu_usage', os.cpus()  );			
		}, 1000
	);

	var interval2 = setInterval( 
		function sendstats() {
			ram = [os.totalmem(),os.freemem()];
			listener.sockets.emit( 'ram_usage', ram );		
		}, 5000
	);	
	
		
});
*/


// ========================= web interface ============================

var app = ex();
server = w.createServer(app);

//app.use(bp());

//app.use(ex.static(__dirname + '/'));
//app.set('port', 62355);
//app.set('Content-Type', 'text/html');




//app.use(ex.methodOverride());

var allowCrossDomain = function(req, res, next) {
	res.header('Access-Control-Allow-Origin','*');
	res.header('Access-Control-Allow-Headers','X-Requested-With');
	res.header('Access-Control-Allow-Methods','GET,PUT,POST,DELETE');

	if ("OPTIONS" == req.method) {
		res.send(200);
	} else {
		next();
	}
};
app.use(allowCrossDomain);
app.set('Content-Type','text/html');
app.use(bp());
app.use(ex.static(__dirname+'/'));
app.set('port',62355);
app.set('views', __dirname);
app.engine('ejs',engine);


// homepage rendered on the fly,
// send plugin files
app.get('/', function(req, res){

	res.write('_tereo(\'{');
	
	// send scripts files
	var i=0;
	/*scripts.forEach(function (s) {
		if (i>0) {
			res.write(',');
		}
		i=1;
		res.write('"script":"'+s+'"');
	});
	
	// send style files
	styles.forEach(function (s) {
		if (i>0) {
			res.write(',');
		}
		i=1;
		res.write('"style":"'+s+'"');
	});	*/
		
	res.end('}\')');
});

app.get('/:file', function (req, res) {
	console.log(file);
	file = req.params.file
	
	if (file == "config") {
	
		res.render('config.ejs', {
			messageok: "",
			messagebad: ""
		});
	
	} else {
		console.log("file:"+file);
		res.sendfile(file);
	}
});

// configuration
app.get('/config', function(req, res){

	// todo : only for localhost
	


});

// configuration validation
app.post('/config', function(req, res){

    var login = req.body.login;
	var pass = req.body.password;
	var server = req.body.server;
	var name = os.hostname();

	re({
		uri: req.body.server+"content/register_machine.php",
		method: "POST",
		form : { login: login, pass: pass, name: name}
	}, function(error, response, body) {

		console.log("response:"+body);
		try {
			var j = JSON.parse(body);

			if (j['status'] == 'OK')  {
				res.render('config.ejs', {
					messageok: "Registration successfull",
					messagebad: ""
				});			
			} else if (j['status'] == 'BAD')  {
				res.render('config.ejs', {
					messageok: "",
					messagebad: j['description']
				});		
			} else {
				res.render('config.ejs', {
					messageok: "",
					messagebad: "Error"
				});			
			}
			
		} catch (e) {
			console.error("Parsing error:", e); 
		}
		
	});
	

});





// Start the web server
server.listen(62355, function() {
  console.log('Started listening on port: ' + 62355);


});


httpServ.on("clientError",function(err) {
	console.log(err);
});

try {
	httpServ.listen(3000);
} catch(e) { console.log(e); }


