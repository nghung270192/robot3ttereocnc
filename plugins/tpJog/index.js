var tpJog;

var tpJog = function tpJog() {

	this.pluginName = "Jog";
	this.pluginVersion = "0.1";
	this.pluginAuthor = "Olivier Martinez";
	this.panel;
	
};

tpJog.prototype = {

	"register":function(parent) {

		//mod = imp.load_source('tereocnc.plugins.jog', './plugins/jog/jog.py')
		//tereoFunctions["Jog.jog"] = tpJog_on_Jogjog_activate;
		//var msg ={};
		//msg['msg'] = "jogpanel";
		//tereoMain.add_icon('Jog','plugins/tpJogPanel/Jog.png', true, "@tpJog&on_jog_activate",msg,'tpJog');

	},
	
	"on_jog_activate":function(arg) {
	
	
		//console.log("on jog activate");
		
		var f = $("#jog_panel");
		if(f.length){
		
			//L'élément existe
			if ($("#tpJogPanelJog").hasClass("active")) {
				$("#jog_panel").hide();
			} else {
				$("#jog_panel").show();
			}
		  
		} else {
		
			//L'élément n'existe pas
			this.panel = new jogView();
			this.panel.robotname = arg;
			//parent.add_object("Jog_panel", f);
			this.panel.__init__(arg);
			$("#jog_panel").show();
			
		}
		
		this.panel.populate();
		
		$(window).trigger('resize');
		
	
	},

	"unload":function() {
	
	}
	

};


tereoPlugins['tpJog'] = new tpJog();


var jogView = function() {

	this.uid = (((1+Math.random())*0x100000)|0).toString(16).substring(1);
	this.lastmsg ="";
	this.Jogs = {};
	this.calibrations = {};
	this.robotname ="";

}

jogView.prototype = {

	"__init__": function (rn) {

		$( "#tereopanels").append('<div class="tpPanel" id="jog_panel"><div class="tpPanelTitle">Jog panel - '+rn+'</div><div id="jogcontent"></div></div>');

	},

	"populate":function () {

		//console.log("populate");

	
		var render = "<table>";
		render += "<tr><th>1</th><th>2</th><th>3</th><th>4</th><th>5</th><th>6</th></tr>";
		render += "<tr>";
		render += "	<td><button class=\"form_button\" onclick=\"tpJogPanel_on_up(0);\"><img src=\"media/gtk-sort-ascending.png\"></button></td>";
		render += "	<td><button class=\"form_button\" onclick=\"tpJogPanel_on_up(1);\"><img src=\"media/gtk-sort-ascending.png\"></button></td>";
		render += "	<td><button class=\"form_button\" onclick=\"tpJogPanel_on_up(2);\"><img src=\"media/gtk-sort-ascending.png\"></button></td>";
		render += "	<td><button class=\"form_button\" onclick=\"tpJogPanel_on_up(3);\"><img src=\"media/gtk-sort-ascending.png\"></button></td>";
		render += "	<td><button class=\"form_button\" onclick=\"tpJogPanel_on_up(4);\"><img src=\"media/gtk-sort-ascending.png\"></button></td>";
		render += "	<td><button class=\"form_button\" onclick=\"tpJogPanel_on_up(5);\"><img src=\"media/gtk-sort-ascending.png\"></button></td>";
		render += "</tr>";
		render += "<tr>";
		render += "	<td><button class=\"form_button\" onclick=\"tpJogPanel_on_down(0);\"><img src=\"media/gtk-sort-descending.png\"></button></td>";
		render += "	<td><button class=\"form_button\" onclick=\"tpJogPanel_on_down(1);\"><img src=\"media/gtk-sort-descending.png\"></button></td>";
		render += "	<td><button class=\"form_button\" onclick=\"tpJogPanel_on_down(2);\"><img src=\"media/gtk-sort-descending.png\"></button></td>";
		render += "	<td><button class=\"form_button\" onclick=\"tpJogPanel_on_down(3);\"><img src=\"media/gtk-sort-descending.png\"></button></td>";
		render += "	<td><button class=\"form_button\" onclick=\"tpJogPanel_on_down(4);\"><img src=\"media/gtk-sort-descending.png\"></button></td>";
		render += "	<td><button class=\"form_button\" onclick=\"tpJogPanel_on_down(5);\"><img src=\"media/gtk-sort-descending.png\"></button></td>";
		render += "</tr>";
		render += "<tr>";
		render += "	<td><div id=\"axisvalue1\">0.0</div></td>";
		render += "	<td><div id=\"axisvalue2\">0.0</div></td>";
		render += "	<td><div id=\"axisvalue3\">0.0</div></td>";
		render += "	<td><div id=\"axisvalue4\">0.0</div></td>";
		render += "	<td><div id=\"axisvalue5\">0.0</div></td>";
		render += "	<td><div id=\"axisvalue6\">0.0</div></td>";
		render += "</tr></table>";
		//console.log(render);
		$("#jogcontent").html(render);
		tereoObjects['jog'] = this;

		tereoMain.add_trigger("broadcast_position", tpLog_update_robots);		
		
	},
	
	"refresh":function () {
		
		//console.log("refresh");
		
		
		
	}
	
}



function tpJogPanel_on_up(ax) {

	//console.log("jog up");
	

	jog = tereoObjects['jog'];
	

/*
	for (r in jog.robots) { //self.tereo.env.robots:
		
		if (jog.robots.hasOwnProperty(r)) {
			
			if (jog.robotname == jog.robots[r].name) {
			*/
				var msg = {}
				msg['msg'] = "JOG_AXIS";
				msg['uid'] = jog.uid+"jau";
				msg['arg'] = jog.robotname+":2:"+ax+":1:10";
				msg['callback'] = "";
				tereoMain.call("sendMqttMsg",JSON.stringify(msg));		
			
/*			}
			
		}
		
	}*/


}


function tpJogPanel_on_down(ax) {

	//console.log("jog down");

	jog = tereoObjects['jog'];
/*
	for (r in jog.robots) { //self.tereo.env.robots:
		
		if (jog.robots.hasOwnProperty(r)) {
			
			if (jog.robotname == jog.robots[r].name) {
			*/
				var msg = {}
				msg['msg'] = "JOG_AXIS";
				msg['uid'] = jog.uid+"jad";
				msg['arg'] = jog.robotname+":2:"+ax+":-1:10";
				msg['callback'] = "";
				tereoMain.call("sendMqttMsg",JSON.stringify(msg));		
			
	/*		}
			
		}
		
	}*/

}


function tpLog_update_robots(arg) {


	var dirty = false;
	
	jog = tereoObjects['jog'];
	
	try {
		//arg = arg.replace(' ','');
		arg = arg.replace('\n','');
		arg = arg.replace('\r','');
		var list = JSON.parse(arg);
		list = list["position"];
	} catch(e) { console.log(e);
		return;
	}
	
	

	if (jog.robotname == "") {
		
		return;
	}
	
	//console.log("list")
	//console.log(list);
	
	
	if (typeof list.name !== "undefined") {
	
		if (list.name == jog.robotname) {
			
			$("#axisvalue1").html(parseInt(list['X']).toFixed(1));
			$("#axisvalue2").html(parseInt(list['Y']).toFixed(1));
			$("#axisvalue3").html(parseInt(list['Z']).toFixed(1));
			$("#axisvalue4").html(parseInt(list['A']).toFixed(1));
			$("#axisvalue5").html(parseInt(list['B']).toFixed(1));
			$("#axisvalue6").html(parseInt(list['C']).toFixed(1));
		
		}
	
	}
			
}


