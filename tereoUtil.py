#!/usr/bin/python
import gtk
#import gtk.gdkgl as gdkgl
#import gtk.gtkgl as gtkgl

class splashScreen():
	def __init__(self,filename):
		print "init splash"
		#DONT connect 'destroy' event here!
		self.window = gtk.Window(gtk.WINDOW_TOPLEVEL)
		self.window.set_decorated(False)
		#self.window.set_title('Your app name')
		self.window.set_position(gtk.WIN_POS_CENTER)

		main_frame = gtk.Frame()
		self.window.add(main_frame)

		vbox = gtk.VBox(False, 0)
		main_frame.add(vbox)
		img = gtk.Image()
		vbox.pack_start(img, False, False, 0)
		self.label = gtk.Label("Loading plugin :")
		vbox.pack_start(self.label, False, False, 0)
		img.set_from_file(filename)


		self.window.show_all()

		print "end init splah"
