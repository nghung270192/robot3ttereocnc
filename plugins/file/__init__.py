# -*- coding: utf-8 -*-


import gtk
import os.path


pluginName = "File"
version = "0.1"
author = "Olivier Martinez"
file_icon = None

def register(parent):

	mod = imp.load_source('file', './plugins/file/file.py')

	# file button
	img = gtk.Image()
	img.set_from_file('./plugins/file/file.png')
	img.show()
	file_icon = gtk.ToolButton (img, "File")
	file_icon.connect("clicked", mod.on_file_activate, parent)
	parent.add_icon(file_icon)


