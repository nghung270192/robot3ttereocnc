# -*- coding: utf-8 -*-

import pygtk
pygtk.require("2.0")
import gtk




####################################
######### network window ###########
####################################

class network_view(gtk.Frame):

	def __init__(self,tereo):

		gtk.Frame.__init__(self)

		self.tereo = tereo

		self.set_label_align(0,0)


		al = gtk.Alignment(0.5, 0.5, 1.0, 1.0)
		al.set_padding(0,0,12,0)

		self.set_label("Network panel")

		self.add(al)


		al.show()

		sv = gtk.ScrolledWindow()

		al.add(sv)

		sv.show()

		vp = gtk.Viewport()
		sv.add(vp)
		vp.show()


		f = gtk.Frame()
		vp.add(f)

		f.show()

		self.vbox = gtk.VBox(False, 0)
		f.add(self.vbox)
		self.vbox.show()
		self.set_size_request(300,-1)
		self.show()

		self.populate()

		return



	def populate(self):


		if self.tereo.env == None:
			return

		#clear
		children = self.vbox.get_children()
		for iter in children:
			subchildren = iter.get_children()
			for subiter in subchildren:
				subiter.destroy()
			iter.destroy()

		#self.vbox.pack_start(ln,False,True,0)
		
		i = 0
		
		
		
		# 1 - robot
		
		for r in self.tereo.env.robots:

			# new robot
			frm = gtk.Frame()
			self.vbox.pack_start(frm,False,True,0)
			frm.show()

			tbl = gtk.Table(7,3,False)
			frm.add(tbl)
			tbl.show()

			ln = gtk.Label(str(i))
			ln.set_justify(gtk.JUSTIFY_RIGHT)
			ln.show()
			tbl.attach(ln,0,1,0,1)

			ln = gtk.Label(r.name)
			ln.set_justify(gtk.JUSTIFY_LEFT)
			ln.show()
			tbl.attach(ln,1,2,0,1)

			ln = gtk.Label("Status")
			ln.set_justify(gtk.JUSTIFY_RIGHT)
			ln.show()
			tbl.attach(ln,0,1,1,2)

			lbl = ""
			if r.client == None:
				lbl = "No connector"
			else:
				if hasattr(r.client,"instance") == True:
					if r.client.instance == None :
						lbl = "Not initialized"
					else:
						print "todo"
						#if r.client.instance.connected == True:
						#	lbl = "Connected"
						#else:
						#	if r.connecting == True:
						#		lbl = "Connecting..."
						#	else:
						#		lbl = "Disconnected"

			ln = gtk.Label(lbl)
			ln.set_justify(gtk.JUSTIFY_LEFT)
			ln.show()
			tbl.attach(ln,1,2,1,2)
			
			
			laste = ""
			#laste = r.client.instance.lasterror
	
			
			ln = gtk.Label("Err?")
			ln.set_justify(gtk.JUSTIFY_RIGHT)
			ln.show()
			tbl.attach(ln,0,1,2,3)

			ln = gtk.Label(laste)
			ln.set_justify(gtk.JUSTIFY_LEFT)
			ln.show()
			tbl.attach(ln,1,2,2,3)

			#--------------------


			ln = gtk.ToggleButton("Network client")
			ln.show()
			tbl.attach(ln,0,1,3,4)
			ln.connect("toggled", self.on_setnetclient, i)

			#---------------------

			

			i+=1


		return


	def on_setnetclient(self,widget,data):
		print "on setnetclient"
		if (widget.get_active()) :
			self.tereo.execute_function ("command.inout.start","Command inout start",self.tereo)
		else:
			self.tereo.execute_function ("command.inout.stop","Command inout stop",self.tereo)
		
		return




def check_network(self):

	print "check network"

	if hasattr(self,"env") == False:
		#print "no env"
		return True

	if self.env == None:
		#print "env null"
		return True

	for r in self.env.robot:
		#print "proto : " + r.proto
		if r.proto == "cross":
			#print "check cross"
			if r.client != None:
				if r.ip != "" and r.chan > 0:
					#print "ip chan ok"
					if r.client.connected == False:
						#print "not connected"
						if r.client.connecting == False:
							r.client.connecting = True
							self.applog("network",0,"not connected retrying...")
							#r.client.connectTCP(r.ip,r.chan, r.client)
							r.client.initialize()

	# update network
	netwin = self.builder.get_object("winNet")
	for w in gtk.window_list_toplevels():
		if w == netwin:
			self.populate_network()
			return w

	return True



