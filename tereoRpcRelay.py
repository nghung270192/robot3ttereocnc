#!/usr/bin/python

from gladevcp.gladebuilder import GladeBuilder
import hal
import collections
import os
import sys
import copy
import linuxcnc
import gobject

from twisted.internet import gtk2reactor
gtk2reactor.install()
from twisted.internet import reactor
from twisted.internet.protocol import Factory
from twisted.protocols.basic import LineReceiver
import tereoNet


print sys.argv



homedir = os.path.expanduser('~') + '/linuxcnc' 

inifile = ''
# from axis
if sys.argv[1] != "-ini": 
	raise SystemExit, "-ini must be first argument"

inifile = sys.argv[2]


c = linuxcnc.command()
s = linuxcnc.stat()
e = linuxcnc.error_channel()


comp = hal.component("tereoEntry")
comp.newpin("X", hal.HAL_FLOAT, hal.HAL_IN)
comp.newpin("Y", hal.HAL_FLOAT, hal.HAL_IN)
comp.newpin("Z", hal.HAL_FLOAT, hal.HAL_IN)
comp.newpin("A", hal.HAL_FLOAT, hal.HAL_IN)
comp.newpin("B", hal.HAL_FLOAT, hal.HAL_IN)
comp.newpin("C", hal.HAL_FLOAT, hal.HAL_IN)
#comp.newpin("grip", hal.HAL_FLOAT, hal.HAL_IN)
comp.ready()



hal.connect("tereoEntry.X","J0pos")
hal.connect("tereoEntry.Y","J1pos")
hal.connect("tereoEntry.Z","J2pos")
hal.connect("tereoEntry.A","J3pos")
hal.connect("tereoEntry.B","J4pos")
hal.connect("tereoEntry.C","J5pos")



#####################################################################
#	MAIN CLASS
#####################################################################

class tereo:



	##########
	## init ##
	##########
	def __init__(self):


		# load ini file
		#self.env = tereoEnvironment.environment()
		#self.env.confname = 'tereo1'  #todo : get from inifile path
		#self.env.set_home_dir(homedir)
		#self.env.pdb.load_ini_file(inifile)

		#self.env.load_tools()
		#self.env.load_flow()

		print "estop : %d"%(linuxcnc.STATE_ESTOP)
		print "state_on : %d"%(linuxcnc.STATE_ON)
		print "state_off : %d"%(linuxcnc.STATE_OFF)
		print "estop_reset : %d"%(linuxcnc.STATE_ESTOP_RESET)

		return



class serverFactory(Factory):
	def __init__(self):
		print "serverfactory init"
		#self.users = {} # maps user names to Chat instances
		return

	def buildProtocol(self, addr):
		print "buildprotocol"
		return tereoNet.server(c,s,e)




if __name__ == "__main__":
	tereo()
	#gtk.main()#!/usr/bin/env python
	#print "ok"
	reactor.listenTCP(8123, serverFactory())
	reactor.run()





