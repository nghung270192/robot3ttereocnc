# -*- coding: utf-8 -*-

import pygtk
pygtk.require("2.0")
import gtk


class file_view(gtk.Window):

	def __init__(self, tereo):

		gtk.Window.__init__(self)

		self.set_position(gtk.WIN_POS_CENTER)
		self.set_default_size(640, 480)

		self.tereo = tereo

		self.choix = 1

		box1 = gtk.VBox(False, 0)
		self.add(box1)

		self.fp = gtk.FileChooserWidget(action=gtk.FILE_CHOOSER_ACTION_OPEN)
		box1.pack_start(self.fp, True, True, 0)
		box1.show()
		self.fp.show()

		hbox1 = gtk.HBox(False,0)
		hbox1.set_size_request(-1,32)
		box1.pack_end(hbox1,False,False,0)

		img = gtk.Image()
		img.set_from_file('./media/dialog-ok.png')
		img.show()
		button = gtk.Button("Open")
		button.set_image(img)
		button.connect_object("clicked", self.on_open_file, None)
		button.set_flags(gtk.CAN_DEFAULT)
		button.grab_default()
		button.set_size_request(-1,32)
		button.show()
		hbox1.pack_start(button,False,False,0)

		img = gtk.Image()
		img.set_from_file('./media/delete.png')
		img.show()
		button = gtk.Button("Cancel")
		button.connect_object("clicked", self.on_cancel_file, None)
		button.set_flags(gtk.CAN_DEFAULT)
		button.grab_default()
		button.set_size_request(-1,32)
		button.show()
		hbox1.pack_end(button,False,False,1)

		hbox1.show()

		self.show()

		return




	def on_cancel_file(self, widget, data=None):

		self.hide()

		return




	def on_open_file(self, widget, data=None):

		filename = self.fp.get_filename()

		# file exists?
		try:
			f = open(filename)
		except:
			# error
			self.showerror('file <b>' + os.path.basename(filename) + '</b> n\'does not exits.', True)
		else:
			# open it
			f.close()

			self.tereo.applog("File",0,"Open file " + filename)

			# import file

			lst = self.tereo.object_search_path("file.importer")

			for info in lst:
				if info[1].get_extension() == filename[-3:]:
					self.tereo.applog("File",0,"Extension found")
					info[1].open_file(filename,self.tereo)

		self.hide()

		return

