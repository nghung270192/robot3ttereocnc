#-------------------------------------------------------------------------------
# Name:        module1
# Purpose:
#
# Author:      itinerant
#
# Created:     01/07/2013
# Copyright:   (c) itinerant 2013
# Licence:     <your licence>
#-------------------------------------------------------------------------------


from twisted.internet import reactor
from txjsonrpc.web.jsonrpc import Proxy

class kukaAxis_t():
	def __init__(self):
		self.a1 = 0.0 #kukaReal_t
		self.a2 = 0.0
		self.a3 = 0.0
		self.a4 = 0.0
		self.a5 = 0.0
		self.a6 = 0.0

def printValue(value):
    print repr(value)
    reactor.stop()

def printError(error):
    print 'error', error
    reactor.stop()

KUKA_VARNAME_LEN = 14

class kukaVal():
	def __init__(self):
		self.type = 0 # kukaType_t();
		self.kukaVal_u = None


# kukaVar_s
class kukaVar_t():
	def __init__(self):
		self.nom = [KUKA_VARNAME_LEN] #char
		self.valeur = kukaVal();


GETVAR = 11
SETVAR = 12
KUKA_AXIS = 0x0012

proxy = Proxy('140.80.10.27')

axes = kukaVar_t()
axes.nom = "$AXIS_ACT"
axes.valeur.type = KUKA_AXIS
axes.valeur.kukaVal_u = kukaAxis_t()

d = proxy.callRemote(GETVAR, '$AXIS_ACT',KUKA_AXIS, 0,0,0,0,0,0)
d.addCallback(printValue).addErrback(printError)
reactor.run()


#def main():
#    pass

#if __name__ == '__main__':
#    main()
