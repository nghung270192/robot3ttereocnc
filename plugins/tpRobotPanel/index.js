var tpRobotPanel;

var tpRobotPanel = function tpRobotPanel() {

	this.pluginName = "RobotPanel";
	this.pluginVersion = "0.1";
	this.pluginAuthor = "Olivier Martinez";
	this.panel;
	
};

tpRobotPanel.prototype = {

	"register":function() {

		var msg ={};
		msg['msg'] = "Robotpanel";
		tereoMain.add_icon('Robot','plugins/tpRobotPanel/robot.png', true, "@tpRobotPanel&on_robot_toggle",msg,'tpRobotPanel');

	},
	
	"on_robot_toggle":function() {
	
		//console.log("on robot toggle");
		
		var f = $("#robot_panel");
		if(f.length){
		
			//L'élément existe
			if ($("#tpRobotPanelRobot").hasClass("active")) {
				$("#tpRobotPanelRobot").removeClass("active");
				$("#robot_panel").hide();
			} else {
				$("#tpRobotPanelRobot").addClass("active");
				$("#robot_panel").show();
			}
		  
		} else {
		
			//L'élément n'existe pas
			this.panel = new robotPanelView();
			//parent.add_object("robot_panel", f);
			this.panel.__init__();
			$("#robot_panel").show();
			
		}
		
		this.panel.populate();
		
		$(window).trigger('resize');
		
	
	},

	"unload":function() {
	
	}
	

};

tereoPlugins['tpRobotPanel'] = new tpRobotPanel();



var robotPanelView = function() {

	this.uid = (((1+Math.random())*0x100000)|0).toString(16).substring(1);
	this.lastmsg ="";
	this.robots = {};
	this.calibrations = {};

}

robotPanelView.prototype = {

	"__init__": function () {

		$( "#tereopanels").append('<div class="tpPanel" id="robot_panel"><div class="tpPanelTitle">Robot panel</div><div><button class="form_button" onclick="tpRobotPanel_on_robot_new();"><img src="media/add.png">Add a robot</button></div><div id="robotPanelScroll" class="tpScroll"><div id="robots_list"></div></div></div>');

	},

	"populate":function () {
	
		//console.log("populate");
		
		// todo get robot object list		
		// todo get calibration list
		var msg = {}
		msg['msg'] = "get_robot_list";
		msg['uid'] = this.uid+"grl";
		msg['callback'] = "tpRobotPanel_on_get_robot_list";
		//console.log (tpRobotPanel_on_get_robot_list);
		//console.log (msg);
		
		tereoMain.call("sendMqttMsg",JSON.stringify(msg));

		msg['msg'] = "get_calibration_list";
		msg['uid'] = this.uid+"gcl";
		msg['callback'] = "tpRobotPanel_on_get_calibration_list";
		tereoMain.call("sendMqttMsg",JSON.stringify(msg));
		
		tereoObjects['robotPanel'] = this;
	
	},

	"refresh":function () {
		
		//console.log("refresh");
		
		if (scene = tereoObjects['scene'] != "undefined") {
			scene = tereoObjects['scene'];
			scene.editor.signals.windowResize.dispatch();
		}
		
		//clear
		var myNode = document.getElementById("robots_list");
		while (myNode.firstChild) {
			myNode.removeChild(myNode.firstChild);
		}
		
		i = 0;
		var render="<table>";

		for (r in this.robots) { //self.tereo.env.robots:
		
			if (this.robots.hasOwnProperty(r)) {
			
				rob = this.robots[r];

				render += "<tr><td colspan=2><div class=\"tpPanelChild\"><table cellpadding=0 cellspacing=0><tr><td class=\"tpPanelProps\">"+i+"</td><td>"+rob['name']+"</td></tr>";
				render += "<tr><td class=\"tpPanelProps\">Model</td><td>"+rob['model']+"</td></tr>";
				render += "<tr><td class=\"tpPanelProps\">Constructor</td><td>"+rob['constructor']+"</td></tr>";
				render += "<tr><td class=\"tpPanelProps\">Status</td><td>"+rob['status']+"</td></tr>";

				render += "<tr><td colspan=2><table cellpadding=0 cellspacing=0><td><button class=\"form_button\" onclick=\"tpRobotPanel_on_jog('"+rob.name+"');\"><img src=\"media/updown.png\">Jog</button></td>";

				render += "<td><button class=\"form_button\" onclick=\"tpRobotPanel_on_info('"+rob.name+"');\"><img src=\"media/configure.png\">Info</button></td>";
				
				render += "<td><button class=\"form_button\" onclick=\"tpRobotPanel_on_robot_home('"+rob.name+"');\"><img src=\"media/home.png\">Home</button></td>";
				
				render += "<td><button class=\"form_button\" onclick=\"tpRobotPanel_on_delete('"+rob.name+"');\"><img src=\"media/dialog-no.png\">Del</button></td></tr></table></td></tr>";


				//-----------------------

				//lcal = self.tereo.function_search_path("robot.calibration")
				if (this.calibrations.length >0 ) {
				
					render += '<tr><td><img src="media/calibrate.png"></td><td><button class="form_button" onclick="tpRobotPanel_on_robot_calibrate('+r['id']+');">Calib</button></td></tr>';
	//				render += '<tr><td><img src="media/dialog-no.png"></td><td><button onclick="self.on_robot_calibrate, i">Del</button></td></tr>';

					render += '<tr><td colspan=2><select>';

					//slist = []
					//defau = ""
					for (it in this.calibrations) {
						cali = this.calibrations[it];
						//if defau == "":
						//	defau = it[0]
						//slist.append(it[0])
						render += '<option value="'+cali['name']+'">'+cali['name']+'</option>';
					}
					
					render += '</select>';
				}
				
				i+=1;
				render += "</table></div></td></tr>";
			}

		}
		
		render += '</table>';
		//console.log("render:"+render);
		$( "#robots_list").append(render);
		
	}

}


function tpRobotPanel_on_get_robot_list(l) {
	//console.log("onplugin robotlist:"+l);
	//console.log(l);
	if (l == "get_robot_list") { return; }
	delete cnx_callbacks[this.uid+"grl"];
	
	try {
		l = l.replace(' ','');
		l = l.replace('\n','');
		l = l.replace('\r','');
		tereoPlugins['tpRobotPanel'].panel.robots = JSON.parse(l);
	} catch(e) { console.log(e); }
	
	//console.log(tereoPlugins['tpRobotPanel'].panel.robots);
	tereoPlugins['tpRobotPanel'].panel.refresh();
}

function tpRobotPanel_on_get_calibration_list(l) {
	//console.log("onplugin caliblist:"+l);
	if (l == "get_calibration_list") { return; }
	delete cnx_callbacks[this.uid+"gcl"];
	
	try {
		l = l.replace(' ','');
		l = l.replace('\n','');
		l = l.replace('\r','');
		//console.log("after:"+l);
		tereoPlugins['tpRobotPanel'].panel.calibrations = JSON.parse(l);
	} catch(e) { console.log(e); }
	//console.log("calib:");
	//console.log(tereoPlugins['tpRobotPanel'].panel.calibrations);	
	tereoPlugins['tpRobotPanel'].panel.refresh();
}

function tpRobotPanel_on_info(i) {
	tereoMain.execute_function ("robot.info","Robot info", i)
}

function tpRobotPanel_on_jog(i) {
	tereoMain.call("@tpJog&on_jog_activate",i);
}

function tpRobotPanel_on_delete(i) {
	//todo
}

function tpRobotPanel_on_robot_home(data) {

	//console.log("robot home");

	panel = tereoObjects['robotPanel'];

	for (r in panel.robots) { //self.tereo.env.robots:
		
		if (panel.robots.hasOwnProperty(r)) {
			
			if (data == panel.robots[r].name) {
			
				
				
				var msg = {}
				msg['msg'] = "ROBOT_HOME";
				msg['uid'] = panel.uid+"orh";
				msg['arg'] = panel.robots[r].name;
				msg['callback'] = "";
				tereoMain.call("sendMqttMsg",JSON.stringify(msg));
					
		
			
			}
			
		}
		
	}


	/*if self.env == None:
		return
	if len(self.tereo.env.robots)<data :
		return
	if self.tereo.env.robots[data] == None:
		return

	r = self.tereo.env.robots[data]
	if ((r.ip != "") and (r.chan != 0)) {
		if (r.proto == "lcnc") {
			r.send("command home -1")
		elif r.proto == "cross":
			if r.calculateur != None:
				r.send("command home -1")
			r.setspeed(5)
			r.moveto(r.home)
	*/
	
}


function tpRobotPanel_on_sethome(i) {


		/*#fo = frame6(1325.943970, 1189.238037, 984.228027, -156.870804, 8.596017, -179.800507)
		#fx = frame6(1434.425049, 23.797190, 1002.853027, -156.870102, 8.595991, -179.800293)
		#fy = frame6(1640.725952, 1205.558960, 985.950378, -156.869598, 8.595720, -179.800095)

		#fo = frame6(1324.321045, 1189.478027, 983.686279, -3.525528, -1.604558, 2.647153)
		#fx = frame6(1428.770020, 75.391869, 1001.255981, -3.525337, -1.604669, 2.647267)
		#fy = frame6(1711.005005, 1210.520020, 984.209229, -3.548363, -1.593004, 2.639205)

		#print "load matrix:"
		#print self.env.flow[0].matrix

		#self.env.flow[0].calibrate_3points(fo,fx,fy,self.env.tools[0])


		#r = self.env.robot[1]
		#r.selected_tool = self.env.tools[0]
		#r.moveto(1000,0,0,-156.87,8.59,-179,0)

		#return



		#print "___ 4 points calibration ________________________________________"

		##fa = frame6(1495.522, 504.9047, 1525.907, -165.2402, 28.81118, -170.4057)
		##fb = frame6(1513.386, 393.9939, 1487.916, 168.9515, 30.31429, 139.7817)
		##fc = frame6(1647.298, 356.1783, 1482.640, -156.4968, 5.572189, 127.5535)
		##fd = frame6(1627.613, 557.0178, 1559.532, -166.1921, -20.37669, -165.2694)
		#fa = frame6(1537, 434, 1433, -170, -81, -123)
		#fb = frame6(1527, 479, 1395, 116, -56, -47)
		#fc = frame6(1595, 536, 1484, 58, -52, 12)
		#fd = frame6(1624, 316, 1489, -53, -49, 140)
		#m = tereoTool.calc_tool_matrix_4points(fa,fb,fc,fd)
		#print m
		#self.env.tools[1].matrix = m
*/
	

	/*	#print "____ 2 points calibration _______________________________________"

		# tool
		##f1 = frame6(1629.251, 168.4417, 1156.281, -176.9179, 16.18393, 175.3015)
		##f1 = frame6(1268.146, 1.818468, 967.7661, -25.99339, -1.687192, 1.305336)
		##f1 = frame6(321.4, 29.9, 242.6, 0, -10, 25)
		# kuka tag
		##f2 = frame6(1822.499, 229.2046, 1026.493, -176.8476, 20.12885, 175.5276)
		##f2 = frame6(1417.630, 17.39453, 879.4035, -34.93858, -34.44539, 24.17662)
		##f2 = frame6(485.9, 100, 249.6, 0, -30, 40)
		##self.env.tools[0].matrix = tereoTool.calc_tool_matrix_2points(f1,f2)
		#print "tools 0 matrix:"
		#print self.env.tools[0].matrix


		#print "________ flow calibration __________"

		## dummy calibration
		#f = self.env.flow[0]

		##fo = frame6(1203.081,1290.059,975.1141,18.69,-0.47,6.03)
		##fx = frame6(1315.066,371.2379,962.8544,18.57446,-1.001771,4.656073)
		##fy = frame6(1666.011,1325.473, 965.1870, 18.70872, 0.17521, 7.9231)

		#fo = frame6(1054.010, 1539.677, 949.5551, -176.9089, 16.19157, 175.3011)
		#fo = frame6_bymatrix(fo,self.env.tools[0].matrix)

		#fx = frame6(1251.474, -40.88403, 956.3293, -176.9137, 16.19132, 175.3013)
		#fx = frame6_bymatrix(fx,self.env.tools[0].matrix)

		#fy = frame6(1497.534, 1577.288, 939.4255, -176.9096, 16.19387, 175.3054)
		#fy = frame6_bymatrix(fy,self.env.tools[0].matrix)

		#f.calibrate_3points(fo,fx,fy)


		#r = self.env.robot[1]
		#r.selected_tool = self.env.tools[0]
		#rc = self.env.robot[0]

		##r.moveto(1528,0,0,-176.9137,16.19387,175.3013,0)
		#r.moveto(1528,0,0,-169.3,7.22,159.07,0)




		return
*/
}

function tpRobotPanel_on_robot_new() {

	// display robot_library
	/*self.robotsmodelwinrethandler = self.on_robot_new_ok
	self.robotsmodelwin.maximize()
	self.robotsmodelwin.show()
	self.robotsmodelwin_init()*/
		
}


function tpRobotPanel_on_robot_new_ok() {

/*
	self.tmprobot.set_dummy_comp()
	self.tmprobot.db = self.tmpdb
	self.tmprobot.load_mesh()
	self.tereo.env.robots.append(self.tmprobot)
	r = self.tereo.env.robots[len(self.tereo.env.robots)-1]
	self.robotsmodelwinrethandler = None
	if self.builder.get_object("toolbutton12").get_active()==True:
		self.populate_robot_panel()

	rob = r.get_collection()
	m = Rotate([rob],r.angle_z,0,0,1)
	m = Translate([m],r.position_x * 0.03937,r.position_y * 0.03937,r.position_z * 0.03937)
	m = Collection([m])
	rob.vmodel = m
	self.scenevismachframe.addmodel(m);
*/	
	
}


//---------- robot calibration --------------------

function tpRobotPanel_on_robot_calibrate(r,co) {

	// calibration variables
	/*
	co = r.tereo.new_calibration_obj()
	co.currentRobot=data
	co.calib_title = "Robot calibration"
	co.calib_nb_question = 1
	co.calib_questions = ['Move the probe at the center top of the calibration box']
	co.calib_robot = r
	co.calib_robot_vars = ['$POS_ACT']
	co.calib_nb_img = 1
	co.calib_imgs = ['robotcalib.png']
	co.calib_handler = on_robot_calibrate_return
	co.calib_tag = [r,co]
	co.calib_win_init()
	*/

}

function tpRobotPanel_on_robot_calibrate_return(r,co) {
/*
	if co.calib_ret == None:
		return

	if len(co.calib_ret) == 0:
		return

	center = co.calib_ret[0]

	//print "box center :"
	r.env.robots[co.calib_tag-1].calibrate(center)
*/

}




