#!/usr/bin/python

import tereoFlow
from math import *
import tereoMath
#import tereoPanda

#import linuxcnc

class dummypart():
	def __init__(self):
		self.width = 0.0
		self.length = 0.0
		self.height = 0.0


def calcMatrix(part,flow):

	mat = []

	lumber_width = part.width * 0.03937
	lumber_length = part.length * 0.03937
	lumber_height = part.height * 0.03937
	ww = flow.working_width
	wl = flow.working_length
	za = flow.z_angle
	xi = flow.x_inclination
	px = flow.origin_x
	pz = -flow.origin_z
	py = flow.origin_y
	ox = flow.offset_x
	oy = flow.offset_y
	oz = flow.offset_z

	if flow.unity == 'mm':
		pz = pz * 0.03937
		px = px * 0.03937
		py = py * 0.03937
		wl = wl * 0.03937
		ww = ww * 0.03937
		ox = ox * 0.03937
		oy = oy * 0.03937
		oz = oz * 0.03937


	fo = flow.flow_origin

	if fo == 0:	mat.append(['translation',ox,oz,oy])
	elif fo == 1:	mat.append(['translation',ox+ww-lumber_width,oz,oy])
	elif fo == 2:	mat.append(['translation',ox,oz+wl-lumber_length,oy])
	elif fo == 3:	mat.append(['translation',ox+ww-lumber_width,oz+wl-lumber_length,oy])

	if za != 0:	mat.append(['rotation',za,0,0,1])

	if xi != 0:	mat.append(['rotation',xi,1,0,0])

	mat.append(['translation',px,py,pz])

	return mat

def myRotate(v, a, x, y, z):

	#print "myrotate a:%f"%(a)+" x:%d"%(x)+" y:%d"%(y)+" z:%d"%(z)

	a = a * 0.01745329
	xp = 0.0
	yp = 0.0
	zp = 0.0

	if x == 1:
		yp = v[1]*cos(a)-v[2]*sin(a)
		zp = v[1]*sin(a)+v[2]*cos(a)
		xp = v[0]

	elif y == 1:
		zp = v[2]*cos(a)-v[0]*sin(a)
		xp = v[2]*sin(a)+v[0]*cos(a)
		yp = v[1]

	elif z == 1:
		xp = v[0]*cos(a)-v[1]*sin(a)
		yp = v[0]*sin(a)+v[1]*cos(a)
		zp = v[2]

	v[0] = xp
	v[1] = yp
	v[2] = zp

	return

def applyMatrix2Vertex(mat, v):

	for ope in mat:
		if ope[0] == 'translation':
			#print "translation x:%f"%(ope[1])+" y:%f"%(ope[2])+" z:%f"%(ope[3])
			v[0] = v[0] + ope[1]
			v[1] = v[1] + ope[2]
			v[2] = v[2] + ope[3]

		elif ope[0] == 'rotation':
			m = myRotate(v,ope[1],ope[2],ope[3],ope[4])
			#for a in v:
			#	print "a:%f"%(a)
	return


def showPart(part,flow, vm):

	lumber_width = part.width * 0.03937
	lumber_length = part.length * 0.03937
	lumber_height = part.height * 0.03937
	ww = flow.working_width
	wl = flow.working_length
	za = flow.z_angle
	xi = flow.x_inclination
	px = flow.origin_x
	pz = -flow.origin_z
	py = flow.origin_y
	ox = flow.offset_x
	oy = flow.offset_y
	oz = flow.offset_z
	#print "oz:%f"%(oz)
	if flow.unity == 'mm':
		pz = pz * 0.03937
		px = px * 0.03937
		py = py * 0.03937
		wl = wl * 0.03937
		ww = ww * 0.03937
		ox = ox * 0.03937
		oy = oy * 0.03937
		oz = oz * 0.03937


	# add lumber
	lumber = Box(0,0,0, lumber_width, lumber_length, lumber_height)
	lumber = Color([0.59,0.29,0.0,1.0],[lumber])
	fo = flow.flow_origin

	if fo == 0:	lumber = Translate([lumber],ox,oz,oy)
	elif fo == 1:	lumber = Translate([lumber],ox+ww-lumber_width,oz,oy)
	elif fo == 2:	lumber = Translate([lumber],ox,oz+wl-lumber_length,oy)
	elif fo == 3:	lumber = Translate([lumber],ox+ww-lumber_width,oz+wl-lumber_length,oy)

	if za != 0:	lumber = Rotate([lumber],za,0,0,1)

	if xi != 0:	lumber = Rotate([lumber],xi,1,0,0)

	lumber = Translate([lumber],px,pz,py)
	vm.addmodel(lumber)

	return lumber


# show which side is the reference
# s= side
# p = part
def drawSideTriangle(s,p,w):

	print "drawsidetriangle :" + s


	if s == '1':
		#glBegin(GL_TRIANGLES)
		#glNormal3f(0,0,1)
		#glVertex3f(0.0, 0.0, w)
		#glVertex3f(0.0, 0.0, 0.0)
		#glVertex3f(w, 0.0, 0.0)
		#glEnd()
		tri = tereoPanda.triangle(0.0,0.0,w, 0.0,0.0,0.0, w,0.0,0.0, 0,0,1)
	elif s == '2':
		#glBegin(GL_TRIANGLES)
		#glNormal3f(-1,0,0)
		#glVertex3f(0.0, p.height, 0.0)
		#glVertex3f(w, p.height, 0.0)
		#glVertex3f(0.0, p.height-w, 0.0)
		#glEnd()
		tri = tereoPanda.triangle(0.0,p.height,0.0, w,p.height,0, 0.0,p.height-w,0.0, -1,0,0)
	elif s == '3':
		#glBegin(GL_TRIANGLES)
		#glNormal3f(-1,0,0)
		#glVertex3f(w, p.height, p.width)
		#glVertex3f(0.0, p.height, p.width)
		#glVertex3f(0.0, p.height, p.width-3)
		#glEnd()
		tri = tereoPanda.triangle(w,p.height,p.width, 0.0,p.height,p.width, 0.0,p.height,p.width-3, -1,0,0)
	elif s == '4':
		#glBegin(GL_TRIANGLES)
		#glNormal3f(-1,0,0)
		#glVertex3f(0.0, 0.0, p.width)
		#glVertex3f(0.0, w, p.width)
		#glVertex3f(w, 0.0, p.width)
		#glEnd()
		tri = tereoPanda.triangle(0.0,0.0,p.width, 0.0,w,p.width, w,0.0,p.width, -1,0,0)

	#glColor4f(0.75,0.88,0.81,0.5)
	tri.setColor(0.75,0.88,0.81,0.5)

	return tri

def getSideTriangleModel(s,p,w):
	print "getSideTriangleModel :" + s

	ph = p.height * 0.03937
	pw = p.width * 0.03937

	if s == '1':
		m = tereoPanda.triangle(0.0,w,0.0, 0.0,0.0,0.0, w,0.0,0.0, 0,0,1).generate()
		#m = Triangle(0.0,w,0.0, 0.0,0.0,0.0, w,0.0,0.0, 0,0,1)
	elif s == '2':
		m = tereoPanda.triangle(0.0,0.0,ph, w,0.0,ph, 0.0,0.0,ph-w, -1,0,0).generate()
	elif s == '3':
		m = tereoPanda.triangle(w,pw,ph, 0.0,pw,ph, 0.0,pw-w,ph, -1,0,0).generate()
	elif s == '4':
		m = tereoPanda.triangle(0.0,pw,0.0, 0.0,pw,w, w,pw,0.0, -1,0,0).generate()
	#m = Color([0.5,1.0,0.5,1.0],[m])
	m.setColor(0.5,1.0,0.5,1.0)

	return m

def dimBySide(s, vertex):

	if s == '1':
		vertex[1] = -vertex[1]
	elif s == '2':
		t = vertex[1]
		vertex[1] = -vertex[2]
		vertex[2] = -t
	elif s == '3':
		vertex[2] = -vertex[2]
	elif s == '4':
		t = vertex[1]
		vertex[1] = vertex[2]
		vertex[2] = vertex[1]

	return


def planeBySide(s, vertex, part):

	print "plane by side :" + s
	ph = part.height*0.03937
	pw = part.width*0.03937

	## sides
	if s == '1':
		# back  y = z
		y = vertex[1]
		vertex[1] = vertex[2]
		vertex[2] = y
	elif s == '2':
		# Bottom y = -y
		vertex[1] = pw - vertex[1]
	elif s == '3':
		# front y = -z
		y = vertex[1]
		vertex[1] = pw - vertex[2]
		vertex[2] = ph - y
		return
	#elif s == '4':
		# top y = y


	return


class pParameters(object):
	def __init__(self,*args):
		self.name = ''
		self.uid = ''
		self._params = args

	def params(self):
		return self._params


class Storing():
	def __init__(self,part):
		self.name = "storing"
		self.part = part
		self.uid = "st-" + part.singleMemberNumber
		self.obj = None
		self.mat = None
		self.vmlumber = None
		return

	def init_robot(self,env,vm,ter):
		print "init store"
		# lumber position
		print "part uid:"+self.part.singleMemberNumber
		mypart = self.part
		self.mat = calcMatrix(mypart,env.flow[env.woodworking_flow])

		# show lumber
		v1 = [0.,0.,0.]
		applyMatrix2Vertex(self.mat,v1)
		# inch
		v2 = [mypart.width * 0.03937,mypart.length * 0.03937,mypart.height * 0.03937]
		# mm
		#v2 = [self.part.width,self.part.length,self.part.height]

		#lumber = Box(v1[0],v1[1],v1[2],v2[0],v2[1],v2[2])

		# add flow offset
		applyMatrix2Vertex(self.mat,v2)

		# show in simu(vismach)
		lumber = Box(v1[0],v1[1],v1[2],v2[0],v2[1],v2[2])
		self.vmlumber = Color([0.59,0.29,0.0,1.0],[lumber])
		vm.addmodel(self.vmlumber)

		# move robot to the lumber center
		r = [(v2[0] + v1[0])/2,(v2[1] + v1[1])/2,(v2[2] + v1[2])/2]
		# top face
		if r[2] < v2[2]: r[2] = v2[2]
		if r[2] < v1[2]: r[2] = v1[2]


		# 3'' above
		r[2] = r[2] + 10

		#print "N0110 G00 X%f"%(r[0])+" Y%f"%(r[1])+" Z%f"%(r[2])


		# rotate the tool to be parallel
		#gcs.append("G10 L2 P0 R180")

		# if length > 1680 mm  use 4 ventouses : offset +175mm
		# else 2 ventouses : offset -175mm
		#r[1] = r[1] - 6.88975
		#print ">>> G00X%f"%(r[0])+"Y%f"%(r[1])+"Z%f"%(-r[2])
		ter.send_gcode("G00X%f"%(r[0])+"Y%f"%(r[1])+"Z%f"%(-r[2]))

		# touche the piece
		r[2] = r[2] - 10
		#print ">>> G00X%f"%(r[0])+"Y%f"%(r[1])+"Z%f"%(-r[2])
		ter.send_gcode("G00X%f"%(r[0])+"Y%f"%(r[1])+"Z%f"%(-r[2]))


		return

	def run_robot(self,env, vm, ter):
		print "run store"

		# remove lumber from table
		vm.removemodel(self.vmlumber)

		# add lumber to robot tool
		v2 = [self.part.width * 0.03937,self.part.length * 0.03937,self.part.height * 0.03937]
		lumber = Box(-v2[0]/2,-v2[1]/2,0,v2[0]/2,v2[1]/2,v2[2])
		lumber = Rotate([lumber],90,0,0,1)
		lumber = Rotate([lumber],90,1,0,0)
		lumber = Translate([lumber],0,-1.2,6.88975)
		self.vmlumber = Color([1.0,0.0,0.0,1.0],[lumber])
		vm.attach2object(env.robot[0].tool,self.vmlumber)

		ter.send_gcode("G00Y45X40Z-43")
		ter.send_gcode("G00X67Y0Z-35.433")
		ter.send_gcode("G00X67Y%f"%(env.flow[env.storing_flow].storing_offset-48.0)+"Z-35.433")
		ter.send_gcode("G00X30")
		ter.send_gcode("G00Z%f"%(-7.433-v2[2]))

		return

	def end_robot(self,env,vm, ter):
		print "end store"

		# detach lumber
		vm.detach2object(env.robot[0].tool) #,self.vmlumber)

		# add lumber to scene
		mypart = self.part
		self.mat = calcMatrix(mypart,env.flow[env.storing_flow])
		v1 = [0.,0.,0.]
		applyMatrix2Vertex(self.mat,v1)
		v2 = [mypart.width * 0.03937,mypart.length * 0.03937,mypart.height * 0.03937]
		applyMatrix2Vertex(self.mat,v2)
		v1[1] = v1[1] + env.flow[env.storing_flow].storing_offset
		v2[1] = v2[1] + env.flow[env.storing_flow].storing_offset
		l = v2[0] - v1[0]
		v1[0] = 30 - l/2
		v2[0] = 30 + l/2
		lumber = Collection([Box(v1[0],v1[1],v1[2],v2[0],v2[1],v2[2])])


		#self.mat = calcMatrix(self.part,env.flow[f])
		#v2 = [self.part.width * 0.03937,self.part.length * 0.03937,self.part.height * 0.03937]
		#v1 = [30 - v2[0]/2, -60 -v2[1]/2, -7,433]
		#v = [30 + v2[0]/2, -60 + v2[1]/2, -7,433+v2[2] ]
		#lumber = Box(v1[0],v1[1],v1[2],v[0],v[1],v[2])

		vm.addmodel(lumber)

		ter.send_gcode("G0X30Y60Z-35")
		ter.send_gcode("G0X67Y0")
		ter.send_gcode("G0X40Y45")
		ter.send_gcode("G0X7.874Y51.181Z-43.8582")

		env.flow[env.storing_flow].storing_offset = env.flow[env.storing_flow].storing_offset - self.part.width * 0.03937 - 0.75

		env.flow[env.storing_flow].stored_parts.append([self.part.singleMemberNumber,v1[0],v1[1],v1[2], lumber])

		return


class Unpacking():
	def __init__(self,part):
		self.name = "unpack"
		self.part = part
		self.uid = "un-" + part.singleMemberNumber
		self.obj = None
		self.mat = None
		self.vmlumber = None
		self.stored_lumber = None
		self.subprocess_name = ["init","ending"]
		self.subprocess = [self.init,self.ending]
		self.subprocess_index = 0
		return

	def init(self,env,vm,ter):

		print "unpack init"

		#print "part uid:"+self.part.singleMemberNumber

		rob = env.robot[ter.grab_robot]
		#uf = env.flow[env.unpacking_flow]


		# move robot to the lumber center
		print "lumber dim : length%f"%(self.part.length)+" height%f"%(self.part.height)+" width%f"%(self.part.width)

		r = [0.0,0.0,0.0]
		# todo
		#if self.part.designation == "osb":
		#	r[0] = self.part.length/2
		#	r[1] = self.part.width/2
		#	r[2] = 20 # 20mm
		#else:
		r[0] = self.part.length/2
		r[1] = self.part.height/2
		r[2] = 0 #self.part.width + 20 # 20mm

		# move robot to home/neutral position
		f = tereoMath.frame6(rob.home[0],rob.home[1],rob.home[2],rob.home[3],rob.home[4],rob.home[5])
		rob.moveto(f,-1)

		# rotate ?
		# todo
		#if self.part.width > 300:
		#	# rotate 180
		#	rob.moveto(rob.home[0],rob.home[1],rob.home[2],rob.home[3]+180,rob.home[4],rob.home[5],-1)
		#	r[0] -= 302
		#else:
		# calibrator in center
		r[1] -= 96.5


		# which material ?
		mat = -1
		for i in range(0,7):
			if ter.baseMaterial[i] == self.part.material:
				mat = i+1

		print "mat : %d"%(mat)


		uf = None
		if mat>-1:
			uf = env.flow[mat]
		# todo flow direction osb/ossature
		if uf:
			if self.part.material == "osb":
				print "uf offset z : %f"%(uf.offset_z)
				r[2] += uf.offset_z
			else:
				print "uf offset z : %f"%(uf.offset_y)
				r[1] += uf.offset_y

        # prehension angles ?
		# todo
		#if self.part.width > 300:
		#	rob.prehension(r[0],r[1],r[2],180,0,0,mat)
		#else:

		#compensation outil
		r[0] = r[0] - 35
		r[1] = r[1] + 205

		#prehension
		f = tereoMath.frame6(r[0],r[1],r[2],0,0,0)
		rob.prehension(f,mat)


	def ending(self,env,vm,ter):

		print "unpack ending"

		rob = env.robot[ter.grab_robot]

		# which material ?
		mat = -1
		for i in range(0,7):
			if ter.baseMaterial[i] == self.part.material:
				mat = i

		uf = None
		if mat>-1:
			uf = env.flow[mat]

		# todo flow direction osb/ossature
		if uf:
			if self.part.material == "osb":
				uf.offset_z -= self.part.height
			else:
				uf.offset_z += self.part.height


		# move to home
		if self.part.material == "osb":
			f = tereoMath.frame6(rob.home[0],rob.home[1],rob.home[2],rob.home[3],rob.home[4],rob.home[5])
			rob.moveto(f,-1)
		else:
			f = tereoMath.frame6(rob.home[0],rob.home[1],rob.home[2],rob.home[3],rob.home[4],rob.home[5])
			rob.moveto(f,-1)


		rob.end_process()
		rob.client.instance.waiting_job = True
		return


class Assembling():
	def __init__(self,part):

		self.name = "assemble"
		self.part = part
		self.uid = "as-" + part.singleMemberNumber
		self.obj = None
		self.mat = None
		self.vmlumber = None
		self.subprocess_name = ["init","ending"]
		self.subprocess = [self.init,self.ending]
		self.subprocess_index = 0
		self.guide = None
		self.proc_model = None
		self.saved_coords = tereoMath.frame6()
		return

	def init(self,env,vm,ter):
		print "init assemble"


		rob = env.robot[ter.grab_robot]
		af = env.flow[env.assembling_flow]


		# mat
		rx = self.part.transformations[0].xx
		ry = self.part.transformations[0].xy
		tx = self.part.transformations[0].ox*0.03937
		ty = self.part.transformations[0].oy*0.03937
		# normalize
		n = (rx*rx)+(ry*ry)
		n = sqrt(n)
		rx = rx * n
		#ry = ry * n
		# angle
		a = acos(rx)
		a = a * 57.295788
		if ry<0: a = a - 180

		print "angle : %f"%(a)

		# todo angle calc
		tx = self.part.transformations[0].ox
		ty = self.part.transformations[0].oy
		tz = self.part.transformations[0].oz

		angle = 0
		if self.part.designation == "montant":
			angle = 90
		elif self.part.designation == "lisse":
			ty = ty - self.part.width


		# rotate ?
		# todo
		#if self.part.width > 300:
			# rotate 180
		#	angle = angle + 180
		#	tx -= 302
		#else:
		# calibrator in center
		ty -= 96.5
		#ty = ty - 30

		#compensation outil
		tx = tx - 35 -50 - 280 - 10
		ty = ty + 205 -570


		if self.part.material == "osb":
			print "osb"
			ty = ty + self.part.length/2
			tx = tx + self.part.width/2
			tz = tz + self.part.height
		else:
			print "pas osb"
			ty = ty + self.part.length/2
			tx = tx + self.part.height/2
			tz = tz + self.part.width

		#tz = tz - self.part.width
		print "width : %f"%(self.part.width)

		f = tereoMath.frame6(tx,ty+100,tz-400,0,0,0)
		rob.moveto(f,7)

		f = tereoMath.frame6(tx,ty,tz-15+26,0,0,1)
		rob.move_contact(f,7) # base 7 = chariot


		######rob.moveto(lc[0],lc[1],lc[2],a,0,0)

		xo = tx+(af.origin_x-af.offset_x)*0.03937
		yo = -af.origin_z*0.03937
		zo = -af.offset_y*0.03937-ty-(self.part.length/2)*0.03937

		self.saved_coords = tereoMath.frame6(tx,ty,tz,0,0,0)


		return


	def ending(self,env,vm, ter):
		print "end assemble"

		rob = env.robot[ter.grab_robot]

		if self.part.material == "osb":
			f = tereoMath.frame6(self.saved_coords[0],self.saved_coords[1],self.saved_coords[2]-400,0,0,0)
			rob.deprehension(f,7)
			f = tereoMath.frame6(rob.home[0],rob.home[1],rob.home[2],rob.home[3],rob.home[4],rob.home[5])
			rob.moveto(f,-1)

		rob.end_process()

		return


class Disassembling():
	def __init__(self,part):

		self.name = "disassemble"
		self.part = part
		self.uid = "as-" + part.singleMemberNumber
		self.obj = None
		self.mat = None
		self.vmlumber = None
		self.subprocess_name = ["init","ending"]
		self.subprocess = [self.init,self.ending]
		self.subprocess_index = 0
		self.guide = None
		self.proc_model = None
		self.saved_coords = tereoMath.frame6()
		return

	def init(self,env,vm,ter):
		print "init disassemble"


		rob = env.robot[ter.grab_robot]
		# move robot to home/neutral position
		f = tereoMath.frame6(rob.home[0],rob.home[1],rob.home[2],rob.home[3],rob.home[4],rob.home[5])
		rob.deprehension(f,-1)

		return


	def ending(self,env,vm, ter):
		print "end disassemble"

		rob = env.robot[ter.grab_robot]

		rob.end_process()

		return


class Cut(pParameters):

	def __init__(self,*arg):

		pParameters.__init__(self,*arg)

		side, part, group, p1, p2, p3, p6, p7 = self.params()

		self.name = "cut"
		self.part = part
		self.uid = "cu-" + part.singleMemberNumber
		self.obj = None
		self.mat = None
		self.vmlumber = None
		self.stored_lumber = None
		self.subprocess_name = ["init","grab","run","ending","translate","translate2","translate3"]
		self.subprocess = [self.init,self.grab,self.run,self.ending,self.translate,self.translate2,self.translate3]
		self.subprocess_index = 0
		self.guide = None
		self.proc_model = None
		self.trans_length = 0
		self.saved_coords = tereoMath.frame6()

	def model(self):
		print "get model"
		if self.proc_model == None:
			self.proc_model = self.calc_proc_model()
		return self.proc_model

	def draw(self):
		#p01 Distance from beam start to the reference point (0)
		#p02 Distance from the reference edge to the reference point (0)
		#p03 Distance from the reference side to the reference point (orthogonal) (0)
		#p06 Angle between cut edge and reference edge (90)
		#p07 Inclination between face and reference side (90)
		side, part, group, p1, p2, p3, p6, p7 = self.params()
		drawSideTriangle(side,part,part.width/3)

		l = part.length
		h = part.height
		w = part.width

		glPushMatrix()

		glTranslate(p1,0,0)

		vertexo = [p1,p2,p3]
		planeBySide(side, vertexo, part)

		if group == '1':

			if p2 == 0 and p3 == 0:
				if side == 3 or side == 1:
					# width
					r = part.width/sin(p6*0.01745329)
					vertex1 = [-r * cos(p6*0.01745329),part.width,part.height]
				#	if p6 != 0: glRotatef(p6,0,1,0)
				#	if p7 != 0: glRotatef(p7,0,0,1)
				else:
					# height
					r = part.height/sin(p6*0.01745329)
					vertex1 = [-r * cos(p6*0.01745329),part.height,part.width]
				#	if p6 != 0: glRotatef(p6,0,0,1)
				#	if p7 != 0: glRotatef(p7,0,1,0)
				planeBySide(side, vertex1, part)
			else:
				print "not implemented"
				#not implemented
				return

		elif group == '2':

			if p2 == 0 and p3 == 0:
				if side == 3 or side == 1:
					# width
					r = part.width/sin(p6*0.01745329)
					vertex1 = [r * cos(p6*0.01745329),part.width,part.height]
				#	if p6 != 0: glRotatef(p6,0,1,0)
				#	if p7 != 0: glRotatef(p7,0,0,1)
				else:
					# height
					r = part.height/sin(p6*0.01745329)
					vertex1 = [r * cos(p6*0.01745329),part.height,part.width]
				#	if p6 != 0: glRotatef(p6,0,0,1)
				#	if p7 != 0: glRotatef(p7,0,1,0)
				planeBySide(side, vertex1, part)
			else:
				print "not implemented"
				#not implemented
				return

		print "vertexbysideo : %f"%(vertexo[0]) + " - %f"%(vertexo[1]) + " - %f"%(vertexo[2])
		print "vertexbyside1 : %f"%(vertex1[0]) + " - %f"%(vertex1[1]) + " - %f"%(vertex1[2])

		glColor4f(0.0,0.0,1.0,0.2)
		glPolygonMode( GL_FRONT_AND_BACK, GL_LINE )
		glBegin (GL_QUADS)
		glVertex ( 0, vertexo[1], vertexo[2])
		glVertex ( vertex1[0], vertex1[1], vertexo[2])
		glVertex ( vertex1[0], vertex1[1], vertex1[2])
		glVertex ( 0, vertexo[1], vertex1[2])
		glEnd ()
		glPolygonMode( GL_FRONT_AND_BACK, GL_FILL )
		glBegin (GL_QUADS)
		glVertex ( 0, vertexo[1], vertexo[2])
		glVertex ( vertex1[0], vertex1[1], vertexo[2])
		glVertex ( vertex1[0], vertex1[1], vertex1[2])
		glVertex ( 0, vertexo[1], vertex1[2])
		glEnd ()

		glPopMatrix()

		return


	def calc_proc_model(self):

		print "calc proc model"

		side, part, group, p1, p2, p3, p6, p7 = self.params()

		l = part.length*0.03937
		h = part.height*0.03937
		w = part.width*0.03937

		t = getSideTriangleModel(side,part,w/3)

		vertexo = [p1*0.03937,p2*0.03937,p3*0.03937]
		#print "vertexo x:%f"%(vertexo[0])+" y:%f"%(vertexo[1])+" z:%f"%(vertexo[2])

		planeBySide(side, vertexo, part)
		#print "vertexo2 x:%f"%(vertexo[0])+" y:%f"%(vertexo[1])+" z:%f"%(vertexo[2])


		if group == '1':

			if p2 == 0 and p3 == 0:
				if side == 3 or side == 1:
					# width
					r = w/sin(p6*0.01745329)
					vertex1 = [-r * cos(p6*0.01745329),w,h]
				#	if p6 != 0: glRotatef(p6,0,1,0)
				#	if p7 != 0: glRotatef(p7,0,0,1)
				else:
					# height
					r = h/sin(p6*0.01745329)
					vertex1 = [-r * cos(p6*0.01745329),h,w]
				#	if p6 != 0: glRotatef(p6,0,0,1)
				#	if p7 != 0: glRotatef(p7,0,1,0)
				planeBySide(side, vertex1, part)
			else:
				print "not implemented"
				#not implemented
				return

		elif group == '2':

			if p2 == 0 and p3 == 0:
				if side == 3 or side == 1:
					# width
					r = w/sin(p6*0.01745329)
					vertex1 = [r * cos(p6*0.01745329),h,w]
				#	if p6 != 0: glRotatef(p6,0,1,0)
				#	if p7 != 0: glRotatef(p7,0,0,1)
				else:
					# height
					r = h/sin(p6*0.01745329)
					vertex1 = [r * cos(p6*0.01745329),w,h]
					print "vertex1 x:%f"%(vertex1[0])+" y:%f"%(vertex1[1])+" z:%f"%(vertex1[2])

				#	if p6 != 0: glRotatef(p6,0,0,1)
				#	if p7 != 0: glRotatef(p7,0,1,0)
				planeBySide(side, vertex1, part)
			else:
				print "not implemented"
				#not implemented
				return

		print "vertexo3 x:%f"%(vertexo[0])+" y:%f"%(vertexo[1])+" z:%f"%(vertexo[2])
		print "vertex12 x:%f"%(vertex1[0])+" y:%f"%(vertex1[1])+" z:%f"%(vertex1[2])

		#b1 = Box(0,vertexo[1],vertexo[2], vertex1[0]+0.03937,vertex1[1],vertex1[2])
		#b1 = Wireframe([b1])
		#b1 = Color([1.0,0.0,0.0,0.5],[b1])
		b1 = tereoPanda.wireframeBox(0,vertexo[1],vertexo[2], vertex1[0]+0.03937,vertex1[1],vertex1[2]).generate()
		b1.setColor(1.0,0.0,0.0,0.5)
		#b2 = Box(0,vertexo[1],vertexo[2], vertex1[0],vertex1[1],vertex1[2])
		#b2 = Color([1.0,0.0,0.0,0.7],[b2])
		b2 = tereoPanda.box(0,vertexo[1],vertexo[2], vertex1[0],vertex1[1],vertex1[2]).generate()
		b2.setColor(1.0,0.0,0.0,0.7)

		#b3 = Box(0,0,0,1,1,100)
		#b3 = Color([1.0,0.0,0.0,1.0],[b3])

		m = Node()
		m.addNode(t)
		m.addNode(b1)
		m.addNode(b2)
		#m = Collection([t,b1,b2])

		#print m

		if p1 != 0:
			#m = Translate([m],p1*0.03937,0,0)
			m.setPos(p1*0.03937,0.0,0.0)

		return m


	def init(self,env,vm,ter):
		print "init cut"

		rob = env.robot[ter.grab_robot]
		wf = env.flow[env.woodworking_flow]

		r = rob.last_coords
		sp = rob.attached_object

		rob2 = env.robot[ter.tool_robot]
		cc = wf.get_corner_coords()
		# move tool robot to prepare cut
		f = tereoMath.frame6(cc[0] + 3.937+7.08+7,cc[1]-15,cc[2],-90,0,-90)
		rob2.moveto(f,-1)

		self.guide = self.model()

		#if env.flow[env.unpacking_flow].z_angle != 0:
		#	self.guide = Rotate([self.guide],env.flow[env.unpacking_flow].z_angle,0,0,1)

		# move grab robot to cut length + 100mm (3,937 inch)
		#r[0] = r[0] - sp.length + wf.working_length*0.03937
		#print "r X%f"%(r[0])+" Y%f"%(r[1])+" Z%f"%(r[2])

		print "part length : %f"%(self.part.length)
		print "sp length : %f"%(sp.length/2*25.4)

		# move lumber to the nearest side
		if self.part.length > (sp.length/2*25.4):
			# reverse coords
			print "reverse"

			self.guide = Rotate([self.guide],180,0,0,1)

			#r[0] = r[0] + 3.937 + (sp.length - self.part.length*0.03937)
			#print "r0 : %f"%(r[0])
			self.guide = Translate([self.guide],self.part.length*0.03937,self.part.width*0.03937,0)
			#self.guide = Translate([self.guide],3.937+sp.length-self.part.length*0.03937,0,0)


			self.trans_length = sp.length - (wf.working_width*0.03937) - 3.937 - (sp.length-self.part.length*0.03937)
			print "sp length : %f"%(sp.length)
			print "wf width : %f"%(wf.working_width)
			print "part length : %f"%(self.part.length)
			#self.trans_length = self.part.length*0.03937 -(wf.working_width*0.03937) - 3.937
			self.trans_length = - self.trans_length;

		else:

			print "no reverse"

			self.trans_length = sp.length - wf.working_width*0.03937 - (self.part.length*0.03937) - 3.937

			#r[0] = r[0] + 3.937 + self.part.length*0.03937
			#self.guide = Translate([self.guide],(sp.length/2-self.part.length)*0.03937,0,0)
			self.guide = Translate([self.guide],3.937+sp.length-self.part.length*0.03937,0,0)
			print "translation2 : %f"%(sp.length/2-self.part.length*0.03937)

		print "trans length : %f"%(self.trans_length)

		#self.saved_coords = r

		# tool compensation
#		self.guide = Translate([self.guide],0,-3.838575,6.88975)

		vm.attach2object(sp.topmodel,self.guide)


		#print "r X%f"%(r[0])+" Y%f"%(r[1])+" Z%f"%(r[2])

		rob.send_gcode("F150")

		print "choice"

		#print "rob.position.X : %f"%(rob.origin_x*0.03937)


		# decompose mvt?

		#self.trans_length = r[0] - (rob.origin_x*0.03937)

		if self.trans_length < -50:
			print "to the left"
			self.subprocess_index = 3
			return

		elif self.trans_length > 50:

			print "to the right"
			self.subprocess_index = 3
			return

		else:
			print "other"
			f = tereoMath.frame6(r[0] - self.trans_length,r[1],r[2],0,0,0)
			rob.moveto(f,-1)

		return

	def translate(self,env,vm,ter):

		print "translate"

		rob = env.robot[ter.grab_robot]
		r = rob.last_coords

		print "distance before: %f"%(self.trans_length)

##		if self.trans_length>0:
##			distance = r[0] -(rob.origin_x*0.03937 + 50)
##			rob.moveto(rob.origin_x*0.03937 + 50, r[1],r[2],0,0,0)
##			self.trans_length = self.trans_length-distance
##			print "distance + : %f"%(distance)
##		else:
##			distance = r[0] - (rob.origin_x*0.03937 - 50)
##			print "distance - : %f"%(distance)
##			rob.moveto(rob.origin_x*0.03937 -50, r[1],r[2],0,0,0)
##			self.trans_length = self.trans_length+distance

		print "distance after: %f"%(self.trans_length)

	def translate2(self,env,vm,ter):

		print "translate2"

		rob = env.robot[ter.grab_robot]
		wf = env.flow[env.woodworking_flow]
		sp = rob.attached_object

		# detach from robot
		print "detach from robot"
		rob.detach_object(vm)

		# attach to flow
		print "attach to flow"
		wf.attach_object(sp,rob,vm)
		sp  = wf.attached_object

		vm.attach2object(sp.topmodel,self.guide)

		# move to the side - tool length/2
		v = wf.get_corner_coords()
		lc = rob.last_coords

		#rob.send_gcode("F150")
		rob.setspeed(75)
		print "up"
		rob.moveto(lc[0],lc[1],lc[2]+3,0,0,0)

		print "tarns_length:%f"%(self.trans_length)

		print "right"
		f = tereoMath.frame6(v[0]-35.433-self.trans_length,lc[1],lc[2]+3,0,0,0)
		rob.moveto(f,-1)
		print "down"
		f = tereoMath.frame6(v[0]-35.433-self.trans_length, lc[1],lc[2],0,0,0)
		rob.moveto(f,-1)
		print "r0 : %f"%(v[0]-35.433)
		return

	def translate3(self,env,vm,ter):

		print "translate3"


		wf = env.flow[env.woodworking_flow]
		rob = env.robot[ter.grab_robot]
		sp=wf.attached_object

		v = wf.get_corner_coords()
		lc = rob.last_coords

		# detach from flow
		wf.detach_object(vm)

		# attach to robot
		rob.attach_object(sp,vm)
		sp = rob.attached_object
		vm.attach2object(sp.topmodel,self.guide)

		# move
		print "left"


		#distance parcourue = de (rob.origin_x - 70) a (wf.get_corner_coords - 35.433)
		r = self.saved_coords


##		if self.trans_length>0:
##			r[0] = r[0] + (v[0] - 35.433) - (rob.origin_x*0.03937 - 70)
##			print "ro zero : %f"%(r[0])
##			rob.move(r[0],r[1],r[2],0,0,0)
##		else:
##			r[0] = r[0] + (v[0] - 35.433) - (rob.origin_x*0.03937 - 70)
##			print "ro zero : %f"%(r[0])
##			rob.move(r[0],r[1],r[2],0,0,0)

		print "distance before: %f"%(self.trans_length)
		print "lc0 : %f"%(lc[0])

		f = tereoMath.frame6(lc[0]+self.trans_length,lc[1],lc[2],0,0,0)
		rob.moveto(f,-1)


		print "lc : %f"%(lc[0])
		self.subprocess_index = 0
		return

	def grab(self,env,vm,ter):

		print "grab"
		rob = env.robot[ter.grab_robot]
		lc = rob.last_coords
		f = tereoMath.frame6(lc[0],lc[1],lc[2],0,0,0)
		rob.moveto(f,-1)


		return

		rob = env.robot[ter.grab_robot]
		wf = env.flow[env.woodworking_flow]

		r = rob.last_coords


		# les pinces ont attraped le bois

		sp = rob.attached_object
		# detach from robot
		print "detach from robot"
		rob.detach_object(vm)

		# attach to flow
		print "attach to flow"
		wf.attach_object(sp,rob,vm)
		sp  = wf.attached_object






		# move upper
		r[2] = r[2] + 5 # (env.flow[env.unpacking_flow].working_length-mypart.width)*0.03937
		#rob.send_gcode("F100")
		rob.setspeed(50)
		f = tereoMath.frame6(r[0],r[1],r[2],0,0,0)
		rob.moveto(-1)

		# move at the other side of the lumber
		cc = wf.get_corner_coords()

		# wf corner + 20 + tool length /2 = 1800 /2 = 35,433
		r[0] = cc[0] + 7.874 + 35.433
		#if self.part.length > (self.stored_lumber[4]/2*25.4):
		#	r[0] = r[0] + self.part.length*0.03937 - self.stored_lumber[4]/2
		#else:
		#	r[0] = r[0] + self.stored_lumber[4]/2 - self.part.length*0.03937

		f = tereoMath.frame6(r[0],r[1],r[2],0,0,0)
		rob.moveto(f,-1)

		r[2] = r[2] - 5
		#rob.send_gcode("F100")
		rob.setspeed(50)
		tereoMath.frame6(r[0],r[1],r[2],0,0,0)
		rob.moveto(f,-1)

		return

	def run(self,env,vm,ter):
		print "run cut"
		rob = env.robot[ter.grab_robot]
		lc = rob.last_coords
		f = tereoMath.frame6(lc[0],lc[1],lc[2],0,0,0)
		rob.moveto(f,-1)
		return

	def ending(self,env,vm,ter):
		print "end cut"
		rob = env.robot[ter.grab_robot]
		lc = rob.last_coords
		f = tereoMath.frame6(lc[0],lc[1],lc[2],0,0,0)
		rob.moveto(f,-1)

##		wf = env.flow[env.woodworking_flow]
##		rob = env.robot[ter.grab_robot]
##		sp=wf.attached_object
##
##		lc = rob.last_coords
##
##		# detach from flow
##		wf.detach_object(vm)
##
##		# attach to robot
##		rob.attach_object(sp,vm)
##		sp = rob.attached_object
##		vm.attach2object(sp.topmodel,self.guide)

		return

class LongitudinalCut(pParameters):
	def draw(self):
		#P01 Distance from beam start to the reference point
		#P02 Distance from the reference edge to the reference point
		#P04 Limit of the 2 ends, binary code
		#P07 Inclination to the reference side
		#P11 Depth
		#P12 Length
		#P13 Angle in face at start
		#P14 Angle in face at end
		side, part, group, p1, p2, p4, p7, p11, p12, p13, p14 = self.params()
		drawSideTriangle(side,part,part.width/3)
		return

	def model(self):
		side, part, group, p1, p2, p4, p7, p11, p12, p13, p14 = self.params()
		t = getSideTriangleModel(side,part,part.width*0.03937/3)
		return t

class DoubleCut(pParameters):
	def draw(self):
		#P01 Distance from beam start to the reference point
		#P02 Distance from the reference point to the reference edge
		#P06 Angle between the first cuttin edge and the reference edge
		#P07 Inclination of the first cutting towards the reference side
		#P08 Angle between the second cutting edge and the reference edge
		#P09 Inclination of the second cutting towards the reference side
		side, part, group, p1, p2, p6, p7, p8, p9 = self.params()
		drawSideTriangle(side,part,part.width/3)
		return

	def model(self):
		side, part, group, p1, p2, p6, p7, p8, p9 = self.params()
		t = getSideTriangleModel(side,part,part.width*0.03937/3)
		return t

class RidgeValleyCut(pParameters):
	def draw(self):
		#P01 Distance from beam start to the reference point
		#P02 Distance from the reference point to the reference edge
		#P04 Limit of the 2 ends, binary code
		#P07 Inclination between the first face and the reference side
		#P09 Inclination between the second face and the reference side
		#P11 Depth
		#P12 Length if P12 and P04 = 0, the processing is performed along the whole component length
		#P13 Angle in face at reference edge at start
		#P14 Angle in face at reference edge at end
		#P15 Angle in face in opposite to reference edge at start
		#P16 Angle in face in opposite to refenrece edge at end
		side, part, group, p1, p2, p4, p7, p8, p9, p11, p12, p13, p14, p15, p16 = self.params()
		drawSideTriangle(side,part,part.width/3)
		return

	def model(self):
		side, part, group, p1, p2, p4, p7, p8, p9, p11, p12, p13, p14, p15, p16 = self.params()
		t = getSideTriangleModel(side,part,part.width*0.03937/3)
		return t

class sawCut(pParameters):
	def draw(self):
		#P01 Distance from beam start to the reference point
		#P02 Distance from the reference point to the reference edge
		#P03 Displacement to the reference side
		#P06 Angle between cut edge and reference edge
		#P07 Inclination to the reference side
		#P08 Angle to the reference edge in the cut face
		#P11 Depth, orthogonal to the reference side
		#P12 Length
		side, part, group, p1, p2, p3, p6, p7, p8, p11, p12 = self.params()
		drawSideTriangle(side,part,part.width/3)
		return

	def model(self):
		side, part, group, p1, p2, p3, p6, p7, p8, p11, p12 = self.params()
		t = getSideTriangleModel(side,part,part.width*0.03937/3)
		return t

class slot(pParameters):
	def draw(self):
		#P01 Distance from beam start to the reference point
		#P02 Distance from the reference edge to the reference point
		#P03 Distance to the refrerence point orthogonal to the reference side
		#	P03 = 0 : slot on one of the 4 sides ot the component
		#	P03 > 0 : Slot on one of the 2 front sides of the component
		#P04 Limit of the 6 faces of the slot, binary code
		#P06 Angle to the reference edge in the reference side
		#P07 Inclination to the reference side
		#P08 Interior angle at reference point
		#P09 Interior angle at opposite of reference point
		#P10 Addition to P09
		#P11 Depth orthogonal to the reference side
		#P12 Length
		#P13 Thickness
		#P14 Displacement of the entrance edge at reference point
		#P15 Displacement of the entrance edge at opposite of reference point
		side, part, group, p1, p2, p3, p4, p6, p7, p8, p9, p10, p11, p12, p13, p14, p15 = self.params()
		drawSideTriangle(side,part,part.width/3)
		return

	def model(self):
		side, part, group, p1, p2, p3, p4, p6, p7, p8, p9, p10, p11, p12, p13, p14, p15 = self.params()
		t = getSideTriangleModel(side,part,part.width*0.03937/3)
		return t

class FrontSlot(pParameters):
	def draw(self):
		#P01 Distance from beam start to the reference point
		#P02 Distance from the reference point to the reference edge
		#P03 Distance to the reference point orthogonal to the reference side
		#P06 Angle between cut edge and reference edge
		#P07 Inclination to the reference side
		#P08 Angle between the longitudinal axis of the slot and the reference side
		#P11 Depth
		#P12 Length
		#P13 Width
		side, part, group, p1, p2, p3, p6, p7, p8, p11, p12, p13 = self.params()
		drawSideTriangle(side,part,part.width/3)

		glPushMatrix()

		vertex = [ p1, p2, p3 ]
		planeBySide(s, vertex, part)
		print "vertexbyside : %f"%(vertex[0]) + " - %f"%(vertex[1]) + " - %f"%(vertex[2])

		dim =  [ p12, p13, p11 ]
		planeBySide(s, dim, part)

		print "dim : %f"%(dim[0]) + " - %f"%(dim[1]) + " - %f"%(dim[2])

		#origin
		glBegin (GL_LINES)
		glColor4f(1.0,0.0,1.0,1.0)
		glVertex (0.0,0.0,0.0)
		glVertex (vertex[0],0.0,0.0)
		glColor4f(0.0,1.0,0.0,1.0)
		glVertex (vertex[0],0.0,0.0)
		glVertex (vertex[0],vertex[1],0.0)
		glColor4f(0.0,0.0,1.0,1.0)
		glVertex (vertex[0],vertex[1],0.0)
		glVertex (vertex[0],vertex[1],vertex[2])
		glEnd ()

		glColor4f(0.0,0.0,1.0,0.2)
		glTranslate(vertex[0]-(dim[0]/2),vertex[1]-dim[1],vertex[2]-(dim[2]/2))
		glPolygonMode( GL_FRONT_AND_BACK, GL_LINE )
		tereoGL.drawCube(0,0,0,dim[0],dim[1],dim[2])
		glPolygonMode( GL_FRONT_AND_BACK, GL_FILL )
		tereoGL.drawCube(0,0,0,dim[0],dim[1],dim[2])

		glPopMatrix()

		return

	def model(self):
		side, part, group, p1, p2, p3, p6, p7, p8, p11, p12, p13 = self.params()
		t = getSideTriangleModel(side,part,part.width*0.03937/3)
		return t

class BirdsMouth(pParameters):
	def draw(self):
		#P01 Distance from beam start to the reference point
		#P02 Distance from the refrence edge to the reference point
		#P05 P05 = 1 : drilhole for rafter nail, P05=0 : no drilhole
		#	the machineside defines place and direction of the drilhole
		#P06 Angle to the reference adge in the reference area
		#P07 Inclination between face 1 and reference side
		#P08 Inclination between face 2 and reference side
		#P09 First cut angle of the counterpart
		#	If P09 is zero, the limit face beside face 1 is parallel to component side
		#P10 First cut inclination of the counterpart
		#	If P10 is zero, the limit face beside face 1 is parallel to component side
		#P11 Depth 1 orthogonal to reference side
		#P12 Depth 2 orthogonal to reference side
		#P13 Grooving depth in the traverse direction of the component
		#	if P13 is zero, then its value must be calculated: P13 = WRS-P02
		#P14 Height counterpart. Zero means: no limit.
		#	Measurement orthogonal to face 1 (P07)
		#P15 Width counterpart. Zero means: no limit.
		#	Measurement orthogonal to face 2 (P08)
		side, part, group, p1, p2, p5, p6, p7, p8, p9, p10, p11, p12, p13, p14, p15 = self.params()
		drawSideTriangle(side,part,part.width/3)
		return

	def model(self):
		side, part, group, p1, p2, p5, p6, p7, p8, p9, p10, p11, p12, p13, p14, p15 = self.params()
		t = getSideTriangleModel(side,part,part.width*0.03937/3)
		return t


class RidgeLap(pParameters):
	def draw(self):
		#P01 Distance from beam start to the reference point
		#P02 0: reference point on reference edge
		#	1 : reference point on the opposite edge
		#P06 Angle to the reference edge in the reference side
		#P11 Depth of half lap
		#P12 Width of half lap
		#P13 Drill hole diameter
		side, part, group, p1, p2, p6, p11, p12, p13 = self.params()
		drawSideTriangle(side,part,part.width/3)
		return

	def model(self):
		side, part, group, p1, p2, p6, p11, p12, p13 = self.params()
		t = getSideTriangleModel(side,part,part.width*0.03937/3)
		return t

class LapJoint(pParameters):
	def draw(self):
		#P01 Distance from beam start to the reference point
		#P02 Distance from the reference edge to the reference point
		#P03 Displacement to the reference side
		#P04 Limit of the 6 faces of the lap, binary code
		#P06 Angle to the reference edge in the reference side
		#P07 Inclination to the reference side
		#P08 Angle between edge and reference side in face
		#P09 Angle in the floor face
		#P10 Angle between base face and one face of lap
		#P11 Distance from the reference side to the reference point (orthogonal)
		#P12 Length
		#P13 Chamfer angle
		#P14 Grooving depth ( length of the lapped scarf in traverse direction)
		#	if 0 : its value must be calculated: P14 = WRS-P02
		side, part, group, p1, p2, p3, p4, p6, p7, p8, p9, p10, p11, p12, p13, p14 = self.params()
		drawSideTriangle(side,part,part.width/3)

		vertexo = [ p1, p2, p3 ]
		planeBySide(side, vertexo, part)
		print "part.width : %f"%(part.width)
		print "vertexbyside : %f"%(vertexo[0]) + " - %f"%(vertexo[1]) + " - %f"%(vertexo[2])

		if p14 == 0:
			if side == 1 or side == 3:
				p14 = part.height
			else:
				p14 = part.width

		dim = [p12,-p11,p14]
		dimBySide(side, dim)

		print "dimbyside : %f"%(dim[0]) + " - %f"%(dim[1]) + " - %f"%(dim[2])

		glPushMatrix()
		glColor4f(0.0,0.0,1.0,0.2)
		glTranslate(vertexo[0],vertexo[1],vertexo[2])
		glPolygonMode( GL_FRONT_AND_BACK, GL_LINE )
		tereoGL.drawCube(0,0,0,dim[0],dim[1],dim[2])
		glPolygonMode( GL_FRONT_AND_BACK, GL_FILL )
		tereoGL.drawCube(0,0,0,dim[0],dim[1],dim[2])
		glPopMatrix()

		return

	def model(self):
		side, part, group, p1, p2, p3, p4, p6, p7, p8, p9, p10, p11, p12, p13, p14 = self.params()
		t = getSideTriangleModel(side,part,part.width*0.03937/3)
		return t

class NotchRabbet(pParameters):
	def draw(self):
		#P01 Distance from beam start to the reference point
		#P02 Distance from the reference edge to the reference point
		#P04 Limit of the 6 faces of the notch/rabbet, binary  code
		#P11 Notch/Rabbet depth
		#P12 Notch/Rabbet Length
		#P13 Notch/Rabbet width
		side, part, group, p1, p2, p4, p11, p12, p13 = self.params()
		drawSideTriangle(side,part,part.width/3)
		return

	def model(self):
		side, part, group, p1, p2, p4, p11, p12, p13 = self.params()
		t = getSideTriangleModel(side,part,part.width*0.03937/3)
		return t

class BlockHouse(pParameters):
	def draw(self):
		#P01 Distance from beam start to the reference point
		#P06 Angle between cut edge and reference edge
		#P11 Depth of the half lap on the reference side
		#P12 Depth of the half lap opposite of the reference  side
		#P13 Length of the half lap/dado
		side, part, group, p1, p6, p11, p12, p13 = self.params()
		drawSideTriangle(side,part,part.width/3)
		return

	def model(self):
		side, part, group, p1, p6, p11, p12, p13 = self.params()
		t = getSideTriangleModel(side,part,part.width*0.03937/3)
		return t

class SeathingCut(pParameters):
	def draw(self):
		#P01 Distance from beam start to the reference point
		#P11 Depth of seathing cut
		#P12 Length of seathing cut
		side, part, group, p1, p11, p12 = self.params()
		drawSideTriangle(side,part,part.width/3)
		return

	def model(self):
		side, part, group, p1, p11, p12 = self.params()
		t = getSideTriangleModel(side,part,part.width*0.03937/3)
		return t

class FrenchRidgeLap(pParameters):
	def draw(self):
		#P01 Distance from beam start to the reference point
		#P02  0: reference point on reference edge
		#	1 : reference point on the opposite edge
		#P06 Angle to the reference edge in the reference side
		#P13 Drill hole diameter
		side, part, group, p1, p2, p6, p13 = self.params()
		drawSideTriangle(side,part,part.width/3)
		return

	def model(self):
		side, part, group, p1, p2, p6, p13 = self.params()
		t = getSideTriangleModel(side,part,part.width*0.03937/3)
		return t

class Chamfer(pParameters):
	def draw(self):
		#P01 Distance from beam start to the reference point
		#P04 Input of edge(s) to be beveled, binary code
		#    0 = edge 1, 1 = edge 2, 2 = edge 3, 3 = edge 4
		#P11 Depth
		#P12 Length if P12 = 0 processing is performed along the whole component length
		#P15 Shape for bevel exit
		#  0 = orthogonal, 1 = at 45deg, 2 = round
		side, part, group, p1, p4, p11, p12, p15 = self.params()
		drawSideTriangle(side,part,part.width/3)
		return

	def model(self):
		side, part, group, p1, p4, p11, p12, p15 = self.params()
		t = getSideTriangleModel(side,part,part.width*0.03937/3)
		return t


class BlockHouseHalfLap(pParameters):
	def draw(self):
		#P01 Distance from beam start to the reference point
		#P03 Depth orthogonal to reference side of the lap 2 and 4
		#P04 0: all laps are symmetric to each other
		#	1 : lap on reference edge moved to end beam
		#		lap in opposite of reference edge moved to start beam
		#	2 : lap on reference edge moved to start beam
		#		lap in opposite of reference edge moved to end beam
		#P05 P05=1:drilhole for drop rod, p05=0: no dilhole
		#	the machines defines place and direction of the drilhole
		#P08 lap1 : depth
		#P09 lap1 : length
		#P10 lap2 : depth
		#P11 lap2 : length
		#P12 lap3 : depth
		#P13 lap3 : length
		#P14 lap4 : depth
		#P15 lap4 : length
		#P16 distance from end of arc orthogonal to the reference side
		#P17 radius of arc
		#P18 distance reference point to center of arc
		#P19 which arc (A B C D) is to prouduced, binary code
		side, part, group, p1, p3, p4, p5, p8, p9, p10, p11, p12, p13, p14, p15, p16, p17, p18, p19 = self.params()
		drawSideTriangle(side,part,part.width/3)
		return

	def model(self):
		side, part, group, p1, p3, p4, p5, p8, p9, p10, p11, p12, p13, p14, p15, p16, p17, p18, p19 = self.params()
		t = getSideTriangleModel(side,part,part.width*0.03937/3)
		return t


class BlockHouseFront(pParameters):
	def draw(self):
		#P01 distance from beam start to the refrence point
		#P04 0: only one lap on reference side
		#    1: one lap on reference side and one on the opposite side
		#P06 angle to the reference edge in the reference side
		#P11 depth at reference point
		#P12 depth opposite to the reference point
		#P13 depth at reference edge
		#P15 length
		side, part, group, p1, p4, p6, p11, p12, p13, p15 = self.params()
		drawSideTriangle(side,part,part.width/3)
		return

	def model(self):
		side, part, group, p1, p4, p6, p11, p12, p13, p15 = self.params()
		t = getSideTriangleModel(side,part,part.width*0.03937/3)
		return t


class Pocket(pParameters):
	def draw(self):
		#P01 distance from beam start to the reference point
		#P02 distance from the reference edge to the reference point
		#P04 limit of the 6 faces of the pocket, binary code
		#P06 rotation angle around the local z-axis of the cuboid
		#P07 rotation angle around the local y-axis of the cuboid rotated with p06
		#P08 rotation angle around the local x-axis od the cuboid rotated with P06 and P07
		#P10 internal angle at the reference point
		#P11 depth of reference point orthogonal to reference side
		#P12 length of half lap
		#P13 width of half lap
		side, part, group, p1, p2, p4, p6, p7, p8, p10, p11, p12, p13 = self.params()
		drawSideTriangle(side,part,part.width/3)
		return

	def model(self):
		side, part, group, p1, p2, p4, p6, p7, p8, p10, p11, p12, p13 = self.params()
		t = getSideTriangleModel(side,part,part.width*0.03937/3)
		return t


class Drilling(pParameters):
	def draw(self):

		#P01 Distance from beam start to the reference point
		#P02 Distance from the reference edge to the reference point
		#P03 Distance from the reference face to the reference point
		#	P03 = 0 : drilling on one of the 4 sides of the component
		#	P03 <> 0 : drilling on one of the 2 front sides of the component
		#P06
		#	P03 = 0 : angle to the reference edge in the reference side
		#	P03 <> 0 : angle in front side
		#P07 Inclination between drilling and reference side
		#	P03 = 0 : inclination between drilling and reference side
		#	P03 <> 0 : inclination between drilling and front side
		#P11 Depth, orthogonal to reference side or front side
		#P12 Drill hole diameter
		side, part, group, p1, p2, p3, p6, p7, p11, p12 = self.params()
		drawSideTriangle(side,part,part.width/3)

		glPushMatrix()
		q = gluNewQuadric()
		q2 = gluNewQuadric()
		vertex = [ p1, p2, 0 ]
		planeBySide(side, vertex, part)
		if p3 == 0:
			glTranslate(vertex[0],vertex[1],vertex[2])
		else:
			print "p03 not null"

		glColor4f(0.0,0.0,1.0,0.2)
		glPolygonMode( GL_FRONT_AND_BACK, GL_FILL )
		gluCylinder(q2, p12/2, p12/2, p11, 32, 1)
		glPopMatrix()

		return

	def model(self):
		side, part, group, p1, p2, p3, p6, p7, p11, p12 = self.params()
		t = getSideTriangleModel(side,part,part.width*0.03937/3)
		return t

	def init_robot(self,flow):
		side, part, group, p1, p2, p3, p6, p7, p11, p12 = self.params()
		return

	def run_robot(self,flow):
		side, part, group, p1, p2, p3, p6, p7, p11, p12 = self.params()
		return

	def end_robot(self,flow):
		side, part, group, p1, p2, p3, p6, p7, p11, p12 = self.params()
		return


class Tenon(pParameters):
	def draw(self):
		#P01 distance from beam start to the reference point
		#P02 distance from the reference edge to the reference point
		#P04 rounding
		#P05 chamfer
		#P06 angle between cut edge and reference edge
		#P07 inclination between face and reference  side
		#P08 angle between axis of the tenon and reference side
		#P10 radius for P04=4
		#P11 tenon height
		#P12 tenon width
		#P14 margin on the reference side
		#P15 margin opposite the reference side
		side, part, group, p1, p2, p4, p5, p6, p7, p8, p10, p11, p12, p14, p15 = self.params()
		drawSideTriangle(side,part,part.width/3)

		if group == '1':

			# plane
			vertex = [p1,0,0]
			planeBySide(side, vertex, part)
			print "vertexbyside : %f"%(vertex[0]) + " - %f"%(vertex[1]) + " - %f"%(vertex[2])
			l = part.length
			w = part.width
			h = part.height
			glPushMatrix()
			glColor4f(0.0,0.0,1.0,0.2)
			glTranslate(vertex[0],0,0)
			glPolygonMode( GL_FRONT_AND_BACK, GL_LINE )
			glBegin (GL_QUADS)
			glVertex ( 0, 0, 0)
			glVertex ( 0,  h, 0)
			glVertex ( 0,  h, w)
			glVertex ( 0, 0, w)
			glEnd ()
			glPolygonMode( GL_FRONT_AND_BACK, GL_FILL )
			glBegin (GL_QUADS)
			glVertex ( 0, 0, 0)
			glVertex ( 0,  h, 0)
			glVertex ( 0,  h, w)
			glVertex ( 0, 0, w)
			glEnd ()
			glPopMatrix()

			# tenon

			glPushMatrix()
			glColor4f(0.0,0.0,1.0,0.2)
			vertex = [p1,p2,0]
			planeBySide(side, vertex, part)

			glPolygonMode( GL_FRONT_AND_BACK, GL_LINE )

			glTranslate(vertex[0],vertex[1],vertex[2])

			#print "p8 : %f"%(p8)
			glRotatef(p8,1,0,0)

			if p8 == 90:
				if side == 3 or side == 1:
					w = -(part.height-p14-p15)
				else:
					w = -(part.width-p14-p15)
			else:
				if side == 3 or side == 1:
					w = -(part.width-p14-p15)
				else:
					w = -(part.height-p14-p15)

			vertex = [0,-p12/2,-p14]
			dimBySide(side, vertex)
			#print "vertexbyside rot : %f"%(vertex[0]) + " - %f"%(vertex[1]) + " - %f"%(vertex[2])
			glTranslate(vertex[0],vertex[1],vertex[2])
			vertex[0] = p11
			vertex[1] = p12
			vertex[2] = w


			#print "av dimbyside rot : %f"%(vertex[0]) + " - %f"%(vertex[1]) + " - %f"%(vertex[2])
			dimBySide(side,vertex)
			#print "dimbyside rot : %f"%(vertex[0]) + " - %f"%(vertex[1]) + " - %f"%(vertex[2])
			tereoGL.drawCube(0,0,0,vertex[0],vertex[1],vertex[2])
			glPolygonMode( GL_FRONT_AND_BACK, GL_FILL )
			tereoGL.drawCube(0,0,0,vertex[0],vertex[1],vertex[2])
			glPopMatrix()

		return

	def model(self):
		side, part, group, p1, p2, p4, p5, p6, p7, p8, p10, p11, p12, p14, p15 = self.params()
		t = getSideTriangleModel(side,part,part.width*0.03937/3)
		return t

class Mortise(pParameters):
	def draw(self):
		#P01 distance from beam start to the reference point
		#P02 distance from the  reference edge to the reference point
		#P03 displacement to the reference side
		#P04 rounding
		#P06 angle between axis and reference edge
		#P07 inclination beween strut and reference side
		#P08 inclination of hole side walls towards reference side
		#P10 radius P04=4
		#P11 mortise depth
		#P12 mortise width
		#P13 height of strut
		#P14 margin on the reference point
		#P15 margin opposite the reference point
		#P16 inclination of hole front side towards reference side
		side, part, group, p1, p2, p3, p4, p6, p7, p8, p10, p11, p12, p13, p14, p15, p16 = self.params()
		drawSideTriangle(side,part,part.width/3)

	def model(self):
		side, part, group, p1, p2, p3, p4, p6, p7, p8, p10, p11, p12, p13, p14, p15, p16 = self.params()
		t = getSideTriangleModel(side,part,part.width*0.03937/3)
		return t

class MortiseFront(pParameters):
	def draw(self):
		#P01 distance from beam start to the reference point
		#P02 distance from the reference edge to the reference point
		#P04 Rounding
		#P06 Angle between cut edge and reference edge
		#P07 inclination between face and reference side
		#P08 angle between axis of the tenon and reference side
		#P10 radius for P04=4
		#P11 mortise depth
		#P12 mortise width
		#P14 margin on the reference side
		#P15 margin opposite the reference side
		side, part, group, p1, p2, p4, p6, p7, p8, p10, p11, p12, p14, p15 = self.params()
		drawSideTriangle(side,part,part.width/3)

	def model(self):
		side, part, group, p1, p2, p4, p6, p7, p8, p10, p11, p12, p14, p15 = self.params()
		t = getSideTriangleModel(side,part,part.width*0.03937/3)
		return t

class House(pParameters):
	def draw(self):
		#P05 not defined
		#P09 processident of the associated tenon or dovetail tenon
		side, part, group, p5, p9 = self.params()
		drawSideTriangle(side,part,part.width/3)

	def model(self):
		side, part, group, p5, p9 = self.params()
		t = getSideTriangleModel(side,part,part.width*0.03937/3)
		return t

class HouseMortise(pParameters):
	def draw(self):
		#P09 processident of the associated tenon or dovetail tenon
		side, part, group, p9 = self.params()
		drawSideTriangle(side,part,part.width/3)

	def model(self):
		side, part, group, p9 = self.params()
		t = getSideTriangleModel(side,part,part.width*0.03937/3)
		return t

class DovetailTenon(pParameters):
	def draw(self):
		#P01 distance from beam start to the reference point
		#P02 distance from the reference edge to the reference point
		#P04 0 = with rounding at the bottom ; 1 = without rounding, unbounded
		#P06 angle between edge and reference edge
		#P07 inclination between face and reference side
		#P08 angle between axis of tenon and reference side
		#P09 middle flattering
		#P10 angle of cone
		#P11 tenon height
		#P12 diameter of the curve
		#P14 margin on the reference side
		#P15 margin opposite the reference side
		side, part, group, p1, p2, p4, p6, p7, p8, p9, p10, p11, p12, p14, p15 = self.params()
		drawSideTriangle(side,part,part.width/3)

	def model(self):
		side, part, group, p1, p2, p4, p6, p7, p8, p9, p10, p11, p12, p14, p15 = self.params()
		t = getSideTriangleModel(side,part,part.width*0.03937/3)
		return t


class DovetailMortise(pParameters):
	def draw(self):
		#P01 distance from beam start to the reference point
		#P02 distance from the reference edge to the reference point
		#P03 displacement to the reference side
		#P04 0 = with rounding at the bottom ; 1 = without rounding, unbounded
		#P05 0=with elongation; 1=with pocket
		#P06 angle between edge and reference edge
		#P07 inclination between face and reference side
		#P09 middle flattering
		#P10 angle of cone
		#P11 mortise depth
		#P12 diameter of the curve
		#	if P12 < 0, then radius must be defined on the machine side
		#P13 height  of strut
		#P14 margin on the reference point
		#P15 margin opposite the reference point
		side, part, group, p1, p2, p3, p4, p5, p6, p7, p9, p10, p11, p12, p13, p14, p15 = self.params()
		drawSideTriangle(side,part,part.width/3)

	def model(self):
		side, part, group, p1, p2, p3, p4, p5, p6, p7, p9, p10, p11, p12, p13, p14, p15 = self.params()
		t = getSideTriangleModel(side,part,part.width*0.03937/3)
		return t


class DovetailMortiseFront(pParameters):
	def draw(self):
		#P01 distance from beam start to the reference point
		#P02 distance from the reference edge to the reference point
		#P03 displacement to the front side
		#P04 0 = with rounding at the bottom ; 1 = without rounding, unbounded
		#P05 0 = with elongation , 1 = pocket
		#P06 angle between cut edge and reference edge
		#P07 inclination between face and reference side
		#P08 angle between axis of tenon and reference side
		#P09 middle flattering
		#P10 angle of cone
		#P11 mortise depth
		#P12 diameter of the curve
		#	if P12 < 0, then the radius must be defined on the machineside
		#P14 margin on the reference side
		#P15 margin opposite the reference side
		side, part, group, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, p12, p14, p15 = self.params()
		drawSideTriangle(side,part,part.width/3)

	def model(self):
		side, part, group, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, p12, p14, p15 = self.params()
		t = getSideTriangleModel(side,part,part.width*0.03937/3)
		return t

class MarkingLabeling(pParameters):
	def draw(self):
		#P01 distance from beam start to the reference point
		#P02 distance from the reference edge to the reference point
		#P04 position and alignment of the text
		#	P04 = 0 if there is no text
		#P06 angle between axis and reference edge to the refrence point
		#P07 interior angle if p07 =0 2 single markins have to be produced
		#P11 width of quadrangle
		#	if P11=0 there is only a single marking
		#P12 height of quadrangle
		#	if P12=0 marking is limited by the edge opposite to the reference edge
		#P13 height of text
		#P15 text(string max 256 car)
		side, part, group, p1, p2, p4, p6, p7, p11, p12, p13, p15 = self.params()
		drawSideTriangle(side,part,part.width/3)

	def model(self):
		side, part, group, p1, p2, p4, p6, p7, p11, p12, p13, p15 = self.params()
		t = getSideTriangleModel(side,part,part.width*0.03937/3)
		return t

class Text(pParameters):
	def draw(self):
		#P01 distance from beam start to the refrence point
		#P02 distance from the reference edge to the reference point
		#P06 angle between axis and reference edge
		#P09 alignment vertical
		#	0=bottom, 1=middle, 2=top
		#P10 alignment horintal
		#	0 = left, 1=middle, 2=right
		#P11 alignment in case of a multiline text
		#	0=standard, 1=letteres horizontal placed
		#P13 height of text
		#P15 text(256 car max)
		side, part, group, p1, p2, p6, p9, p10, p11, p13, p15 = self.params()
		drawSideTriangle(side,part,part.width/3)

	def model(self):
		side, part, group, p1, p2, p6, p9, p10, p11, p13, p15 = self.params()
		t = getSideTriangleModel(side,part,part.width*0.03937/3)
		return t


class SimpleScarf(pParameters):
	def draw(self):
		#P01 distance from beam start to the reference point
		#P11 depth at the reference side
		#P12 depth at the opposite of reference side
		#P13 length of the overlap
		#P14 drilling 1 diameter
		#	P15=0 this drilling is placed at 1/2 p13
		#	P15>0 this drilling is placed at 1/3 P13
		#P15 drilling 2 diameter
		#	placed at 2/3 p13
		side, part, group, p1, p11, p12, p13, p14, p15 = self.params()
		drawSideTriangle(side,part,part.width/3)

	def model(self):
		side, part, group, p1, p11, p12, p13, p14, p15 = self.params()
		t = getSideTriangleModel(side,part,part.width*0.03937/3)
		return t


class ScarfJoint(pParameters):
	def draw(self):
		#P01 distance from beam start to the reference point
		#P07 angle of inclination of the lapped scarf base
		#P09 shape of the lapped scarf or classic data identification:
		#	1: when cutting orthogonal to reference side
		#	-1: when cutting orthogonal to base side
		#	0: classic definition, P10 and P12 are not used
		#P10 length of the lapped scarf base
		#P11 depth of the lapped  scarf base
		#P12 depth of the lapped scarf base orthogonal to reference side
		#P13 length
		#P14 drilling 1 diameter
		#	P15=0 this drilling is placed at 1/2 p13
		#	P15>0 this drilling is placed at 1/3 P13
		#P15 drilling 2 diameter placed at 2/3 P13
		side, part, group, p1, p7, p9, p10, p11, p12, p13, p14, p15 = self.params()
		drawSideTriangle(side,part,part.width/3)

	def model(self):
		side, part, group, p1, p7, p9, p10, p11, p12, p13, p14, p15 = self.params()
		t = getSideTriangleModel(side,part,part.width*0.03937/3)
		return t


class StepJoint(pParameters):
	def draw(self):
		#P01 distance from beam start to the reference point
		#P04 type of heel notch : 0=normal, 1=tapered
		#P07 inclination strut
		#P11 depth step joint
		#P12 depth heel notch
		#P14 height tenon
		#	which implementation (A or B) is used depends on the machine
		#P15 width tenon
		side, part, group, p1, p4, p7, p11, p12, p14, p15 = self.params()
		drawSideTriangle(side,part,part.width/3)

	def model(self):
		side, part, group, p1, p4, p7, p11, p12, p14, p15 = self.params()
		t = getSideTriangleModel(side,part,part.width*0.03937/3)
		return t



class StepJointNotch(pParameters):
	def draw(self):
		#P01 distance from beam start to the reference point
		#P02 distance from the reference edge to the reference point
		#P04 type of heel notch : 0=normal 1=tapered
		#P07 inclination between strut and reference  side
		#P10 width of the notch
		#P11 depth step joint
		#P12 depth heel notch
		#P13 height of strut
		#P14 depth of mortise
		#	which implementation (A or B) is used depends on the machine
		#P15 width of mortise
		side, part, group, p1, p2, p4, p7, p10, p11, p12, p13, p14, p15 = self.params()
		drawSideTriangle(side,part,part.width/3)

	def model(self):
		side, part, group, p1, p2, p4, p7, p10, p11, p12, p13, p14, p15 = self.params()
		t = getSideTriangleModel(side,part,part.width*0.03937/3)
		return t

class Planing(pParameters):
	def draw(self):
		#P01 distance from beam start to the reference point
		#P04 specification of side(s) to be planed; binary coded
		#P11 planing depth
		#P12 length of the area to be planed
		side, part, group, p1, p4, p11, p12 = self.params()
		drawSideTriangle(side,part,part.width/3)

	def model(self):
		side, part, group, p1, p4, p11, p12 = self.params()
		t = getSideTriangleModel(side,part,part.width*0.03937/3)
		return t


class ProfileFront(pParameters):
	def draw(self):
		#P01 distance from beam start to the reference point
		#P03 distance from the reference edge to the reference point
		#P06 rotation angle of the first curve of the profile
		#P07 rotation angle of the profile towards the reference edge
		#P08 offset angle
		#P11 radius of the first curve
		#P12 radius of the second curve
		side, part, group, p1, p3, p6, p7, p8, p11, p12 = self.params()
		drawSideTriangle(side,part,part.width/3)

	def model(self):
		side, part, group, p1, p3, p6, p7, p8, p11, p12 = self.params()
		t = getSideTriangleModel(side,part,part.width*0.03937/3)
		return t


class ProfileHeadconcave(pParameters):
	def draw(self):
		#P01 distance from beam to the reference point
		#P11 radius
		#P12 depth
		#P13 displacement
		#P14 depth
		#P15 displacement
		side, part, group, p1, p11, p12, p13, p14, p15 = self.params()
		drawSideTriangle(side,part,part.width/3)

	def model(self):
		side, part, group, p1, p11, p12, p13, p14, p15 = self.params()
		t = getSideTriangleModel(side,part,part.width*0.03937/3)
		return t


class ProfileHeadconvex(pParameters):
	def draw(self):
		#P01 distance from beam start to the reference point
		#P11 radius
		#P12 depth
		#P13 displacement
		#P14 depth
		#P15 displacement
		side, part, group, p1, p11, p12, p13, p14, p15 = self.params()
		drawSideTriangle(side,part,part.width/3)

	def model(self):
		side, part, group, p1, p11, p12, p13, p14, p15 = self.params()
		t = getSideTriangleModel(side,part,part.width*0.03937/3)
		return t


class ProfileHeadcambered(pParameters):
	def draw(self):
		#P01 distance from beam start to the reference point
		#P10 profile length
		#P11 depth at the reference point
		#P12 maximum depth of profile
		#P13 minimum depth of profile
		#P14 depth at the profile end
		#P15 premil: 0=round, 1=angular
		side, part, group, p1, p10, p11, p12, p13, p14, p15 = self.params()
		drawSideTriangle(side,part,part.width/3)

	def model(self):
		side, part, group, p1, p10, p11, p12, p13, p14, p15 = self.params()
		t = getSideTriangleModel(side,part,part.width*0.03937/3)
		return t


class RoundArch(pParameters):
	def draw(self):
		#P01 distance from beam start to the reference point
		#P11 depth of the arch segment
		#P12 length of arch segment
		side, part, group, p1, p11, p12 = self.params()
		drawSideTriangle(side,part,part.width/3)

	def model(self):
		side, part, group, p1, p11, p12 = self.params()
		t = getSideTriangleModel(side,part,part.width*0.03937/3)
		return t


class ProfileHead(pParameters):
	def draw(self):
		#P01 distance from beam start to the reference point
		#P04 type of arc(convex,concave), binary coded
		#P09 length of lap1
		#P10 depth of lap1
		#P11 displacement arc1
		#P12 horizontal length arc1
		#P13 vertical length arc1
		#P14 camber arc2
		#P15 length lap2
		#P16 depth lap2
		#P17 displacement arc2
		#P18 horizontal length arc2
		#P19 vertical length arc2
		#P20 camber arc2
		#P21 length of lap 3
		#P22 depth of lap 3
		side, part, group, p1, p4, p9, p10, p11, p12, p13, p14, p15, p16, p17, p18, p19, p20, p21, p22 = self.params()
		drawSideTriangle(side,part,part.width/3)

	def model(self):
		side, part, group, p1, p4, p9, p10, p11, p12, p13, p14, p15, p16, p17, p18, p19, p20, p21, p22 = self.params()
		t = getSideTriangleModel(side,part,part.width*0.03937/3)
		return t


class Sphere(pParameters):
	def draw(self):
		#P01 distance from beam start to the reference point
		#P02 distance from the reference edge to the reference point
		#P03 distance from the reference side to the reference point
		#P11 radius
		#P12 start offset
		#P13 length
		side, part, group, p1, p2, p3, p11, p12, p13 = self.params()
		drawSideTriangle(side,part,part.width/3)

	def model(self):
		side, part, group, p1, p2, p3, p11, p12, p13 = self.params()
		t = getSideTriangleModel(side,part,part.width*0.03937/3)
		return t


class TriangleCut(pParameters):
	def draw(self):
		#P01 distance from beam start to the reference point
		#P02 distance from the reference edge to the reference point
		#P03 distance from the reference side to the reference point
		#P10 normal vector 1: length X component
		#P11 normal vector 1 : length Y component
		#P12 normal vector 1 : length Z component
		#P13 normal vector 2 : length X component
		#P14 normal vector 2 : length Y component
		#P15 normal vector 2 : length Z component
		side, part, group, p1, p2, p3, p10, p11, p12, p13, p14, p15 = self.params()
		drawSideTriangle(side,part,part.width/3)

	def model(self):
		side, part, group, p1, p2, p3, p10, p11, p12, p13, p14, p15 = self.params()
		t = getSideTriangleModel(side,part,part.width*0.03937/3)
		return t



class TyroleanDovetail(pParameters):
	def draw(self):
		#P01 distance from beam start to the reference point
		#P02 distance between "inside" an side of part
		#P03 distance orthogonal to the reference side
		#P04 0: inside at reference edge
		#	1 : inside at opposite of reference edge
		#P05 0: without rebate or mitre
		#	-1 : with mitre
		#	>0 : with rebate
		#P06 angle to the reference edge in the reference side
		#P07 width
		#P08 depth
		#P09 inclination
		#P11 height
		#P12 radius
		#P13 length of tool
		#P14 0: angular corner joint
		#	1: straight t-wall connection
		#P15 length
		#P16 0: processing on the reference side and opposite the reference side
		#	1: processing only on the reference side
		#	2 : processing only opposite the reference side
		side, part, group, p1, p2, p3, p4, p5, p6, p7, p8, p9, p11, p12, p13, p14, p15, p16 = self.params()
		drawSideTriangle(side,part,part.width/3)

	def model(self):
		side, part, group, p1, p2, p3, p4, p5, p6, p7, p8, p9, p11, p12, p13, p14, p15, p16 = self.params()
		t = getSideTriangleModel(side,part,part.width*0.03937/3)
		return t



class Dovetail(pParameters):
	def draw(self):
		#P01 distance from beam start to the reference point
		#P02 distance between "inside" an side of part
		#P03 distance orthogonal to the reference side
		#P04 0: inside at reference edge
		#	1 : inside at opposite of reference edge
		#P05 0: without rebate or mitre
		#	-1 : with mitre
		#	>0 : with rebate
		#P09 inclination
		#P11 depth 1
		#P12 depth 2
		#P14 0: european dovetail
		#	1: american dovetail
		#P15 length
		#P16 0: processing on the reference side and opposite the reference side
		#	1: processing only on the reference side
		#	2 : processing only opposite the reference side
		side, part, group, p1, p2, p3, p4, p5, p9, p11, p12, p14, p15, p16 = self.params()
		drawSideTriangle(side,part,part.width/3)

	def model(self):
		side, part, group, p1, p2, p3, p4, p5, p9, p11, p12, p14, p15, p16 = self.params()
		t = getSideTriangleModel(side,part,part.width*0.03937/3)
		return t


class FreeContour(pParameters):
	def __init__(self,*arg):

		print "freecontour object"
		pParameters.__init__(self,*arg)

		side, part, group, p1, p2, p3, p5, p6, p8, p9, p13, p15 = self.params()

		self.name = "nail"
		self.part = part
		self.uid = "fc-" + part.singleMemberNumber
		self.obj = None
		self.mat = None
		self.vmlumber = None
		self.stored_lumber = None
		self.subprocess_name = ["init","ending"]
		self.subprocess = [self.init,self.ending]
		self.subprocess_index = 0
		self.guide = None
		self.proc_model = None
		self.trans_length = 0
		self.saved_coords = [0.0,0.0,0.0,0.0,0.0,0.0]

	def model(self):
		print "get model"
		if self.proc_model == None:
			self.proc_model = self.calc_proc_model()
		return self.proc_model

	def calc_proc_model(self):
		return None

	def draw(self):
		return

	def init(self,env,vm,ter):

		print "freecontour init"
		rob = env.robot[ter.tool_robot]
		lc = rob.last_coords
		af = env.flow[env.assembling_flow]


		# mat
		rx = self.part.transformations[0].xx
		ry = self.part.transformations[0].xy
		tx = self.part.transformations[0].ox*0.03937
		ty = self.part.transformations[0].oy*0.03937
		# normalize
		n = (rx*rx)+(ry*ry)
		n = sqrt(n)
		rx = rx * n
		#ry = ry * n
		# angle
		a = acos(rx)
		a = a * 57.295788
		if ry<0: a = a - 180

		print "angle : %f"%(a)
		#rob.movelinto(lc[0],lc[1],lc[2],a,0,0)

		xo = tx+(af.origin_x-af.offset_x)*0.03937
		yo = -af.origin_z*0.03937
		zo = -af.offset_y*0.03937-ty-(self.part.length/2)*0.03937

		#print "rob move : x:%f"%(xo) + " y:%f"%(yo) + " z:%f"%(zo) + " a:%f"%(a)

		#rob.moveto(xo,yo,zo,a,0,0)
		# { X 16.16749, Y -1294.855, Z 2269.408, A 69.48716, B -79.57098, C 22.24890 }
		#rob.setspeed(50)
		#rob.moveto(1008.552,-181.345,2020.000, -116.000, 63.2690, -108.95800)
		#rob.moveto(10, -1247.855, 2255.00, 69.48716, -79.57098, 22.24890);

		tx = self.part.transformations[0].ox
		ty = self.part.transformations[0].oy
		tz = self.part.transformations[0].oz

		if self.part.designation == "lisse":
			ty = ty - self.part.width


		# with proc params
		side, part, group, p1, p2, p3, p5, p6, p8, p9, p13, p15 = self.params()

		print "nail translation : p1:%f"%(p1) + " p2:%f"%(p2) + " p3:%f"%(p3)

		tx = tx + p1
		ty = ty + p2
		tz = tz - p3
		#print "+nail translation : x:%f"%(tx) + " y:%f"%(ty) + " z:%f"%(tz)

		# tool compensation here

		f = tereoMath.frame6(tx,ty,tz,0,0,0)
		rob.nail(f,7)

		return


	def ending(self,env,vm,ter):
		print "ending"
		rob = env.robot[ter.tool_robot]
		#rob.movelinto(10.0000,-1247.397000,1455.000, 0.000000, 90.000000, 90.000000)
		#rob.moveto(10, -1347.855, -1455.00, 90.0, 90.0, 0.0)
		##rob.moveto(10, -1247.855, 1655.00, 69.48716, -79.57098, 22.24890);
		#rob.moveto(1008.552,-181.345,2020.000, -116.000, 63.2690, -108.95800)
		rob.end_process()
		return


class Variant(pParameters):
	def draw(self):
		return

	def model(self):
		#side, part, group, p1, p4, p11, p12 = self.params()
		#t = getSideTriangleModel(side,part,part.width*0.03937/3)
		return None


class Nail(pParameters):
	def draw(self):
		return


class process_main_element:

	def __init__(self):

		self.projectNumber = ''
		self.projectName = ''
		self.projectPart = ''
		self.projectGuid = ''
		self.listName = ''
		self.customer = ''
		self.architect = ''
		self.editor = ''
		self.deliveryDate = ''
		self.exportDate = ''
		self.exportTime = ''
		self.exportRelease = ''
		self.language = ''
		self.geneRange = ''
		self.scaleUnit = 1
		self.processingQuality = ''
		self.computerName = ''
		self.user = ''
		self.sourceFile = ''
		self.exportFile = ''
		self.recess = ''
		self.comment = ''
		self.parts = []
		self.material = []

		return


class process_element:

	def __init__(self):

		self.singleMemberNumber = 0
		self.assemblyNumber = ''
		self.orderNumber = 0
		self.designation = ''
		self.annotation = ''
		self.storey = ''
		self.group = ''
		self.package = ''
		self.material = ''
		self.timberGrade = ''
		self.qualityGrade = ''
		self.count = 0
		self.length = 0
		self.height = 0
		self.width = 0
		self.colour = ''
		self.planingLength = 0
		self.startOffset = 0
		self.endOffset = 0
		self.uid = [] # integer
		self.transformations = [] #btlMatrix
		self.glmatrix = [] # transformations in opengl format
		self.camber = ''
		self.partoffset = ''
		self.processingQuality = ''
		self.outline = ''
		self.aperture = ''
		self.recess = ''
		self.storeyType = ''
		self.elementNumber = ''
		self.layer = 0
		self.moduleNumber = ''
		self.comment = ''
		self.grainDirection = ''
		self.referenceSide = ''
		self.process = [] # object


