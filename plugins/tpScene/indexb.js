var tpScene;

var tpScene = function tpScene() {

	this.pluginName = "Scene";
	this.pluginVersion = "0.1";
	this.pluginAuthor = "Olivier Martinez";
	this.scene;
	
};

tpScene.prototype = {

	"register":function(parent) {

//		mod = imp.load_source('tereocnc.plugins.scene', './plugins/scene/scene.py')
		this.scene = new_scene_frame();
		tereoMain.add_object('gui.frame','Scene 3D',this.scene);

	},

	"unload":function(parent) {
	
	}

};


tereoPlugins['tpScene'] = new tpScene();



function new_scene_frame() {

	f = tereoMain.get_object('frame','Scene')
	if (typeof f === 'undefined') {
		f = new sceneView();
				f.__init__();
		tereoMain.add_object('gui.frame','Scene',f);
	}

	return f;
	
}


var sceneView = function() {

	this.uid = (((1+Math.random())*0x100000)|0).toString(16).substring(1);
	this.lastmsg ="";
	
	
	this.lat=0;
	this.lon=0;
	this.mousex = 0;
	this.mousey = 0;
	this.minlat = -180;
	this.maxlat = 180;
	this.rotating = false;
	this.glarea = null;
	

	this.model = new Array();
	this.size = 60.0; //float(size)
	this.distance = this.size * 5.0;
	this.near = 0.06; // float(size) * 0.01
	this.far = this.size * 10.0;
	/*this.tool2view = tool;
	this.world2view = world;
	this.work2view = work;*/

	this.scene = null;
	this.base = null;
	this.grid = null;
	this.render ="";
	this.gridGuide = null;
	
	this.robotsnb = -1;
	
	this.meshes = {};
	this.meshes_link = {};
	this.links = {};	
	this.robots = {};
	
	

}

sceneView.prototype = {

	"__init__": function () {

		this.render = "<div   style=\"width:100%;height:100%;\" class=\"tpFrame\" style=\"border:1px #FF0000;\" id=\"scene\"><canvas id=\"canvas\"  style=\"width:100%;height:100%;padding:0;margin:0;overflow:hidden;\"></canvas></div>";

	},


	"attach_frame":function() {

		console.log ("attach frame");
		
		var canvas = document.getElementById('canvas');
		
		
			
		canvas.onselectstart = function () { return false; } // ie
		canvas.onmousedown = function () { return false; } // mozilla
		var scaleLocked = false;
		var grid = 20;

		var babyEdit = new BABYLON.Editor(canvas);
		//var controlPanel = new PANEL.control();
		babyEdit.createBasicScene();
		
		
		// Resize
		$(window).on('load resize', function(){
			console.log("engine resize");
			babyEdit.engine.resize();
		});
 
		this.scene = babyEdit.scene;
		tereoObjects["scene"] = this;
		
		this.buildScene(babyEdit);
		


		
		/*
		var canvas = document.getElementById("webglcanvas"); 

		// Get the canvas element from our HTML below

		  // Load BABYLON 3D engine
		  var engine = new BABYLON.Engine(canvas, true);

		//And let your imagination run wild as you create incredible scenes like this one:

		  this.scene = new BABYLON.Scene(engine);

		  // Creating a camera looking to the zero point (0,0,0), a light, and a sphere of size 1
		  //var camera = new BABYLON.ArcRotateCamera("Camera", 1, 0.8, 10, new BABYLON.Vector3(0, 0, 0), scene);
		  var light0 = new BABYLON.PointLight("Omni", new BABYLON.Vector3(0, 0, 10), this.scene);
		  var origin = BABYLON.Mesh.CreateSphere("origin", 10, 1.0, this.scene);
		  var camera = new BABYLON.ArcRotateCamera("ArcRotateCamera", 1, 1.3, 100, new BABYLON.Vector3(0, 0, 0), this.scene);
          //camera.angularSensibility = 1000;
		  this.scene.activeCamera.maxZ = 1000;
		  // Attach the camera to the scene
		  this.scene.activeCamera.attachControl(canvas);

		  //newScene.activeCamera.attachControl(canvas);
		  camera.attachControl(canvas, true);
		  
		  // Once the scene is loaded, just register a render loop to render it
		  var that = this;
		  engine.runRenderLoop(function () {
			that.scene.render();
		  });
		  
		  // Resize
			window.addEventListener("resize", function () {
				console.log("engine resize");
				engine.resize();
			});
			
			this.create_grid();
		

		
		//this.drawScene();
		*/
	
		//BABYLON.SceneLoader.ImportMesh("","", "kuka_link4.babylon", babyEdit.scene, function (newScene) {
            // Wait for textures and shaders to be ready
            /*newScene.executeWhenReady(function () {
                // Attach camera to canvas inputs
                

                // Once the scene is loaded, just register a render loop to render it
                babyEdit.engine.runRenderLoop(function() {
                    newScene.render();
                });
            });*/
			
     /*   }, function (progress) {
            // To do: give progress feedback to user*/
      //  }
		//);

		
	},


	"drawScene":function() {

    },
	
	"buildScene":function() {

		console.log ("build scene");
		
		if (this.robotsnb == -1) {
		
			console.log("no robot data..get robot list");
			
			var msg = {}
			msg['msg'] = "get_robot_list";
			msg['uid'] = this.uid+"grl";
			msg['callback'] = "tpScene_on_get_robot_list";
			tereoMain.call("sendMqttMsg",JSON.stringify(msg));
			return;
			
		}
		
		
		

		/*

		# ---------------- flow ----------------------


		for r in this.tereo.env.robots:
			print "robot"

			p = None
			for l in r.links:

				print "link"

				if sys.platform == 'win32':
					fname = os.path.expanduser('~') + '\\mes documents\\tereocnc\\robots\\'+r.constructor+'\\'+r.model+'\\'+l.file
				else:
					fname = os.path.expanduser('~') + '/tereocnc/robots/'+r.constructor+'/'+r.model+'/'+l.file

				print "fname:"+fname
				pf = Filename.fromOsSpecific(fname)
				try:
					obj = loader.loadModel(pf)
				except:
					print "could not open "+l.file
				else:

					myMaterial = Material()
					myMaterial.setShininess(50.0) #Make this material shiny
					myMaterial.setAmbient(VBase4(l.color[0],l.color[1],l.color[2],l.color[3]))
					myMaterial.setDiffuse(VBase4(l.color[0],l.color[1],l.color[2],l.color[3]))
					myMaterial.setSpecular(VBase4( (l.color[0]+1)/2 , (l.color[1]+1)/2 , (l.color[2]+1)/2, l.color[3]))
					obj.setMaterial(myMaterial)

					if (l.translate[0] + l.translate[1] + l.translate[2])!=0:
						obj.setPos(l.translate[0]*25.4/1000,-l.translate[2]*25.4/1000,l.translate[1]*25.4/1000)
					#if (l.axis[0] + l.axis[1] + l.axis[2])>0:
					#	obj.setHpr(l.axis[2]*90, l.axis[0]*90, l.axis[1]*90)
					if p == None:
						obj.setScale(10.0)
						obj.setHpr(0, 270, 0)
						obj.reparentTo(this.base.render)
					else:
						obj.reparentTo(p)
					p = obj

*/

	}	
	
	
}









//================================


/*
	def on_scene_activate(widget):


		#this.attach_frame(this.container)
		this.show()

		return

*/
/*

	def clear(self):
		this.model = []
		return
*/

/*
	def add_gtk_shortcuts(self, window, actions) :
		# Create the accel group and add it to the window
		accel_group = gtk.AccelGroup()
		window.add_accel_group(accel_group)
		# Create the action group
		action_group = gtk.ActionGroup('ActionGroup')
		# ==
		# Could give the list of action to add_toggle_actions method all at once and then connect each one
		for action in actions :
			action_group.add_toggle_actions([action])
			gtk_action = action_group.get_action(action[0])
			gtk_action.set_accel_group(accel_group)
			gtk_action.connect_accelerator()
		# ==
		return accel_group, action_group

*/
/*

	def setup_panda_events(self,window) :
		""" Setup mouse events in Panda """
		obj = DirectObject()

		return obj
*/
/*

	def draw_world_axis(self):

		if this.base == None:
			return

		cnode = this.CreateOpenShadedCone("xaxis",12)
		nodePath = NodePath(cnode)
		nodePath.reparentTo(this.base.render)
		nodePath.setPos(0, 40, -5)

		return
*/

/*

	# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	#
	# Function name : CreateOpenShadedCone
	#
	# Author : Paul H. Leopard, DESE Research, Inc.
	#
	# Description:
	#
	#   Create and return a GeomNode describing an open shaded cone.
	#
	# Input(s):
	#
	#   nodeName - Name of the GeomNode
	#   sliceCount - Number of slices in the cone.
	#
	# Ou	tput(s):
	#
	#   A GeomNode representing the cone
	#
	# Example Useage:
	#
	#   geomNode = CreateOpenShadedCone('ConeWith32Slices',32)
	#   nodePath = NodePath(geomNode)
	#   nodePath.reparentTo(render)
	#
	def CreateOpenShadedCone(self,nodeName,sliceCount):

		# Create points for a circle
		deltaAngle = 360.0/float(sliceCount-2)
		angle = 0
		points = []
		for i in range(0,sliceCount):
			angleRadians = angle*0.01745329251994
			y = math.sin(angleRadians)
			x = math.cos(angleRadians)
			point = (x,y)
			points.append(point)
			angle += deltaAngle

		# Define the vetex data format (GVF).
		gvf = GeomVertexFormat.getV3n3c4t2()

		# Create the vetex data container (GVD) using the GVF from above to
		# describe its contents.
		gvd = GeomVertexData('GridVertices',gvf,Geom.UHStatic)

		# Create writers for the GVD, one for each type of data (column) that
		# we are going to store in it.
		gvwV = GeomVertexWriter(gvd,'vertex')
		gvwT = GeomVertexWriter(gvd,'texcoord')
		gvwC = GeomVertexWriter(gvd,'color')
		gvwN = GeomVertexWriter(gvd,'normal')

		# Use the writers to add vertex data to the GVD.
		T = (0,0,1)
		C = (1,1,1,1)
		n = len(points)

		# Upload Vertex/Normal/Tex/Color for top point
		gvwV.addData3f(T[0],T[1],T[2])
		gvwN.addData3f(0,0,1)
		gvwC.addData4f(C[0],C[1],C[2],C[3])
		gvwT.addData2f(0,0)
		tIndex = 0

		# For each point sample
		for i in range(0,n):

			# Get first point (A)
			p = points[i]
			A = (p[0],p[1],0)

			# Get next point (B)
			j = (i+1) % n
			p = points[j]
			B = (p[0],p[1],0)

 			# Compute surface normal (N0) for Facet(A,B,T)
			x10 = B[0] - A[0]
 			y10 = B[1] - A[1]
			z10 = B[2] - A[2]
 			x12 = B[0] - T[0]
			y12 = B[1] - T[1]
			z12 = B[2] - T[2]
			cpx = (z10*y12) - (y10*z12)
			cpy = (x10*z12) - (z10*x12)
			cpz = (y10*x12) - (x10*y12)
			r = math.sqrt(cpx*cpx + cpy*cpy + cpz*cpz)
			N = (cpx/r, cpy/r, cpz/r)

			# Get Previous point (C)
			# Compute facet normal (N1) for C,A,T
			# Compute average normal N = (N0 + N1)/2
			# @TODO@ Implement these ^^^ later on

			# Compute normalized texture coordinate TX(U,V) on XY plane
			TX = (A[0],A[1])

			# Upload Vertex/Normal/Tex/Color
			gvwV.addData3f(A[0],A[1],A[2])
			gvwN.addData3f(N[0],N[1],N[2])
			gvwC.addData4f(C[0],C[1],C[2],C[3])
			gvwT.addData2f(TX[0],TX[1])

		# Create a GeomPrimitive object to use the vertices in the GVD
		# then fill it with our cone facets data.

		geom = Geom(gvd)

		for i in range(0,n):

			# Get first point (A)
			aIndex = i+1

			# Get next point (B)
			j = (aIndex+1) % n
			bIndex = j

			# Create and upload face F(A,B,T)
			tris = GeomTriangles(Geom.UHStatic)
			tris.setIndexType(Geom.NTUint32)
			tris.addVertex(aIndex)
			tris.addVertex(bIndex)
			tris.addVertex(tIndex)
			tris.closePrimitive()

			geom.addPrimitive(tris)

			str = "\tTris : %d,%d,%d" % (aIndex,bIndex,tIndex)
			print str

		geomNode = GeomNode(nodeName)
		geomNode.addGeom(geom)

		return geomNode
*/
/*
	def resize_window(self,widget, request):

		props = WindowProperties().getDefault()
		props = WindowProperties(base.win.getProperties())
		props.setOrigin(0, 0)
		print "request w:%d"%(request.width)+" h:%d"%(request.height)
		props.setSize(request.width, request.height)
		props.setParentWindow(widget.window.handle)  #xid
		base.win.requestProperties(props)


		return
*/

/*
	#######################################################
	#	3D navigation
	#######################################################

	def gl_startrotate(self, widget, event):

		this.rotating = True
		this.mousex = event.x
		this.mousey = event.y

	def gl_endrotate(self, widget, event):

		this.rotating = False

	def gl_rotate(self, widget, event):

		if this.rotating == False: return

 		s = 0.05

		this.lat = min(this.maxlat, max(this.minlat, this.lat + (event.y - this.mousey) * s))
		this.lon = (this.lon + (event.x - this.mousex) * s) % 360
		this.glarea.queue_draw()
		return

	def gl_zoomwheel(self, widget, event):
		#print "zoomwheel"

		if event.direction == gtk.gdk.SCROLL_UP:
			#zoomin
			this.distance = this.distance / 1.1
		else:
			#zoomout
			this.distance = this.distance * 1.1
		this.glarea.queue_draw()

	def detach2object(self,o):

		for m in this.model:
			for i in range(len(m.parts)):
				p = m.parts[i]
				#print p #type(p).__name__
				#if type(p).__name__ == "Collection":
				if hasattr(p,"parts") == True:
					r = this.recursive_detach2object(m.parts,i,o)
					if r == True:
						return
				if p == o:
					print "ok found"
					#m = p
					del m.parts[i]
					return
		return

	def recursive_detach2object(self,par,idx,o):
		for i in range(len(par[idx].parts)):
			p = par[idx].parts[i]
			if hasattr(p,"parts") == True:
				ret = this.recursive_detach2object(par[idx].parts,i,o)
				if ret == True:
					return True
			if p == o:
				print "ok found r"
				#par[idx] = p
				del par[idx].parts[i]
				return True
		return False

	def attach2object(self,o,mo):
	

		for m in this.model:
			for i in range(len(m.parts)):
				p = m.parts[i]

				if hasattr(p,"parts") == True:
					r = this.recursive_attach2object(p,o,mo)
					if r == True:
						return
				if p == o:
					#print "ok found"
					m.parts.append(Collection([p,mo]))
					del m.parts[i]
		return

	def recursive_attach2object(self,r,o,m):
		for i in range(len(r.parts)):
			p = r.parts[i]
			if hasattr(p,"parts") == True:
				ret = this.recursive_attach2object(p,o,m)
				if ret == True:
					return True
			if p == o:
				#print "ok found r"
				r.parts.append(Collection([p,m]))
				del r.parts[i]
				return True
		return False


	def addmodel(self,m):
		#print "add model"
		this.model.append(m)
		return

	def removemodel(self,m):
		#print "remove m:"
		#print "range %d"%(len(this.model))

		for i in range(len(this.model)):
			#print "i %d"%(i)
			if this.model[i] == m:
				print "found"
				del this.model[i]

	def basic_lighting(self):

		glLightfv(GL_LIGHT0, GL_POSITION, (-8, 10, 5, 0))
		glLightfv(GL_LIGHT0, GL_AMBIENT, (0.3,0.3,0.3,0))
		glLightfv(GL_LIGHT0, GL_DIFFUSE, (.4,.4,.4,0))

		glLightfv(GL_LIGHT0+1, GL_POSITION, (-1, -1, 1.5, 0))
		glLightfv(GL_LIGHT0+1, GL_AMBIENT, (.0,.0,.0,1))
		glLightfv(GL_LIGHT0+1, GL_DIFFUSE, (.0,.0,.4,0))

		glEnable(GL_CULL_FACE)
		glCullFace(GL_BACK)
		glEnable(GL_LIGHTING)
		glEnable(GL_LIGHT0)
		glShadeModel(GL_SMOOTH)
		glDepthFunc(GL_LESS)
		glEnable(GL_DEPTH_TEST)

		return



	def redraw(self,area,_):

		return


		allocation = area.get_allocation ()
		viewport_width = float (allocation.width)
		viewport_height = float (allocation.height)

		glViewport (0, 0, int (viewport_width), int (viewport_height))
		glClearColor(1.0, 1.0, 1.0, 0.0)


		tereoGL.drawWorldAxis()

		return



#	def on_btn_scene(self, widget, data=None):

		#w = this.get_active_window()
		#w.hide()

		#this.detach_toolbar()

		#this.toolbarhost = "vbox4"
		#vb4 =  this.builder.get_object("vbox4")
		#if vb4 == None:
		#	this.addlog ("gui",1,"vb4 none")
		#if this.toolbar == None:
		#	this.addlog ("gui",1,"tb none")
		#vb4.pack_end(this.toolbar,False,True,0)

		#for w in range(this.toolbar.get_n_items()):
		#	this.toolbar.get_nth_item(w).hide() #set_visible(False)

		#this.builder.get_object(this.TB_IO).show()
		#this.builder.get_object(this.TB_ESTOP).show()
		#this.builder.get_object(this.TB_QUIT).show()
		#this.builder.get_object(this.TB_DRAWING).show()
		#this.builder.get_object(this.TB_TASK).show()
		#this.builder.get_object(this.TB_JOG).show()
		#this.builder.get_object(this.TB_LOG).show()
		#this.builder.get_object(this.TB_NET).show()
		#this.builder.get_object(this.TB_ROBOTS).show()
		#this.builder.get_object(this.TB_FLOWS).show()
		#this.builder.get_object(this.TB_TOOLS).show()
		#this.builder.get_object(this.TB_RUN).show()
		#this.builder.get_object(this.TB_PAUSE).show()
		#this.builder.get_object(this.TB_STOP).show()
		#this.builder.get_object(this.TB_CONFIG).show()

		#fcw = this.builder.get_object("guiWindow")
		#fcw.show()

		#this.scene_init()
		#return



*/

/*






##
##		this.accept("escape", sys.exit)
##		this.accept("arrow_up", this.zoomIn)
##		this.accept("arrow_down", this.zoomOut)
##		this.accept("arrow_left", this.orbitL)
##		this.accept("arrow_left-up", this.orbitoff)
##		this.accept("arrow_right", this.orbitR)
##		this.accept("arrow_right-up", this.orbitoff)
##		this.accept("p", this.getWidgetTransformsF)



##	def orbitR(self):
##		taskMgr.add(this.SpinCameraRTask, "SpinTask")
##	def orbitL(self):
##		taskMgr.add(this.SpinCameraLTask, "SpinTask")
##	def orbitoff(self):
##		taskMgr.remove("SpinTask")
##	def SpinCameraLTask(self, task):
##		base.camera.setHpr(base.camera.getH()-1, 0, 0)
##		this.angleradians = base.camera.getH() * (math.pi / 180.0)
##		base.camera.setPos(this.camDist*math.sin(this.angleradians),-this.camDist*math.cos(this.angleradians),1)
##		return Task.cont
##	def SpinCameraRTask(self, task):
##		base.camera.setHpr(base.camera.getH()+1, 0, 0)
##		this.angleradians = base.camera.getH() * (math.pi / 180.0)
##		base.camera.setPos(this.camDist*math.sin(this.angleradians),-this.camDist*math.cos(this.angleradians),1)
##		return Task.cont
##	def zoomIn(self):
##		radian = base.camera.getH()*math.pi/180.0
##		dx = this.D2M*-math.sin(radian)
##		dy = this.D2M*+math.cos(radian)
##		base.camera.setPos(Vec3(base.camera.getX()+dx,base.camera.getY()+dy,1))
##		this.camDist = this.camDist -1
##	def zoomOut(self):
##		radian = base.camera.getH()*math.pi/180.0
##		dx = this.D2M*+math.sin(radian)
##		dy = this.D2M*-math.cos(radian)
##		base.camera.setPos(Vec3(base.camera.getX()+dx,base.camera.getY()+dy,1))
##		this.camDist = this.camDist +1
##
##












#### ------------------------------------------------------------


class CoordsBase(object):
	def __init__(self, *args):
		this._coords = args
		#this.q = gluNewQuadric()
	def coords(self):
		return this._coords


def AsciiOBJ(filename):

	pandafile = Filename.fromOsSpecific(filename)

	try:
		obj = loader.loadModel(pandafile)
	except:
		print "could not open "+filename
	else:
		obj.reparentTo(render)
		obj.setPos(0, 40, -5)



class Collection(object):
    def __init__(self, parts):
	this.parts = parts
	this.vol = 0

    def traverse(self):
	for p in this.parts:
	    if hasattr(p, "apply"):
		p.apply()
	    if hasattr(p, "capture"):
		p.capture()
	    if hasattr(p, "draw"):
		p.draw()
	    if hasattr(p, "traverse"):
		p.traverse()
	    if hasattr(p, "unapply"):
		p.unapply()

    def volume(self):
	if hasattr(self, "vol") and this.vol != 0:
	    vol = this.vol
	else:
	    vol = sum(part.volume() for part in this.parts)
	#print "Collection.volume", vol
	return vol

    # a collection consisting of overlapping parts will have an incorrect
    # volume, because overlapping volumes will be counted twice.  If the
    # correct volume is known, it can be set using this method
    def set_volume(self,vol):
	this.vol = vol;

class Translate(Collection):
    def __init__(self, parts, x, y, z):
	this.parts = parts
	this.where = x, y, z

    def apply(self):
	glPushMatrix()
	#print "translate : %f"%(this.where[0]) + " %f"%(this.where[1]) + " %f"%(this.where[2])
	glTranslatef(*this.where)

    def unapply(self):
	glPopMatrix()

class Wireframe(Collection):
	def __init__(self, parts):
		this.parts = parts

	def apply(self):
		glPolygonMode( GL_FRONT_AND_BACK, GL_LINE )

	def unapply(self):
		glPolygonMode( GL_FRONT_AND_BACK, GL_FILL )

class Tag(Collection):
	def __init__(self, parts):
		this.parts = parts

class Scale(Collection):
    def __init__(self, parts, x, y, z):
	this.parts = parts
	this.scaleby = x, y, z

    def apply(self):

	glPushMatrix()
	glScalef(*this.scaleby)

    def unapply(self):

	glPopMatrix()

class HalTranslate(Collection):
    def __init__(self, parts, comp, var, x, y, z):
	this.parts = parts
	this.where = x, y, z
	this.comp = comp
	this.var = var

    def apply(self):
	x, y, z = this.where
	v = this.comp[this.var]

	glPushMatrix()
	glTranslatef(x*v, y*v, z*v)

    def unapply(self):

	glPopMatrix()


class HalRotate(Collection):
    def __init__(self, parts, comp, var, th, x, y, z):
	this.parts = parts
	this.where = th, x, y, z
	this.comp = comp
	this.var = var

    def apply(self):
	th, x, y, z = this.where

	glPushMatrix()
	glRotatef(th * this.comp[this.var], x, y, z)

    def unapply(self):

	glPopMatrix()

class Matrix(Collection):
	def __init__(self,part, mat):
		this.parts = part
		this.mat = mat

	def apply(self):
		glPushMatrix()
		glMultMatrixf(this.mat)

	def unapply(self):
		glPopMatrix()

class Rotate(Collection):
    def __init__(self, parts, th, x, y, z):
	this.parts = parts
	this.where = th, x, y, z

    def apply(self):
	th, x, y, z = this.where

	glPushMatrix()
	glRotatef(th, x, y, z)

    def unapply(self):

	glPopMatrix()


# six coordinate version - specify each side of the box
class Box(CoordsBase):
	def draw(self):

		x1, y1, z1, x2, y2, z2 = this.coords()
		if x1 > x2:
			tmp = x1
			x1 = x2
			x2 = tmp
		if y1 > y2:
			tmp = y1
			y1 = y2
			y2 = tmp
 		if z1 > z2:
			tmp = z1
			z1 = z2
			z2 = tmp

		glBegin(GL_QUADS)
		# bottom face
		glNormal3f(0,0,-1)
		glVertex3f(x2, y1, z1)
		glVertex3f(x1, y1, z1)
		glVertex3f(x1, y2, z1)
		glVertex3f(x2, y2, z1)
		# positive X face
		glNormal3f(1,0,0)
		glVertex3f(x2, y1, z1)
		glVertex3f(x2, y2, z1)
		glVertex3f(x2, y2, z2)
		glVertex3f(x2, y1, z2)
		# positive Y face
		glNormal3f(0,1,0)
		glVertex3f(x1, y2, z1)
		glVertex3f(x1, y2, z2)
		glVertex3f(x2, y2, z2)
		glVertex3f(x2, y2, z1)
		# negative Y face
		glNormal3f(0,-1,0)
		glVertex3f(x2, y1, z2)
		glVertex3f(x1, y1, z2)
		glVertex3f(x1, y1, z1)
		glVertex3f(x2, y1, z1)
		# negative X face
		glNormal3f(-1,0,0)
		glVertex3f(x1, y1, z1)
		glVertex3f(x1, y1, z2)
		glVertex3f(x1, y2, z2)
		glVertex3f(x1, y2, z1)
		# top face
		glNormal3f(0,0,1)
		glVertex3f(x1, y2, z2)
		glVertex3f(x1, y1, z2)
		glVertex3f(x2, y1, z2)
		glVertex3f(x2, y2, z2)
		glEnd()

	def volume(self):
		x1, y1, z1, x2, y2, z2 = this.coords()
		vol = abs((x1-x2)*(y1-y2)*(z1-z2))
		#print "Box.volume", vol
		return vol

# capture current transformation matrix
# note that this tranforms from the current coordinate system
# to the viewport system, NOT to the world system
class Capture(object):
    def __init__(self):
		this.t = [0,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0]

    def capture(self):
		a = glGetDoublev(GL_MODELVIEW_MATRIX)
		a = a[:]
		b = []
		for c in a:
			b=b+c[:]
		this.t = b
		return

    def volume(self):
		return 0.0

class Color(Collection):
	def __init__(self, color, parts):
		this.color = color
		Collection.__init__(self, parts)

	def apply(self):
		glPushAttrib(GL_LIGHTING_BIT)
		glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE, this.color)

	def unapply(self):
		glPopAttrib()
		return





# function to invert a transform matrix
# based on http://steve.hollasch.net/cgindex/math/matrix/afforthinv.c
# with simplifications since we don't do scaling

# This function inverts a 4x4 matrix that is affine and orthogonal.  In
# other words, the perspective components are [0 0 0 1], and the basis
# vectors are orthogonal to each other.  In addition, the matrix must
# not do scaling

def invert(src):
	# make a copy
	inv=src[:]
	# The inverse of the upper 3x3 is the transpose (since the basis
	# vectors are orthogonal to each other.
	inv[1],inv[4] = inv[4],inv[1]
	inv[2],inv[8] = inv[8],inv[2]
	inv[6],inv[9] = inv[9],inv[6]
	# The inverse of the translation component is just the negation
	# of the translation after dotting with the new upper3x3 rows. 
	inv[12] = -(src[12]*inv[0] + src[13]*inv[4] + src[14]*inv[8])
	inv[13] = -(src[12]*inv[1] + src[13]*inv[5] + src[14]*inv[9])
	inv[14] = -(src[12]*inv[2] + src[13]*inv[6] + src[14]*inv[10])
	return inv



class Triangle(CoordsBase):
    def draw(self):

		x1, y1, z1, x2, y2, z2, x3, y3, z3, n1, n2, n3 = this.coords()
		glBegin(GL_TRIANGLES)
		glNormal3f(n1,n2,n3)
		glVertex3f(x1, y1, z1)
		glVertex3f(x2, y2, z2)
		glVertex3f(x3, y3, z3)
		glEnd()
		return

class Line(CoordsBase):
	def draw(self):

		x1, y1, z1, x2, y2, z2 = this.coords()
		glBegin(GL_LINES)
		glVertex3f(x1, y1, z1)
		glVertex3f(x2, y2, z2)
		glEnd()


*/



function tpScene_on_get_robot_list(l) {

	console.log("onplugin scene robotlist:"+l);
	console.log(l);
	if (l == "get_robot_list") { console.log("echo"); return; }
	
	scene = tereoObjects['scene'];
	
	delete cnx_callbacks[scene.uid+"grl"];
	
	try {
		l = l.replace(' ','');
		l = l.replace('\n','');
		l = l.replace('\r','');
		var list = JSON.parse(l);
	} catch(e) { console.log(e);
			return;
	}
	
	
	i=0
	firstname = ""
	console.log ("search in list");
	for (r in list) {
		if (list.hasOwnProperty(r)) {
			if (firstname == "") {
				console.log ("robot found in list")
				firstname = list[r].name;
			}
			i=i+1;
			n = list[r].name;
			uid = (((1+Math.random())*0x100000)|0).toString(16).substring(1);
			scene.robots[n] = list[r];
			scene.robots[n].uid = uid;
			scene.robots[n].skeleton = new BABYLON.Skeleton(n, uid, scene.scene);
		}
	}	
	console.log ("nbrobots:"+i);
	scene.robotsnb = i;
		
	// start with first
	if (firstname != "") {
		var msg = {}
		msg['msg'] = "get_robot_kinematic";
		msg['uid'] = scene.uid+"grk";
		msg['arg'] = firstname;
		msg['callback'] = "tpScene_on_get_robot_kinematic";
		tereoMain.call("sendMqttMsg",JSON.stringify(msg));
	}
	
}


function tpScene_on_get_robot_kinematic(l) {
	console.log("onplugin scene robotkine:"+l);
	console.log(l);
	if (l == "get_robot_kinematic") { console.log ("echo"); return; }
	if (l.indexOf("@arg@")>-1) { console.log ("echo"); return; }
	
	scene = tereoObjects['scene'];
	
	delete cnx_callbacks[scene.uid+"grk"];
	
	try {
		l = l.replace(' ','');
		l = l.replace('\n','');
		l = l.replace('\r','');
		var list = JSON.parse(l);
	} catch(e) { console.log(e); return; }
	

	// next link
	found = false;
	nextlinkname ="";
	firstname="";
	i=0;
	for (r in scene.robots) {
		if (scene.robots.hasOwnProperty(r)) {
			if (firstname == "") { firstname = scene.robots[r].constructor+"."+scene.robots[r].model; }
			if (found == true && nextlinkname == "") {
				nextlinkname = scene.robots[r].name;
			}
			if (typeof scene.links[scene.robots[r].name] === 'undefined') {
				console.log ("found link undefined:"+i)
				n = scene.robots[r].name;
				scene.links[n] = list;
				
				// make skeleton
				parentbone = null;
				for (ll in list) {
				
					if (list.hasOwnProperty(ll)) {
					
						console.log("making bone:");
						console.log(list[ll]);
						ar = {};
						//.a  alpha d theta axis color 						
						matr = BABYLON.Matrix.FromValues(1,0,0,0, 0,1,0,0, 0,0,1,0, 0,0,0,1 );//new BABYLON.Matrix();
						console.log(matr);
						//console.log(matr.copyToArray());
						//matr2 = new BABYLON.Matrix();
						//matr.setTranslation(new BABYLON.Vector3(0,0,parseFloat(list[ll].d) ));
						//matr.multiplyToRef (BABYLON.Matrix.RotationX( parseFloat(list[ll].theta) ) , matr2 );
						//matr2.setTranslation(new BABYLON.Vector3( parseFloat(list[ll].a) ,0,0));
						//matr2.multiplyToRef (BABYLON.Matrix.RotationZ( parseFloat(list[ll].alpha) ), matr );
						console.log("i:"+i);
						console.log("new bon:"+ n+i.toString());
						bone = new BABYLON.Bone(n+i.toString(), scene.robots[r].skeleton,parentbone, matr);
						//scene.robots[r].skeleton.bones.push(bone);
						parentbone = bone
						i=i+1;
					
					}
					
				}
				
				found = true;
			}
			console.log(scene.robots[r].skeleton.bones);
			
		}
		
	}	
	
	console.log("========== scene ===========");
	console.log(scene.scene);
	
	// next robot ?
	if (nextlinkname != "") {
	
		console.log ("next link");
		var msg = {}
		msg['msg'] = "get_robot_kinematic";
		msg['uid'] = scene.uid+"grk";
		msg['arg'] = nextlinkname;
		msg['callback'] = "tpScene_on_get_robot_kinematic";	
		tereoMain.call("sendMqttMsg",JSON.stringify(msg));
		
	} else {
	
		console.log ("no next link");
		// mesh list
		var msg = {}
		msg['msg'] = "get_robot_mesh_list";
		msg['uid'] = scene.uid+"grml";
		msg['arg'] = firstname
		msg['callback'] = "tpScene_on_get_robot_mesh_list";
		tereoMain.call("sendMqttMsg",JSON.stringify(msg));
	}

}


function tpScene_on_get_robot_mesh_list(l) {

	console.log("onplugin scene robotmeshlist:"+l);
	console.log(l);
	if (l == "get_robot_mesh_list") { console.log ("echo"); return; }
	if (l.indexOf("@arg@")>-1) { console.log ("echo"); return; }
	
	scene = tereoObjects['scene'];
	
	delete cnx_callbacks[scene.uid+"grml"];
	
	
	try {
		//l = l.replace(' ','');
		l = l.replace('\n','');
		l = l.replace('\r','');
		var list = JSON.parse(l);
	} catch(e) { console.log(e); return; }
	

		console.log(list);

	// next list
	found = false;
	nextlinkname ="";
	firstname = "";
	i=0;
	for (r in scene.robots) {
		if (scene.robots.hasOwnProperty(r)) {
			if (firstname == "") { firstname = scene.robots[r].name; }
			if (found == true && nextlinkname == "") {
				nextlinkname = scene.robots[r].name;
			}
			if (typeof scene.meshes_link[scene.robots[r].name] === 'undefined') {
				console.log ("found link undefined:"+i)
				n = scene.robots[r].name;
				scene.meshes_link[n] = list;
								
				found = true;
			}
		}
		i=i+1;
	}	
	
	// next list
	if (nextlinkname != "") {
		console.log ("no next link");
		// mesh list
		var msg = {}
		msg['msg'] = "get_robot_mesh_list";
		msg['uid'] = scene.uid+"grml";
		msg['arg'] = firstname
		msg['callback'] = "tpScene_on_get_robot_mesh_list";
		tereoMain.call("sendMqttMsg",JSON.stringify(msg));
	} 
	else
	{
	
		if (firstname != "") { 
		
			// first mesh
			/*if (typeof scene.meshes_link[firstname]["0"] !== "undefined") {
				scene.lastlink = scene.meshes_link[firstname]["0"];*/
				scene.lastrobot="";
				scene.lastlink="";
				tpScene_on_get_robot_mesh();
			//} else { console.log("undefined"); }
		
		}
	}

	
}



function tpScene_on_get_robot_mesh() {

	console.log("onplugin scene robotmesh:");
	console.log();
	
	
	
	scene = tereoObjects['scene'];
	
	console.log("lastlink:"+scene.lastlink+" , lastrobot:"+scene.lastrobot);
	
	var rob="";
	var l="";
	var robfound = false;
	var lfound = false;
	
	//find next link
	if (scene.lastrobot=="") {
		
		for (r in scene.robots) {
			if (scene.robots.hasOwnProperty(r)) {
				if (robfound== false) {
					robfound=true;
					rob = r;
				}
			}
		}
		if (scene.lastlink=="") {
			if (rob != "") {
				l = "0"; //scene.meshes_link[firstname]["0"];
				lfound = true;
			}
		}
	}
	else
	{
		rfound=false;
		rfound2=false;
		for (r in scene.robots) {
			if (scene.robots.hasOwnProperty(r)) {
				if (robfound== false) {
					if (r = scene.lastrobot) {
						//console.log("1 robfound");
						robfound == true;
						rob = r;
						i=0;
						for (ml in scene.meshes_link[scene.robots[r].name]) {
							
							//if (scene.meshes_link.hasOwnProperty(ml)) {
							
								//console.log("link:"+ml);
								if ( typeof scene.meshes[scene.robots[r].name] != "undefined") {
								
									if (typeof scene.meshes[scene.robots[r].name][i.toString()] != "undefined") {
										//console.log("2 lfound");
										if (rfound == false) {
											if (i.toString() == scene.lastlink) { rfound = true; console.log("3 lfound"); }
										} 									
										
									}
									else {
								
										
											//console.log("4a next");
											if (rfound2 == false) {
												console.log("4 next l found");
												l = i.toString();
												lfound =true;
												rfound2 = true;
											}
											
								
									}
								
								} 
								
							
							//}
							i=i+1;
						}
						// depacement
						
					}
				} 
			}
		}		

	}
	
	if (lfound == false || robfound == false) {
		console.log("end");
	}
	
	console.log("rob: "+rob+" link:"+l);
	
	if (l == "" || rob == "") {
		console.log("end");
		return;
	}
	
	scene.lastlink = l;
	scene.lastrobot = rob;

	console.log(scene.robots[scene.lastrobot].skeleton);

			
	if (typeof scene.meshes[scene.robots[rob].name] === 'undefined') {
		scene.meshes[scene.robots[rob].name] = {};
	}
	
	scene.meshes[scene.robots[rob].name][l] = "ok";
	
	
	console.log(scene.meshes_link[scene.robots[rob].name]);
	url = "http://192.168.0.168:62355/robots/"+scene.meshes_link[scene.robots[rob].name][parseInt(l)].constructor+"/"+scene.meshes_link[scene.robots[rob].name][parseInt(l)].model+"/";
	url = url.replace (" ","%20");
	//console.log(url);
	
	 BABYLON.SceneLoader.ImportMesh("", url, scene.meshes_link[scene.robots[rob].name][parseInt(l)].FILE, scene.scene, function (newMeshes, particleSystems, skeletons) {
	//BABYLON.SceneLoader.ImportMesh("", url, "Rabbit.babylon", scene.scene, function (newMeshes, particleSystems, skeletons) {
		
			console.log("mesh loaded");
			
			/*var rabbit = newMeshes[1];

			rabbit.scaling = new BABYLON.Vector3(0.4, 0.4, 0.4);
			shadowGenerator.getShadowMap().renderList.push(rabbit);

			var rabbit2 = rabbit.clone("rabbit2");
			var rabbit3 = rabbit.clone("rabbit2");

			rabbit2.position = new BABYLON.Vector3(-50, 0, -20);
			rabbit2.skeleton = rabbit.skeleton.clone("clonedSkeleton");

			rabbit3.position = new BABYLON.Vector3(50, 0, -20);
			rabbit3.skeleton = rabbit.skeleton.clone("clonedSkeleton2");

			scene.scene.beginAnimation(skeletons[0], 0, 100, true, 0.8);
			scene.scene.beginAnimation(rabbit2.skeleton, 73, 100, true, 0.8);
			scene.scene.beginAnimation(rabbit3.skeleton, 0, 72, true, 0.8); 
			
			*/
			
			scene = tereoObjects['scene'];
			
			var mesh = newMeshes[0];
			var skel = scene.robots[scene.lastrobot].skeleton;
			
			//tpScene_attach_mesh2bone(mesh,skel,scene.robots[scene.lastrobot].name+scene.lastlink);
			
			// next link
			tpScene_on_get_robot_mesh();

			
			//scene.scene.beginAnimation(scene.robots[scene.lastrobot].skeleton, 0, 100, true, 1.0);			

	});
				

	
}


function tpScene_attach_mesh2bone(obj,ske,boneName)
{
	matricesWeights =[];
	floatIndices =[];
	boneIndice=-1;
	
	// Find bone's Indice
	for (ii=0;ii<ske.bones.length;ii++)
	{
		if (ske.bones[ii].name==boneName)
		{
			boneIndice=ii;
			break;
		}
	}
    
    if (boneIndice==-1) {console.error("Unable to find bone : "+boneName); return;}

	// Build matrices and indices buffers.
	console.log("total vertices:"+obj._totalVertices);
	for (ii=0;ii<obj._totalVertices;ii++)
	{
		matricesWeights[ii*4+0]=1.0;
		matricesWeights[ii*4+1]=0.0;
		matricesWeights[ii*4+2]=0.0;
		matricesWeights[ii*4+3]=0.0;
		floatIndices[ii*4+0]=boneIndice;
		floatIndices[ii*4+1]=boneIndice;
		floatIndices[ii*4+2]=boneIndice;
		floatIndices[ii*4+3]=boneIndice;
	}
    
    // Mounting the object on the skeleton
	obj.setVerticesData(matricesWeights, BABYLON.VertexBuffer.MatricesWeightsKind, false);
	obj.setVerticesData(floatIndices, BABYLON.VertexBuffer.MatricesIndicesKind, false);
	obj.skeleton = ske;
	
}


