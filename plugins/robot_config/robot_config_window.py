# -*- coding: utf-8 -*-




import pygtk
pygtk.require("2.0")
import gtk
import gobject

import copy



class robot_config_window(gtk.Window):


	def __init__(self,tereo,args=None):

		gtk.Window.__init__(self)

		self.tereo = tereo
		self.args = args

		self.set_default_size(640, 480)
		self.set_position(gtk.WIN_POS_CENTER)

		self.oldaxisindex = -1

		self.tmpdb = copy.deepcopy(self.tereo.env.robots[args].db)

		box1 = gtk.VBox(False, 0)
		box1.show()
		self.add(box1)

		self.box2 = gtk.HBox(False, 0)
		self.box2.show()
		box1.pack_start(self.box2,True,True,0)

		#---- treeview ----

		frame1 = gtk.Frame()
		frame1.set_size_request(200,-1)
		frame1.show()
		frame1.set_label_align(0,0)
		#shadow_type none
		self.box2.pack_start(frame1,False,False,0)


		axesframe = gtk.ScrolledWindow()
		axesframe.show()
		frame1.add(axesframe)





		self.axesTreeStore = gtk.TreeStore(gobject.TYPE_STRING,gobject.TYPE_STRING)

		axestree = gtk.TreeView(self.axesTreeStore)
		axestree.show()
		axestree.set_headers_visible(False)
		axestree.set_enable_search(False)
		#todo
		axestree.connect("cursor-changed", self.treeSelect)
		axesframe.add(axestree)

		ptColImage = gtk.TreeViewColumn()
		ptColName = gtk.TreeViewColumn()

		axestree.append_column(ptColImage)
		axestree.append_column(ptColName)

		ptImgCell = gtk.CellRendererPixbuf()
		ptNameCell = gtk.CellRendererText()

		ptColImage.pack_start(ptImgCell, True)
		ptColName.pack_start(ptNameCell, True)

		ptColImage.add_attribute(ptImgCell, 'stock_id', 0)
		ptColName.add_attribute(ptNameCell, 'text', 1)


		#---- end treeview ----


		#------------- plugin tabs --------------------
		genetabsobj = self.tereo.object_search_path("robot.config.panel.general")
		axistabsobj = self.tereo.object_search_path("robot.config.panel.axis")

		self.tabsgene = gtk.Notebook()
		self.tabsgene.show()
		self.genetabs = []

		for t in genetabsobj:
			f = t[1](self.tereo,self.tmpdb)
			self.genetabs.append(f)
			lbl = gtk.Label(t[0])
			self.tabsgene.append_page(f,lbl)

		self.tabsaxis = gtk.Notebook()
		self.tabsaxis.show()
		self.axistabs = []

		for t in axistabsobj:
			f = t[1](self.tereo,self.tmpdb)
			self.axistabs.append(f)
			lbl = gtk.Label(t[0])
			self.tabsaxis.append_page(f,lbl)

		#box2.pack_star(self.tabs,False,False,1)


		self.populate()

		box3 = gtk.HBox()
		box3.show()
		box1.pack_start(box3,False,False,1)

		button1 = gtk.Button("Apply")
		button1.connect_object("clicked", self.on_apply, self, None)
		box3.pack_start(button1, False, False, 0)
		button1.show()

		button2 = gtk.Button("Cancel")
		button2.connect_object("clicked", self.on_cancel, self, None)
		box3.pack_start(button2, False, False, 1)
		button2.show()

		self.show()

		return



	def populate(self):

		self.axesTreeStore.append(None,('gtk-dialog-info',"General" ))

		for i in range(0,6):
			self.axesTreeStore.append(None,('gtk-dialog-info',"Axis %d"%(i) ))

		return


	def treeSelect(self,widget):

		model, iter = widget.get_selection().get_selected()

		if not iter: return

		name = model.get_value(iter,1)

		print name

		if name == "General":
			self.load_gene_prop()
			return

		if name[:4] == "Axis":
			idx = int(name[5:])-1
			self.load_axis_prop(idx)
			return

		return




	def on_apply(self,widget):

		self.save_old_axis()
		self.save_gene_prop()

		return



	def on_robotsconfigwin_destroy(self,widget):
		# ask save changes
		return


	def on_cancel(self,widget):
		self.hide()
		return



	def load_axis_prop(self,idx):


		self.box2.remove(self.tabsgene)
		self.box2.pack_start(self.tabsaxis,True,True,1)

		self.save_old_axis()


		for f in self.axistabs:
			f.load_axis_prop(idx)

		self.oldaxisindex = idx

		return



	def load_gene_prop(self):

		print "load gene prop"

		self.save_old_axis()
		self.oldaxisindex = -1

		for f in self.genetabs:
			f.load_gene_prop()

		self.box2.remove(self.tabsaxis)
		self.box2.pack_start(self.tabsgene,True,True,1)



		return





	def save_old_axis(self):

		if self.oldaxisindex == -1: return

		for f in self.axistabs:
			f.save_old_axis()

		return



