# -*- coding: utf-8 -*-

import tereoMath


def calc_tool_matrix(p2,p3,p1,p4,c):


	print "calc tool matrix"

	mata = frame6_to_matrix(p1)

	i = matrix_invert(mata)
	if i == None:
		print "i none"
		return

	# multiply center by invert of p1 = b
	b = matrix_mult_vector(i,c)


	#c[0] = c[0] - p1.x
	#c[1] = c[1] - p1.y
	#c[2] = c[2] - p1.z


	n = math.sqrt(c[0]*c[0] + c[1]*c[1] + c[2]*c[2])
	if n == 0:
		print "n zero"
		return

	c[0] = c[0] / n
	c[1] = c[1] / n
	c[2] = c[2] / n

	i[0] = i[0] * c[0]
	i[1] = i[1] * c[0]
	i[2] = i[2] * c[0]

	i[4] = i[4] * c[1]
	i[5] = i[5] * c[1]
	i[6] = i[6] * c[1]

	i[8] = i[8] * c[2]
	i[9] = i[9] * c[2]
	i[10] = i[10] * c[2]

	print i

	ve = [p1.x,p1.y,p1.z]

	v = matrix_mult_vector(i,ve)
	print "test"
	print v

	return

def calc_tool_matrix_2points(p1,p2):


	print "calc tool matrix 2 points"


	# p2 = TCP with kuka tag
	matb = frame6_to_matrix(p2)
	print "matb"
	print matb

	# find the reference object coordinate
	v = [100.0,0.0,0.0]
	v = matrix_mult_vector(matb,v)
	print "reference point at :"
	print v

	# p1 = TCP with known tool
	mata = frame6_to_matrix(p1)

	ia = matrix_invert(matb)
	if ia == None:
		print "ia none"
		return

	v2 = matrix_mult_vector(ia,v)
	print "known tool vector"
	print v2


	ib = matrix_invert(mata)
	if ib == None:
		print "ib none"
		return

	v = matrix_mult_vector(ib,v)
	print "tool vector"
	print v

	v[0] = -v[0]
	v[1] = -v[1]
	v[2] = -v[2]

	frm = frame6(v[0],v[1],v[2],0,0,0)
	mat = frame6_to_matrix(frm)

	print "mat"
	print mat

	#f5 = frame6(400,100,200,0,30,-40)
	#m5 = frame6_to_matrix(f5)
	#mul = matrix_mult_matrix2(m5,mat)
	#print "mul"
	#print mul
	#frm = matrix_to_frame6(mul)
	#print "frm"
	#frm.dump()


	return mat



def calc_tool_matrix_4points(p1,p2,p3,p4):


	mata = [0.0,0.0,0.0,0.0,  0.0,0.0,0.0,0.0,  0.0,0.0,0.0,0.0,  0.0,0.0,0.0,0.0]
	mata[0] = 2 * (p2.x-p1.x)
	mata[1] = 2 * (p2.y-p1.y)
	mata[2] = 2 * (p2.z-p1.z)
	mata[4] = 2 * (p3.x-p2.x)
	mata[5] = 2 * (p3.y-p2.y)
	mata[6] = 2 * (p3.z-p2.z)
	mata[8] = 2 * (p4.x-p3.x)
	mata[9] = 2 * (p4.y-p3.y)
	mata[10] = 2 * (p4.z-p3.z)

	print "mata"
	print mata

	i = matrix_invert(mata)
	if i == None:
		return

	matb = [0.0, 0.0, 0.0]

	matb[0] = (p2.x * p2.x) - (p1.x * p1.x) + (p2.y * p2.y) - (p1.y * p1.y) + (p2.z * p2.z) - (p1.z * p1.z)
	matb[1] = (p3.x * p3.x) - (p2.x * p2.x) + (p3.y * p3.y) - (p2.y * p2.y) + (p3.z * p3.z) - (p2.z * p2.z)
	matb[2] = (p4.x * p4.x) - (p3.x * p3.x) + (p4.y * p4.y) - (p3.y * p3.y) + (p4.z * p4.z) - (p3.z * p3.z)

	print "matb"
	print matb


	c = matrix_mult_vector(i,matb)

	print "reference point at : %f"%(c[0])+" %f"%(c[1])+" %f"%(c[2])



	#dimensions
	l = math.sqrt((c[0]-p1.x)*(c[0]-p1.x) + (c[1]-p1.y)*(c[1]-p1.y) + (c[2]-p1.z)*(c[2]-p1.z))
	#l2 = math.sqrt((c[0]-p2.x)*(c[0]-p2.x) + (c[1]-p2.y)*(c[1]-p2.y) + (c[2]-p2.z)*(c[2]-p2.z))
	#l3 = math.sqrt((c[0]-p3.x)*(c[0]-p3.x) + (c[1]-p3.y)*(c[1]-p3.y) + (c[2]-p3.z)*(c[2]-p3.z))
	#l4 = math.sqrt((c[0]-p4.x)*(c[0]-p4.x) + (c[1]-p4.y)*(c[1]-p4.y) + (c[2]-p4.z)*(c[2]-p4.z))


	print "------"
	print l
	print "------"

	#calc_tool_matrix(p1,p2,p3,p4,c)


	mat1 = frame6_to_matrix(p1)

	i1 = matrix_invert(mat1)
	if i1 == None:
		print "i1 none"
		return

	v = matrix_mult_vector(i1,c)
	print "tool vector"
	print v



	print "--------------------------"
	mat2 = frame6_to_matrix(p2)

	i2 = matrix_invert(mat2)
	if i2 == None:
		print "i2 none"
		return

	v = matrix_mult_vector(i2,c)
	print "tool vector"
	print v




	v = [1495.522, 504.9047, 1525.907]


	p1.x = 0
	p1.y = 0
	p1.z = 0
	mat2 = frame6_to_matrix(p1)
	print "mat2"
	print mat2
	imat2 = matrix_invert(mat2)
	print "imat2"
	print imat2

	offset = matrix_mult_matrix2(mat1,imat2)
	print "offset:"
	print offset

	print "taille outil:"
	v = [0.0,0.0,0.0]
	v = matrix_mult_vector(offset,v)
	print v
	v[0] -= c[0]
	v[1] -= c[1]
	v[2] -= c[2]
	print v


	#i1 = matrix_invert(mat1)

	#i1[3] += c[0]
	#i1[7] += c[1]
	#i1[11] += c[2]

	#print "i1"
	#print i1

	#v = matrix_mult_vector(i1,v)

	#print "v 4pts"
	#print v




	return
