# -*- coding: utf-8 -*-


import os.path

from plugins.robot_panel.robot_panel_view import robot_panel_view
#import plugins.robot_panel.robot_panel_view


def on_robot_toggle(widget,parent):

	f = parent.object_search_path("robot.panel")

	if f == []:
		f = robot_panel_view(parent)
		parent.add_object("robot.panel","Robot panel",f)
	else:
		f=f[0][1]

	parent.detach_panel(f)
	f.show()

	if widget.get_active() == True:

		parent.attach_panel(f)
		f.populate()

	return
