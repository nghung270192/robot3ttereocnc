# -*- coding: utf-8 -*-

import pygtk
pygtk.require("2.0")
import gtk
import gobject
import sys
import platform
#import tereoPanda

#from pandac.PandaModules          import loadPrcFileData, WindowProperties
#from direct.task                  import Task
#from direct.showbase.DirectObject import DirectObject


#from panda3d.core import Material
#from panda3d.core import VBase4
#from panda3d.core        import DirectionalLight
#from panda3d.core        import AmbientLight
#from pandac.PandaModules import LineSegs
#from pandac.PandaModules import NodePath
#from pandac.PandaModules import GeomVertexData
#from pandac.PandaModules import GeomVertexFormat
#from pandac.PandaModules import Geom
#from pandac.PandaModules import GeomVertexWriter
#from pandac.PandaModules import GeomTriangles
#from pandac.PandaModules import GeomNode
#from pandac.PandaModules import AntialiasAttrib
#from panda3d.core import Vec3,Vec4,Mat4


#from panda3d.core import Filename

#loadPrcFileData("", "window-type none")
#loadPrcFileData("", "framebuffer-multisample 1")



class process_view(gtk.Frame):


	def __init__(self,tereo):

		gtk.Frame.__init__(self)


		self.tereo = tereo

		vbox1 = gtk.VBox(False,0)
		vbox1.show()
		self.add(vbox1)

		self.base = None

		mainpanel = gtk.HPaned()
		mainpanel.show()

		#can focus = true
		mainpanel.set_position(250) #250
		vbox1.pack_start(mainpanel,True,True,0)

		leftpanel = gtk.VPaned()
		leftpanel.show()
		#can focus = true
		leftpanel.set_position(140)
		mainpanel.pack1(leftpanel,False,False)

		frame1 = gtk.Frame()
		frame1.set_size_request(200,-1)
		frame1.show()
		frame1.set_label_align(0,0)
		#shadow_type none
		leftpanel.add(frame1)

		partframe = gtk.ScrolledWindow()
		partframe.show()
		#can focus true
		#hscrollbar_policy automatic
		#shadow_type  etched-in
		frame1.add(partframe)


		self.partTreeStore = gtk.TreeStore(gobject.TYPE_STRING,gobject.TYPE_STRING,gobject.TYPE_STRING,gobject.TYPE_STRING)

		parttree = gtk.TreeView(self.partTreeStore)
		parttree.show()
		#can focus = true
		parttree.set_headers_visible(False)
		parttree.set_enable_search(False)
		parttree.connect("cursor-changed", self.partTreeSelect)
		partframe.add(parttree)

		ptColImage = gtk.TreeViewColumn()
		ptColIdx = gtk.TreeViewColumn()
		ptColDesc = gtk.TreeViewColumn()
		ptColType = gtk.TreeViewColumn()

		parttree.append_column(ptColImage)
		parttree.append_column(ptColIdx)
		parttree.append_column(ptColDesc)
		parttree.append_column(ptColType)

		ptImgCell = gtk.CellRendererPixbuf()
		#ptNameCell = gtk.CellRendererText()
		ptIdxCell = gtk.CellRendererText()
		ptDescCell = gtk.CellRendererText()
		ptTypeCell = gtk.CellRendererText()

		ptColImage.pack_start(ptImgCell, True)
		#ptColImage.pack_start(ptNameCell, True)
		ptColIdx.pack_start(ptIdxCell, True)
		ptColDesc.pack_start(ptDescCell, True)
		ptColType.pack_start(ptTypeCell, True)

		ptColImage.add_attribute(ptImgCell, 'stock_id', 0)
		#ptColImage.add_attribute(ptNameCell, 'text', 0)
		ptColIdx.add_attribute(ptIdxCell, 'text', 1)
		ptColDesc.add_attribute(ptDescCell, 'text', 2)
		ptColType.add_attribute(ptTypeCell, 'text', 3)



		self.partTreeSelectIdx = -1
		self.partTreeSelectIdent = -1
		self.partTreeSelectType = -1





		#stock-id 0
		#text 0
        #title column
		#text 1
		#title desc
		#text 2
		#title column


		#partLabel = gtk.Label("Parts")
		#partLabel.show()
		frame1.set_label("Parts")
		#<property name="resize">False</property>
		#<property name="shrink">True</property>

		frame3D = gtk.VBox()
		frame3D.show()
		mainpanel.pack2(frame3D,True,True)
		#<property name="resize">True</property>
		#<property name="shrink">True</property>

		# -- 3D set ---
		self.drawing_area = gtk.DrawingArea()
		color = gtk.gdk.color_parse('#234fdb')
		self.drawing_area.modify_bg(gtk.STATE_NORMAL, color)
		#vbox.pack_start(self.drawing_area, False, False, 0)
		frame3D.pack_end(self.drawing_area)
		self.drawing_area.show()
		# ---------------


		self.tereo.add_function('process','Update',self.process_update)

		self.show()

		return

	def get_widget_id(self,widget) :
		""" Retrieve gtk widget ID to tell the Panda window to draw on it """
		if widget.get_window() == None:
			return
		if platform.system() == "Windows" : return widget.window.handle
		else                              : return widget.window.xid



	def attach_frame(self):

		print "attach frame"
		
		#import direct.directbase.DirectStart
		#props = WindowProperties().getDefault()
		#props.setOrigin(0, 0)
		#props.setParentWindow(self.get_widget_id(self.drawing_area))
		#self.base = base
		#base.openDefaultWindow(props=props)
		#base.setBackgroundColor(0.49,0.78,0.85)
		#self.drawing_area.connect("size_allocate", self.resize_panda_window)

		#alight = AmbientLight('alight')
		#alight.setColor(VBase4(0.2, 0.2, 0.2, 1))
		#alnp = base.render.attachNewNode(alight)
		##base.render.setLight(alnp)

		#dlight = DirectionalLight('dlight')
		#dlight.setColor(VBase4(0.8, 0.8, 0.5, 1))
		## Use a 512x512 resolution shadow map
		#dlight.setShadowCaster(True, 512, 512)
		##dlnp = base.render.attachNewNode(dlight)

		##dlnp.setHpr(0, -60, 0)
		##base.render.setLight(dlnp)

		# Enable the shader generator for the receiving nodes
		##base.render.setShaderAuto()
		#base.render.setAntialias(AntialiasAttrib.MMultisample,1)


		if self.base == None:
			return

		#todo js
		#grid = tereoPanda.grid().generate()
		#grid.reparentTo(self.base.render)

		return

	def resize_panda_window(self,widget, request) :

		# Connected to resize event of the widget Panda is draw on so that the Panda window update its size
		props = WindowProperties().getDefault()
		props = WindowProperties(base.win.getProperties())
		props.setOrigin(0, 0)
		props.setSize(request.width, request.height)
		props.setParentWindow(self.get_widget_id(widget))
		base.win.requestProperties(props)
		base.setBackgroundColor(0.49,0.78,0.85)

		return


	def on_scene_activate(widget):

		#if self.container == None:
		#	return

		#self.attach_frame(self.container)
		self.show()

		return



	## a part is selected ##
	def partTreeSelect(self, treeView):

		model, iter = treeView.get_selection().get_selected()

		if not iter: return

		self.partTreeSelectIdx = model.get_value(iter,1)
		self.partTreeSelectIdent = model.get_value(iter,1)
		self.partTreeSelectType = model.get_value(iter,3)


		if self.base == None:
			return

		#todo
		self.ResetModelPath()
		#self.base.render.clear()


		if self.partTreeSelectType == 'general':

			#parts = []
			for p in self.tereo.process_list.parts:
				m = self.get_main_part(p,True)
			return


		elif self.partTreeSelectType == 'part':

			for bp in self.tereo.process_list.parts:
				if bp.singleMemberNumber == self.partTreeSelectIdx:

					# add object
					m = self.get_main_part(bp,False)
					c = []
					cm = None
					for proc in bp.process:
						cm = proc.tereoproc.model()
						if cm != None:
							#c.append(cm)
							cm.reparentTo(render)
					#cc = Collection([cm])
					#self.drawingvismachframe.addmodel(cc)
					#self.drawingvismachframe.addmodel(m)
					#self.drawingvismachframe.glarea.queue_draw()
					return

			return

		elif self.partTreeSelectType == 'process':
			self.partTreeSelectIdx = model.get_value(model.iter_parent(iter),1)

			# add object

			for p in self.tereo.process_list.parts:
				if p.singleMemberNumber == self.partTreeSelectIdx:
					m = self.get_main_part(p,False)

					#print "ident:"+self.partTreeSelectIdent

					for proc in p.process:
						#print "procid:" + proc.processIdent
						if proc.processIdent == self.partTreeSelectIdent:
							self.drawingviscmachframe.addmodel(proc.tereoproc.model())

					self.drawingviscmachframe.addmodel(m)
					self.drawingvismachframe.glarea.queue_draw()
					return

			return

		return


	def ResetModelPath( self ):
        # Clears the model path, making sure to restore the current working
		# directory (so editor models can still be found).
		#todo
		#getModelPath().clear()
		#getModelPath().prependDirectory( '.' )
		return


	## show parts ##
	def process_update(self,tereo,args=None):

		#print "part tree populate"
		if tereo.process_list == None:
			return

		pl = self.partTreeStore

		pl.clear()

		giter = pl.append(None, ('',tereo.process_list.projectNumber,tereo.process_list.projectName,'general'))

		for part in tereo.process_list.parts:
			#print "a part"
			piter = pl.append(giter, ('',part.singleMemberNumber,part.designation,'part'))
			for proc in part.process:
				#print "a process"
				pl.append(piter, ('',proc.processIdent,proc.processKey,'process'))


		return


	def on_processWindow_destroy(self, widget):

		return




	####################################################
	# review window
	####################################################
	def on_gtk_review_activate(self,widget, data=None):



		self.workStack = []

		if self.partTreeSelectType == 'general':
			for p in self.interface.parts:

				ap = tereoProcess.Unpacking(p)
				ap.uid = "10000"+p.singleMemberNumber
				self.workStack.append(ap)

				ap = tereoProcess.Assembling(p)
				ap.uid = "10002"+p.singleMemberNumber
				self.workStack.append(ap)

				for proc in p.process:
					self.workStack.append(proc.tereoproc)


				ap = tereoProcess.Disassembling(p)
				ap.uid = "10003"+p.singleMemberNumber
				self.workStack.append(ap)

				#ap = tereoProcess.Storing(p)
				#ap.uid = "10001"+p.singleMemberNumber
				#self.workStack.append(ap)



		elif self.partTreeSelectType == 'part':
			for p in self.interface.parts:
				if p.singleMemberNumber == self.partTreeSelectIdx:

					ap = tereoProcess.Unpacking(p)
					ap.uid = "10000"+p.singleMemberNumber
					self.workStack.append(ap)

					ap = tereoProcess.Assembling(p)
					ap.uid = "10002"+p.singleMemberNumber
					self.workStack.append(ap)

					for proc in p.process:
						#print "adding process"+proc.tereoproc.name
						#print "uid"+proc.tereoproc.uid
						self.workStack.append(proc.tereoproc)

					ap = tereoProcess.Disassembling(p)
					ap.uid = "10003"+p.singleMemberNumber
					self.workStack.append(ap)

					#ap = tereoProcess.Storing(p)
					#ap.uid = "10001"+p.singleMemberNumber
					#self.workStack.append(ap)



		elif self.partTreeSelectType == 'process':
			for p in self.interface.parts:
				if p.singleMemberNumber == self.partTreeSelectIdx:

					ap = tereoProcess.Unpacking(p)
					ap.uid = "10000"+p.singleMemberNumber
					self.workStack.append(ap)

					ap = tereoProcess.Assembling(p)
					ap.uid = "10002"+p.singleMemberNumber
					self.workStack.append(ap)

					for proc in p.process:
						if proc.processIdent == self.partTreeSelectIdent:
							self.workStack.append(proc.tereoproc)

					ap = tereoProcess.Disassembling(p)
					ap.uid = "10003"+p.singleMemberNumber
					self.workStack.append(ap)

					#ap = tereoProcess.Storing(p)
					#ap.uid = "10001"+p.singleMemberNumber
					#self.workStack.append(ap)


		#else:
		#	print "nothing to do"
			return

		pw = self.builder.get_object("processWindow")
		pw.hide()

		rw = self.builder.get_object("reviewWindow")

		self.detach_toolbar()
		vb = self.builder.get_object("vbox3")
		self.toolbarhost = "vbox3"
		vb.pack_end(self.toolbar,False,True,0)

		for w in range(self.toolbar.get_n_items()):
			self.toolbar.get_nth_item(w).hide() #set_visible(False)

		self.builder.get_object(self.TB_IO).show()
		self.builder.get_object(self.TB_ESTOP).show()
		self.builder.get_object(self.TB_QUIT).show()
		self.builder.get_object(self.TB_PROCEED).show()
		self.builder.get_object(self.TB_WORKSHOP).show()



		# populate review
		pl = self.builder.get_object("workliststore")
		pl.clear()

		for proc in self.workStack:
			pl.append((True,proc.name, proc.uid,"[to do]"))
			#if hasattr(proc,"part"):
			#	print "proc part uid:" + proc.part.singleMemberNumber


		# populate group
		gr = self.builder.get_object("processGroupStore")
		gr.clear()

		for proc in self.workStack:
			found = False
			for i in gr:
				if i[1] == proc.name:
					found = True
			if found == False:
				gr.append((True,proc.name))


		# populate material
		mt = self.builder.get_object("matstore")
		mt.clear()

		for mat in self.interface.material:
			mt.append((mat,0))


		rw.maximize()
		rw.show()

		return


	def on_btnBackToProcess_clicked(self, widget, data=None):
		rw = self.builder.get_object("reviewWindow")
		rw.destroy()
		return

	def checkreviewgroup(self,cell,path_str):
		# a group was selected/unselected
		#print "group"

		# cell : cellrenderertoggle
		# path_str : line number from 0

		gr = self.builder.get_object("processGroupStore")
		ite = gr.get_iter(path_str)
		name = gr.get_value(ite,1)
		#print "name:"+name

		act = cell.get_active()
		act = not act

		pl = self.builder.get_object("workliststore")
		item = pl.get_iter_first()
		while (item != None):
			n = pl.get_value(item,1)
			if n == name:
				pl.set_value(item,0,act)
			item = pl.iter_next(item)

		gr.set_value(ite,0,act)

		return

	def checkreviewwork(self,cell, path_str):
		# a process was selected/unselected
		#print "process"
		pl = self.builder.get_object("workliststore")
		ite = pl.get_iter(path_str)
		act = cell.get_active()
		act = not act
		pl.set_value(ite,0,act)
		return



	def get_main_part(self, p, world):

		#print "get main part"

		#print "width:%s"%(p.width) + " height:%s"%(p.height) + " length:%s"%(p.length)


		#myMaterial = Material()
		#myMaterial.setShininess(50.0) #Make this material shiny
		#myMaterial.setAmbient(VBase4(0.619,0.421,0.352,1))
		#myMaterial.setDiffuse(VBase4(0.619,0.421,0.352,1))
		#myMaterial.setSpecular(VBase4( 0.619,0.421,0.352,1))
		#obj.setMaterial(myMaterial)


		m = None
		if world == True:

			print "world true"

			for c in range(0,p.count):

				print "part"

				cube = tereoPanda.box(0,0,0,p.length*0.03937,p.height*0.03937,p.width*0.03937).generate()
				cube.setColor(0.619, 0.421, 0.352,0.6)

				cubewf = tereoPanda.wireframeBox(0,0,0,p.length*0.03937,p.height*0.03937,p.width*0.03937).generate()
				cubewf.setColor(0.619, 0.421, 0.352,1)

				if len(p.transformations) == p.count:
					#for t in p.transformations:
					#m = Collection([m1,m2])
					t = p.glmatrix[c]
					if t == None:
						t = p.transformations[c]
						m = tereoPanda.pandaMatrix(t)
						cube.setMat(m)
						cube.setPos(t.ox*0.03937, t.oy*0.03937, t.oz*0.03937)
						cubewf.setMat(m)
						cubewf.setPos(t.ox*0.03937, t.oy*0.03937, t.oz*0.03937)
					else:
						#print "matrix found"
						#print t
						#todo
						print "-"
						m = tereoPanda.gl2pandaMatrix(t)
						cube.setMat(m)
						cubewf.setMat(m)
						#m = Matrix([m],t)

				cube.reparentTo(render)
				cubewf.reparentTo(render)

		else:

			print "world false : wireframe"

			cubewf = tereoPanda.wireframeBox(0,0,0,p.length*0.03937,p.width*0.03937,p.height*0.03937).generate()
			cubewf.setColor(0.0, 0.0, 1.0, 1.0)

			cube = tereoPanda.box(0,0,0,p.length*0.03937,p.width*0.03937,p.height*0.03937).generate()
			cube.setColor(0.59, 0.29, 0.0, 0.3)

			cube.reparentTo(render)
			cubewf.reparentTo(render)

		return

