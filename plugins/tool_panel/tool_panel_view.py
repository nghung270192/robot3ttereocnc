# -*- coding: utf-8 -*-

import pygtk
pygtk.require("2.0")
import gtk




class tool_panel_view(gtk.Frame):

	def __init__(self,tereo):

		gtk.Frame.__init__(self)

		self.tereo = tereo

		self.set_label_align(0,0)


		al = gtk.Alignment(0.5, 0.5, 1.0, 1.0)
		al.set_padding(0,0,12,0)

		self.set_label("Tool panel")

		self.add(al)


		al.show()

		sv = gtk.ScrolledWindow()

		al.add(sv)

		sv.show()

		vp = gtk.Viewport()
		sv.add(vp)
		vp.show()


		f = gtk.Frame()
		vp.add(f)

		f.show()

		self.vbox = gtk.VBox(False, 0)
		f.add(self.vbox)
		self.vbox.show()
		self.set_size_request(300,-1)
		self.show()

		self.populate()

		return



	def populate(self):


		if self.tereo.env == None:
			#print "env null"
			return

		# clear
		children = self.vbox.get_children()
		for iter in children:
			subchildren = iter.get_children()
			for subiter in subchildren:
				subiter.destroy()
			iter.destroy()

		img = gtk.Image()
		img.set_from_file('./media/add.png')
		img.show()
		ln = gtk.Button("New")
		ln.set_image(img)
		ln.show()
		self.vbox.pack_start(ln,False,True,0)
		#ln.connect("clicked", self.on_tool_new)

		i = 1

		for f in self.tereo.env.tools:
			#print "new tool"
			frm = gtk.Frame()
			self.vbox.pack_start(frm,False,True,0)
			frm.show()

			tbl = gtk.Table(3,3,False)
			frm.add(tbl)
			tbl.show()

			ln = gtk.Label(str(i))
			ln.set_justify(gtk.JUSTIFY_RIGHT)
			ln.show()
			tbl.attach(ln,0,1,0,1)

			ln = gtk.Label(f.name)
			ln.show()
			tbl.attach(ln,1,2,0,1)

			# --------------------------------


			lcal = self.tereo.function_search_path("tool.calibration")
			if lcal != []:

				img = gtk.Image()
				img.set_from_file('./media/calibrate.png')
				img.show()
				ln = gtk.Button("Calib.")
				ln.set_image(img)
				ln.show()
				tbl.attach(ln,0,1,1,2)
				ln.connect("clicked", self.on_tool_calibrate, i)

				slist = []
				defau = ""
				for it in lcal:
					if defau == "":
						defau = it[0]
					slist.append(it[0])

				self.combo1 = gtk.Combo()
				self.combo1.set_popdown_strings(slist)
				self.combo1.entry.set_text(defau)
				tbl.attach(self.combo1, 1, 2, 1, 2)
				self.combo1.show()


			# --------------------------------

			img = gtk.Image()
			img.set_from_file('./media/configure.png')
			img.show()
			ln = gtk.Button("Config.")
			ln.set_image(img)
			ln.connect ('clicked', self.tereo.execute_function,'tool.config',"Tool config",i)
			ln.show()
			tbl.attach(ln,0,1,2,3)

			i+=1


		return


	##################################################
	### TOOL CALIBRATION
	##################################################




	def on_tool_calibrate(self,widget,data):

		# calibration variables
		co = self.tereo.new_calibration_obj()
		co.calib_title = "Tool calibration"
		co.calib_nb_question = 4
		co.calib_questions = ['Touch the reference object (point 1)','Touch the reference object (point 2)','Touch the reference object (point 3)','Touch the reference object (point 4)']
		co.calib_robot_vars = ['$POS_ACT','$POS_ACT','$POS_ACT','$POS_ACT']
		co.calib_nb_img = 1
		co.calib_imgs = ['toolcalib.png']
		co.calib_handler = self.on_tool_calibrate_return
		co.calib_tag = data

		co.calib_win_init()

		return

	def on_tool_calibrate_return(self,co):

		if co.calib_ret == None:
			return

		if len(co.calib_ret) == 0:
			return

		a = co.calib_ret[0]
		b = co.calib_ret[1]
		c = co.calib_ret[2]
		d = co.calib_ret[3]

		fa = frame6(a.x,a.y,a.z,a.a,a.b,a.c)
		fb = frame6(b.x,b.y,b.z,b.a,b.b,b.c)
		fc = frame6(c.x,c.y,c.z,c.a,c.b,c.c)
		fd = frame6(d.x,d.y,d.z,d.a,d.b,d.c)


		self.tereo.env.tool[co.calib_tag-1].calc_tcp(fa,fb,fc,fd)


		return
