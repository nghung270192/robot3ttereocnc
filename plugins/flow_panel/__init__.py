# -*- coding: utf-8 -*-


import os.path

pluginName = "Flow panel"
version = "0.1"
author = "Olivier Martinez"
flow_panel_icon = None


def register(parent):

	mod = imp.load_source('flow_panel', './plugins/flow_panel/flow_panel.py')

	# flow panel button
	img = gtk.Image()
	img.set_from_file('./plugins/flow_panel/flow.png')
	img.show()
	flow_icon = gtk.ToggleToolButton ("Flow")
	flow_icon.set_icon_widget(img)
	flow_icon.set_active(False)
	flow_icon.set_label("Flow")
	flow_icon.connect("toggled", mod.on_flow_toggle, parent)
	parent.add_icon(flow_icon)

	return

