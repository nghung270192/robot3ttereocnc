# -*- coding: utf-8 -*-


def toolswin_init(self):

	self.tmptoolc = copy.deepcopy(self.env.toolchanger)
	self.temptools = copy.deepcopy(self.env.tools)

	da = self.builder.get_object("toolchangingpreview")
	da.connect ('expose-event', self.on_toolspreview_draw)

	# show tool mounted
	lv = self.builder.get_object("equipedtool")
	for i in range(0,55):
		for to in self.temptools:
			if to.pocket_number == i:
				#typ = ''
				#if to.type == to.TYPE_DRILL:
				#	typ='TYPE_DRILL'
				#elif to.type == to.TYPE_MILL:
				#	typ='TYPE_MILL'
				#t.set_property('TOOL%d'%(i),'TY'+typ+' DI%f'%(to.diameter)+' OL%f'%(to.overall_length)+' UL%f'%(to.usable_length)+ 'PL%f'%(to.point_length)+' CD%f'%(to.cutting_diameter)+' GR'+to.group+' NM'+to.name)
				nb = lv.iter_n_children(None)
				lv.append(("%d"%(nb+1),to.name))


	self.selectedtoolname=''
	self.selectedtoolgroup=''
	self.selectedtoolchangingname=''
	self.selectedtoolchanginggroup=''

	# tools changing systems
	toolsctv = self.builder.get_object("toolsctv")
	toolsctv.clear()
	for groupname in os.listdir(homedir+'/toolchanging'):
		group = toolsctv.append(None,('',groupname))
		for toolcname in os.listdir(homedir+'/toolchanging/' + groupname):
			toolsctv.append(group,('',toolcname))

	# tools
	toolstv = self.builder.get_object("toolstv")
	toolstv.clear()
	for groupname in os.listdir(homedir+'/tools'):
		group = toolstv.append(None,('',groupname))
		for toolname in os.listdir(homedir+'/tools/' + groupname):
			if toolname != 'schema.png':
				toolstv.append(group,('',toolname))

	while gtk.events_pending():
		gtk.main_iteration()

	if self.tmptoolc:
		#print "toolc ok"
		self.selectedtoolchangingname=self.tmptoolc.name
		self.selectedtoolchanginggroup=self.tmptoolc.group
		tp = self.builder.get_object("toolchanginglv")
		self.tmptoolc.showparams(tp)
		self.tmptoolc.draw(da)

	return

def on_toolspreview_draw(self,area, _):

	if self.tmptoolc:
		da = self.builder.get_object("toolchangingpreview")
		self.tmptoolc.draw(da)
	return

def on_toolswin_select_toolchanging(self,widget):
	model, iter = widget.get_selection().get_selected()
	if not iter:
		return
	name = model.get_value(iter,1)
	group = model.get_value(model.iter_parent(iter),1)
	#print name
	#print group



	if self.tmptoolc:
		del self.tmptoolc

	self.tmptoolc = tereoToolChanging.toolChangingObj()
	self.tmptoolc.group = group
	self.tmptoolc.name = name
	self.tmptoolc.homedir = homedir + '/toolchanging'

	self.tmptoolc.get_from_file()

	tp = self.builder.get_object("toolchanginglv")
	self.tmptoolc.showparams(tp)
	da = self.builder.get_object("toolchangingpreview")
	self.tmptoolc.draw(da)

	return



def on_toolswin_select_tools(self,widget):
	model, iter = widget.get_selection().get_selected()
	if not iter:
		return
	name = model.get_value(iter,1)
	group = model.get_value(model.iter_parent(iter),1)

	self.selectedtoolname = name
	self.selectedtoolgroup = group


	to = tereoTool.toolObj()
	to.name = name
	to.group = group
	to.homedir = homedir + '/tools'
	to.get_from_file()
	tp = self.builder.get_object("toolsparamlv")
	to.showparams(tp)
	del to

	# showimage
	img = self.builder.get_object("toolImage")
	pixbuf = gtk.gdk.pixbuf_new_from_file(homedir + '/tools/' + group + '/schema.png')
	#w = pixbuf.get_width()
	#h = pixbuf.get_height()
	#if h > 0:
	#	f = 150.0/h
	#	scaled_buf = pixbuf.scale_simple(int(w * f),150,gtk.gdk.INTERP_BILINEAR)
	img.set_from_pixbuf(pixbuf)
	#	del scaled_buf
	del pixbuf

	return

def on_toolswin_mount(self,widget):
	if self.selectedtoolname==''or self.selectedtoolgroup=='':
		return
	lv = self.builder.get_object("equipedtool")
	nb = lv.iter_n_children(None)
	lv.append(("%d"%(nb+1),self.selectedtoolname))
	to = tereoTool.toolObj()
	to.name = self.selectedtoolname
	to.group = self.selectedtoolgroup
	to.homedir = homedir + '/tools'
	to.get_from_file()
	to.pocket_number = nb
	self.temptools.append(to)

	return

def on_toolswin_unmount(self,widget):
	return


def on_toolswin_destroy(self,widget):
	# ask save changes
	return

def on_toolswin_cancel(self,widget):
	self.toolswin.hide()
	return

def on_toolswin_ok(self,widget):
	if self.env.toolchanger:
		del self.env.toolchanger
	self.env.toolchanger = copy.deepcopy(self.tmptoolc)

	if self.env.tools:
		del self.env.tools
	self.env.tools = copy.deepcopy(self.temptools)
	self.toolswin.hide()

	return


