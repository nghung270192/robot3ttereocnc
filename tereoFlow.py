#!/usr/bin/python
from tereoVismach import *
import tereoProcess
from math import *
from tereoMath import *


import xml.etree.ElementTree as ET


def get_from_db(db):

	col = []

	for t in db.findall('FLOW'):

		fo = flowObj()
		p = t.get('NUMBER')
		if p: fo.index = int(p)
		p = t.get('BOUNDING_LENGTH')
		if p: fo.bounding_length = float(p)
		p = t.get('BOUNDING_HEIGHT')
		if p: fo.bounding_height = float(p)
		p = t.get('BOUNDING_WIDTH')
		if p: fo.bounding_width = float(p)
		p = t.get('WORKING_LENGTH')
		if p: fo.working_length = float(p)
		p = t.get('WORKING_WIDTH')
		if p: fo.working_width = float(p)
		p = t.get('DIRECTION_X')
		if p: fo.flow_direction_x = float(p)
		p = t.get('DIRECTION_Y')
		if p: fo.flow_direction_y = float(p)
		p = t.get('DIRECTION_Z')
		if p: fo.flow_direction_z = float(p)
		fo.bidirectional = t.get('BIDIRECTIONAL')
		fo.unity = t.get('UNITY')
		fo.group = t.get('GROUP')
		fo.name = t.get('NAME')
		fo.model = t.get('MODEL')
		fo.material = t.get('MATERIAL')
		p = t.get('ORIGIN_X')
		if p: fo.origin_x = float(p)
		p = t.get('ORIGIN_Y')
		if p: fo.origin_y = float(p)
		p = t.get('ORIGIN_Z')
		if p: fo.origin_z = float(p)
		p = t.get('OFFSET_X')
		if p: fo.offset_x = float(p)
		p = t.get('OFFSET_Y')
		if p: fo.offset_y = float(p)
		p = t.get('OFFSET_Z')
		if p: fo.offset_z = float(p)
		p = t.get('COLOR')
		if p:
			wo = p.split(' ')
			for w in wo:
				w.strip()
			fo.color = [float(wo[0]),float(wo[1]),float(wo[2]),1.0]

		p = t.get('COMPENSX')
		if p:
			wo = p.split(' ')
			for w in wo:
				w.strip()
			fo.compensX = [float(wo[0]),float(wo[1]),float(wo[2]),1.0]
		p = t.get('COMPENSY')
		if p:
			wo = p.split(' ')
			for w in wo:
				w.strip()
			fo.compensY = [float(wo[0]),float(wo[1]),float(wo[2]),1.0]
		p = t.get('COMPENSO')
		if p:
			wo = p.split(' ')
			for w in wo:
				w.strip()
			fo.compensO = [float(wo[0]),float(wo[1]),float(wo[2]),1.0]

		p = t.get('MATRIX')
		if p:
			wo = p.split(' ')

			for w in wo:
				w.strip()
			fo.matrix = [float(wo[0]),float(wo[1]),float(wo[2]),float(wo[3]),float(wo[4]),float(wo[5]),float(wo[6]),float(wo[7]),float(wo[8]),float(wo[9]),float(wo[10]),float(wo[11]),float(wo[12]),float(wo[13]),float(wo[14]),float(wo[15])]
		p = t.get('FRAME')
		if p:
			wo = p.split(' ')
			for w in wo:
				w.strip()
			fo.frame = [float(wo[0]),float(wo[1]),float(wo[2]),float(wo[3]),float(wo[4]),float(wo[5])]

		col.append(fo)

	return col

def set_to_db(db,flow):


	if hasattr(db,"getroot"):
		ter = db.getroot()
	else:
		ter = db

	t=ter.find("FLOW")
	while t != None :
		ter.remove(t)
		t = ter.find("FLOW")



	i = 0
	for fo in flow:
		t = ET.SubElement(ter,'TEREOFLOW')
		if t != None:
			t.set('NUMBER',str(fo.index))
			t.set('BOUNDING_LENGTH',str(fo.bounding_length))
			t.set('BOUNDING_HEIGHT',str(fo.bounding_height))
			t.set('BOUNDING_WIDTH',str(fo.bounding_width))
			t.set('WORKING_LENGTH',str(fo.working_length))
			t.set('WORKING_WIDTH',str(fo.working_width))
			t.set('DIRECTION_X',str(fo.flow_direction_x))
			t.set('DIRECTION_Y',str(fo.flow_direction_y))
			t.set('DIRECTION_Z',str(fo.flow_direction_z))
			t.set('BIDIRECTIONAL',fo.bidirectional)
			t.set('UNITY',fo.unity)
			t.set('GROUP',fo.group)
			t.set('NAME',fo.name)
			t.set('MODEL',fo.model)
			t.set('MATERIAL',fo.material)
			t.set('ORIGIN_X',str(fo.origin_x))
			t.set('ORIGIN_Y',str(fo.origin_y))
			t.set('ORIGIN_Z',str(fo.origin_z))
			t.set('OFFSET_X',0) #str(fo.offset_x)])
			t.set('OFFSET_Y',0) #str(fo.offset_y)])
			t.set('OFFSET_Z',0) #str(fo.offset_z)])
			#strg = "%f"%(fo.matrix[0])
			#for j in range(1,16):
			#	strg += " %f"%(fo.matrix[j])
			t.set('MATRIX',fo.matrix.dump2())

			i = i + 1



	return

class flowPart:
	def __init__(self):
		self.width = 0.0
		self.length = 0.0
		self.height = 0.0
		self.x = 0.0
		self.y = 0.0
		self.z = 0.0
		self.model = None
		self.topmodel = None
		self.singleMemberNumber = 0
		self.x_angle = 0.0
		self.y_angle = 0.0
		self.z_angle = 0.0

	def get_center(self):
		l = [self.length,self.width,self.height ]
		if self.z_angle != 0:
			tereoProcess.myRotate(l,self.z_angle,0,0,1)
		r = [self.x + l[0]/2,self.y + l[1]/2,self.z + l[2]/2 ]
		return r

class flowObj:
	def __init__(self):

		self.bounding_length = 0.0
		self.bounding_height = 0.0
		self.bounding_width = 0.0

		self.working_length = 0.0
		self.working_height = 0.0

		self.flow_direction_x = 0.0
		self.flow_direction_y = 0.0
		self.flow_direction_z = 0.0

		self.bidirectional = 'NO'
		self.unity = 'inch'

		self.group = ''
		self.name = ''
		self.model = ''
		self.homedir = ''

		self.model3d = None

		self.origin_x = 0.0
		self.origin_y = 0.0
		self.origin_z = 0.0

		self.material = ""

		self.offset_x = 0.0
		self.offset_y = 0.0
		self.offset_z = 0.0


		self.storing_offset = 0.0
		self.stored_parts = [] # part.singlemembernumber, x, y, z, l, w, h, model3d, za

		self.element_per_width = 0
		self.element_per_height = 0
		self.current_element_row = 0
		self.current_element_col = 0
		self.row_space = 0.0
		self.col_space = 0.0
		self.element_width = 0.0
		self.element_height = 0.0
		self.element_length = 0.0

		self.attached_object = None

		self.color = [0.5,0.5,0.5,1.0]
		self.matrix = tereoMath.matrix44()

		self.frame = tereoMath.frame6() #  X 1489.136, Y -417.2623, Z 730.2250, A -91.73743, B -1.271138, C 88.70203
		self.compensX = [0.0,0.0,0.0]
		self.compensY = [0.0,0.0,0.0]
		self.compensO = [0.0,0.0,0.0]

		self.vmodel = None

		return

	def fill_stored_parts(self):

		ww = self.working_width
		wl = self.working_length
		za = self.z_angle
		xi = self.x_inclination
		px = self.position_x
		pz = self.position_z
		py = self.position_y
		ox = self.offset_x
		oy = self.offset_y
		oz = self.offset_z
		fo = self.flow_origin

		if self.unity == 'mm':
			pz = pz * 0.03937
			px = px * 0.03937
			py = py * 0.03937
			wl = wl * 0.03937
			ww = ww * 0.03937
			ox = ox * 0.03937
			oy = oy * 0.03937
			oz = oz * 0.03937


		xo = 0
		x = 0
		y = 0
		z = 0


		if self.flow_origin == 0:
			x = ox
			yo = oy
			z = oz
		elif fo == 1:
			x = ox+wl
			yo = oy
			z = oz
		elif fo == 2:
			x = ox
			yo = oy+ww
			z = oz
		elif fo == 3:
			x = ox+wl
			yo = oy+ww
			z = oz




		self.stored_parts = []

		for j in range(self.element_per_height):

			y = yo

			#if fo == 0:
			#	y = y - self.element_width*0.03937

			for i in range(self.element_per_width):
#				lumber = Box(0,0,0,self.element_width*0.03937,self.element_length*0.03937,self.element_height*0.03937)
				lumber = Box(0,0,0,self.element_length*0.03937,self.element_width*0.03937,self.element_height*0.03937)
				lumber = Color([0.65,0.45,0.55,1.0],[lumber])
				lumber = Translate([lumber],x,y,z) # position within the palette

				v = [x,y,z]

				if za != 0:
					lumber = Rotate([lumber],za,0,0,1) # rotation
					tereoProcess.myRotate(v,za,0,0,1)
				if xi != 0:
					lumber = Rotate([lumber],xi,1,0,0) # inclination
					tereoProcess.myRotate(v,xi,1,0,0)

				lumber = Translate([lumber],px,py,pz) # position relative to the scene

				v[0]=v[0]+px
				v[1]=v[1]+py
				v[2]=v[2]+pz
				lumber = Collection([lumber])

				sp = flowPart()
				sp.singleMemberNumber = -1
				sp.x = v[0]
				sp.y = v[1]
				sp.z = v[2]
				sp.length = self.element_length*0.03937
				sp.width = self.element_width*0.03937
				sp.height = self.element_height*0.03937
				sp.model3d = lumber
				sp.z_angle = za
				sp.x_angle = xi

				self.stored_parts.append(sp)

				if fo == 0:
					y = y + self.element_width*0.03937 - self.col_space*0.03937
				else:
					y = y - self.element_width*0.03937 + self.col_space*0.03937

			z = z + self.element_height*0.03937 + self.row_space*0.03937

		return

	def attach_object(self,ao,rob,vm):


		lc = rob.last_coords
		print "rob lc X%f"%(lc[0])+" Y%f"%(lc[1])+" Z%f"%(lc[2])


		dum = Box(-1.0,-1.0,0.0,1.0,1.0,50.0)
		dum = Color([1.0,0.0,0.0,1.0],[dum])

		lumber = Box(0,0,0,ao.length,ao.width,ao.height)
		lumber = Wireframe([lumber])
		lumber = Color([0.0,0.0,0.0,1.0],[lumber])
		lumber = Collection([dum,lumber])

		ao.topmodel = lumber

		lumber = Translate([lumber],ao.x,ao.y,ao.z) # position relative to the scene

		print "r X%f"%(ao.x)+" Y%f"%(ao.y)+" Z%f"%(ao.z)

		# cancel tool compensation
		w = [0,6.88975,-3.838575]

		if lc[3] != 0:
			myRotate(w,lc[3],1,0,0)
		if lc[4] != 0:
			myRotate(w,lc[4],0,1,0)
		if lc[5] != 0:
			myRotate(w,lc[5],0,0,1)

		lumber = Translate([lumber],w[0],w[1],w[2])

##		ao.x_angle = ao.x_angle -90 #+ v[3]
##		ao.y_angle = ao.y_angle #homing + v[4]
##		ao.z_angle = ao.z_angle #+ v[5]
##
##		if ao.x_angle != 0:
##			lumber = Rotate([lumber],ao.x_angle,1,0,0) # inclination
##
##		if ao.y_angle != 0:
##			lumber = Rotate([lumber],ao.y_angle,0,1,0) # rotation
##
##		if ao.z_angle != 0:
##			lumber = Rotate([lumber],ao.z_angle,0,0,1) # rotation


##
##		if lc[3] != 0:
##			lumber = Rotate([lumber],lc[3],1,0,0) # inclination
##
##		if lc[4] != 0:
##			lumber = Rotate([lumber],lc[4],0,1,0) # rotation
##
##		if lc[5] != 0:
##			lumber = Rotate([lumber],lc[5],0,0,1) # rotation

##		lumber = Translate([lumber],self.offset_x*0.03937,self.offset_y*0.03937,self.offset_z*0.03937) # position relative to the scene
##
##		if self.z_angle != 0:
##			lumber = Rotate([lumber],self.z_angle,0,0,1) # rotation
##
##		if self.x_inclination != 0:
##			lumber = Rotate([lumber],self.x_inclination,1,0,0) # inclination



		# move object relative to robot
##		ao.x = (+self.offset_x)*0.03937 - lc[0]
##		ao.y = (+self.offset_y)*0.03937 - lc[1]
##		ao.z = (+self.offset_z)*0.03937 - lc[2]
##
		lumber = Translate([lumber],lc[0]-self.position_x*0.03937,lc[1]-self.position_y*0.03937,lc[2]-self.position_z*0.03937) # position relative to the scene

		#ao.x = ao.x+(self.position_x+self.offset_x)*0.03937+w[0];
		#ao.y = ao.y+(self.position_y+self.offset_y)*0.03937+w[1];
		#ao.z = ao.z+(self.position_z+self.offset_z)*0.03937+w[2];

		ao.x = ao.x+lc[0]+w[0];
		ao.y = ao.y+lc[1]+w[1]+6.88975;
		ao.z = ao.z+lc[2]+w[2]+3.838575;

		za = self.z_angle+90
		if za != 0:
			lumber = Rotate([lumber],za,0,0,1) # rotation


		ao.model = lumber

		self.attached_object = ao

		vm.attach2object(self.model3d,lumber)
		return

	def detach_object(self,vm):
		if self.attached_object == None:
			return
		vm.detach2object(self.attached_object.model)
		self.attached_object = None
		return

	def attach_stored_parts(self,vis):
		for p in self.stored_parts:
			vis.addmodel(p.model)

	def get_next_lumber(self):
		self.current_element_col = self.current_element_col - 1
		if self.current_element_col<0:
			self.current_element_col = self.element_per_width-1
			self.current_element_row = self.current_element_row-1
		if self.current_element_row<0:
			return None
		if len(self.stored_parts)==0:
			return None
		l = self.stored_parts[len(self.stored_parts)-1]
		return l

	def detach_lumber(self,sl,vm):
		for lu in self.stored_parts:
			if lu == sl:
				vm.removemodel(sl.model)
				del lu
				return
		return


	def get_corner_coords(self):

		v = [0.0,0.0,0.0]

		fo = self.flow_origin

		if fo == 0:	v = [self.offset_x,self.offset_y,self.offset_z]
		elif fo == 1:	v = [self.offset_x+self.working_length-lumber_length,self.offset_y,self.offset_z]
		elif fo == 2:	v = [self.offset_x,self.offset_y+self.working_width,self.offset_z]
		elif fo == 3:	v = [self.offset_x+self.working_length-lumber_length,self.offset_y+self.working_width,self.offset_z]

		if self.z_angle != 0:
			tereoProcess.myRotate(v,self.z_angle,0,0,1)

		if self.x_inclination != 0:
			tereoProcess.myRotate(v,self.x_inclination,1,0,0)

		v[0] = v[0] + self.position_x
		v[1] = v[1] + self.position_y
		v[2] = v[2] + self.position_z

		v[0] = v[0] * 0.03937
		v[1] = v[1] * 0.03937
		v[2] = v[2] * 0.03937


		print "corner coords: x:%f"%(v[0])+ " y:%f"%(v[1])+" z:%f"%(v[2])

		return v


	def get_from_file(self):

		print "get flow from file"

		try:
			with open(self.homedir + '/' + self.group + '/' + self.model + '/flow.ini', 'r') as f:
				toolfile = f.readlines()
				for line in toolfile:
					line = line.rstrip('\r')
					line = line.rstrip('\n')
					line = line.rstrip('\r')
					print line
					word_list = line.split(' ',1)
					#print word_list
					word = word_list[0]

					if len(word_list) > 1:
						arg = word_list[1]
						arg = arg.strip(' \t=')
					else:
						arg = ''
					print "arg :-"+arg+"-"
					if word == 'UNITY':
						self.unity = arg
					elif word == 'BOUNDING_LENGTH':
						self.bounding_length = float(arg)
					elif word == 'BOUNDING_HEIGHT':
						self.bounding_height = float(arg)
					elif word == 'BOUNDING_WIDTH':
						self.bounding_width = float(arg)
					elif word == 'WORKING_LENGTH':
						self.working_length = float(arg)
					elif word == 'WORKING_WIDTH':
						self.working_width = float(arg)
					elif word == 'FLOW_DIRECTION_X':
						self.flow_direction_x = float(arg)
					elif word == 'FLOW_DIRECTION_Y':
						self.flow_direction_y = float(arg)
					elif word == 'FLOW_DIRECTION_Z':
						self.flow_direction_z = float(arg)
					elif word == 'BIDIRECTIONNAL':
						self.bidirectional = arg
					elif word == 'OFFSET_X':
						self.offset_x = float(arg)
					elif word == 'OFFSET_Y':
						self.offset_y = float(arg)
					elif word == 'OFFSET_Z':
						self.offset_z = float(arg)

				#if self.unity == "mm":
				#	self.bounding_length = self.bounding_length * 0.03937
				#	self.bounding_width = self.bounding_width * 0.03937
				#	self.bounding_height = self.bounding_height * 0.03937

			return;
		except (UnicodeError, LookupError, IOError):
			print "coult not decode file"
		pass

		return

	def get_collection(self):
		if self.model3d == None:
			self.model3d = AsciiOBJ(self.homedir + '/' + self.group + '/' + self.model + '/flow.obj')
		return self.model3d

		v1 = [0.0,0.0,0.0]
		v2 = [1000.0,0.0,0.0]
		v3 = [0.0,1000.0,0.0]

		v1 = matrix_mult_vector(self.matrix,v1)
		v2 = matrix_mult_vector(self.matrix,v2)
		v3 = matrix_mult_vector(self.matrix,v3)

		v1[0] *= 0.03937
		v1[1] *= 0.03937
		v1[2] *= 0.03937
		v2[0] *= 0.03937
		v2[1] *= 0.03937
		v2[2] *= 0.03937
		v3[0] *= 0.03937
		v3[1] *= 0.03937
		v3[2] *= 0.03937

		col = Collection([Line(v1[0],v1[1],v1[2],v2[0],v2[1],v2[2]),  Line(v1[0],v1[1],v1[2],v3[0],v3[1],v3[2])])

		return col

	def frame_to_matrix(self,f):
		m = tereoMath.matrix44()

		c3 = cos(math.radians(f.a))
		S3 = sin(math.radians(f.a))
		c4 = cos(math.radians(f.b))
		s4 = sin(math.radians(f.b))
		c5 = cos(math.radians(f.c))
		s5 = cos(math.radians(f.c))


		m11 = c3*c4
		m12 = s4*s5*c3-c5*s3
		m13 = c3*c5*s4+s5*s3
		m14 = f.x

		m21 = c4*s3
		m22 = c5*c3+s5*s4*s3
		m23 = c5*s4*s3-s5*c3
		m24 = f.y

		m31 = -s4
		m32 = s5*c4
		m33 = c5*c4
		m34 = f.z

		return m


	def matrix_to_frame(self,m):
		f = fram6()
		return f




	def calibrate_3points(self,fo,fx,fy,t):


		mo = frame6_to_matrix(fo)
		mx = frame6_to_matrix(fx)
		my = frame6_to_matrix(fy)

		itm = matrix_invert(t.matrix)

		mulo = matrix_mult_matrix2(mo,itm)
		mulx = matrix_mult_matrix2(mx,itm)
		muly = matrix_mult_matrix2(my,itm)

		fo = matrix_to_frame6(mulo)
		fx = matrix_to_frame6(mulx)
		fy = matrix_to_frame6(muly)


		# o origin
		ox = fo.x
		oy = fo.y
		oz = fo.z
		oa = fo.a
		ob = fo.b
		oc = fo.c

		# x axis
		xx = fx.x - ox
		xy = fx.y - oy
		xz = fx.z - oz
		xa = fx.a
		xb = fx.b
		xc = fx.c

		# y axis
		yx = fy.x - ox
		yy = fy.y - oy
		yz = fy.z - oz
		ya = fy.a
		yb = fy.b
		yc = fy.c


		# a b c compensation
		normx = sqrt(xx*xx+xy*xy+xz*xz)

		if normx == 0:
			print "error div null"
			return

		scalex = 1000 / normx
		self.compensX[0] = (xa - oa) * scalex
		self.compensX[1] = (xb - ob) * scalex
		self.compensX[2] = (xc - oc) * scalex


		normy = sqrt(yx*yx+yy*yy+yz*yz)

		if normy == 0:
			print "error div null"
			return

		scaley = 1000 / normy
		self.compensY[0] = (ya - oa) * scaley
		self.compensY[1] = (yb - ob) * scaley
		self.compensY[2] = (yc - oc) * scaley

		self.compensO[0] = oa
		self.compensO[1] = ob
		self.compensO[2] = oc



		# normalize

		xx = xx / normx
		xy = xy / normx
		xz = xz / normx

		yx = yx / normy
		yy = yy / normy
		yz = yz / normy


		# z cross product
		zx = xy * yz - xz * yy
		zy = xz * yx - xx * yz
		zz = xx * yy - xy * yx


		self.matrix = tereoMath.matrix44( xx, xy, xz, 0.0,
						yx, yy, yz, 0.0,
						zx, zy, zz, 0.0,
						ox, oy, oz, 1.0 )

		print "calibration matrix"
		print self.matrix


		return

