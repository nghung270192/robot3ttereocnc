# -*- coding: utf-8 -*-


import os.path


pluginName = "Robot info"
version = "0.1"
author = "Olivier Martinez"



def register(parent):

	mod = imp.load_source('robot_info', './plugins/robot_info/robot_info.py')
	parent.add_function("robot.info","Robot info",mod.on_robotinfo_activate)
	return
