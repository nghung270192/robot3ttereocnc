var tpCommand;

var tpCommand = function tpCommand() {

	this.pluginName = "Command";
	this.pluginVersion = "0.1";
	this.pluginAuthor = "Olivier Martinez";
	this.uid = (((1+Math.random())*0x100000)|0).toString(16).substring(1);
	
};

tpCommand.prototype = {

	"register":function(parent) {

		//mod = imp.load_source('tereocnc.plugins.command', './plugins/command/command.py')

		//------ add icons


		//estop
		/*img = gtk.Image()
		img.set_from_file('./plugins/command/estop.png')
		estop_icon = gtk.ToggleToolButton ("E-Stop")
		img.show()
		estop_icon.set_icon_widget(img)
		estop_icon.set_label("E-Stop")
		estop_icon.connect("toggled", mod.on_estop_toggled, parent)
		estop_icon.set_active(True)*/
		
		
		
		var msg ={};
		msg['msg'] = "Command";
		tereoMain.add_icon('E-Stop','plugins/tpCommand/start.png', true, "@tpCommand&on_estop_toggle",msg,'tpCommand');
		$("#tpCommandE-Stop").addClass("active");
		
		//start
		/*img = gtk.Image()
		img.set_from_file('./plugins/command/start.png')
		img.show()
		start_icon = gtk.ToggleToolButton ("I/O")
		start_icon.set_icon_widget(img)
		start_icon.set_label("I/O")
		start_icon.connect("toggled", mod.on_start_toggled, None)
		start_icon.set_active(False)*/
		
		var msg ={};
		msg['msg'] = "Command";
		tereoMain.add_icon('Power','plugins/tpCommand/start.png', true, "@tpCommand&on_power_toggle",msg,'tpCommand');

		tereoMain.add_trigger("broadcast_state", tpCommand_update_state);
		
		

	},
	
	
	"on_power_toggle":function() {
	
		console.log("on power toggle");
		
		var f = $("#robot_panel");
		if(f.length){
		
			//L'élément existe
			if ($("#tpCommandPower").hasClass("active")) {
					
				$("#tpCommandPower").removeClass("active");
				var msg ={};
				msg['msg'] = "POW_OFF"; 
				msg['uid'] = this.uid+"poff";
				tereoMain.call("sendMqttMsg",JSON.stringify(msg));
				
			} else {
				
				$("#tpCommandPower").addClass("active");
				var msg ={};
				msg['msg'] = "POW_ON"; 
				msg['uid'] = this.uid+"pon";
				tereoMain.call("sendMqttMsg",JSON.stringify(msg));
			}
		}
	},

	"on_estop_toggle":function() {
	
		console.log("on estop toggle");
		
		var f = $("#robot_panel");
		if(f.length){
		
			//L'élément existe
			if ($("#tpCommandE-Stop").hasClass("active")) {

				$("#tpCommandE-Stop").removeClass("active");
				var msg ={};
				msg['msg'] = "E-STOP-RESET"; 
				msg['uid'] = this.uid+"esr";
				tereoMain.call("sendMqttMsg",JSON.stringify(msg));
				
			} else {
				
				$("#tpCommandE-Stop").addClass("active");
				var msg ={};
				msg['msg'] = "E-STOP"; 
				msg['uid'] = this.uid+"es";
				tereoMain.call("sendMqttMsg",JSON.stringify(msg));
				
			}
		}
	},	

	"unload":function(parent) {
	
	}

};


tereoPlugins['tpCommand'] = new tpCommand();



function tpCommand_update_state(arg) {
	
	//RCS_DONE
	
	// RCS_EXEC

	// RCS_ERROR. 
	
	
}