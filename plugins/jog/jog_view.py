# -*- coding: utf-8 -*-


def on_jog_up(widget,parent):

	#print "jog up"
	#c.mode(linuxcnc.MODE_MANUAL)
	#c.wait_complete()
	m = self.builder.get_object("menu2")
	idx = m.get_active()
	if not idx: return
	l = idx.get_label()


	#speed = 10 # todo: traj default velocity
	#a = 1
	#self.broadcast("command jog %d"%(self.LINUXCNC_JOG_INCREMENT)+" %d"%(a)+" %d"%(speed)+" 5")

	if self.env == None:
		return
	for r in self.env.robot:
		#r.setspeed(10)

		if r.client != None:
			if r.client.instance != None:
				# translation
				#a = "XYZABCUVW".index(l)
				lc = r.client.instance.position
				if r.proto == "cross":
					lc[l] = lc[l] + 100
				else:
					lc[l] = lc[l] + 10
				r.moveto(lc['X'],lc['Y'],lc['Z'],lc['A'],lc['B'],lc['C'])

				# rotation
				#lc = r.client.instance.joint_position
				#lc[l] = lc[l] + 10
				#r.rotateto(lc[0],lc[1],lc[2],lc[3],lc[4],lc[5])

	return

def on_jog_down(widget,parent):
##		c.mode(linuxcnc.MODE_MANUAL)
##		c.wait_complete()
	m = self.builder.get_object("menu2")
	idx = m.get_active()
	if not idx: return
	l = idx.get_label()

	#speed = 10 # todo: traj default velocity
	#self.broadcast("command jog %d"%(self.LINUXCNC_JOG_INCREMENT)+" %d"%(a)+" %d"%(speed)+" 5")

	if self.env == None:
		return
	for r in self.env.robot:
		#r.setspeed(10)

		if r.client != None:
			if r.client.instance != None:
				# translation
				#a = "XYZABCUVW".index(l)
				lc = r.client.instance.position
				if r.proto == "cross":
					lc[l] = lc[l] - 100
				else:
					lc[l] = lc[l] - 10
				r.moveto(lc['X'],lc['Y'],lc['Z'],lc['A'],lc['B'],lc['C'])

				# rotation
				#lc = r.client.instance.joint_position
				#lc[l] = lc[l] + 10
				#r.rotateto(lc[0],lc[1],lc[2],lc[3],lc[4],lc[5])


	return




def on_winjogclose_clicked(self,widget,data=None):
	wj = self.builder.get_object("winJog")
	wj.hide()
	return

def on_buttonj1_clicked(self, widget, data=None):

	xe = self.builder.get_object("entryj1")
	x = float(xe.get_text())
	ye = self.builder.get_object("entryj2")
	y = float(ye.get_text())
	ze = self.builder.get_object("entryj3")
	z = float(ze.get_text())
	ae = self.builder.get_object("entryj4")
	a = float(ae.get_text())
	be = self.builder.get_object("entryj5")
	b = float(be.get_text())
	ce = self.builder.get_object("entryj6")
	c = float(ce.get_text())
	r = self.env.robot[0]
	r.selected_tool = self.env.tools[0]
	r.moveto(x,y,z,a,b,c,-1)
	r.end_process()
	r.client.instance.waiting_job = True


	return


