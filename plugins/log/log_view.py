import pygtk
pygtk.require("2.0")
import gtk



class log_window(gtk.Window):


	def __init__(self,tereo):

		gtk.Window.__init__(self)
		self.tereo = tereo

		self.sw = log_content(tereo)
		self.add(self.sw)
		self.sw.show()

		self.connect("delete-event", self.on_log_window_destroy)
		self.show()

		return

	def on_log_window_destroy(self,widget,event):

		print "delete event"
		self.hide()
		return True
		return

	def new_log(self,tmp,ico,sec,desc):

		if self.sw != None:
			self.sw.new_log(tmp,ico,sec,desc)

		return

	def update(self):

		if self.sw != None:
			self.sw.update()

		return





class log_view(gtk.Frame):


	def __init__(self,tereo):

		gtk.Frame.__init__(self)

		self.tereo = tereo

		self.sw = log_content(tereo)
		self.add(self.sw)
		self.sw.show()

		self.show()

		return


	def attach_frame(self):
		self.update()
		return


	def new_log(self,tmp,ico,sec,desc):

		if self.sw != None:
			self.sw.new_log(tmp,ico,sec,desc)

		return


	def update(self):

		if self.sw != None:
			self.sw.update()

		return


class log_content(gtk.ScrolledWindow):

	def __init__(self,tereo):

		self.tereo = tereo

		gtk.ScrolledWindow.__init__(self)


		self.lvs = gtk.ListStore(str, str, str, str) #gtk.gdk.Pixbuf

		tv = gtk.TreeView(self.lvs)
		tv.set_headers_visible(True)

		col1 = gtk.TreeViewColumn('')
		col2 = gtk.TreeViewColumn('')
		col3 = gtk.TreeViewColumn('Section')
		col4 = gtk.TreeViewColumn('Description')

		tv.append_column(col1)
		tv.append_column(col2)
		tv.append_column(col3)
		tv.append_column(col4)

		temp = gtk.CellRendererText()
		ico = gtk.CellRendererPixbuf()
		sec = gtk.CellRendererText()
		desc = gtk.CellRendererText()

		col1.pack_start(temp, True)
		col2.pack_start(ico, True)
		col3.pack_start(sec, True)
		col4.pack_start(desc, True)

		col1.add_attribute(temp, 'text', 0)
		col2.add_attribute(ico, 'stock_id', 1)
		col3.add_attribute(sec, 'text', 2)
		col4.add_attribute(desc, 'text', 3)

		self.add(tv)
		tv.show()
		self.tv = tv

		self.tereo.add_object('log.listener','Log win',self.new_log)

		self.show()

		self.update()


	def new_log(self,tmp,ico,sec,desc):

		if ico == 0:
			icol = 'gtk-dialog-info'
		elif ico == 1:
			icol = 'gtk-dialog-warning'
		else :
			icol = 'gtk-dialog-error'

		self.lvs.append((tmp,icol,sec,desc))

		return



	def update(self):

		self.lvs.clear()

		for msg in self.tereo.app_log:
			self.new_log(msg[0],msg[1],msg[2],msg[3])

		return

