# -*- coding: utf-8 -*-


import os.path


pluginName = "Simulation controller"
version = "0.1"
author = "Olivier Martinez"


def register(parent):

	mod = imp.load_source('simulation_controller', './plugins/simulation_controller/simulation_controller.py')
	parent.add_object('controller','Simulation',mod.new_simulation_controller)

	return
