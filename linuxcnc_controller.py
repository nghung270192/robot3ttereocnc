# -*- coding: utf-8 -*-


import os.path
import tereoController
from twisted.protocols.basic import LineReceiver
from twisted.internet.protocol import ReconnectingClientFactory

class linuxcnc_controller(tereoController.controller):

	def __init__(self,tereo):

		print "linux controller init"
		tereoController.controller.__init__(self,tereo)

		self.instance = self
		self.tereo = tereo
		self.LINUXCNC_STATE_ESTOP = 1
		self.LINUXCNC_STATE_ON = 4
		self.LINUXCNC_STATE_OFF = 3
		self.LINUXCNC_STATE_ESTOP_RESET = 2

		self.LINUXCNC_JOG_STOP = 0
		self.LINUXCNC_JOG_CONTINUOUS = 1
		self.LINUXCNC_JOG_INCREMENT = 2


		self.client = tereoClient(self);
		self.client.buidProtocol("127.0.0.1")

		#comm = []
		#self.comm_stack = collections.deque(comm)
		#self.waiting_response = False
		#self.last_comm = ""
		#self.last_response = ""
		#self.last_cnc_error = ""
		#self.position = {'X':0, 'Y':0, 'Z':0, 'A':0, 'B':0, 'C':0, 'U':0, 'V':0, 'W':0}
		#self.joint_position = {'X':0, 'Y':0, 'Z':0, 'A':0, 'B':0, 'C':0, 'U':0, 'V':0, 'W':0}
		#self.waiting_job = True
		#self.speed = 5
		#self.position_updated = True

		return

	def set_estop(self,es):
		print "estop"
		if es == True:
			self.tereo.applog("Linuxcnc",2,"Emergency stop!")
			self.send("command state %d"%(self.LINUXCNC_STATE_ESTOP))
		else:
			self.tereo.applog("Linuxcnc",1,"Emergency stop reset")
			self.send("command state %d"%(self.LINUXCNC_STATE_ESTOP_RESET))
		tereoController.controller.estop(self,es)
		return

	def stop_motion(self):
		#send to robot
		tereoController.controller.stop_motion(self)
		return

	def power(self,p):
		if p == True:
			self.tereo.applog("Linuxcnc",0,"Powering up")
			self.send("command state %d"%(self.LINUXCNC_STATE_ON))
		else:
			self.tereo.applog("Linuxcnc",0,"Powering down")
	        self.send("command state %d"%(self.LINUXCNC_STATE_OFF))
		tereoController.controller.power(self,p)
		return

	def set_new_path(self,points):
		#todo : if current path not complete : stop and start new
		#todo : if power off  : power on
		tereoController.controller.set_new_path(self)
		return


	def get_current_position(self):
		#send request to robot
		tereoController.controller.get_current_position(self)
		self.send("stat joint_position")
		return


	def get_target_position(self):
		#send request to robot
		tereoController.controller.get_target_position(self)
		return

	def get_state_error(self):
		#send request to robot
		tereoController.controller.get_state_error(self)
		return

	def get_state_mode(self):
		#send request to robot
		tereoController.controller.get_state_mode(self)
		return

	def get_state_estop(self):
		#send request to robot
		tereoController.controller.get_state_estop(self)
		return

	def get_state_power(self):
		#send request to robot
		tereoController.controller.get_state_power(self)
		return


	#---------- linuxcnc ------------------------

	def send(self,com):
		self.position_updated = True
		return

	def new_network_error(self,e):
		self.network_error = e
		tereoController.controller.update_network_status()
		return

	def connection_made(self):
		print "connection made"
		self.network_error = ""
		self.network_connected = True
		self.network_connecting = False
		tereoController.controller.update_network_status()
		return




#-------------- client linuxcnc --------------------


class tereoProtocol(LineReceiver):

	def __init__(self,ctrl):
		self.ctrl = ctrl
		self.connected = False
		self.connecting = False
		self.lasterror = ""
		comm = []
		self.comm_stack = collections.deque(comm)
		self.waiting_response = False
		self.last_comm = ""
		self.last_response = ""
		self.last_cnc_error = ""
		self.position = {'X':0, 'Y':0, 'Z':0, 'A':0, 'B':0, 'C':0, 'U':0, 'V':0, 'W':0}
		self.joint_position = {'X':0, 'Y':0, 'Z':0, 'A':0, 'B':0, 'C':0, 'U':0, 'V':0, 'W':0}
		self.waiting_job = False
		self.position_updated = True
		self.speed = 0
		self.calculateur = False
		self.ask_result = False
		self.calculateur_function = None

	def send(self,com):
		if (com == "stat position") or (com == "stat joint_position"):
			self.sendLine(com)
			return

		if self.waiting_response == True:
			self.comm_stack.append(com)
		else:
			self.ask_result = True
			#print "ask result true"
			self.sendLine(com)
			self.last_comm = com

	def lineReceived(self, line):
		#print "line received : " + line

		if line == "nextjob":

			#print "next job from lcnc"
			if self.calculateur == True:
				if self.ask_result == True:
					#print "send stat position"
					self.ask_result = False
					self.send("stat joint_position")
			else:
				#print "line recevied wj"
				self.waiting_job = True
			return

		line.strip(' []')

		word_list2 = line.split(' ')
		if word_list2[0] == "error":
			t = line.split(' ',1)
			self.last_cnc_error = t[1]
			return


		#print word_list2
		if len(word_list2) < 2:
			self.last_cnc_error = "unknown response:"+line
			return

		t = word_list2[1].split(':')
		word_list2[1] = t[0]
		if len(t)>1:
		 	word_list2.insert(2,t[1])

		#print word_list2

		if (word_list2[0] == "stat") and (word_list2[1] == 'position'):
			#print word_list2
			self.position['X'] = float(word_list2[2])
			self.position['Y'] = float(word_list2[3])
			self.position['Z'] = float(word_list2[4])
			self.position['A'] = float(word_list2[5])
			self.position['B'] = float(word_list2[6])
			self.position['C'] = float(word_list2[7])
			self.position['U'] = float(word_list2[8])
			self.position['V'] = float(word_list2[9])
			self.position['W'] = float(word_list2[10])
			self.position_updated = True
			return

		if (word_list2[0] == "stat") and (word_list2[1] == 'joint_position'):
			#print "joint pos"
			#print word_list2
			self.joint_position['X'] = float(word_list2[2])
			self.joint_position['Y'] = float(word_list2[3])
			self.joint_position['Z'] = float(word_list2[4])
			self.joint_position['A'] = float(word_list2[5])
			self.joint_position['B'] = float(word_list2[6])
			self.joint_position['C'] = float(word_list2[7])
			self.joint_position['U'] = float(word_list2[8])
			self.joint_position['V'] = float(word_list2[9])
			self.joint_position['W'] = float(word_list2[10])
			self.position_updated = True
			if self.calculateur == True:
				if self.calculateur_function != None:
					self.calculateur_function()
					#print "waiting job after result"
					self.waiting_job = True
			return

		if self.last_comm == "":
			self.last_cnc_error = "unexpected response : " + line
		else:

			word_list1 = self.last_comm.split(' ')
			#print word_list1
			t = word_list1[1].split(':')
			word_list1[1] = t[0]
 			if len(t)>1:
			 	word_list2.insert(2,t[1])
 			#print word_list1

			if (word_list1[0] == word_list2[0]) and (word_list2[1] == word_list1[1]):
				#print "comm ok"
				self.last_response = line
				if len(self.comm_stack) > 0:
					self.last_comm = self.comm_stack.popleft()
					self.sendLine(self.last_comm)


	def connectionMade(self):
		self.ctrl.connection_made()
		return

	def connectionLost(self, reason):
		self.ctrl.new_network_error("Lost connection.  Reason:" + str(reason))
		return



#----------- basic network client --------------



class tereoClient(ReconnectingClientFactory):
	protocol = tereoProtocol

	def __init__(self, ctrl):
		self.ctrl = ctrl
		self.instance = None

	def buildProtocol(self, addr):
		print "buildprotocol2"
		self.instance = self.protocol(self.ctrl)
		self.resetDelay()
		return self.instance

	def clientConnectionLost(self, connector, reason):
		self.ctrl.new_network_error("Lost connection.  Reason:" + str(reason))
		ReconnectingClientFactory.clientConnectionLost(self, connector, reason)

	def clientConnectionFailed(self, connector, reason):
		self.ctrl.new_network_error("Connection failed. Reason:" + str(reason))
		ReconnectingClientFactory.clientConnectionFailed(self, connector, reason)












def new_linuxcnc_controller(tereo):

	return linuxcnc_controller(tereo)












