import sys
import math

#import direct.directbase.DirectStart
from pandac.PandaModules import GeomVertexFormat, GeomVertexData, GeomVertexWriter, GeomTriangles, Geom, GeomNode, NodePath, GeomPoints
from pandac.PandaModules import LineSegs
from panda3d.core import Vec3,Vec4,Mat4


class box:
	def __init__(self,x1=-1.0,y1=-1.0,z1=-1.0,x2=1.0,y2=1.0,z2=1.0):
        # self.smooth = True/False
        # self.uv = True/False or Spherical/Box/...
        # self.color = Method1/Method2/...
        # self.subdivide = 0/1/2/...
		self.x1 = x1
		self.y1 = y1
		self.z1 = z1
		self.x2 = x2
		self.y2 = y2
		self.z2 = z2

	def myNormalize(self,myVec):
		myVec.normalize()
		return myVec

	def generate(self):
 		#format = GeomVertexFormat.getV3()
		format = GeomVertexFormat.getV3n3t2()
		data = GeomVertexData("Data", format, Geom.UHStatic)
 		vertices = GeomVertexWriter(data, "vertex")
		normal = GeomVertexWriter(data, 'normal')

		vertices.addData3f(self.x1, self.y1, self.z1)
 		vertices.addData3f(self.x2, self.y1, self.z1)
		vertices.addData3f(self.x1, self.y2, self.z1)
 		vertices.addData3f(self.x2, self.y2, self.z1)
 		vertices.addData3f(self.x1, self.y1, self.z2)
 		vertices.addData3f(self.x2, self.y1, self.z2)
		vertices.addData3f(self.x1, self.y2, self.z2)
 		vertices.addData3f(self.x2, self.y2, self.z2)

		normal.addData3f(self.myNormalize(Vec3(2*self.x1-1, 2*self.y1-1, 2*self.z1-1)))
		normal.addData3f(self.myNormalize(Vec3(2*self.x2-1, 2*self.y1-1, 2*self.z1-1)))
		normal.addData3f(self.myNormalize(Vec3(2*self.x2-1, 2*self.y2-1, 2*self.z2-1)))
		normal.addData3f(self.myNormalize(Vec3(2*self.x1-1, 2*self.y2-1, 2*self.z2-1)))

		triangles = GeomTriangles(Geom.UHStatic)

		def addQuad(v0, v1, v2, v3):
 			triangles.addVertices(v0, v1, v2)
			triangles.addVertices(v0, v2, v3)
			triangles.closePrimitive()

		addQuad(4, 5, 7, 6) # Z+
		addQuad(0, 2, 3, 1) # Z-
		addQuad(3, 7, 5, 1) # X+
		addQuad(4, 6, 2, 0) # X-
 		addQuad(2, 6, 7, 3) # Y+
		addQuad(0, 1, 5, 4) # Y+

 		geom = Geom(data)
		geom.addPrimitive(triangles)

		node = GeomNode("box")
		node.addGeom(geom)

		return NodePath(node)


class wireframeBox:
	def __init__(self,x1=-1.0,y1=-1.0,z1=-1.0,x2=1.0,y2=1.0,z2=1.0):

		self.x1 = x1
		self.y1 = y1
		self.z1 = z1
		self.x2 = x2
		self.y2 = y2
		self.z2 = z2


	def generate(self):

		g = LineSegs()
		g.setThickness(1)

		g.moveTo(self.x1, self.y1, self.z1)
		g.drawTo(self.x1, self.y2, self.z1)
		g.drawTo(self.x2, self.y2, self.z1)
		g.drawTo(self.x2, self.y1, self.z1)
		g.drawTo(self.x1, self.y1, self.z1)

		g.moveTo(self.x1, self.y1, self.z2)
		g.drawTo(self.x1, self.y2, self.z2)
		g.drawTo(self.x2, self.y2, self.z2)
		g.drawTo(self.x2, self.y1, self.z2)
		g.drawTo(self.x1, self.y1, self.z2)

		g.moveTo(self.x1, self.y1, self.z1)
		g.drawTo(self.x1, self.y1, self.z2)

		g.moveTo(self.x1, self.y2, self.z1)
		g.drawTo(self.x1, self.y2, self.z2)

		g.moveTo(self.x2, self.y2, self.z1)
		g.drawTo(self.x2, self.y2, self.z2)

		g.moveTo(self.x2, self.y1, self.z1)
		g.drawTo(self.x2, self.y1, self.z2)

		g = g.create()
		return NodePath(g)



def pandaMatrix(t):
	zx = t.xy * t.yz - t.xz * t.yy
	zy = t.xz * t.yx - t.xx * t.yz
	zz = t.xx * t.yy - t.xy * t.yx
	rot = Mat4( t.xx, t.xy, t.xz, 0.0,
			t.yx, t.yy, t.yz, 0.0,
			zx, zy, zz, 0.0,
			0.0, 0.0, 0.0, 1.0 )
	return rot #glMultMatrixf(rot)

def gl2pandaMatrix(t):
	rot = Mat4( t[0], t[1], t[2], t[3],
			t[4], t[5], t[6], t[7],
			t[8], t[9], t[10], t[11],
			t[12], t[13], t[14], t[15] )
	return rot




class grid:

	def __init__(self):

		return

	def generate(self):

		g = LineSegs()
		g.setThickness(1)
		g.setColor(0.5, 0.5, 0.5)

		for i in range(-500, 501, 50):
			g.moveTo(-500, i, 0)
			g.drawTo(500, i, 0)
			g.moveTo(i, -500, 0)
			g.drawTo(i, 500, 0)

		g = g.create()
		return NodePath(g)

class triangle:

	def __init__(self, x1,y1,z1, x2,y2,z2, x3,y3,z3, xn,yn,zn):

		self.x1 = x1
		self.y1 = y1
		self.z1 = z1

		self.x2 = x2
		self.y2 = y2
		self.z2 = z2

		self.x3 = x3
		self.y3 = y3
		self.z3 = z3

		self.xn = xn
		self.yn = yn
		self.zn = zn

		return

	def generate(self):

		format = GeomVertexFormat.getV3n3t2()
		data = GeomVertexData("Data", format, Geom.UHStatic)
 		vertices = GeomVertexWriter(data, "vertex")
		normal = GeomVertexWriter(data, 'normal')

		vertices.addData3f(self.x1, self.y1, self.z1)
 		vertices.addData3f(self.x2, self.y2, self.z2)
 		vertices.addData3f(self.x3, self.y3, self.z3)

		normal.addData3f(self.xn, self.yn, self.zn)

		triangles = GeomTriangles(Geom.UHStatic)
		triangles.addVertices(0, 1, 2)
		triangles.closePrimitive()

 		geom = Geom(data)
		geom.addPrimitive(triangles)

		node = GeomNode("triangle")
		node.addGeom(geom)

		return NodePath(node)
