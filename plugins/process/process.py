# -*- coding: utf-8 -*-


import os.path

from plugins.process.process_view import process_view



def on_process_activate(widget,data):

	return


def new_process_frame(parent):

	f = parent.get_object('frame','Process')
	if f == None:
		f = process_view(parent)
		parent.add_object('gui.frame','Process',f)

	return f




def on_scene_go(self,widget):



	#r = self.env.robot[1]
	#r.selected_tool = self.env.tools[0]
	r = self.env.robot[0]


	# osb 1
	#a1 = self.nivel(1100,400)
	#r.moveto(1100,400,20,a1[0],a1[1],a1[2],1)
	#a1 = self.nivel(200,400)
	#r.moveto(200,400,20,a1[0],a1[1],a1[2],1)
	#a1 = self.nivel(100,100)
	#r.moveto(100,100,20,a1[0],a1[1],a1[2],1)
	#a1 = self.nivel(1100,100)
	#r.moveto(1100,100,20,a1[0],a1[1],a1[2],1)
	#r.end_process()
	r.prehension(1319.7, 556, 905, -155.6, 8.6, -179.8,-1)
	r.moveto(1319.7, 556, 1593, -155.6, 8.6, -179.8,-1)
	#r.moveto(-420.5653, 14.35985, -403.1481, 157.7571, -1.939161E-02, 2.092498, 7)
	#r.move_contact(-392.4898, -5.531453, 40.68298, 157.7615, -1.239665E-02, 2.081941,7) # base 7 = chariot
	r.moveto(1319.7, 556, 905, -155.6, 8.6, -179.8,-1)
	r.deprehension(1319.7, 556, 905, -155.6, 8.6, -179.8,-1)
	#r.deprehension(-420.5653, 14.35985, -403.1481, 157.7571, -1.939161E-02, 2.092498, 7)
	r.moveto(1319.7, 556, 1593, -155.6, 8.6, -179.8,-1)
	r.end_process()
	return

	# osb 2
	#r.prehension(653.472800,406.500300,-0.767360,-69.863690, 6.736245, 179.936200,1)
	#r.moveto(r.home[0],r.home[1],r.home[2],r.home[3],r.home[4],r.home[5],-1)
	#r.moveto(779.397000,636.853600,-669.528000,161.517900,-7.309665,-8.515718,7)
	#r.move_contact(855.4782, 568.3027, -36.42215, 159.6048, -4.303246, -0.820904,7) # base 7 = chariot
	#r.deprehension(779.397000,636.853600,-669.528000,161.517900,-7.309665,-8.515718,7)
	#r.moveto(r.home[0],r.home[1],r.home[2],r.home[3],r.home[4],r.home[5],-1)
	#r.end_process()

	# bois 1
	r.prehension(480.3710, 322.8567, 42.0, -65.98022, 9.756203, -172.9584,2)
	r.moveto(r.home[0],r.home[1],r.home[2],r.home[3],r.home[4],r.home[5],-1)
	r.moveto(860.1144, 752.1094, -859.6844, -171.7700, 62.43276, -89.06218,7)
	r.move_contact(598.2409, 46.84233, -236.9905, -160.1076, 67.55907, -72.04584,7) # base 7 = chariot

	# clou 1 et 2
	#rc.nail(66.83891, 34.73627, -23.16257, 57.29891, 21.05503, 175.9257, 7)
	#rc.nail(302.2720, 34.72675, -23.12246, 57.30344, 21.05652, 175.9267, 7)
	#rc.nail(538.9701, 34.72749, -23.12193, 57.30360, 21.05651, 175.9266, 7)
	#rc.nail(740.9342, 34.72921, -23.12259, 57.30366, 21.05642, 175.9268, 7)
	#rc.nail(1058.676, 41.15969, -23.12210, 57.30330, 21.05634, 175.9265, 7)

	# suite bois 1
	#r.deprehension(860.1144, 752.1094, -859.6844, -171.7700, 62.43276, -89.06218,7)
	#r.moveto(r.home[0],r.home[1],r.home[2],r.home[3],r.home[4],r.home[5],-1)
	#r.end_process()

	# bois 2
	#r.prehension(901.8018, 189.9944, 22.0, -65.01130, 8.996653, -171.3438,2)
	#r.moveto(r.home[0],r.home[1],r.home[2],r.home[3],r.home[4],r.home[5],-1)
	#r.moveto(438.6951, 892.2050, -446.0628, -91.43528, 61.02964, -83.41776,7)
	#r.move_contact(-44.55852, 857.0835, -213.9745, -71.02929, 59.98930, -70.67390,7) # base 7 = chariot

	#rc.nail(38.55633, 109.9807, -19.01021, 57.30295, 21.05634, 175.9267, 7)
    	#rc.nail(42.93529, 734.3135, -35.00197, 57.30299, 21.05601, 175.9264, 7)
	#rc.nail(5.542454, 1025.133, -20.19548, 57.30342, 21.05613, 175.9268, 7)
	#rc.nail(-6.482877E-01, 1251.541, -9.562905, 57.30340, 21.05608, 175.9267, 7)
	#rc.nail(183.6658, 1512.035, 15.51419, -69.52541, 18.33765, 165.4858, 7)
	#rc.nail()

	#r.deprehension(438.6951, 892.2050, -446.0628, -91.43528, 61.02964, -83.41776,7)
	#r.moveto(r.home[0],r.home[1],r.home[2],r.home[3],r.home[4],r.home[5],-1)
	#r.end_process()

	# bois 3
	#r.prehension(901.8066, 50.98506, 22.0, -64.98746, 8.927678,  -172.9584,2)
	#r.moveto(r.home[0],r.home[1],r.home[2],r.home[3],r.home[4],r.home[5],-1)
	#r.moveto(964.7714, 898.5931, -179.8952, -69.01865, 63.43784, -70.64475,7)
	#r.move_contact(964.7751, 974.9832, -426.2418, -69.01899, 63.43800, -70.64512,7) # base 7 = chariot
	#r.deprehension(964.7714, 898.5931, -179.8952, -69.01865, 63.43784, -70.64475,7)
	#r.moveto(r.home[0],r.home[1],r.home[2],r.home[3],r.home[4],r.home[5],-1)
	#r.end_process()

	# bois 4
	#r.prehension(493.9761, -95.22495, 14.0, -66.46462, 9.267556, -172.9584,2)
	#r.moveto(r.home[0],r.home[1],r.home[2],r.home[3],r.home[4],r.home[5],-1)
	#r.moveto(860.1144, 752.1094, -859.6844, -171.7700, 62.43276, -89.06218,7)
	#{ X 569.7996, Y 1463.403, Z -475.7332, A 7.323934, B 64.21149, C -83.26756, S 2, T 10, E1 0.0, E2 0.0, E3 0.0, E4 0.0, E5 0.0, E6 0.0}
	#r.move_contact(569.8033, 1725.192, -172.1793, 7.324267, 64.21193, -83.26698,7) # base 7 = chariot

	#r.deprehension({ X 569.7996, Y 1463.403, Z -475.7332, A 7.323934, B 64.21149, C -83.26756, S 2, T 10, E1 0.0, E2 0.0, E3 0.0, E4 0.0, E5 0.0, E6 0.0},7)
	#r.moveto(r.home[0],r.home[1],r.home[2],r.home[3],r.home[4],r.home[5],-1)
	#r.end_process()




	return

	self.applog("task",0,"task launched")

	# inch
	##self.broadcast_gcode("G20")

	# absolute coordinate
	##self.broadcast_gcode("G90")

	# tool 1
	#self.broadcast_gcode("T#1M6")
	#self.broadcast_gcode("M8")
	# speed
	#self.send_gcode("S#1600M3")

	### prehension robot ###

	self.env.unpacking_flow = 3
	self.env.storing_flow = 1
	self.env.assembling_flow = 2
	self.env.woodworking_flow = 0
	self.grab_robot = 1
	self.tool_robot = 0
	self.env.robot[0].number=0
	self.env.robot[1].number=1

	#for r in self.env.robot:
	#	r.moveto(16.16749, -1294.855, 2269.408, 69.48716, -79.57098, 22.24890);

	i = 0
	#for r in self.env.robot:
		#if i == 0:
			# table position
			# X=0 Y=1500 Z=-1910
			#########r.send_gcode("G00X58Z20")
#				r.send_gcode("G00X59.055Y0")
#				r.send_gcode("G00X51")
			# X=-200 Y=1300 Z=-900
#				r.send_gcode("G0X40Y40Z-43.8582")
#				r.send_gcode("G0X20Y45Z-43.8582")
#				r.send_gcode("G0X7.874Y51.181Z-43.8582")
##			elif i == 1:
##				# table position
##				# X=0 Y=1500 Z=-1910
##				r.send_gcode("G0Z-75.1967")
##				r.send_gcode("G0X59.055Y0")
##				r.send_gcode("G0X51")
##				# X=-200 Y=1300 Z=-900
##				r.send_gcode("G0X40Y40Z-43.8582")
##				r.send_gcode("G0X20Y45Z-43.8582")
##				r.send_gcode("G0X7.874Y51.181Z-43.8582")
		#i = i + 1

	# osb stack



	# lumber stack

	i = 0
##		for f in self.env.flow:
##			if i == 1:
##				f.element_per_width = 1
##				f.element_per_height = 5
##				f.current_element_row = 1
##				f.current_element_col = 5
##				f.row_space = 0.0
##				f.col_space = 0.0
##				f.element_width = 1250.0
##				f.element_height = 13.0
##				f.element_length = 2500.0
##				f.fill_stored_parts()
##				f.attach_stored_parts(self.scenevismachframe)
##
##			elif i == 3:
##				f.element_per_width = 15
##				f.element_per_height = 4
##				f.current_element_row = 15
##				f.current_element_col = 4
##				f.row_space = 0.0
##				f.col_space = 2.0
##				f.element_width = 45.0
##				f.element_height = 145.0
##				f.element_length = 4000.0
##				f.fill_stored_parts()
##				f.attach_stored_parts(self.scenevismachframe)
##
##			i = i + 1


	self.do_work = True

	self.next_job()
	i = 0
	for r in self.env.robot:
		#if i == 0:
			#r.moveto(1247.0000,-10.0,-2055.000, 90.000000, 90.000000, 0.000000)
			#r.moveto(880.0000,-880.0,-2055.000, 90.000000, 90.000000, 0.000000)
			#r.moveto(10.0000,-1247.0,-2055.000, 90.000000, 90.000000, 0.000000)
			#13.67  -1019  1769   -55  -83  147
			#r.moveto(313.67,-1019.0,-1769.000, 90.000000, 90.000000, 0.000000)

			#r.setspeed(15000)
			#r.moveto(10.0000,-1247.397000,2255.000, 90.000000, 0.000000, 0.000000)
			#r.moveto(13.67,-1019,1769, 90.000000, 0.000000, 0.000000)
			#r.moveto(13.67,-1019,1469, 90.000000, 0.000000, 0.000000)
		#if i == 1:
		#	r.moveto(0.0,0.0,0.0, 90.000000, 0.000000, 0.000000)
		i = i + 1

	return


def on_task_proceed(self,widget,data=None):
	rw = self.builder.get_object("reviewWindow")
	rw.hide()

	self.detach_toolbar()

	fcw = self.builder.get_object("sceneWindow")
	self.toolbarhost = "vbox4"
	vb4 =  self.builder.get_object("vbox4")
	vb4.pack_end(self.toolbar,False,True,0)

	for w in range(self.toolbar.get_n_items()):
		self.toolbar.get_nth_item(w).hide() #set_visible(False)

	self.builder.get_object(self.TB_IO).show()
	self.builder.get_object(self.TB_ESTOP).show()
	self.builder.get_object(self.TB_QUIT).show()
	self.builder.get_object(self.TB_DRAWING).show()
	self.builder.get_object(self.TB_TASK).show()
	self.builder.get_object(self.TB_LOG).show()
	self.builder.get_object(self.TB_NET).show()
	self.builder.get_object(self.TB_ROBOTS).show()
	self.builder.get_object(self.TB_FLOWS).show()
	self.builder.get_object(self.TB_TOOLS).show()
	self.builder.get_object(self.TB_RUN).show()
	self.builder.get_object(self.TB_PAUSE).show()
	self.builder.get_object(self.TB_STOP).show()
	#self.builder.get_object(self.TB_CONFIG).show()

	fcw.show()

	return



def check_job(self):

	#print "check next jb"

	if self.do_work == False:
		#print "do work true"
		return True

	if hasattr(self,"env") == False:
		return True

	if self.env == None:
		return True

	if len(self.env.robot) < 1:
		return True

	# tool_robot grab
	r = self.env.robot[self.tool_robot]
	if r != None:


		if r.calculateur != None:
			if r.calculateur.instance != None:
				if r.calculateur.instance.waiting_job == True:
					r.calculateur.instance.waiting_job = False
					#print "calculateur waiting job"
					self.do_work =False
					self.next_job()
					return True

		else:

			if r.client != None:
				if r.client.instance != None:
					if r.client.instance.waiting_job == True:
						r.client.instance.waiting_job = False
						self.do_work = False
						#print "client instance waiting job"
						self.next_job()
						return True



	r = self.env.robot[self.grab_robot]
	if r != None:


		if r.calculateur != None:
			if r.calculateur.instance != None:
				if r.calculateur.instance.waiting_job == True:
					#r.calculateur.instance.waiting_job = False
					#print "calculateur waiting job"
					self.do_work =False
					self.next_job()
					return True

		else:

			if r.client != None:
				if r.client.instance != None:
					if r.client.instance.waiting_job == True:
						#r.client.instance.waiting_job = False
						self.do_work = False
						#print "client instance waiting job"
						self.next_job()
						return True
					#else:
					#	print "client instance not waiting job"

	return True




def execute_task(self):

	return

def next_job(self):

	#print "-next job------------------------"

	# loop task
	pl = self.builder.get_object("workliststore")
	item = pl.get_iter_first()

	#if item == None:
		#print "workliststore empty"

	while (item != None):

		#print "task:" + pl.get_value(item,1)
		# task checked ?

		chk = pl.get_value(item,0)

		if chk == True:
			#print "checked"

			# checked
			n = pl.get_value(item,1)
			sta = pl.get_value(item,3)
			#print n+":"+sta
			# not done ?
			if sta != "[done]":

				n = pl.get_value(item,1)
				idx = pl.get_value(item,2)
				#print "["+idx+"]"

				for proc in self.workStack:

					#print "next workstack"
					#print "proc : "+proc.name
					#print "["+proc.uid+"]"

					if proc.uid == idx:
						#print "found,execute "+n


						if sta == "[to do]":
							#print "to do"
							proc.subprocess_index = 0
							proc.init(self.env,self.scenevismachframe,self)
							pl.set_value(item,3,"[running]")

						elif sta == "[running]":
							self.applog("task",0,"proc index before : %d"%(proc.subprocess_index))
							proc.subprocess_index = proc.subprocess_index+1
							if proc.subprocess_name[proc.subprocess_index] == "ending":
								#print "ending found"
								proc.ending(self.env,self.scenevismachframe,self)
								pl.set_value(item,3,"[done]")
							else:
								proc.subprocess[proc.subprocess_index](self.env,self.scenevismachframe,self)
							#print "proc idnex fater : %d"%(proc.subprocess_index)
						else:
							self.applog("task",2,"error")
						#print "return1"
						self.do_work = True
						return
			#else:
			#	print "done"
		#else:
		#	print "non checked"

		item = pl.iter_next(item)
		#if item == None:
		#	print "no more item"

	#print "return2"
	return
