var tpRobotActions;

var tpRobotActions = function tpRobotActions() {

	this.pluginName = "RobotActions";
	this.pluginVersion = "0.1";
	this.pluginAuthor = "Olivier Martinez";
	
};

tpRobotActions.prototype = {

	"register":function(parent) {
	

		/*mod = imp.load_source('tereocnc.plugins.robot_actions', './plugins/robot_actions/robot_actions.py')

		#def add_function(self,func_path,name,RobotActions):

		parent.add_function("robot.action","Send gcode",mod.send_gcode)
		parent.add_function("robot.action","Send setspeed",mod.send_setspeed)
		parent.add_function("robot.action","Send rotate",mod.send_rotate)
		parent.add_function("robot.action","Send move lin",mod.send_move_lin)
		parent.add_function("robot.action","Send deprehension",mod.send_deprehension)
		parent.add_function("robot.action","Send nail",mod.send_nail)
		parent.add_function("robot.action","Send prehension",mod.send_prehension)
		parent.add_function("robot.action","Send move contact",mod.send_move_contact)
		parent.add_function("robot.action","Send probe axis",mod.send_probe_axis)
		parent.add_function("robot.action","Send probe",mod.send_probe)
		parent.add_function("robot.action","Send move",mod.send_move)
		parent.add_function("robot.action","Send",mod.send)


		parent.add_function("robot.action","Set speed",mod.setspeed)
		parent.add_function("robot.action","Rotateto",mod.rotateto)
		parent.add_function("robot.action","Movelinto",mod.movelinto)
		parent.add_function("robot.action","Deprehension",mod.deprehension)
		parent.add_function("robot.action","Nail",mod.nail)
		parent.add_function("robot.action","Prehension",mod.prehension)
		parent.add_function("robot.action","Move_contact",mod.move_contact)
		parent.add_function("robot.action","Probe",mod.probe)
		parent.add_function("robot.action","Moveto",mod.moveto)*/
		

	/*	var msg ={};
		msg['msg'] = "START";
		parent.add_icon('I/O','plugins/tpRobotActions/start.png', true, "sendMqttMsg",msg);
*/

	},

	"unload":function(parent) {
	
	}

};


tereoPlugins['tpRobotActions'] = new tpRobotActions();
