# -*- coding: utf-8 -*-


import os.path


pluginName = "Flow config"
version = "0.1"
author = "Olivier Martinez"
flow_icon = None


def register(parent):

	mod = imp.load_source('flow_config', './plugins/flow_config/flow_config.py')
	parent.add_function("flow.config","Flow config",mod.on_configwin_flow)

	return


