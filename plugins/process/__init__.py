# -*- coding: utf-8 -*-


import os.path


pluginName = "Process"
version = "0.1"
author = "Olivier Martinez"
proc_icon = None



def register(parent):


	mod = imp.load_source('process', './plugins/process/process.py')

	parent.add_object('gui.frame','Process',mod.new_process_frame)

	img = gtk.Image()
	img.set_from_file('./plugins/process/process.png')
	img.show()
	proc_icon = gtk.ToolButton (img, "Task")
	proc_icon.connect("clicked", mod.on_process_activate, parent)
	parent.add_icon(proc_icon)

	return


