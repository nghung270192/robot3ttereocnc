
import pygtk
pygtk.require("2.0")
import gtk


class gui_window(gtk.Window):

	def __init__(self, tereo):

		gtk.Window.__init__(self)

		self.tereo = tereo

		self.connect_object("destroy", self.on_gui_destroy, None)

		self.vbox4 = gtk.VBox()
		self.add(self.vbox4)
		self.vbox4.show()
		self.hbox101 = gtk.HBox()
		self.vbox4.pack_start(self.hbox101,True,True,0)
		self.hbox101.show()
		self.guitable = gtk.Table(2,2,False)
		self.hbox101.pack_start(self.guitable,True,True,0)
		self.guitable.show()


		img = gtk.Image()
		img.set_from_file('./media/logout.png')
		img.show()
		quit_icon = gtk.ToolButton (img, "Quit")
		quit_icon.connect("clicked", self.on_gui_quit)
		self.tereo.add_icon(quit_icon)

        #<property name="expand">False</property>
        #<property name="homogeneous">True</property>


		#--- end toolbar ---




		self.show()
		self.maximize()

		return


	def attach_toolbar(self,tb):
   		self.vbox4.pack_end(tb,False,True,0)
		return

	def attach_panel(self,f):
		if f == None:
			return
		self.hbox101.pack_end(f,False,True,0)
		return

	def remove_panel(self,f):
		if f == None:
			return
		self.hbox101.remove(f)
		return


	def set_homogeneous(self,h):
		self.guitable.set_homogeneous(h)
		return

	def on_gui_quit(self,widget):


		# save
		self.tereo.env.save_env()
		del self.tereo.env

		self.tereo.env = None

		#if self.scenevismachframe != None:
		#	# purge scene
		#	self.scenevismachframe.clear()

		# show mainwin
		self.tereo.show_mainwin()

		self.destroy()

		return



	def attach_frame(self,f,c1,c2,l1,l2):
		self.guitable.attach(f,c1,c2,l1,l2)
		f.attach_frame()
		return


	def remove_children(self):

		for kid in self.guitable.get_children():
			self.guitable.remove(kid)

		return


	def on_gui_destroy(self,widget):

		# ask save changes

		# save screenshot

##		if self.scenevismachframe != None:
##
##			w = self.scenevismachframe.glarea #self.builder.get_object("sceneWindow")
##
##			sz = w.get_size()
##			print "size ot glscene is %d x %d" % (sz[0],sz[1])
##			pb = gtk.gdk.Pixbuf(gtk.gdk.COLORSPACE_RGB,False,8,sz[0],sz[1])
##			pb = pb.get_from_drawable(w,w.get_colormap(),0,0,0,0,sz[0],sz[1])
##			if (pb != None):
##				pb.save(homedir + '/configs/' + self.env.confname + '/thumbnail.png','png')
##			else:
##				print "unable to get screenshot"

		# save
		if self.tereo.env != None:
			self.tereo.env.save_env()
			del self.tereo.env
			self.tereo.env = None

		return
