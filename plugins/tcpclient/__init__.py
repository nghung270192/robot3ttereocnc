# -*- coding: utf-8 -*-

import gtk
import os.path
import imp


pluginName = "TcpClient"
version = "0.1"
author = "Olivier Martinez"



def register(parent):

	print "register tcpclient"
	mon = imp.load_source('tcpclient', './plugins/tcpclient/tcpclient.py')
	parent.add_function("command.inout.start","Command inout start",mon.clientserver_start)
	parent.add_function("command.inout.stop","command inout stop",mon.clientserver_stop)
	parent.add_function("command.inout.message","command inout message",mon.clientserver_message)

	print "end register"
	return


def unload(parent):

	return

