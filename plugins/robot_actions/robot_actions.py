# -*- coding: utf-8 -*-

import tereoMath


def send(com,tereo,robot):

	word_list = com.split(' ')

	if robot.calculateur != None:
		print word_list
		if robot.calculateur.instance != None:
			if word_list[1] == 'home':
				robot.calculateur.instance.send(com)
			if word_list[1] == 'state':
				robot.calculateur.instance.send(com)
				return
			if word_list[1] == 'stat':
				if word_list[2] != 'position' and word_list[2] != 'joint_position':
					robot.calculateur.instance.send(com)
					return

	if robot.client == None:
		return
	if robot.client.instance == None:
		return
	if robot.proto == "lcnc":
		self.client.instance.send(com)
	if robot.proto == "cross":
		if word_list[0] == 'stat' and word_list[1] == 'position':
			robot.client.instance.getpos()
		if word_list[0] == 'stat' and word_list[1] == 'joint_position':
			robot.client.instance.getaxes()
	if robot.proto == "simu":
		robot.client.instance.send(com)

	return



def send_move(self,x,y,z,a,b,c,base,tereo,robot):

	if robot.client == None:
		return
	if robot.client.instance == None:
		return

	f = tereoMath.frame6(x,y,z,a,b,c)
	robot.client.instance.setpos(f,base)

	return


def send_probe(x,y,z,a,b,c,d,base,tereo,robot):

	if robot.client == None:
		return
	if robot.client.instance == None:
		return

	f = tereoMath.frame6(x,y-90,z,a,b,c)
	robot.client.instance.setprobe(f,d,base)
	robot.end_process()

	return

def send_probe_axis(x,y,z,a,b,c,d,base,tereo,robot):

	if robot.client == None:
		return
	if robot.client.instance == None:
		return

	f = tereoMath.frame6(x,y-90,z,a,b,c)
	robot.client.instance.setprobe(f,d,base)
	robot.end_process()

	return


def send_move_contact(x,y,z,a,b,c,base,tereo,robot):

	if robot.client == None:
		return
	if robot.client.instance == None:
		return

	f = tereoMath.frame6(x,y,z,a,b,c)
	robot.client.instance.setmove_contact(f,base)

	return


def send_prehension(x,y,z,a,b,c,base,tereo,robot):

	if robot.client == None:
		return
	if robot.client.instance == None:
		return

	f = tereoMath.frame6(x,y,z,a,b,c)
	robot.client.instance.setprehension(f,base)

	return


def send_nail(x,y,z,a,b,c,base,tereo,robot):

	if robot.client == None:
		return
	if robot.client.instance == None:
		return

	f = frame6(x,y,z,a,b,c)
	robot.client.instance.setnail(f,base)

	return

def send_deprehension(x,y,z,a,b,c,base,tereo,robot):

	if robot.client == None:
		return
	if robot.client.instance == None:
		return

	f = tereoMath.frame6(x,y,z,a,b,c)
	robot.client.instance.setdeprehension(f,base)

	return


def send_move_lin(x,y,z,a,b,c,base,tereo,robot):

	if robot.client == None:
		return
	if robot.client.instance == None:
		return

	f = frame6(x,y,z,a,b,c)
	robot.client.instance.setposlin(f,base)

	return


def send_rotate(x,y,z,a,b,c,tereo,robot):
	print "send rotate : x:%f"%(x) + " y:%f"%(y) + " z:%f"%(z) + " a:%f"%(a) + " b:%f"%(b) + " c:%f"%(c)
	if robot.client == None:
		return
	if robot.client.instance == None:
		return

	robot.client.instance.setaxes(x,y,z,a,b,c)

	return


def send_setspeed(s,tereo,robot):

	if robot.client == None:
		return
	if robot.client.instance == None:
		return

	robot.client.instance.setspeed(s)

	return

def send_gcode(gc,tereo,robot):

	if robot.calculateur != None:
		if robot.calculateur.instance != None:
			print "start calculate:"+gc
			robot.calculateur.instance.send("command mdi "+gc)
		return

	if robot.client == None:
		return
	if robot.client.instance == None:
		return

	print "client instance send gcode:"+gc
	#self.client.instance.waiting_job = True
	robot.client.instance.send("command mdi "+gc)

	return



def rotateto(x,y,z,a,b,c,tereo,robot):
	print "robot rotateto"
	if robot.proto=="lcnc":
		v = robot.robot_matrix(x,y,-z,a,b,c)
		robot.send_gcode("G01X%f"%(v[0])+"Y%f"%(v[1])+"Z%f"%(-v[2])+"A%f"%(v[3])+"B%f"%(v[4])+"C%f"%(v[5]) )
	elif robot.proto=="cross":
		if robot.calculateur:
			robot.last_func = self.FUNC_ROTATE
			robot.send_gcode("G01X%f"%(v[0])+"Y%f"%(v[1])+"Z%f"%(v[2])+"A%f"%(v[3])+"B%f"%(v[4])+"C%f"%(v[5]) )
		else:
			robot.send_rotate(x,y,-z,a,b,c)
	robot.last_coords = [x,y,z,a,b,c]

def movelinto(x,y,z,a,b,c,tereo,robot):
	print "robot movelinto"
	if robot.proto=="lcnc":
		v = robot.robot_matrix(x,y,-z,a,b,c)
		#todo mettre G01 pour vitesse lente
		#print "send gcode : G00X%f"%(v[0])+"Y%f"%(v[1])+"Z%f"%(-v[2])+"A%f"%(v[3])+"B%f"%(v[4])+"C%f"%(v[5])
		robot.send_gcode("G00X%f"%(v[0])+"Y%f"%(v[1])+"Z%f"%(-v[2])+"A%f"%(v[3])+"B%f"%(v[4])+"C%f"%(v[5]) )
	elif robot.proto=="cross":
		if robot.calculateur:
			robot.last_func = self.FUNC_MOVELIN
		robot.send_move_lin(x,y,z,a,b,c)
	robot.last_coords = [x,y,z,a,b,c]

def moveto(x,y,z,a,b,c,base,tereo,robot):
	# for test
	#if base>-1:

	#ang = self.compensate_error(x,y,z,base)
	#a += ang[0]
	#b += ang[1]
	#c += ang[2]
	frm = robot.transform(x,y,z,a,b,c,base)
	x = frm[0]
	y = frm[1]
	z = frm[2]
	a = frm[3]
	b = frm[4]
	c = frm[5]


	print "robot moveto X:%f"%(x)+" Y:%f"%(y)+" Z:%f"%(z)+" A:%f"%(a)+" B:%f"%(b)+" C:%f"%(c)+" base:%d"%(base)
	if robot.proto=="lcnc":
		#v = self.robot_matrix(x,y,-z,a,b,c)
		#todo mettre G01 pour vitesse lente
		#print "send gcode : G00X%f"%(x/25.4)+"Y%f"%(y/25.4)+"Z%f"%(z/25.4)+"A%f"%(a)+"B%f"%(b)+"C%f"%(c)
		self.send_gcode("G00X%f"%(x/25.4)+"Y%f"%(y/25.4)+"Z%f"%(-z/25.4)+"A%f"%(a)+"B%f"%(b)+"C%f"%(c) )
	elif robot.proto=="cross":
		if robot.calculateur:
			robot.last_func = robot.FUNC_MOVE
			# kuka->lcnc conversion
			#print "send gcode to calc : G00X%f"%(x/25.4)+"Y%f"%(y/25.4)+"Z%f"%(-z/25.4)+"A%f"%(a)+"B%f"%(b)+"C%f"%(c)
			robot.send_gcode("G00X%f"%(x/25.4)+"Y%f"%(y/25.4)+"Z%f"%(z/25.4)+"A%f"%(a)+"B%f"%(b)+"C%f"%(c) )
		else:
			robot.send_move(x,y,z,a,b,c,base)
	robot.last_coords = [x,y,z,a,b,c]


def probe(x,y,z,a,b,c,d,base,tereo,robot):

	#frm = self.transform(x,y,z,a,b,c,base)
	#x = frm[0]
	#y = frm[1]
	#z = frm[2]

	#a = a - 225 -43
	#c = c + 225 -77
	#b = b + 72
	if a <-180:
		a = a + 180
	if a > 180:
		a = a - 10
	if b < -180:
		b = b + 180
	if b > 180:
		b = b - 180
	if c <-180:
		c = c + 180
	if c > 180:
		c = c - 180

	if a <-180:
		a = a + 180
	if a > 180:
		a = a - 10
	if b < -180:
		b = b + 180
	if b > 180:
		b = b - 180
	if c <-180:
		c = c + 180
	if c > 180:
		c = c - 180


	frm = robot.transform(x,y,z,a,b,c,base)
	x = frm[0]
	y = frm[1]
	z = frm[2]
	a = frm[3]
	b = frm[4]
	c = frm[5]

	print "robot probe X:%f"%(x)+" Y:%f"%(y)+" Z:%f"%(z)+" A:%f"%(a)+" B:%f"%(b)+" C:%f"%(c)+" d:%d"%(d)+" base:%d"%(base)
	if robot.proto=="lcnc":
		#v = self.robot_matrix(x,y,-z,a,b,c)
		#todo mettre G01 pour vitesse lente
		#print "send gcode : G00X%f"%(x/25.4)+"Y%f"%(y/25.4)+"Z%f"%(z/25.4)+"A%f"%(a)+"B%f"%(b)+"C%f"%(c)
		robot.send_gcode("G00X%f"%(x/25.4)+"Y%f"%(y/25.4)+"Z%f"%(-z/25.4)+"A%f"%(a)+"B%f"%(b)+"C%f"%(c) )
		if d == 1:
			robot.send_gcode("G00X%f"%(x/25.4)+"Y%f"%(y/25.4)+"Z%f"%(-(z-100)/25.4)+"A%f"%(a)+"B%f"%(b)+"C%f"%(c) )
		elif d == 3:
			robot.send_gcode("G00X%f"%(x/25.4)+"Y%f"%((y-100)/25.4)+"Z%f"%(-z/25.4)+"A%f"%(a)+"B%f"%(b)+"C%f"%(c) )
		elif d == 4:
			robot.send_gcode("G00X%f"%(x/25.4)+"Y%f"%((y+100)/25.4)+"Z%f"%(-z/25.4)+"A%f"%(a)+"B%f"%(b)+"C%f"%(c) )
		elif d == 5:
			robot.send_gcode("G00X%f"%((x-100)/25.4)+"Y%f"%(y/25.4)+"Z%f"%(-z/25.4)+"A%f"%(a)+"B%f"%(b)+"C%f"%(c) )
		elif d == 6:
			robot.send_gcode("G00X%f"%((x+100)/25.4)+"Y%f"%(y/25.4)+"Z%f"%(-z/25.4)+"A%f"%(a)+"B%f"%(b)+"C%f"%(c) )

	elif robot.proto=="cross":
		if robot.calculateur:
			robot.last_func = robot.FUNC_PROBE
			# kuka->lcnc conversion
			#print "send gcode to calc : G00X%f"%(x/25.4)+"Y%f"%(y/25.4)+"Z%f"%(-z/25.4)+"A%f"%(a)+"B%f"%(b)+"C%f"%(c)
			robot.last_base_dir = d
			robot.send_gcode("G00X%f"%(x/25.4)+"Y%f"%(y/25.4)+"Z%f"%(z/25.4)+"A%f"%(a)+"B%f"%(b)+"C%f"%(c) )
		else:
			robot.send_probe(x,y,z,a,b,c,d,-1)
	robot.last_coords = [x,y,z,a,b,c]



def move_contact(x,y,z,a,b,c,base,tereo,robot):
	if base >-1:
		ang = robot.compensate_error(x,y,z,base)
		#a += ang[0]
		#b += ang[1]
		#c += ang[2]
		frm = robot.transform(x,y,z,a,b,c)
		x = frm[0]
		y = frm[1]
		z = frm[2]

	print "robot move_contact X:%f"%(x)+" Y:%f"%(y)+" Z:%f"%(z)+" A:%f"%(a)+" B:%f"%(b)+" C:%f"%(c)+" base:%d"%(base)
	if robot.proto=="lcnc":
		#v = self.robot_matrix(x,y,-z,a,b,c)
		#todo mettre G01 pour vitesse lente
		#print "send gcode : G00X%f"%(x/25.4)+"Y%f"%(y/25.4)+"Z%f"%(z/25.4)+"A%f"%(a)+"B%f"%(b)+"C%f"%(c)
		robot.send_gcode("G00X%f"%(x/25.4)+"Y%f"%(y/25.4)+"Z%f"%(-z/25.4)+"A%f"%(a)+"B%f"%(b)+"C%f"%(c) )
	elif robot.proto=="cross":
		if robot.calculateur:
			robot.last_func = robot.FUNC_CONTACT
			# kuka->lcnc conversion
			#print "send gcode to calc : G00X%f"%(x/25.4)+"Y%f"%(y/25.4)+"Z%f"%(-z/25.4)+"A%f"%(a)+"B%f"%(b)+"C%f"%(c)
			robot.send_gcode("G00X%f"%(x/25.4)+"Y%f"%(y/25.4)+"Z%f"%(z/25.4)+"A%f"%(a)+"B%f"%(b)+"C%f"%(c) )
		else:
			robot.send_move_contact(x,y,z,a,b,c,base)
	robot.last_coords = [x,y,z,a,b,c]

def prehension(x,y,z,a,b,c,base,tereo,robot):
	if base>-1:
		#ang = self.compensate_error(x,y,z,base)
		#a += ang[0]
		#b += ang[1]
		#c += ang[2]
		frm = robot.transform(x,y,z,a,b,c)
		x = frm[0]
		y = frm[1]
		z = frm[2]


	print "robot prehension X:%f"%(x)+" Y:%f"%(y)+" Z:%f"%(z)+" A:%f"%(a)+" B:%f"%(b)+" C:%f"%(c)+" base:%d"%(base)
	if robot.proto=="lcnc":
		#v = self.robot_matrix(x,y,-z,a,b,c)
		#todo mettre G01 pour vitesse lente
		#print "send gcode : G00X%f"%(x/25.4)+"Y%f"%(y/25.4)+"Z%f"%(z/25.4)+"A%f"%(a)+"B%f"%(b)+"C%f"%(c)
		robot.send_gcode("G00X%f"%(x/25.4)+"Y%f"%(y/25.4)+"Z%f"%(-z/25.4)+"A%f"%(a)+"B%f"%(b)+"C%f"%(c) )
		robot.send_gcode("G00X%f"%(x/25.4)+"Y%f"%(y/25.4)+"Z%f"%(-(z-20)/25.4)+"A%f"%(a)+"B%f"%(b)+"C%f"%(c) )
	elif robot.proto=="cross":
		if robot.calculateur:
			robot.last_func = robot.FUNC_PREHEN
			# kuka->lcnc conversion
			#print "send gcode to calc : G00X%f"%(x/25.4)+"Y%f"%(y/25.4)+"Z%f"%(-z/25.4)+"A%f"%(a)+"B%f"%(b)+"C%f"%(c)
			robot.send_gcode("G00X%f"%(x/25.4)+"Y%f"%(y/25.4)+"Z%f"%(z/25.4)+"A%f"%(a)+"B%f"%(b)+"C%f"%(c) )
		else:
			robot.send_prehension(x,y,z,a,b,c,base)
	robot.last_coords = [x,y,z-20,a,b,c]

def nail(x,y,z,a,b,c,base,tereo,robot):
	if base>-1:
		ang = robot.compensate_error(x,y,z,base)
		#a += ang[0]
		#b += ang[1]
		#c += ang[2]
		frm = robot.transform(x,y,z,a,b,c)
		x = frm[0]
		y = frm[1]
		z = frm[2]


	print "robot nail X:%f"%(x)+" Y:%f"%(y)+" Z:%f"%(z)+" A:%f"%(a)+" B:%f"%(b)+" C:%f"%(c)+" base:%d"%(base)
	if robot.proto=="lcnc":
		#v = self.robot_matrix(x,y,-z,a,b,c)
		#todo mettre G01 pour vitesse lente
		#print "send gcode : G00X%f"%(x/25.4)+"Y%f"%(y/25.4)+"Z%f"%(z/25.4)+"A%f"%(a)+"B%f"%(b)+"C%f"%(c)
		robot.send_gcode("G00X%f"%(x/25.4)+"Y%f"%(y/25.4)+"Z%f"%(-z/25.4)+"A%f"%(a)+"B%f"%(b)+"C%f"%(c) )
		robot.send_gcode("G00X%f"%(x/25.4)+"Y%f"%(y/25.4)+"Z%f"%(-(z-20)/25.4)+"A%f"%(a)+"B%f"%(b)+"C%f"%(c) )
	elif robot.proto=="cross":
		if robot.calculateur:
			robot.last_func = robot.FUNC_NAIL
			# kuka->lcnc conversion
			#print "send gcode to calc : G00X%f"%(x/25.4)+"Y%f"%(y/25.4)+"Z%f"%(-z/25.4)+"A%f"%(a)+"B%f"%(b)+"C%f"%(c)
			robot.send_gcode("G00X%f"%(x/25.4)+"Y%f"%(y/25.4)+"Z%f"%(z/25.4)+"A%f"%(a)+"B%f"%(b)+"C%f"%(c) )
		else:
			robot.send_nail(x,y,z,a,b,c,base)
	robot.last_coords = [x,y,z-20,a,b,c]


def deprehension(x,y,z,a,b,c,base,tereo,robot):
	if base>-1:
		ang = robot.compensate_error(x,y,z,base)
		#a += ang[0]
		#b += ang[1]
		#c += ang[2]
		frm = robot.transform(x,y,z,a,b,c)
		x = frm[0]
		y = frm[1]
		z = frm[2]


	print "robot deprehension X:%f"%(x)+" Y:%f"%(y)+" Z:%f"%(z)+" A:%f"%(a)+" B:%f"%(b)+" C:%f"%(c)+" base:%d"%(base)
	if robot.proto=="lcnc":
		#v = self.robot_matrix(x,y,-z,a,b,c)
		#todo mettre G01 pour vitesse lente
		#print "send gcode : G00X%f"%(x/25.4)+"Y%f"%(y/25.4)+"Z%f"%(z/25.4)+"A%f"%(a)+"B%f"%(b)+"C%f"%(c)
		robot.send_gcode("G00X%f"%(x/25.4)+"Y%f"%(y/25.4)+"Z%f"%(-z/25.4)+"A%f"%(a)+"B%f"%(b)+"C%f"%(c) )
		#self.send_gcode("G00X%f"%(x/25.4)+"Y%f"%(y/25.4)+"Z%f"%((z-20)/25.4)+"A%f"%(a)+"B%f"%(b)+"C%f"%(c) )
	elif robot.proto=="cross":
		if robot.calculateur:
			robot.last_func = robot.FUNC_DEPREHEN
			# kuka->lcnc conversion
			#print "send gcode to calc : G00X%f"%(x/25.4)+"Y%f"%(y/25.4)+"Z%f"%(-z/25.4)+"A%f"%(a)+"B%f"%(b)+"C%f"%(c)
			robot.send_gcode("G00X%f"%(x/25.4)+"Y%f"%(y/25.4)+"Z%f"%(z/25.4)+"A%f"%(a)+"B%f"%(b)+"C%f"%(c) )
		else:
			robot.send_deprehension(x,y,z,a,b,c,base)
	robot.last_coords = [x,y,z-20,a,b,c]

def setspeed(s,tereo,robot):

	if robot.proto=="lcnc":
		robot.send_gcode("F%f"%(s*2))
	elif robot.proto=="cross":
		robot.send_setspeed(s)
	robot.last_speed = s

#def move_slow(self,x,y,z,a,b,c):
#	v = self.robot_matrix(x,y,-z,a,b,c)
#	self.send_gcode( "G01X%f"%(v[0])+"Y%f"%(v[1])+"Z%f"%(v[2])+"A%f"%(v[3])+"B%f"%(v[4])+"C%f"%(v[5]) )
#	self.last_coords = [x,y,z,a,b,c]
