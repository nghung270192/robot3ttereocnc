var tpNetwork;

var tpNetwork = function tpNetwork() {

	this.pluginName = "Network";
	this.pluginVersion = "0.1";
	this.pluginAuthor = "Olivier Martinez";
	this.panel;
	
};

tpNetwork.prototype = {

	"register":function(parent) {

		var msg ={};
		msg['msg'] = "Network";
		parent.add_icon('Network','plugins/tpNetwork/network.png', true, "@tpNetwork&on_network_toggle",msg,"tpNetwork");
		
	},
	
	
	"on_network_toggle":function() {
	
		//console.log("on network toggle");
		
		var f = $("#network_panel");
		if(f.length){
		
			//L'élément existe
			if ($("#tpNetworkNetwork").hasClass("active")) {
				$("#tpNetworkNetwork").removeClass("active")
				$("#network_panel").hide();
			} else {
				$("#tpNetworkNetwork").addClass("active")
				$("#network_panel").show();
			}
		  
		} else {
		
			//L'élément n'existe pas
			this.panel = new networkPanelView();
			//parent.add_object("robot_panel", f);
			this.panel.__init__();
			$("#network_panel").show();
			
		}
		
		this.panel.populate();
		
	
	},

	"unload":function() {
	
	}
	
	
};


tereoPlugins['tpNetwork'] = new tpNetwork();



var networkPanelView = function() {

	this.uid = (((1+Math.random())*0x100000)|0).toString(16).substring(1);
	this.lastmsg ="";
	this.nodes = {};


}

networkPanelView.prototype = {

	"__init__": function () {

		$( "#tereopanels").append('<div class="tpPanel" id="network_panel"><div class="tpPanelTitle">Network</div><div id="networkPanelScroll" class="tpScroll"><div id="network_list"></div></div></div>');

	},

	"populate":function () {
	
		//console.log("populate");

		// populate net
		//ntv = self.builder.get_object("nethosttv")
		
		// todo get robot object list		
		// todo get calibration list
		var msg = {}
		msg['msg'] = "get_network_list";
		msg['uid'] = this.uid+"grl";
		msg['callback'] = "tpNetworkPanel_on_get_network_list";
		//console.log (msg);
		
		tereoMain.call("sendMqttMsg",JSON.stringify(msg));


	},

	"refresh":function () {
		
		//console.log("refresh");
		
		//clear
		var myNode = document.getElementById("network_list");
		while (myNode.firstChild) {
			myNode.removeChild(myNode.firstChild);
		}
		
		i = 0;
		var render="<table>";

		for (n in this.network) { //self.tereo.env.robots:
		
			if (this.network.hasOwnProperty(n)) {
			
				net = this.network[r];

				render += "<tr><td colspan=2><div class=\"tpPanelChild\"><table cellpadding=0 cellspacing=0><tr><td class=\"tpPanelProps\">"+i+"</td><td>"+net['name']+"</td></tr>";
				render += "<tr><td class=\"tpPanelProps\">Model</td><td>"+net['ip']+"</td></tr>";
				//render += "<tr><td class=\"tpPanelProps\">Constructor</td><td>"+net['chan']+"</td></tr>";
				render += "<tr><td class=\"tpPanelProps\">Status</td><td>"+net['status']+"</td></tr>";

				i+=1;
				render += "</table></div></td></tr>";
			}

		}
		
		render += '</table>';
		//console.log("render:"+render);
		$( "#network_list").append(render);
		
	}

}


function tpNetworkPanel_on_get_network_list(l) {
	//console.log("onplugin networklist:"+l);
	//console.log(l);
	if (l == "get_network_list") { return; }
	delete cnx_callbacks[this.uid+"gnl"];
	
	try {
		l = l.replace(' ','');
		l = l.replace('\n','');
		l = l.replace('\r','');
		tereoPlugins['tpNetwork'].panel.network = JSON.parse(l);
	} catch(e) { console.log(e); }
	
	//console.log(tereoPlugins['tpNetwork'].panel.network);
	tereoPlugins['tpNetwork'].panel.refresh();
}
		
		
function check_network() {
/*
	#print "check network"

	if hasattr(self,"env") == False:
		#print "no env"
		return True

	if self.env == None:
		#print "env null"
		return True

	for r in self.env.robot:
		#print "proto : " + r.proto
		if r.proto == "cross":
			#print "check cross"
			if r.client != None:
				if r.ip != "" and r.chan > 0:
					#print "ip chan ok"
					if r.client.connected == False:
						#print "not connected"
						if r.client.connecting == False:
							r.client.connecting = True
							self.applog("network",0,"not connected retrying...")
							#r.client.connectTCP(r.ip,r.chan, r.client)
							r.client.initialize()

	# update network
	netwin = self.builder.get_object("winNet")
	for w in gtk.window_list_toplevels():
		if w == netwin:
			self.populate_network()
			return w
*/
}



	
	
	
