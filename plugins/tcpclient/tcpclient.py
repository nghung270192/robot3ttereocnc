# -*- coding: utf-8 -*-



from twisted.internet.protocol import Factory
from twisted.protocols.basic import LineReceiver



class tcpserver(LineReceiver) :

	def __init__ (self,users,p):
		self.parent = p
		self.users = users
		self.LINUXCNC_STATE_ESTOP = 1
		self.LINUXCNC_STATE_ON = 4
		self.LINUXCNC_STATE_OFF = 3
		self.LINUXCNC_STATE_ESTOP_RESET = 2
		self.LINUXCNC_STATE_JOG_STOP = 0
	
	def connectionMade(self):
		print "client connected"

	def connectionLost(self,msg):
		print "connection lost"

	def lineReceived(self,message):
		#dispatch
		dummy=0

		print "["+message+"]"

		pos = -1

		try:
			pos = message.index('@rc@');
		except Exception, e:
			print e
					
		uid = ""
		arg = ""
		msg = message

		if (pos>-1):
			uid = message[:pos]
			msg = message[pos+4:]

		print "uid:"+uid
		print "message:"+msg

		try:
			pos2 = msg.index('@arg@');
			if pos2>1:
				arg = msg[pos2+5:]
				msg = msg[:pos2]
		except Exception, e:
			print e

		print "msg:"+msg
		print "arg:"+arg		


		if msg == "POW_ON":
			print "power on"
			#self.parent.broadcast("command state %d"%(self.LINUXCNC_STATE_ON))
			if self.parent != None:
				if self.parent.env != None:
					if self.parent.env.robots != None:
						for r in self.parent.env.robots:
							if r.client != None:
								r.client.set_power(True)
			
			
		elif msg == "POW_OFF":
			self.parent.broadcast("command state %d"%(self.LINUXCNC_STATE_OFF))
			
		elif msg == "MOVE_LEFT":		
			dummy=1
		elif msg == "MOVE_UP":
			dummy=1
		elif msg == "MOVE_STOP":
			dummy=1
		elif msg == "MOVE_RIGHT":
			dummy=1
		elif msg == "MOVE_DOWN":
			dummy=1
		elif msg == "SPEED_UP":
			dummy=1
		elif msg == "SPEED_NORMAL":
			dummy=1
		elif msg == "SPEED_DOWN":
			dummy=1
		elif msg == "CAM_UP":
			dummy=1
		elif msg == "CAM_HORIZ":
			dummy=1
		elif msg == "CAM_DOWN":
			dummy=1
		elif msg == "CAM_ON":
			dummy=1
		elif msg == "CAM_OFF":
			dummy=1
		elif msg == "E-STOP-RESET" or msg=="E-STOP":
			if self.parent.env != None:
				if self.parent.env.robots != None:
					for r in self.parent.env.robots:
						if r.client != None:
							r.client.set_estop(False)

		elif msg == "ROBOT_HOME":
			if self.parent.env != None:
				if self.parent.env.robots != None:
					for r in self.parent.env.robots:
						if r.client != None:
							r.client.set_home()		
			
		elif msg == "get_calibration_list":
			list = self.parent.get_calibration_list();
			self.sendLine(uid+"@rc@"+list+"\r\n");
		elif msg == "get_config_list":
			list = self.parent.get_config_list();
			self.sendLine(uid+"@rc@"+list+"\r\n");
		elif msg == "load_config":
			list = self.parent.load_config(arg);
			self.sendLine(uid+"@rc@"+list+"\r\n");
		elif msg == "get_robot_list":
			list = self.parent.get_robot_list();
			self.sendLine(uid+"@rc@"+list+"\r\n");
		elif msg == "get_toolcalibration_list":
			list = self.parent.get_toolcalibration_list();
			self.sendLine(uid+"@rc@"+list+"\r\n");
		elif msg == "get_network_list":
			list = self.parent.get_network_list();
			self.sendLine(uid+"@rc@"+list+"\r\n");
		elif msg == "get_robot_kinematic":
			list = self.parent.get_robot_kinematic(arg);
			self.sendLine(uid+"@rc@"+list+"\r\n");
		elif msg == "get_robot_mesh":
			list = self.parent.get_robot_mesh(arg);
			self.sendLine(uid+"@rc@"+list+"\r\n");
			#print "ok"
		elif msg == "MOVE_TO":
			arglist = arg.split(':');
			if self.parent != None:
				if self.parent.env != None:
					if self.parent.env.robots != None:
						for r in self.parent.env.robots:
							if r.client != None:
								r.client.moveto(arglist[0],arglist[1],arglist[2])


		elif msg == "JOG_AXIS":
			arglist = arg.split(':');
			print arglist	
			if self.parent != None:
				if self.parent.env != None:
					if self.parent.env.robots != None:
						for r in self.parent.env.robots:
							if r.name == arglist[0]:
								if r.client != None:
									r.client.jog(arglist[1],arglist[2],arglist[3],arglist[4])
	
		elif msg == "get_robot_mesh_list":
			list = self.parent.get_robot_mesh_list(arg);
			self.sendLine(uid+"@rc@"+list+"\r\n");
		else:
			print "messasge received : "+msg
	
		return











	def sendMessage (self, msg):
		self.sendLine(msg)
		return;



class tcpserver_factory(Factory):

	def __init__(self,p):
		
		self.users = {}
		self.p = p
		self.s = None

	def buildProtocol(self,addr):
		self.s = tcpserver(self.users,self.p)
		return self.s





def clientserver_start(widget,parent):

	print "tcpclient start"

	if parent == None:
		print "parent null"
		return

	#try:
	myserver = tcpserver_factory(parent)
	parent.listentcp(myserver,4444)
	parent.add_object("Command.inout","tcpclient",myserver)
	#except:
	#	print "error:"+e

	return


def clientserver_stop(widget, parent):

	print "tcp client stop"

	if parent == None:
		return

	#todo


def clientserver_message(msg, parent):

	myserver = parent.get_object("Command.inout","tcpclient")

	if myserver != None:

		myserver.sendMessage(msg)

	return	
