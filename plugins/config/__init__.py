# -*- coding: utf-8 -*-

import gtk
import os.path
import imp


pluginName = "Config"
version = "0.1"
author = "Olivier Martinez"
conf_icon = None


def register(parent):


	mod = imp.load_source('config', './plugins/config/config.py')

	# log icon
	img = gtk.Image()
	img.set_from_file('./plugins/config/config.png')
	img.show()
	conf_icon = gtk.ToolButton (img, "Config")
	conf_icon.connect("clicked", mod.on_config_activate, parent)
	parent.add_icon(conf_icon)

	return




