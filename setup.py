from distutils.core import setup
import py2exe

excludes = ['OpenGL']
dll_excludes = ["iconv.dll","intl.dll","libatk-1.0-0.dll",
		"libgdk_pixbuf-2.0-0.dll","libgdk-win32-2.0-0.dll",
		"libglib-2.0-0.dll","libgmodule-2.0-0.dll",
		"libgobject-2.0-0.dll","libgthread-2.0-0.dll",
		"libgtk-win32-2.0-0.dll","libpango-1.0-0.dll",
		"libpangowin32-1.0-0.dll"]
includes = ['cairo', 'pango', 'pangocairo', 'atk', 'gobject', 'gio']
packages = ['twisted']

           

options = {
"excludes": excludes,
"includes": includes,
"packages": packages,
"dll_excludes": dll_excludes
}
setup(
version = "0.6.0",
     description = "Tereocnc programm",
     name = "Tereocnc",
options = {"py2exe": options},
windows = ['tereocnc.py'],
)