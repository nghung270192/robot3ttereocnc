# -*- coding: utf-8 -*-

###########################################################################
#    Copyright (C) 2003 by Wido Depping
#    <widod@users.sourceforge.net>
#
# Copyright: See COPYING file that comes with this distribution
#
###########################################################################

#from qt import *
#from base.utils.gui.SearchForm import SearchForm
#from base.utils.gui.SearchResultView import SearchResultView

class collada_view(QWidget):

    def __init__(self, parent=None, name=None, fl=0):

        QWidget.__init__(self, parent, name, fl)

        self.vLayout = QVBoxLayout(self)

        self.searchForm = SearchForm(self)
        self.resultView = SearchResultView(self)

        self.vLayout.addWidget(self.searchForm)
        self.vLayout.addWidget(self.resultView)

        self.connect(self.searchForm, PYSIGNAL("ldap_result"), \
                self.resultView.setResult)

#--------------------


#!/usr/bin/python
###########################################################
#	COLLADA INTERFACE
###########################################################


from xml.dom import minidom
from OpenGL.GL import *
import tereoProcess
from collada import *
from tereoBTL import *
import numpy


class colladaInterface():

	def __init__(self):

		self.application = None
##		self.btlGeneral = False
##		self.btlPart = False
##		self.btlRawPart = False
##		#self.btlUid = False
##		self.btlProcess = False
##		self.currentPart = None
##		self.currentUid = 0
##		self.currentProcess = None
		self.genePart = geneObject()
		self.parts = []
##		self.version = ''
##		self.build = ''
##		self.edition = ''
		self.filename = ''
##		self.btlError = False
##		self.parsing_dict = {	'VERSION:' :			self.parse_version,
##								'BUILD:' :				self.parse_build,
##								'EDITION:' :			self.parse_edition,
##								'[GENERAL]' :			self.parse_general,
##								'PROJECTNUMBER:' :		self.parse_projectnumber,
##								'PROJECTNAME:' :		self.parse_projectname,
##								'PROJECTPART:' :		self.parse_projectpart,
##								'PROJECTGUID:' :		self.parse_projectguid,
##								'LISTNAME:' :			self.parse_listname,
##								'CUSTOMER:' :			self.parse_customer,
##								'ARCHITECT:' :			self.parse_architect,
##								'EDITOR:' :				self.parse_editor,
##								'DELIVERYDATE:' :		self.parse_deliverydate,
##								'EXPORTDATE:' :			self.parse_exportdate,
##								'EXPORTTIME:' :			self.parse_exporttime,
##								'EXPORTRELEASE:' :		self.parse_exportrelease,
##								'LANGUAGE:' :			self.parse_language,
##								'RANGE:' :				self.parse_range,
##								'SCALEUNIT:' :			self.parse_scaleunit,
##								'COMPUTERNAME:' :		self.parse_computername,
##								'USER:' :				self.parse_user,
##								'SOURCEFILE:' :			self.parse_sourcefile,
##								'EXPORTFILE:' :			self.parse_exportfile,
##								'RECESS:' :				self.parse_recess,
##								'COMMENT:' :			self.parse_comment,
##								'[RAWPART]' :			self.parse_rawpart,
##								'PROCESSKEY:' :			self.parse_processkey,
##								'REFERENCEPLANE:' :		self.parse_referenceplane,
##								'PROCESSPARAMETERS:' :	self.parse_processparameters,
##								'[PART]' :				self.parse_part,
##								'SINGLEMEMBERNUMBER:' :	self.parse_singlemembernumber,
##								'ASSEMBLYNUMBER:' :		self.parse_assemblynumber,
##								'ORDERNUMBER:' :		self.parse_ordernumber,
##								'DESIGNATION:' :		self.parse_designation,
##								'ANNOTATION:' :			self.parse_annotation,
##								'STOREY:' :				self.parse_storey,
##								'GROUP:' :				self.parse_group,
##								'PACKAGE:' :			self.parse_package,
##								'MATERIAL:' :			self.parse_material,
##								'TIMBERGRADE:' :		self.parse_timbergrade,
##								'QUALITYGRADE:' :		self.parse_qualitygrade,
##								'COUNT:' :				self.parse_count,
##								'LENGTH:' :				self.parse_length,
##								'HEIGHT:' :				self.parse_height,
##								'WIDTH:' :				self.parse_width,
##								'COLOUR:' :				self.parse_colour,
##								'PLANINGLENGTH:' :		self.parse_planninglength,
##								'STARTOFFSET:' :		self.parse_startoffset,
##								'ENDOFFSET:' :			self.parse_endoffset,
##								'UID:' :				self.parse_uid,
##								'TRANSFORMATION:' :		self.parse_transformation,
##								'CAMBER:' :				self.parse_camber,
##								'PARTOFFSET:' :			self.parse_partoffset,
##								'PROCESSINGQUALITY:' :	self.parse_processingquality,
##								'OUTLINE:' :			self.parse_outline,
##								'APERTURE:' :			self.parse_aperture,
##								'STOREYTYPE:' :			self.parse_storeytype,
##								'ELEMENTNUMBER:' :		self.parse_elementnumber,
##								'LAYER:' :				self.parse_layer,
##								'MODULENUMBER:' :		self.parse_modulenumber,
##								'GRAINDIRECTION:' :		self.parse_graindirection,
##								'REFERENCESIDE:' :		self.parse_refenreceside,
##								'PROCESSIDENT:' :		self.parse_processident,
##								'PRIORITY:' :			self.parse_priority,
##								'PROCESS:' :			self.parse_process,
##								'[COMPOSITE]' :			self.parse_composite,
##								'TYPE:' :				self.parse_type }
##
##		self.general_properties = {
##                    'Project number' : 'projectNumber',
##                    'Project name' : 'projectName',
##                    'Project part' : 'projectPart',
##                    'Project GUID' : 'projectGuid',
##                    'List name' : 'listName',
##                    'Customer' : 'customer',
##
##                    'Architect' : 'architect',
##                    'Editor' : 'editor',
##                    'Delivery date' : 'deliveryDate',
##                    'Export date' : 'exportDate',
##                    'Export time' : 'exportTime',
##                    'Export release' : 'exportRelease',
##                    'Language' : 'language',
##                    'Range' : 'geneRange',
##
##                    'Scale unit' : 'scaleUnit',
##                    'Processing quality' : 'processingQuality',
##                    'Computer name' : 'computerName',
##                    'User' : 'user',
##                    'Source file' : 'sourceFile',
##                    'Export file' : 'exportFile',
##                    'Recess' : 'recess',
##                    'Comment' : 'comment' }
##
##		self.part_properties = { 'Single member number' : 'singleMemberNumber',
##                    'Assembly number' : 'assemblyNumber',
##                    'Order number' : 'orderNumber',
##                    'Designation' : 'designation',
##                    'Annotation' : 'annotation',
##                    'Storey' : 'storey',
##                    'Group' : 'group',
##                    'Package' : 'package',
##                    'Material' : 'material',
##                    'Timber grade' : 'timberGrade',
##                    'Quality grade' : 'qualityGrade',
##                    'Count' : 'count',
##                    'Length' : 'length',
##                    'Height' : 'height',
##                    'Width' : 'width',
##                    'Colour' : 'colour',
##                    'Planing length' : 'planingLength',
##                    'Start offset' : 'startOffset',
##                    'End offset' : 'endOffset',
##
##                    'Camber' : 'camber',
##                    'Part offset' : 'partOffset',
##                    'Processing quality' : 'processingQuality',
##                    'Outline' : 'outline',
##                    'Aperture' : 'aperture',
##                    'Recess' : 'recess',
##                    'Storey type' : 'storeyType',
##                    'Element number' : 'elementNumber',
##                    'Layer' : 'layer',
##                    'Module number' : 'moduleNumber',
##                    'Comment' : 'comment',
##                    'Grain direction' : 'grainDirection',
##                    'Refrence side' : 'referenceSide' }
##
##
##		self.process_properties = { 'Process key' : 'processKey',
##                       'Reference plane' : 'referencePlane',
##                       'Process parameters' : 'processParameters',
##                       'Process ident' : 'processIdent',
##                       'Processing quality' : 'processingQuality',
##                       'Comment' : 'comment',
##                       'Priority' : 'priority',
##                       'Recess' : 'recess',
##                       'Process' : 'process' }
##



		return

	def openfile(self,filename):
		print "fn : "+filename

		try:

			self.genePart.projectNumber = '#'
			self.genePart.projectName = filename
			self.genePart.scaleUnit = 1

			skmatrix = [] #id + quaternion[16]

			# ----------------- get sketchup nodes ------------------

			doc = minidom.parse(filename)
			lvs = doc.documentElement.getElementsByTagName("visual_scene")
			sknode = lvs.item(0)
			sknodes = sknode.getElementsByTagName("node")

			for el in sknodes:
				n = el.getAttribute("name")
				if n != "SketchUp":

					matrixname = ""

					# get <matrix>
					matr = el.getElementsByTagName("matrix")
					for i in range(matr.length):
						it = matr.item(i)
						q = it.childNodes[0].nodeValue
						q = q.split(' ')
						print q
						q = map(float, q)
						print q

					# get <instance_geometry url="#ID3">
					inst = el.getElementsByTagName("instance_geometry")
					for i in range(inst.length):
						it = inst.item(i)
						matrixname = it.getAttribute("url")

					skmatrix.append([matrixname,q])

			# ------------ end get sketchup matrix --------------

			f = open(filename, 'r')
			mesh = Collada(f)

			print "geo : %d"%(len(mesh.geometries))
			currentId = ""
			# geometry = part (convention)
			for geom in mesh.geometries:
				print "=================================="
				print geom.id
				print geom.id[2:]
				currentId = "#"+geom.id
				for prim in geom.primitives:
					print prim

##						for i in range(len(prim.normal)):
##							n = prim.normal[i]
##							if aface == -1:
##								if n[0] == 0.0 and n[1] == 1.0 and n[2] == 0.0:
##									aface = i
##
##							if bface == -1:
##								if n[0] == 0.0 and n[1] == -1.0 and n[2] == 0.0:
##									bface = i


					cpo = btlPartObject()
					cpo.singleMemberNumber = geom.id[2:]
					cpo.designation = geom.id
					cpo.count = 1

					x = 0.0
					y = 0.0
					z = 0.0

					for v in prim.vertex:
						if v[0] > x : x = v[0]
						if v[1] > y : y = v[1]
						if v[2] > z : z = v[2]

					cpo.length = x*25.4
					cpo.height = y*25.4
					cpo.width = z*25.4

					print "length : %f"%(cpo.length)+ " height:%f"%(cpo.height)+" width:%f"%(cpo.width)

					t = [1.0, 0.0, 0.0, 0.0,
						0.0, 1.0, 0.0, 0.0,
						0.0, 0.0, 1.0, 0.0,
						0.0, 0.0, 0.0, 1.0]

					for m in skmatrix:
						if m[0] == currentId:
							q = m[1]
							t[0] = q[0]
							t[1] = q[4]
							t[2] = q[8]
							t[3] = q[12]

							t[4] = q[1]
							t[5] = q[5]
							t[6] = q[9]
							t[7] = q[13]

							t[8] = q[2]
							t[9] = q[6]
							t[10] = q[10]
							t[11] = q[14]

							t[12] = q[3]
							t[13] = q[7]
							t[14] = q[11]
							t[15] = q[15]

					cpo.glmatrix.append(t)

					t = btlMatrix()
					t.ox = 0.0
					t.oy = 0.0
					t.oz = 0.0
					t.xx = 0.0
					t.xy = 1.0
					t.xz = 0.0
					t.yx = 0.0
					t.yy = 0.0
					t.yz = 1.0
					cpo.transformations.append(t)




					self.parts.append(cpo)


##				btlfile = f.readlines()


##				#print len(btlfile)
##
##				for line in btlfile:
##	            	#print "--------"
##					line = line.rstrip('\r')
##					line = line.rstrip('\n')
##					line = line.rstrip('\r')
##					if len(line)>0 :
##						#print line
##						word_list = line.split(' ',1)
##						#print word_list
##						word = word_list[0]
##
##						if len(word_list) > 1:
##							arg = word_list[1]
##							arg = arg.strip()
##						else:
##							arg = ''
##
##						self.parsing_dict[word](arg)
##
##						if self.btlError:
##							print "error"
##							return

				f.close()

##			self.parse_end()

			self.filename = filename

			return;
		except (UnicodeError, LookupError, IOError):
			print "coult not decode file"
		pass

		return


def calcNormal(v1,v2):
	v = [0.0,0.0,0.0]
	v[0] = v1[1] * v2[2] - v2[1] * v1[2]
	v[1] = v1[2] * v2[0] - v2[2] * v1[0]
	v[2] = v1[0] * v2[1] - v2[0] * v1[1]
	return v

