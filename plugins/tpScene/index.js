var tpScene;

var tpScene = function tpScene() {

	this.pluginName = "Scene";
	this.pluginVersion = "0.1";
	this.pluginAuthor = "Olivier Martinez";
	this.scene;
	
};

tpScene.prototype = {

	"register":function(parent) {

//		mod = imp.load_source('tereocnc.plugins.scene', './plugins/scene/scene.py')
		this.scene = new_scene_frame();
		tereoMain.add_object('gui.frame','Scene 3D',this.scene);

	},

	"unload":function(parent) {
	
	}

};


tereoPlugins['tpScene'] = new tpScene();



function new_scene_frame() {

	f = tereoMain.get_object('frame','Scene')
	if (typeof f === 'undefined') {
		f = new sceneView();
				f.__init__();
		tereoMain.add_object('gui.frame','Scene',f);
	}

	return f;
	
}


var sceneView = function() {

	this.uid = (((1+Math.random())*0x100000)|0).toString(16).substring(1);
	this.lastmsg ="";
	
	
	this.lat=0;
	this.lon=0;
	this.mousex = 0;
	this.mousey = 0;
	this.minlat = -180;
	this.maxlat = 180;
	this.rotating = false;
	this.glarea = null;
	

	this.model = new Array();
	this.size = 60.0; //float(size)
	this.distance = this.size * 5.0;
	this.near = 0.06; // float(size) * 0.01
	this.far = this.size * 10.0;
	/*this.tool2view = tool;
	this.world2view = world;
	this.work2view = work;*/

	this.scene = null;
	this.base = null;
	this.grid = null;
	this.render ="";
	this.gridGuide = null;
	
	this.robotsnb = -1;
	
	this.meshes = {};
	this.meshes_link = {};
	this.links = {};	
	this.robots = {};
	this.joints_axis = new Array();
	
	this.lastmesh3D = null;
	
	

}

sceneView.prototype = {

	"__init__": function () {

		this.render = "<div style=\"width:100%;height:100%;\" class=\"tpFrame\" id=\"scene\"><div>";

	},


	"attach_frame":function() {

		console.log ("attach frame");
		
		var canvas = document.getElementById('scene');

		var grid = 20;

		try {
		
		var editor = new Editor();
		
		editor.config.clear();

		// add subtle ambient lighting
		var ambientLight = new THREE.AmbientLight(0x222222);
		editor.scene.add(ambientLight);
      
		// directional lighting
		var directionalLight = new THREE.DirectionalLight(0xffffff);
		directionalLight.position.set(1, 1, 1).normalize();
		editor.scene.add(directionalLight);
		
		
		var viewport = new Viewport( editor ).setId( 'viewport' );
		canvas.appendChild( viewport.dom );
	
		var canv = document.querySelector('canvas');
		//fitToContainer(canv);

		//function fitToContainer(canvas){
		// Make it visually fill the positioned parent
		canv.style.width ='100%';
		canv.style.height='100%';
		// ...then set the internal size to match
		/*canv.width  = canv.offsetWidth;
		canv.height = canv.offsetHeight;*/
		//}
		
		var menubar = new Menubar( editor ).setId( 'menubar' );
		document.body.appendChild( menubar.dom );


		editor.setTheme( editor.config.getKey( 'theme' ) );
		
		
		

		//var babyEdit = new BABYLON.Editor(canvas);
		//babyEdit.createBasicScene();

		

		//this.scene = babyEdit.scene;
		this.scene = editor.scene;
		this.editor = editor;
		

		} catch(e) { console.log(e); }

		tereoObjects["scene"] = this;
		
		tereoMain.add_trigger("broadcast_position", tpScene_update_robots);
		
		this.buildScene();
		
		
		

	},


	"drawScene":function() {

    },
	
	"buildScene":function() {

		console.log ("build scene");
		
		if (this.robotsnb == -1) {
		
			console.log("no robot data..get robot list");
			
			var msg = {}
			msg['msg'] = "get_robot_list";
			msg['uid'] = this.uid+"grl";
			msg['callback'] = "tpScene_on_get_robot_list";
			tereoMain.call("sendMqttMsg",JSON.stringify(msg));
			return;
			
		}


	}	
	
	
}




function tpScene_on_get_robot_list(l) {

	console.log("onplugin scene robotlist:"+l);
	console.log(l);
	if (l == "get_robot_list") { console.log("echo"); return; }
	
	scene = tereoObjects['scene'];
	
	delete cnx_callbacks[scene.uid+"grl"];
	
	try {
		l = l.replace(' ','');
		l = l.replace('\n','');
		l = l.replace('\r','');
		var list = JSON.parse(l);
	} catch(e) { console.log(e);
			return;
	}
	
	
	i=0
	firstname = ""
	//console.log ("search in list");
	for (r in list) {
		if (list.hasOwnProperty(r)) {
			if (firstname == "") {
				//console.log ("robot found in list")
				firstname = list[r].name;
			}
			i=i+1;
			n = list[r].name;
			uid = (((1+Math.random())*0x100000)|0).toString(16).substring(1);
			scene.robots[n] = list[r];
			scene.robots[n].uid = uid;
			scene.robots[n].joints = []; //BABYLON.Skeleton(n, uid, scene.scene);
		}
	}	
	//console.log ("nbrobots:"+i);
	scene.robotsnb = i;
		
	// start with first
	if (firstname != "") {
		var msg = {}
		msg['msg'] = "get_robot_kinematic";
		msg['uid'] = scene.uid+"grk";
		msg['arg'] = firstname;
		msg['callback'] = "tpScene_on_get_robot_kinematic";
		tereoMain.call("sendMqttMsg",JSON.stringify(msg));
	}
	
}


function tpScene_on_get_robot_kinematic(l) {
	console.log("onplugin scene robotkine:"+l);
	//console.log(l);
	if (l == "get_robot_kinematic") { console.log ("echo"); return; }
	if (l.indexOf("@arg@")>-1) { console.log ("echo"); return; }
	
	scene = tereoObjects['scene'];
	
	delete cnx_callbacks[scene.uid+"grk"];
	
	try {
		l = l.replace(' ','');
		l = l.replace('\n','');
		l = l.replace('\r','');
		var list = JSON.parse(l);
	} catch(e) { console.log(e); return; }
	

	// next link
	found = false;
	nextlinkname ="";
	firstname="";
	i=0;
	for (r in scene.robots) {
		if (scene.robots.hasOwnProperty(r)) {
			if (firstname == "") { firstname = scene.robots[r].constructor+"."+scene.robots[r].model; }
			if (found == true && nextlinkname == "") {
				nextlinkname = scene.robots[r].name;
			}
			if (typeof scene.links[scene.robots[r].name] === 'undefined') {
				//console.log ("found link undefined:"+i)
				n = scene.robots[r].name;
				scene.links[n] = list;
				
				// make skeleton
	

				parentjoint = null;		
				
				for (ll in list) {
				
					if (list.hasOwnProperty(ll)) {
					
						console.log("making bone:");
						//console.log(list[ll]);
						ar = {};
						//.a  alpha d theta axis color 		
						//matr = BABYLON.Matrix.FromValues(1,0,0,0, 0,1,0,0, 0,0,1,0, 0,0,0,1 );//new BABYLON.Matrix();
							
						
						//matr.setTranslation(new BABYLON.Vector3(0,0,parseFloat(list[ll].d) ));

						/*
						var matr2 = new THREE.Matrix4();
						matr2.makeTranslation( 0, 0, parseFloat(list[ll].d) );							
						//console.log("after trans z");
						//console.log(JSON.stringify(matr2));
						
					
						//matr.multiplyToRef (BABYLON.Matrix.RotationX( parseFloat(list[ll].theta) ) , matr2 );
						var matr = new THREE.Matrix4();
						matr.makeRotationX( parseFloat(list[ll].theta) );						
						matr2.multiply(matr);						
						//console.log("after mult x");
						//console.log(JSON.stringify(matr2));						
						
						//matr2.setTranslation(new BABYLON.Vector3( parseFloat(list[ll].a) ,0,0));
						var matr = new THREE.Matrix4();	
						matr.makeTranslation( parseFloat(list[ll].a), 0, 0 );		
						matr2.multiply(matr);						
						//console.log("after transx");
						//console.log(JSON.stringify(matr2));
						
						//matr2.multiplyToRef (BABYLON.Matrix.RotationZ( parseFloat(list[ll].alpha) ), matr );
						matr = new THREE.Matrix4();	
						matr.makeRotationZ( parseFloat(list[ll].alpha) );
						matr2.multiply(matr);
						//console.log("after rotz");
						//console.log(JSON.stringify(matr2));
						
						
						//console.log("i:"+i);
						console.log("new bon:"+ n+i.toString());
						bone = new BABYLON.Bone(n+i.toString(), scene.robots[r].skeleton,parentbone, matr);
						//scene.robots[r].skeleton.bones.push(bone);
						
						
						*/
						matr2 = matrix44_calc_ldhm_matrix(list[ll].a,list[ll].alpha,list[ll].d,list[ll].theta);
						
						var vect = new THREE.Vector3( 1, 1, 1 );
						vect.setFromMatrixPosition(matr2);
						//vect = matr2.applyToVector3Array(vect);
						
						//console.log("vect");
						//console.log(vect);
						
						joint = new THREE.Object3D();
						
						
						/*joint.position.z = vect.x/25.9;
						joint.position.y = vect.y/25.9;
						joint.position.y = -vect.z/25.9;*/
						
						//console.log("translate:");
						//console.log(list[ll].tranlate);
						
						joint.position.y = parseFloat(list[ll].tranlate["z"]);
						joint.position.x = parseFloat(list[ll].tranlate["x"]);
						joint.position.z = -parseFloat(list[ll].tranlate["y"]);
						
						//console.log("joint.position:");
						//console.log(joint.position);
						jaxis = "";
						
						
						
						if (list[ll].axis["x"] == "1") {
							
							jaxis = 'x'; // x axis is controlled
						} else if (list[ll].axis["y"] == "1") {
						
							jaxis = 'z'; // y axis is controlled
						} else if (list[ll].axis["z"] == "1") {
						
							jaxis = 'y'; // z axis is controlled
						}
						
						
						scene.robots[r].joints.push(joint);
						scene.joints_axis.push(jaxis);
						
						//console.log(parentjoint);
						if (typeof parentjoint !== "undefined" && parentjoint != null) {
							parentjoint.add(joint);
						}
						
						parentjoint = joint;
						
						i=i+1;
					
					}
					
				}
				
				found = true;
			}
			//console.log(scene.robots[r].joints);
			
		}
		
	}	
	
	console.log("========== scene ===========");
	//console.log(scene.scene);
	
	// next robot ?
	if (nextlinkname != "") {
	
		//console.log ("next link");
		var msg = {}
		msg['msg'] = "get_robot_kinematic";
		msg['uid'] = scene.uid+"grk";
		msg['arg'] = nextlinkname;
		msg['callback'] = "tpScene_on_get_robot_kinematic";	
		tereoMain.call("sendMqttMsg",JSON.stringify(msg));
		
	} else {
	
		//console.log ("no next link");
		// mesh list
		var msg = {}
		msg['msg'] = "get_robot_mesh_list";
		msg['uid'] = scene.uid+"grml";
		msg['arg'] = firstname
		msg['callback'] = "tpScene_on_get_robot_mesh_list";
		tereoMain.call("sendMqttMsg",JSON.stringify(msg));
	}

}


function tpScene_on_get_robot_mesh_list(l) {

	console.log("onplugin scene robotmeshlist:"+l);
	//console.log(l);
	if (l == "get_robot_mesh_list") { console.log ("echo"); return; }
	if (l.indexOf("@arg@")>-1) { console.log ("echo"); return; }
	
	scene = tereoObjects['scene'];
	
	delete cnx_callbacks[scene.uid+"grml"];
	
	
	try {
		//l = l.replace(' ','');
		l = l.replace('\n','');
		l = l.replace('\r','');
		var list = JSON.parse(l);
	} catch(e) { console.log(e); return; }
	

	//console.log(list);

	// next list
	found = false;
	nextlinkname ="";
	firstname = "";
	i=0;
	for (r in scene.robots) {
		if (scene.robots.hasOwnProperty(r)) {
			if (firstname == "") { firstname = scene.robots[r].name; }
			if (found == true && nextlinkname == "") {
				nextlinkname = scene.robots[r].name;
			}
			if (typeof scene.meshes_link[scene.robots[r].name] === 'undefined') {
				//console.log ("found link undefined:"+i)
				n = scene.robots[r].name;
				scene.meshes_link[n] = list;
								
				found = true;
			}
		}
		i=i+1;
	}	
	
	// next list
	if (nextlinkname != "") {
		//console.log ("no next link");
		// mesh list
		var msg = {}
		msg['msg'] = "get_robot_mesh_list";
		msg['uid'] = scene.uid+"grml";
		msg['arg'] = firstname
		msg['callback'] = "tpScene_on_get_robot_mesh_list";
		tereoMain.call("sendMqttMsg",JSON.stringify(msg));
	} 
	else
	{
	
		if (firstname != "") { 
		
			// first mesh
			/*if (typeof scene.meshes_link[firstname]["0"] !== "undefined") {
				scene.lastlink = scene.meshes_link[firstname]["0"];*/
				scene.lastrobot="";
				scene.lastlink="";
				tpScene_on_get_robot_mesh();
			//} else { console.log("undefined"); }
		
		}
	}

	
}



function tpScene_on_get_robot_mesh() {

	console.log("onplugin scene robotmesh:");
	
	
	
	scene = tereoObjects['scene'];
	
	//console.log("lastlink:"+scene.lastlink+" , lastrobot:"+scene.lastrobot);
	
	var rob="";
	var l="";
	var robfound = false;
	var lfound = false;
	
	//find next link
	if (scene.lastrobot=="") {
		
		for (r in scene.robots) {
			if (scene.robots.hasOwnProperty(r)) {
				if (robfound== false) {
					robfound=true;
					rob = r;
				}
			}
		}
		if (scene.lastlink=="") {
			if (rob != "") {
				l = "0"; //scene.meshes_link[firstname]["0"];
				lfound = true;
			}
		}
	}
	else
	{
		rfound=false;
		rfound2=false;
		for (r in scene.robots) {
			if (scene.robots.hasOwnProperty(r)) {
				if (robfound== false) {
					if (r = scene.lastrobot) {
						//console.log("1 robfound");
						robfound == true;
						rob = r;
						i=0;
						for (ml in scene.meshes_link[scene.robots[r].name]) {
							
							//if (scene.meshes_link.hasOwnProperty(ml)) {
							
								//console.log("link:"+ml);
								if ( typeof scene.meshes[scene.robots[r].name] != "undefined") {
								
									if (typeof scene.meshes[scene.robots[r].name][i.toString()] != "undefined") {
										//console.log("2 lfound");
										if (rfound == false) {
											if (i.toString() == scene.lastlink) { rfound = true; console.log("3 lfound"); }
										} 									
										
									}
									else {
								
										
											//console.log("4a next");
											if (rfound2 == false) {
												console.log("4 next l found");
												l = i.toString();
												lfound =true;
												rfound2 = true;
											}
											
								
									}
								
								} 
								
							
							//}
							i=i+1;
						}
						// depacement
						
					}
				} 
			}
		}		

	}
	
	if (lfound == false || robfound == false) {
		console.log("end");
	}
	
	console.log("rob: "+rob+" link:"+l);
	
	if (l == "" || rob == "") {
		console.log("end");
		return;
	}
	
	scene.lastlink = l;
	scene.lastrobot = rob;

	//console.log(scene.robots[scene.lastrobot].skeleton);

			
	if (typeof scene.meshes[scene.robots[rob].name] === 'undefined') {
		scene.meshes[scene.robots[rob].name] = {};
	}
	
	scene.meshes[scene.robots[rob].name][l] = "ok";
	
	
	//console.log(scene.meshes_link[scene.robots[rob].name]);
	url = "http://192.168.0.168:62355/robots/"+scene.meshes_link[scene.robots[rob].name][parseInt(l)].constructor+"/"+scene.meshes_link[scene.robots[rob].name][parseInt(l)].model+"/";
	url = url.replace (" ","%20");
	

	
	//console.log(url + scene.meshes_link[scene.robots[rob].name][parseInt(l)].FILE);

	
	var manager = new THREE.LoadingManager();
	manager.onProgress = function ( item, loaded, total ) {

		console.log( item, loaded, total );

	};
				
	urlc = url + scene.meshes_link[scene.robots[rob].name][parseInt(l)].FILE;
	urlc = urlc.replace('babylon','obj');
	urlc = urlc.replace('link','kuka_link');
	urlc = urlc.replace('base','kuka_base');
	
	var loader = new THREE.OBJLoader( manager );
	loader.load( urlc, function ( object ) {

		console.log("mesh loaded");
		/*object.traverse( function ( child ) {

			if ( child instanceof THREE.Mesh ) {

				child.material.map = texture;

			}

		} );*/

		//object.position.y = - 80;
		scene = tereoObjects['scene'];
		
		
		/*mat = new THREE.MeshPhongMaterial( { 
			color: 0x996633, 
			ambient: 0x996633, // should generally match color
			specular: 0x050505,
			shininess: 100
		} ) */
		
			var mcolor = '#00abb1';
			
			if (typeof scene.links[scene.robots[scene.lastrobot].name] != "undefined") {
				console.log("link:");
				if (typeof scene.links[scene.robots[scene.lastrobot].name][scene.lastlink] != "undefined") {
					console.log(scene.links[scene.robots[scene.lastrobot].name][scene.lastlink]);
					col = scene.links[scene.robots[scene.lastrobot].name][scene.lastlink].color;
					//mcolor = "#"+(parseInt(col["r"]*255)).toString(16) + (parseInt(col["g"]*255)).toString(16) + (parseInt(col["b"]*255)).toString(16);
					
					mcolor = new THREE.Color(col["r"], col["g"],col["b"] );
					
				}
			}
		
		scene.scene.add( object,
		
			new THREE.MeshPhongMaterial({
				// light
				specular: '#09fcff',
				// intermediate
				color: mcolor,
				// dark
				emissive: '#006063',
				shininess: 100 
			})

		);
		
		
		if (parseInt(scene.lastlink) == 0) {
			object.add (scene.robots[scene.lastrobot].joints[0]); 			
		} else {			
		
			last = parseInt(scene.lastlink);
		
			//console.log(scene.lastrobot);
			//console.log(scene.robots);
			
			//lastjoint = scene.robots[scene.lastrobot].joints.pop();
			//scene.robots[scene.lastrobot].joints.push(lastjoint);
		
			scene.robots[scene.lastrobot].joints[last].add(object);
			
			// adding joint for arm1 
			//dummy.addChild(joint);
		}
		//scene.meshes3D.push(joint);
		//scene.lastmesh3D = object;
		
		scene.editor.signals.objectAdded.dispatch( object );
		scene.editor.signals.sceneGraphChanged.dispatch();
		
		
		// next link
		tpScene_on_get_robot_mesh();
		
		


	} );
	
}


function matrix44_calc_ldhm_matrix(a,alpha,d,theta) {


	alpha = alpha*Math.PI / 180;
	theta = theta*Math.PI / 180;
	c = Math.cos(theta);
	s = Math.sin(theta);
	ca = Math.cos(alpha);
	sa = Math.sin(alpha);

	//console.log("alpha:"+alpha+"theta:"+theta);
	
	m = new THREE.Matrix4(c, -s, 0, a,    s*ca, c*ca, -sa, -sa*d, s*sa, c*sa, ca, ca*d, 0, 0, 0, 1);

	return m;

}



function tpScene_update_robots(arg) {

	//console.log("update robot");

	var dirty = false;
	console.log(arg);
	scene = tereoObjects['scene'];
	
	try {
		//arg = arg.replace(' ','');
		arg = arg.replace('\n','');
		arg = arg.replace('\r','');
		var list = JSON.parse(arg);
		list = list["position"];
	} catch(e) { console.log(e);
		return;
	}
	
	

	if (typeof scene.robots === "undefined") {
		
		return;
	}
	
	//console.log("list")
	//console.log(list);
	
	for (r in scene.robots) {
	
	
		if (scene.robots.hasOwnProperty(r)) {
		
			if (typeof list.name !== "undefined") {
			
				n = scene.robots[r].name;
				//console.log(scene.links[n]);
				//console.log(list.name);
			
				if (list.name == n) {
					//console.log("ok");
					if (typeof scene.links[n] !== "undefined") {
						
						//console.log("ok2");
						for (l in scene.links[n]) {
						
							//console.log("ok3");
							ax = scene.links[n][l].axislink;
							//console.log("ax:"+ax);
							if (typeof list[ax] !== "undefined") {
							
								//console.log("ok4");
								
							
								j = scene.robots[r].joints[l];
															
								
								//console.log(j);
								//console.log(scene.joints_axis);
								
								if (scene.joints_axis[l] == "x") {
									j.rotation.x = list[ax]*Math.PI/180;
									dirty = true;
									//console.log("j rot x"+list[ax]);
								} else if (scene.joints_axis[l] == "y") {
									j.rotation.y = list[ax]*Math.PI/180;
									dirty = true;
									//console.log("j rot y"+list[ax]);
								} else if (scene.joints_axis[l] == "z") {
									j.rotation.z = list[ax]*Math.PI/180;
									dirty = true;
									//console.log("j rot z"+list[ax]);
								}
								
								//console.log("joint:");
								//console.log(scene.robots[r].joints[l]);
							
							}
						
							
						}
								
					}
				
				}
			
			}
			
		} 
	}
			
	//tereoMain.broadcast("stat joint_position");
	if (dirty == true) { scene.editor.signals.sceneGraphChanged.dispatch(); }
	

}