# -*- coding: utf-8 -*-

def on_configwin_flow(widget,parent):

	parent.flowwin.show()
	screen = parent.flowwin.get_screen()
	parent.flowwin.resize(screen.get_width(),screen.get_height())
	parent.flowwin_init()

	return
