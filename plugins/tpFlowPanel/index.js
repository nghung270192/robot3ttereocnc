var tpFlowPanel;

var tpFlowPanel = function tpFlowPanel() {

	this.pluginName = "FlowPanel";
	this.pluginVersion = "0.1";
	this.pluginAuthor = "Olivier Martinez";
	
};

tpFlowPanel.prototype = {

	"register":function(parent) {

		/*mod = imp.load_source('tereocnc.plugins.flow_panel', './plugins/flow_panel/flow_panel.py')

		# flow panel button
		img = gtk.Image()
		img.set_from_file('./plugins/flow_panel/flow.png')
		img.show()
		flow_icon = gtk.ToggleToolButton ("Flow")
		flow_icon.set_icon_widget(img)
		flow_icon.set_active(False)
		flow_icon.set_label("Flow")
		flow_icon.connect("toggled", mod.on_flow_toggle, parent)*/
		var msg ={};
		msg['msg'] = "FLOW";
		parent.add_icon('Flow','plugins/tpFlowPanel/flow.png', true, "sendMqttMsg",msg,"tpFlowPanel");

	}
	
}

tereoPlugins['tpFlowPanel'] = new tpFlowPanel();
