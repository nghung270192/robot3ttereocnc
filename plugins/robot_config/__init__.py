# -*- coding: utf-8 -*-


import os.path


pluginName = "Robot config"
version = "0.1"
author = "Olivier Martinez"



def register(parent):

	mod = imp.load_source('robot_config', './plugins/robot_config/robot_config.py')
	parent.add_function("robot.config","Robot config",mod.on_robotconfig)
	return
