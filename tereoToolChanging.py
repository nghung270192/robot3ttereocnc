#!/usr/bin/python

import pygtk
pygtk.require('2.0')
import gtk
#import gtk.gdkgl as gdkgl
#import gtk.gtkgl as gtkgl

############## TOOL CHANGING ###############
class toolChangingObj():
	def __init__(self):
		self.PRIMITIVE_SQUARE = 0
		self.PRIMITIVE_ARC = 1
		self.PRIMITIVE_CIRCLE = 2
		self.PRIMITIVE_MANUAL = 3
		self.primitive = self.PRIMITIVE_MANUAL
		self.diameter = 0.0
		self.length = 0.0
		self.height = 0.0
		self.width = 0.0
		self.cols = 0
		self.rows = 0
		self.group = ''
		self.name = ''
		self.homedir = ''
		return

	def get_from_db(self, db):

		t = db.find('TOOLCHANGING')
		if t:
			self.group = t.findtext('GROUP')
			self.name = t.findtext('NAME')
			p = t.findtext('DIAMETER')
			if p: self.diameter = float(p)
			p = t.findtext('LENGTH')
			self.length = float(p)
			p = t.findtext('HEIGHT')
			self.height = float(p)
			p = t.findtext('WIDTH')
			self.width = float(p)
			p = t.findtext('COLS')
			self.cols = int(p)
			p = t.findtext('ROWS')
			self.rows = int(p)
			p = t.findtext('PRIMITIVE')
			if p == 'SQUARE':
				self.primitive = self.PRIMITIVE_SQUARE
			elif p == 'ARC':
				self.primitive = self.PRIMITIVE_ARC
			elif p == 'CIRCLE':
				self.primitive = self.PRIMITIVE_CIRCLE
			elif p == 'MANUAL':
				self.primitive = self.PRIMITIVE_MANUAL
		print "return get from db"
		return

	def set_to_db(self, db):
		t = db.find('TOOLCHANGING')
		if t:
			t.set("GROUP", self.group)
			t.set("NAME",	self.name)
			t.set("DIAMETER", str(self.diameter))
			t.set("LENGTH", str(self.length))
			t.set("HEIGHT", str(self.height))
			t.set("WIDTH", str(self.width))
			t.set("COLS", str(self.cols))
			t.set("ROWS", str(self.rows))


			if self.primitive == self.PRIMITIVE_SQUARE:
				t.set('PRIMITIVE', 'SQUARE')

			elif self.primitive == self.PRIMITIVE_ARC:
				t.set('PRIMITIVE', 'ARC')

			elif self.primitive == self.PRIMITIVE_CIRCLE:
				t.set('PRIMITIVE', 'CIRCLE')

			elif self.primitive == self.PRIMITIVE_MANUAL:
				t.set('TOOL_CHANGER_PRIMITIVE', 'MANUAL')


		return


	def get_from_file(self):

		try:
			with open(self.homedir + '/' + self.group + '/' + self.name, 'r') as f:
				toolfile = f.readlines()
				for line in toolfile:
					line = line.rstrip('\r')
					line = line.rstrip('\n')
					line = line.rstrip('\r')
					print line
					word_list = line.split(' ',1)
					word = word_list[0]

					if len(word_list) > 1:
						arg = word_list[1]
						arg = arg.strip()
					else:
						arg = ''

					if word == 'DIAMETER':
						self.diameter = float(arg)
					elif word == 'LENGTH':
						self.length = float(arg)
					elif word == 'HEIGHT':
						self.height = float(arg)
					elif word == 'WIDTH':
						self.width = float(arg)
					elif word == 'COLS':
						self.cols = int(arg)
					elif word == 'ROWS':
						self.rows = int(arg)
					elif word == 'PRIMITIVE':
						if arg == 'SQUARE':
							self.primitive = self.PRIMITIVE_SQUARE
						elif arg == 'ARC':
							self.primitive = self.PRIMITIVE_ARC
						elif arg == 'CIRCLE':
							self.primitive = self.PRIMITIVE_CIRCLE
						elif arg == 'MANUAL':
							self.primitive = self.PRIMITIVE_MANUAL


			return;
		except (UnicodeError, LookupError, IOError):
			print "coult not decode file"
		pass

		return

	def showparams(self,widget):
		widget.clear()
		if self.primitive == self.PRIMITIVE_SQUARE:
			widget.append(('Primitive','Square'))
		elif self.primitive == self.PRIMITIVE_ARC:
			widget.append(('Primitive','Arc'))
		elif self.primitive == self.PRIMITIVE_CIRCLE:
			widget.append(('Primitive','Circle'))
		elif self.primitive == self.PRIMITIVE_MANUAL:
			widget.append(('Primitive','Manual'))
			return
		widget.append(('Length',self.length))
		widget.append(('Width',self.width))
		widget.append(('Height',self.height))
		widget.append(('Diameter',self.diameter))
		widget.append(('Cols',self.cols))
		widget.append(('Rows',self.rows))
		return

	def draw(self,area):
		print "draw"
		if self.primitive == self.PRIMITIVE_MANUAL:
			return

		drawable = area.window
		style = area.get_style()
		cg = style.fg_gc[gtk.STATE_NORMAL]

#		cg = drawable.new_gc(gtk.gdk.Color(65535,65535,65535), gtk.gdk.Color(0,0,0),
#							 None, -1, -1, None, None, None, -1, -1, -1,
#							 -1, -1, -1, -1, -1, -1, -1)

		dsize = drawable.get_size()
		w = dsize[0]
		h = dsize[1]

		cg.set_rgb_fg_color(gtk.gdk.Color(65535,65535,65535))
		drawable.draw_rectangle (cg, True, 0,0, w, h)

		print "w:%d"%(w)
		print "h:%d"%(h)

		hole_size = 0
		if (w/self.cols) < (h/self.rows):
			hole_size = int((w-10)/self.cols)
		else:
			hole_size = int((h-10)/self.rows)

		print "hole size:%d"%(hole_size)

		cg.set_rgb_fg_color(gtk.gdk.Color(0,0,0))
		drawable.draw_rectangle (cg, False, 5,5,hole_size * self.cols,hole_size * self.rows)
		for i in range(1,self.cols):
			drawable.draw_line( cg, 5 + i * hole_size, 5, 5 + i * hole_size, 5 + self.rows * hole_size)
		for i in range(1,self.rows):
			drawable.draw_line( cg, 5, 5 + i * hole_size, 5 + self.cols * hole_size, 5 + i * hole_size)

		#fnt = gtk.gdk.Font("-*-helvetica-bold-r-normal--*-120-*-*-*-*-*")

		k = 0
		for i in range(0,self.cols):
			for j in range(0,self.rows):
				k = k + 1
				drawable.draw_arc( cg, False, 5 + i * hole_size + int(hole_size/8), 5 + j * hole_size + int(hole_size/8), hole_size - int(hole_size/4), hole_size - int(hole_size/4), 0, 360*64)
				drawable.draw_text( fnt , cg, 5 + i * hole_size + int(hole_size/2)-5, 5 + j * hole_size + int(hole_size/2)+5, "%d"%k)


		return

