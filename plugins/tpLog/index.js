var tpLog;

var tpLog = function tpLog() {

	this.pluginName = "Log";
	this.pluginVersion = "0.1";
	this.pluginAuthor = "Olivier Martinez";
	this.panel;
	
};

tpLog.prototype = {

	"register":function(parent) {

		var msg ={};
		msg['msg'] = "LOG";
		parent.add_icon('Log','plugins/tpLog/log.png', true, "@tpLog&on_log_toggle",msg,"tpLog");
		
	},
	
	"on_log_toggle":function() {
	
		console.log("on log toggle");
		
		var f = $("#log_panel");
		if(f.length){
		
			//L'élément existe
			if ($("#tpLogLog").hasClass("active")) {
				$("#log_panel").hide();
			} else {
				$("#log_panel").show();
			}
		  
		} else {
		
			//L'élément n'existe pas
			this.panel = new logPanelView();
			this.panel.__init__();
			$("#log_panel").show();
			
		}
	
	
	},

	"unload":function() {
	
	}
	

	
};

tereoPlugins['tpLog'] = new tpLog();



var logPanelView = function() {

	this.uid = (((1+Math.random())*0x100000)|0).toString(16).substring(1);
	this.lastmsg ="";
	this.logs = {};


}

logPanelView.prototype = {

	"__init__": function () {

		$( "#tereopanels").append('<div class="tpPanel" id="log_panel"><div class="tpPanelTitle">Logs panel</div><div><button class="form_button" onclick="tpLog_on_clear();"><img src="media/delete.png">Clean</button></div><div id="logPanelScroll" class="tpScroll"><table id="log_list" border=1></table></div></div>');

	}
	
}


function tpLog_on_new_log() {
/*
		if self.sw != None:
			self.sw.new_log(tmp,ico,sec,desc)
*/

	//append to log_list <tr><td></td></tr>
	
	/*
	def new_log(self,tmp,ico,sec,desc):

		if ico == 0:
			icol = 'gtk-dialog-info'
		elif ico == 1:
			icol = 'gtk-dialog-warning'
		else :
			icol = 'gtk-dialog-error'

		self.lvs.append((tmp,icol,sec,desc))
		
				col1 = gtk.TreeViewColumn('') txt
		col2 = gtk.TreeViewColumn('') ico
		col3 = gtk.TreeViewColumn('Section')
		col4 = gtk.TreeViewColumn('Description')

		self.tereo.add_object('log.listener','Log win',self.new_log)





		return
*/
	

}





