# -*- coding: utf-8 -*-

import gtk
import os.path
import imp


pluginName = "Command"
version = "0.1"
author = "Olivier Martinez"
estop_icon = None
start_icon = None


def register(parent):

	mod = imp.load_source('command', './plugins/command/command.py')

	# add icons


	# estop
	img = gtk.Image()
	img.set_from_file('./plugins/command/estop.png')
	estop_icon = gtk.ToggleToolButton ("E-Stop")
	img.show()
	estop_icon.set_icon_widget(img)
	estop_icon.set_label("E-Stop")
	estop_icon.connect("toggled", mod.on_estop_toggled, parent)
	estop_icon.set_active(True)
	parent.add_icon(estop_icon)

	# start
	img = gtk.Image()
	img.set_from_file('./plugins/command/start.png')
	img.show()
	start_icon = gtk.ToggleToolButton ("I/O")
	start_icon.set_icon_widget(img)
	start_icon.set_label("I/O")
	start_icon.connect("toggled", mod.on_start_toggled, None)
	start_icon.set_active(False)
	parent.add_icon(start_icon)

	return


def unload(parent):

	return

