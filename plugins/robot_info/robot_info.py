# -*- coding: utf-8 -*-


import os.path

from plugins.robot_info import robot_info_window


def on_robotinfo_activate(parent,idx):

	f = parent.object_search_path("robot.info")

	if f == []:
		f = robot_info_window.robot_info_window(parent,idx)
		parent.add_object("robot.info","Robot info",f)
		f.show()
	else:
		for frm in f:
			if frm != None:
				print frm
				f=frm[1]
				print f
				f.show()
				return

	return