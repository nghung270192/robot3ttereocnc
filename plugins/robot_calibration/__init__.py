# -*- coding: utf-8 -*-

import gtk
import os.path
import imp


pluginName = "Robot calibration"
version = "0.1"
author = "Olivier Martinez"



def register(parent):


	mod = imp.load_source('robot_calibration', './plugins/robot_calibration/robot_calibration.py')

	parent.add_function("robot.calibration","Calibrate",mod.calibrate)



	return


def unload(parent):

	return

