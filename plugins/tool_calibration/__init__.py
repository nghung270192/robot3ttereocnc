# -*- coding: utf-8 -*-

import gtk
import os.path
import imp


pluginName = "Tool calibration"
version = "0.1"
author = "Olivier Martinez"



def register(parent):


	mod = imp.load_source('tool_calibration', './plugins/tool_calibration/tool_calibration.py')

	parent.add_function("tool.calibration","Calibration 2 points",mod.calc_tool_matrix_2points)
	parent.add_function("tool.calibration","Calibration 4 points",mod.calc_tool_matrix_2points)




	return


def unload(parent):

	return

