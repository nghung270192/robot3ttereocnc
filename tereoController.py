
import tereoMath


class axis_coordinate_point:

	def __init__(self):
		self.position = 0.0
		self.velocity = 0.0
		self.acceleration = 0.0
		self.effort = 0.0
		return

class coordinate_point:

	def __init__(self):
		self.frame = tereoMath.frame6()
		self.axis_coordinate_points = [] # axis_coordinate_point
		self.axis_names = [] # str
		return





class controller:

	def __init__(self,tereo):

		self.tereo = tereo
		self.stop_motion_handler = []
		self.get_current_position_handler = []
		self.get_target_position_handler = []
		self.get_state_error_handler = []
		self.get_state_mode_handler = []
		self.get_state_estop_handler = []
		self.get_state_power_handler = []
		self.get_network_status_handler = []

		self.command_point_path = [] #coordinate_point
		self.state_current_position = None #coordinate_point
		self.state_target_position = None #coordinate_point

		self.state_error_code = 0
		self.state_error_description = ""
		self.state_mode = -1 # undetermined
		self.power = False
		self.estop = False

		self.network_connected = False
		self.network_connecting = False
		self.network_error = ""

		return


	def add_network_status_handler(self,handler):
		self.get_network_status_handler.append(handler)
		return

	def update_network_status(self):
		for h in self.get_network_status_handler:
			h(self.network_connected,self.network_connecting,self.network_error)
		return

	#------------------------------------------------


	# p = bool
	def power(self,p):
		return

	#-----------------------------------------------


	# es = bool
	def estop(self,es):
		return

	#------------------------------------------------


	def stop_motion(self):
		#send to robot
		return

	def add_stop_motion_handler(self,handler):
		self.stop_motion_handler.append(handler)
		return

	def remove_stop_motion_handler(self,handler):
		self.stop_motion_handler.remove(handler)
		return

	def update_stop_motion(self):
		for h in self.stop_motion_handler:
			h()
		return

	#---------------------------------------------

	def set_new_path(self,points):
		#todo : if current path not complete : stop and start new
		#todo : if power off  : power on
		self.command_point_path = points
		return


	#---------------------------------------------

	def get_current_position(self):
		#send request to robot
		return

	def add_current_position_handler(self,handler):
		self.get_current_position_handler.append(handler)
		return

	def remove_current_position_handler(self,handler):
		self.get_current_position_handler.remove(handler)
		return

	def update_current_position(self):
		for h in self.get_current_position_handler:
			h(self.state_current_position)
		return

	#---------------------------------------------

	def get_target_position(self):
		#send request to robot
		return

	def add_target_position_handler(self,handler):
		self.get_target_position_handler.append(handler)
		return

	def remove_target_position_handler(self,handler):
		self.get_target_position_handler.remove(handler)
		return

	def update_target_position(self):
		for h in self.get_target_position_handler:
			h(self.state_target_position)
		return

	#---------------------------------------------

	def get_state_error(self):
		#send request to robot
		return

	def add_state_error_handler(self,handler):
		self.get_state_error_handler.append(handler)
		return

	def remove_state_error_handler(self,handler):
		self.get_state_error_handler.remove(handler)
		return

	def update_state_error(self):
		for h in self.get_state_error_handler:
			h(self.state_error_code,self.state_error_description)
		return

	#-------------------------------------------

	def get_state_mode(self):
		#send request to robot
		return

	def add_state_mode_handler(self,handler):
		self.get_state_mode_handler.append(handler)
		return

	def remove_state_mode_handler(self,handler):
		self.get_state_mode_handler.remove(handler)
		return

	def update_state_mode(self):
		for h in self.get_state_mode_handler:
			h(self.state_mode)
		return

	#--------------------------------------------

	def get_state_estop(self):
		#send request to robot
		return

	def add_state_estop_handler(self,handler):
		self.get_state_estop_handler.append(handler)
		return

	def remove_state_estop_handler(self,handler):
		self.get_state_estop_handler.remove(handler)
		return

	def update_state_estop(self):
		for h in self.get_state_estop_handler:
			h(self.estop)
		return

	#------------------------------------------

	def get_state_power(self):
		#send request to robot
		return

	def add_state_power_handler(self):
		self.get_state_power_handler.append(handler)
		return

	def remove_state_power_handler(self):
		self.get_state_power_handler.remove(handler)
		return

	def update_state_power(self):
		for h in self.get_state_power_handler:
			h(self.power)
		return

