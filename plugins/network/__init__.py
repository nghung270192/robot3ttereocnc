# -*- coding: utf-8 -*-


import os.path


pluginName = "Network"
version = "0.1"
author = "Olivier Martinez"
net_icon = None



def register(parent):

	mod = imp.load_source('network', './plugins/network/network.py')

	img = gtk.Image()
	img.set_from_file('./plugins/network/network.png')
	img.show()
	net_icon = gtk.ToggleToolButton ("Network")
	net_icon.set_icon_widget(img)
	net_icon.set_label("Network")
	net_icon.connect("toggled", mod.on_network_toggle, parent)
	net_icon.set_active(False)
	parent.add_icon(net_icon)

	return

