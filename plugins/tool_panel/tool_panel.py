# -*- coding: utf-8 -*-


import os.path

from plugins.tool_panel.tool_panel_view import tool_panel_view


def on_tool_toggle(widget,parent):

	f = parent.object_search_path("tool.panel")

	if f == []:
		f = tool_panel_view(parent)
		parent.add_object("tool.panel","Tool panel",f)
	else:
		f=f[0]
		f=f[1]

	parent.detach_panel(f)
	f.show()

	if widget.get_active() == True:

		parent.attach_panel(f)
		f.populate()

	return

