# -*- coding: utf-8 -*-

import pygtk
pygtk.require("2.0")
import gtk


class flow_panel_view(gtk.Frame):

	def __init__(self,tereo):

		gtk.Frame.__init__(self)

		self.tereo = tereo

		self.set_label_align(0,0)


		al = gtk.Alignment(0.5, 0.5, 1.0, 1.0)
		al.set_padding(0,0,12,0)

		self.set_label("Flow panel")

		self.add(al)


		al.show()

		sv = gtk.ScrolledWindow()

		al.add(sv)

		sv.show()

		vp = gtk.Viewport()
		sv.add(vp)
		vp.show()


		f = gtk.Frame()
		vp.add(f)

		f.show()

		self.vbox = gtk.VBox(False, 0)
		f.add(self.vbox)
		self.vbox.show()
		self.set_size_request(300,-1)
		self.show()

		self.populate()

		return


	def populate(self):

		#print "populate flow"

		# todo clear list
		if self.tereo.env == None:
			print "env null"
			return

		children = self.vbox.get_children()
		for iter in children:
			subchildren = iter.get_children()
			for subiter in subchildren:
				subiter.destroy()
			iter.destroy()


		img = gtk.Image()
		img.set_from_file('./media/add.png')
		img.show()
		ln = gtk.Button("New")
		ln.set_image(img)
		ln.show()
		self.vbox.pack_start(ln,False,True,0)
		#ln.connect("clicked", self.on_flow_new)


		i = 0

		for f in self.tereo.env.flow:

			# new frame
			frm = gtk.Frame()
			self.vbox.pack_start(frm,False,True,0)
			frm.show()

			tbl = gtk.Table(4,3,False)
			frm.add(tbl)
			tbl.show()

			ln = gtk.Label(str(i))
			ln.set_justify(gtk.JUSTIFY_RIGHT)
			ln.show()
			tbl.attach(ln,0,1,0,1)

			ln = gtk.Label(f.name)
			ln.show()
			tbl.attach(ln,1,2,0,1)

			ln = gtk.Label("Color")
			ln.set_justify(gtk.JUSTIFY_RIGHT)
			ln.show()
			tbl.attach(ln,0,1,1,2)

			ln = gtk.ColorButton(gtk.gdk.Color(f.color[0],f.color[1],f.color[2]))
			ln.show()
			tbl.attach(ln,1,2,1,2)

			ln = gtk.Label("Material")
			ln.set_justify(gtk.JUSTIFY_RIGHT)
			ln.show()
			tbl.attach(ln,0,1,2,3)

			ln = gtk.Button(f.material)
			ln.show()
			tbl.attach(ln,1,2,2,3)

			# ----------------------------------

			lcal = self.tereo.function_search_path("flow.calibration")
			if lcal != []:

				img = gtk.Image()
				img.set_from_file('./media/calibrate.png')
				img.show()
				ln = gtk.Button("Calib.")
				ln.set_image(img)
				ln.show()
				tbl.attach(ln,0,1,3,4)
				#ln.connect("clicked", self.on_robot_calibrate, i)

				slist = []
				defau = ""
				for it in lcal:
					if defau == "":
						defau = it[0]
					slist.append(it[0])

				self.combo1 = gtk.Combo()
				self.combo1.set_popdown_strings(slist)
				self.combo1.entry.set_text(defau)
				tbl.attach(self.combo1, 1, 2, 3, 4)
				self.combo1.show()

 			i+=1


		return


	def on_flow_calibrate(self,widget,data):

		# calibration variables
		co = self.tereo.new_calibration_obj()
		co.calib_title = "Flow calibration"
		co.calib_nb_question = 3
		co.calib_questions = ['Go to the XY corner of the flow and press Next','Move along the X axis and press Next','Move along the Y axis and press End']
		co.calib_robot_vars = ['$POS_ACT','$POS_ACT','$POS_ACT']
		co.calib_nb_img = 3
		co.calib_imgs = ['flowcalib1.png','flowcalib2.png','flowcalib3.png']
		co.calib_handler = self.on_flow_calibrate_return
		co.calib_tag = data

		co.calib_win_init()

		return

	def on_flow_calibrate_return(self,co):

		if co.calib_ret == None:
			return

		if len(co.calib_ret) == 0:
			return

		o = co.calib_ret[0]
		x = co.calib_ret[1]
		y = co.calib_ret[2]

		#print "result 3 points:"

		fo = frame6(o.x,o.y,o.z,o.a,o.b,o.c)
		fx = frame6(x.x,x.y,x.z,x.a,x.b,x.c)
		fy = frame6(y.x,y.y,y.z,y.a,y.b,y.c)

		fo.dump()
		fx.dump()
		fy.dump()

		self.tereo.env.flow[co.calib_tag-1].calibrate_3points(fo,fx,fy)


		return

	def on_flow_calibrate(self,widget):
		return




