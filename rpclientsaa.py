
import rpc
import socket
import collections
import Queue
import gobject
from time import sleep
from tereoMath import *

KUKAPROG = 0x20000002
KUKAVERS = 1

##static struct timeval TIMEOUT = { 25, 0 };


INITIALIZE = 1
UNINITIALIZE = 2
GETVAR = 11
SETVAR = 12
GETCROSSERROR = 31

#kuka type
KUKA_UNKNOWN = 0x0000
KUKA_SIMPLE = 0x0001
KUKA_INT = 0x0011
KUKA_REAL = 0x0021
KUKA_BOOL = 0x0031
KUKA_CHAR = 0x0041
KUKA_STRING = 0x0051
KUKA_STRUCT = 0x0002
KUKA_AXIS = 0x0012
KUKA_FRAME = 0x0022
KUKA_POS = 0x0032
KUKA_ERROR = 0x000a
KUKA_TABLO = 0X00cc


KUKA_FCT_POS = 1
KUKA_FCT_SPEED = 2
KUKA_FCT_PRE = 3
KUKA_FCT_DEPRE = 4
KUKA_AXES = 5
KUKA_FCT_NAIL = 6
KUKA_FCT_CTC = 7
KUKA_FCT_PROBE = 8
KUKA_FCT_PROBE_AXIS = 9


# kuka error type
KUKA_KUKA_E = 0x001a
KUKA_CROSS_E = 0x002a
KUKA_RPC_E = 0x003a
KUKA_API_E = 0x004a

KUKA_STRING_LEN = 128

KUKA_VARNAME_LEN = 14


class kukaAxis_t():
	def __init__(self):
		self.a1 = 0.0 #kukaReal_t
		self.a2 = 0.0
		self.a3 = 0.0
		self.a4 = 0.0
		self.a5 = 0.0
		self.a6 = 0.0


class kukaFrame_t():
	def __init__(self):
		self.x = 0.0 #kukaReal_t
		self.y = 0.0
		self.z = 0.0
		self.a = 0.0
		self.b = 0.0
		self.c = 0.0


class kukaPos_t():
	def __init__(self):
		self.x = 0.0 #kukaReal_t
		self.y = 0.0
		self.z = 0.0
		self.a = 0.0
		self.b = 0.0
		self.c = 0.0
		self.s = 0 #kukaInt_t
		self.t = 0

class kukaError_t():
	def __init__(self):
		self.type = 0 #kukaErrorType_t
		self.no = 0 #long
		self.desc = [KUKA_STRING_LEN] #kukaString_t

class kukaTablo_t():
	def __init__(self):
		self.fct = [] # kukaAxis_t()
		self.val = [] # kukaFrame_t()
		self.base = -1

class kukaVal():
	def __init__(self):


		self.type = 0 # kukaType_t();
		self.kukaVal_u = None


# kukaVar_s
class kukaVar_t():
	def __init__(self):
		self.nom = [KUKA_VARNAME_LEN] #char
		self.valeur = kukaVal();


class kukaStack():
	def __init__(self,func,val,flag):
		self.func = func
		self.val = val
		self.flag = flag


class rpcClient(rpc.Client):
	def __init__(self,host):

		#super(rpcClient, self).__init__()

		# standard TereoProtoclol values
		self.instance = self
		self.connected = False
		self.connecting = False
		self.lasterror = ""
		comm = []
		self.comm_stack = collections.deque(comm)
		self.waiting_response = False
		self.last_comm = ""
		self.last_response = ""
		self.last_cnc_error = ""
		self.position = {'X':0, 'Y':0, 'Z':0, 'A':0, 'B':0, 'C':0, 'U':0, 'V':0, 'W':0}
		self.position_updated = False
		self.joint_position = {'X':0, 'Y':0, 'Z':0, 'A':0, 'B':0, 'C':0, 'U':0, 'V':0, 'W':0}
		self.waiting_job = True
		self.speed = 5
		self.mode_tablo = True
		tab_arr = []
		self.tablo = collections.deque(tab_arr)
		self.base_data = []
		self.tool_data = []


		self.base_data.append(kukaFrame_t())
		self.base_data.append(kukaFrame_t())
		self.base_data.append(kukaFrame_t())
		self.base_data.append(kukaFrame_t())
		self.base_data.append(kukaFrame_t())
		self.base_data.append(kukaFrame_t())
		self.base_data.append(kukaFrame_t())
		self.base_data.append(kukaFrame_t())

		self.tool_data.append(kukaFrame_t())
		self.tool_data.append(kukaFrame_t())
		self.tool_data.append(kukaFrame_t())
		self.tool_data.append(kukaFrame_t())
		self.tool_data.append(kukaFrame_t())
		self.tool_data.append(kukaFrame_t())
		self.tool_data.append(kukaFrame_t())
		self.tool_data.append(kukaFrame_t())

		# end standard TereoProtocol values

 		pmap = rpc.TCPPortMapperClient(host)
		port = pmap.Getport((KUKAPROG, KUKAVERS, rpc.IPPROTO_TCP, 0))
		pmap.close()

		self.sock = None #socket.socket()

		rpc.Client.__init__(self,host,KUKAPROG,KUKAVERS,port)

		gobject.timeout_add(500, self.check_stack)

		return


	def initialize(self):
		print "init"
		res = self.make_call(INITIALIZE, None, None, self.unpack_struct)
		self.connected = True
		#todo : check res
		return

	def end_process(self):

		print "end process kuka"

		flag = kukaVar_t()
		flag.nom = "TABLO_FLAG"
		flag.valeur.type = KUKA_BOOL
		flag.valeur.kukaVal_u = True

		self.send_stack(SETVAR,None,flag)

		return


	def check_stack(self):

		if self.waiting_response == True:
			print "waiting response true"
			return True

		if len(self.comm_stack) == 0:
			#print "stack empty"
			#self.waiting_job = True
			return True

		mycom = self.comm_stack.popleft()


		if mycom.flag != "":
			# do nothing until flag is false
			res = self.make_call(GETVAR, mycom.flag, self.pack_struct, self.unpack_struct)
			if res == None:
				print "res nothing"
				self.comm_stack.appendleft(mycom)
				return True
			if res.valeur == None:
				print "no value"
				self.comm_stack.appendleft(mycom)
				return True
			if res.valeur.type == None:
				print "no value type"
				self.comm_stack.appendleft(mycom)
				return True
			if res.valeur.type != KUKA_BOOL:
				print "unexpected type"
				self.comm_stack.appendleft(mycom)
				return True
			if res.valeur.kukaVal_u == None:
				print "value null"
				self.comm_stack.appendleft(mycom)
				return True
			if res.valeur.kukaVal_u == True:
				print "job not done"
				self.comm_stack.appendleft(mycom)
				return True

			print "ok flag false"
			print "nom flag : " + mycom.flag.nom
			if mycom.flag.nom == "TABLO_FLAG":
				print "debut while"
				i=1
				while len(self.tablo)>0:
					mytablo = self.tablo.popleft()

					# function+base number
					ka = kukaFrame_t()
					ka.x = mytablo.fct
					ka.y = mytablo.base
					if mytablo.fct == KUKA_FCT_PROBE:
						ka.z = mytablo.val.s
					if mytablo.fct == KUKA_FCT_PROBE_AXIS:
						ka.z = mytablo.val.s
					fct = kukaVar_t()
					fct.nom = "TABLO_FCT[%d"%(i)+"]"
					fct.valeur.type = KUKA_FRAME
					fct.valeur.kukaVal_u  = ka
					res = self.make_call(SETVAR, fct, self.pack_struct, self.unpack_struct)

##					if mytablo.axes != None:
##						axes = kukaVar_t()
##						axes.nom = "TABLO_AXES[%d"%(i)+"]"
##						axes.valeur.type = KUKA_AXIS
##						axes.valeur.kukaVal_u  = mytablo.axes
##						res = self.make_call(SETVAR, axes, self.pack_struct, self.unpack_struct)

					val = kukaVar_t()
					val.nom = "TABLO_VAL[%d"%(i)+"]"
					val.valeur.type = KUKA_FRAME
					if mytablo.fct == KUKA_FCT_PROBE:
						tmpval = kukaFrame_t()
						tmpval.a = mytablo.val.a
						tmpval.b = mytablo.val.b
						tmpval.c = mytablo.val.c
						tmpval.x = mytablo.val.x
						tmpval.y = mytablo.val.y
						tmpval.z = mytablo.val.z
						val.valeur.kukaVal_u  = tmpval
					elif mytablo.fct == KUKA_FCT_PROBE_AXIS:
						tmpval = kukaAxis_t()
						tmpval.a1 = mytablo.val.a
						tmpval.a2 = mytablo.val.b
						tmpval.a3 = mytablo.val.c
						tmpval.a4 = mytablo.val.x
						tmpval.a5 = mytablo.val.y
						tmpval.a6 = mytablo.val.z
						val.valeur.kukaVal_u  = tmpval
					else:
						val.valeur.kukaVal_u  = mytablo.val
					res = self.make_call(SETVAR, val, self.pack_struct, self.unpack_struct)

					i = i + 1
				print "fin while"
				if i>0:
					print "count"
					count = kukaVar_t()
					count.nom = "TABLO_COUNT"
					count.valeur.type = KUKA_INT
					count.valeur.kukaVal_u  = i-1
					res = self.make_call(SETVAR, count, self.pack_struct, self.unpack_struct)

				self.waiting_job = True
				print "waiting_job true"

			else:
				res = self.make_call(mycom.func, mycom.val, self.pack_struct, self.unpack_struct)

			print "envoi flag"
			res = self.make_call(SETVAR, mycom.flag, self.pack_struct, self.unpack_struct)

			return True

		print "make call"
		res = self.make_call(mycom.func, mycom.val, self.pack_struct, self.unpack_struct)

		self.last_comm = kukaStack(mycom.func,mycom.val,mycom.flag)

		return True


	def send_stack(self,funct,val,flag):
		print "send stack"
		self.comm_stack.append(kukaStack(funct,val,flag))
		return


	def send(self,com):

		if com == "stat position":
			self.getpos()
			return
		if com == "stat joint_position":
			self.getaxes()
			return



	################# set ################

	def setaxes(self,x,y,z,a,b,c):

		print "setaxes"
		return

		ka = kukaAxis_t()

		ka.a1 = x #1000.0
		ka.a2 = y #0.0
		ka.a3 = z #1710.0
		ka.a4 = a #0.0
		ka.a5 = b #90.0
		ka.a6 = c #0.0

		if self.mode_tablo == False:

			axes = kukaVar_t()
			axes.nom = "R_AXES"
			axes.valeur.type = KUKA_AXIS
			axes.valeur.kukaVal_u  = ka

			flag = kukaVar_t()
			flag.nom = "AXES_FLAG"
			flag.valeur.type = KUKA_BOOL
			flag.valeur.kukaVal_u = True

			self.send_stack(SETVAR,axes,flag)

		else:

			te = kukaTablo_t()
			te.axes = ka
			te.speed = self.speed

			self.tablo.append(te)
			self.waiting_job = True
			print "waiting jb true"


		return

	# f = frame6
	def setpos(self,f,base):

		print "setpos to kuka : x:%f"%(f.x) + " y:%f"%(f.y) + " z:%f"%(f.z) + " a:%f"%(f.a) + " b:%f"%(f.b) + " c:%f"%(f.c)

		ka = kukaFrame_t()
		ka.x = f.x #1000.0
		ka.y = f.y #0.0
		ka.z = f.z #1710.0
		ka.a = f.a #0.0
		ka.b = f.b #90.0
		ka.c = f.c #0.0

		if self.mode_tablo == False:

			flag = kukaVar_t()
			flag.nom = "CIBLE_FLAG"
			flag.valeur.type = KUKA_BOOL
			flag.valeur.kukaVal_u = True

			cible = kukaVar_t()
			cible.nom = "CIBLE"
			cible.valeur.type = KUKA_FRAME
			cible.valeur.kukaVal_u  = ka

			self.send_stack(SETVAR,cible,flag)

		else:

			te = kukaTablo_t()
			te.val = ka
			te.base = base
			te.fct = KUKA_FCT_POS

			self.tablo.append(te)
			self.waiting_job = True
			print "waiting_job true"

		return


	# f = frame6
	def setprobe(self,f,d,base):

		print "setprobe to kuka : x:%f"%(f.x) + " y:%f"%(f.y) + " z:%f"%(f.z) + " a:%f"%(f.a) + " b:%f"%(f.b) + " c:%f"%(f.c) + " d:%f"%(d)

		ka = kukaPos_t()
		ka.x = f.x #1000.0
		ka.y = f.y #0.0
		ka.z = f.z #1710.0
		ka.a = f.a #0.0
		ka.b = f.b #90.0
		ka.c = f.c #0.0
		ka.s = d

		if self.mode_tablo == False:

			flag = kukaVar_t()
			flag.nom = "PROBE_FLAG"
			flag.valeur.type = KUKA_BOOL
			flag.valeur.kukaVal_u = True

			probe = kukaVar_t()
			probe.nom = "PROBE"
			probe.valeur.type = KUKA_POS
			probe.valeur.kukaVal_u  = ka

			self.send_stack(SETVAR,probe,flag)

		else:

			te = kukaTablo_t()
			te.val = ka
			te.base = base
			te.fct = KUKA_FCT_PROBE

			self.tablo.append(te)
			self.waiting_job = True
			print "waiting_job true"

		return

	# f = frame6
	def setprobeaxis(self,f,d,base):

		print "setprobe to kuka : x:%f"%(f.x) + " y:%f"%(f.y) + " z:%f"%(f.z) + " a:%f"%(f.a) + " b:%f"%(f.b) + " c:%f"%(f.c) + " d:%f"%(d)

		ka = kukaPos_t()
		ka.x = f.x #1000.0
		ka.y = f.y #0.0
		ka.z = f.z #1710.0
		ka.a = f.a #0.0
		ka.b = f.b #90.0
		ka.c = f.c #0.0
		ka.s = d

		if self.mode_tablo == False:

			flag = kukaVar_t()
			flag.nom = "PROBE_FLAG"
			flag.valeur.type = KUKA_BOOL
			flag.valeur.kukaVal_u = True

			probe = kukaVar_t()
			probe.nom = "PROBE"
			probe.valeur.type = KUKA_POS
			probe.valeur.kukaVal_u  = ka

			self.send_stack(SETVAR,probe,flag)

		else:

			te = kukaTablo_t()
			te.val = ka
			te.base = base
			te.fct = KUKA_FCT_PROBE_AXIS

			self.tablo.append(te)
			self.waiting_job = True
			print "waiting_job true"

		return


	# f = frame6
	def setmove_contact(self,f,base):

		print "setmove_contact : x:%f"%(f.x) + " y:%f"%(f.y) + " z:%f"%(f.z) + " a:%f"%(f.a)

		ka = kukaFrame_t()
		ka.x = f.x #1000.0
		ka.y = f.y #0.0
		ka.z = f.z #1710.0
		ka.a = f.a #0.0
		ka.b = f.b #90.0
		ka.c = f.c #0.0

		if self.mode_tablo == False:

			flag = kukaVar_t()
			flag.nom = "CIBLE_FLAG"
			flag.valeur.type = KUKA_BOOL
			flag.valeur.kukaVal_u = True

			cible = kukaVar_t()
			cible.nom = "CIBLE"
			cible.valeur.type = KUKA_FRAME
			cible.valeur.kukaVal_u  = ka

			self.send_stack(SETVAR,cible,flag)

		else:

			te = kukaTablo_t()
			te.val = ka
			te.base = base
			te.fct = KUKA_FCT_CTC

			self.tablo.append(te)
			self.waiting_job = True
			print "waiting_job true"

		return


	def setprehension(self,f,base):

		print "setprehension : x:%f"%(f.x) + " y:%f"%(f.y) + " z:%f"%(f.z) + " a:%f"%(f.a)

		ka = kukaFrame_t()
		ka.x = f.x #1000.0
		ka.y = f.y #0.0
		ka.z = f.z #1710.0
		ka.a = f.a #0.0
		ka.b = f.b #90.0
		ka.c = f.c #0.0

		if self.mode_tablo == False:

			flag = kukaVar_t()
			flag.nom = "CIBLE_FLAG"
			flag.valeur.type = KUKA_BOOL
			flag.valeur.kukaVal_u = True

			cible = kukaVar_t()
			cible.nom = "CIBLE"
			cible.valeur.type = KUKA_FRAME
			cible.valeur.kukaVal_u  = ka

			self.send_stack(SETVAR,cible,flag)

		else:

			te = kukaTablo_t()
			te.val = ka
			te.base = base
			te.fct = KUKA_FCT_PRE

			self.tablo.append(te)
			self.waiting_job = True
			print "waiting_job true"

		return

	def setdeprehension(self,f,base):

		print "setdeprehension : x:%f"%(f.x) + " y:%f"%(f.y) + " z:%f"%(f.z) + " a:%f"%(f.a)

		ka = kukaFrame_t()
		ka.x = f.x #1000.0
		ka.y = f.y #0.0
		ka.z = f.z #1710.0
		ka.a = f.a #0.0
		ka.b = f.b #90.0
		ka.c = f.c #0.0

		if self.mode_tablo == False:

			flag = kukaVar_t()
			flag.nom = "CIBLE_FLAG"
			flag.valeur.type = KUKA_BOOL
			flag.valeur.kukaVal_u = True

			cible = kukaVar_t()
			cible.nom = "CIBLE"
			cible.valeur.type = KUKA_FRAME
			cible.valeur.kukaVal_u  = ka

			self.send_stack(SETVAR,cible,flag)

		else:

			te = kukaTablo_t()
			te.val = ka
			te.base = base
			te.fct = KUKA_FCT_DEPRE

			self.tablo.append(te)
			self.waiting_job = True
			print "waiting_job true"

		return

	def setnail(self,f,base):

		print "setnail : x:%f"%(f.x) + " y:%f"%(f.y) + " z:%f"%(f.z) + " a:%f"%(f.a)

		ka = kukaFrame_t()
		ka.x = f.x #1000.0
		ka.y = f.y #0.0
		ka.z = f.z #1710.0
		ka.a = f.a #0.0
		ka.b = f.b #90.0
		ka.c = f.c #0.0

		if self.mode_tablo == False:

			flag = kukaVar_t()
			flag.nom = "CIBLE_FLAG"
			flag.valeur.type = KUKA_BOOL
			flag.valeur.kukaVal_u = True

			cible = kukaVar_t()
			cible.nom = "CIBLE"
			cible.valeur.type = KUKA_FRAME
			cible.valeur.kukaVal_u  = ka

			self.send_stack(SETVAR,cible,flag)

		else:

			te = kukaTablo_t()
			te.val = ka
			te.base = base
			te.fct = KUKA_FCT_NAIL

			self.tablo.append(te)
			self.waiting_job = True
			print "waiting_job true"

		return



	def setposlin(self,f,base):

		print "setpos : x:%f"%(f.x) + " y:%f"%(f.y) + " z:%f"%(f.z) + " a:%f"%(f.a)

		flag = kukaVar_t()
		flag.nom = "CIBLE_LIN_FLAG"
		flag.valeur.type = KUKA_BOOL
		flag.valeur.kukaVal_u = True


		cible = kukaVar_t()
		cible.nom = "CIBLE_LIN"
		cible.valeur.type = KUKA_FRAME
		cible.valeur.kukaVal_u  = kukaFrame_t()
		cible.valeur.kukaVal_u.x = f.x #1000.0
		cible.valeur.kukaVal_u.y = f.y #0.0
		cible.valeur.kukaVal_u.z = f.z #1710.0
		cible.valeur.kukaVal_u.a = f.a #0.0
		cible.valeur.kukaVal_u.b = f.b #90.0
		cible.valeur.kukaVal_u.c = f.c #0.0

		self.send_stack(SETVAR, cible, flag)


		return


	def setspeed(self,s):
		print "setspeed---"

		self.speed = s

		if self.mode_tablo == False:

			flag = kukaVar_t()
			flag.nom = "VITESSE_FLAG"
			flag.valeur.type = KUKA_BOOL
			flag.valeur.kukaVal_u = True


			speed = kukaVar_t()
			speed.nom = "VITESSE"
			speed.valeur.type = KUKA_INT
			speed.valeur.kukaVal_u  = s # protection lock

			self.send_stack(SETVAR,speed,flag)

		else:

			ka = kukaFrame_t()
			ka.x = s

			te = kukaTablo_t()
			te.fct = KUKA_FCT_SPEED
			te.val = ka

			self.tablo.append(te)
			self.waiting_job = True
			print "waiting_job true"

		return


	############################# get ########################

	def getpos(self):

		print "getpos"

		cible = kukaVar_t()
		cible.nom = "$POS_ACT"
		cible.valeur.type = KUKA_FRAME
		cible.valeur.kukaVal_u  = kukaFrame_t()

		self.send_stack(GETVAR,cible,"")

		return

	def getaxes(self):

		print "getaxes"
		return
		axes = kukaVar_t()
		axes.nom = "$AXIS_ACT"
		axes.valeur.type = KUKA_AXIS
		axes.valeur.kukaVal_u = kukaAxis_t()

		self.send_stack(GETVAR, axes,"")


		return


	def getspeed(self):

		print "getspeed"

		speed = kukaVar_t()
		speed.nom = "$OV_PRO"
		speed.valeur.type = KUKA_INT
		speed.valeur.kukaVal_u  = 0

		self.send_stack(GETVAR, speed,"")

		return


	def do_call(self):
		#print "do call"

		call = self.packer.get_buf()
		#print len(call)

		try:
			rpc.sendrecord(self.sock, call)
		except(RuntimeError):
			print "send error"
		pass

		try:
			reply = rpc.recvrecord(self.sock)
		except(RuntimeError):
			print "recv erro"
		pass

		u = self.unpacker
		print reply
		u.reset(reply)


		return

	def makesocket(self):
		print "make socket"
		self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
		return



	def pack_struct(self,arg):

		# send ok header

		#print "len buf pack : %d"%(len(self.packer.buf))

		# nom ()
		for i in arg.nom:
			self.packer.pack_int(ord(i))

		for i in range(1,KUKA_VARNAME_LEN - len(arg.nom)+1):
			self.packer.pack_int(0)

		#print "len buf pack : %d"%(len(self.packer.buf))

		if arg.valeur.type == KUKA_FRAME:

			self.packer.pack_int(KUKA_FRAME)
			self.packer.pack_float(arg.valeur.kukaVal_u.x)
			self.packer.pack_float(arg.valeur.kukaVal_u.y)
			self.packer.pack_float(arg.valeur.kukaVal_u.z)
			self.packer.pack_float(arg.valeur.kukaVal_u.a)
			self.packer.pack_float(arg.valeur.kukaVal_u.b)
			self.packer.pack_float(arg.valeur.kukaVal_u.c)


		elif arg.valeur.type == KUKA_AXIS:

			self.packer.pack_int(KUKA_AXIS)
			self.packer.pack_float(arg.valeur.kukaVal_u.a1)
			self.packer.pack_float(arg.valeur.kukaVal_u.a2)
			self.packer.pack_float(arg.valeur.kukaVal_u.a3)
			self.packer.pack_float(arg.valeur.kukaVal_u.a4)
			self.packer.pack_float(arg.valeur.kukaVal_u.a5)
			self.packer.pack_float(arg.valeur.kukaVal_u.a6)


		elif arg.valeur.type == KUKA_BOOL:
			self.packer.pack_int(KUKA_BOOL)
			self.packer.pack_bool(arg.valeur.kukaVal_u)

		elif arg.valeur.type == KUKA_INT:
			self.packer.pack_int(KUKA_INT)
			self.packer.pack_int(arg.valeur.kukaVal_u)

		return


	def unpack_struct(self):

		print "unpack struct"

		kval = kukaVar_t()
		#self.waiting_job = True

		#print "buf : %d"%(len(self.unpacker.buf))

		try:

			r = self.unpacker.unpack_replyheader()
			#print "retour replyheader :"
			#print r

		except EOFError,msg:
			print "nothing"
			print msg
			return
		except RuntimeError,msg:
			print "error"
			print msg
			return
		pass

		#print "pos : %d"%(self.unpacker.pos)

		# extract kavvar name
		str =''
		for i in range(1,KUKA_VARNAME_LEN+1):
			asc = self.unpacker.unpack_int()
			if asc > 0:
				str = str + chr(asc)

		kval.nom = str

		#print "nom : [" + kval.nom + "]"

		#print "pos : %d"%(self.unpacker.pos)

		# type
		kval.valeur.type = self.unpacker.unpack_int()

		#print "type : %d"%(kval.valeur.type)

		# kukaint
		if kval.valeur.type == KUKA_INT:
			#print "kuka int"
			kval.valeur.kukaVal_u = self.unpacker.unpack_int()
			k = kval.valeur.kukaVal_u
			if kval.nom == "$OV_PRO":
				self.speed = k
		# kukareal
		elif kval.valeur.type == KUKA_REAL:
			#print "kuka real"
			kval.valeur.kukaVal_u = self.unpacker.unpack_float()
		# kukabool
		elif kval.valeur.type == KUKA_BOOL:
			#print "kuka char"
			kval.valeur.kukaVal_u = self.unpacker.unpack_int()
		# kukachar
		elif kval.valeur.type == KUKA_CHAR:
			#print "kuka char"
			kval.valeur.kukaVal_u = self.unpacker.unpack_fstring(1)
		# kukastring
		elif kval.valeur.type == KUKA_STRING:
			#print "kuka string"
			kval.valeur.kukaVal_u = self.unpacker.unpack_fstring(KUKA_STRING_LEN)
		# kukaaxis
		elif kval.valeur.type == KUKA_AXIS:
			#print "kuka axis"
			kval.valeur.kukaVal_u = kukaAxis_t()
			kval.valeur.kukaVal_u.a1 = self.unpacker.unpack_float()
			kval.valeur.kukaVal_u.a2 = self.unpacker.unpack_float()
			kval.valeur.kukaVal_u.a3 = self.unpacker.unpack_float()
			kval.valeur.kukaVal_u.a4 = self.unpacker.unpack_float()
			kval.valeur.kukaVal_u.a5 = self.unpacker.unpack_float()
			kval.valeur.kukaVal_u.a6 = self.unpacker.unpack_float()

			k = kval.valeur.kukaVal_u

			if kval.nom == "$AXIS_ACT":
 				self.joint_position['X'] = -k.a1
				self.joint_position['Y'] = k.a2+90.0
				self.joint_position['Z'] = k.a3-90.0
				self.joint_position['A'] = k.a4
				self.joint_position['B'] = k.a5
				self.joint_position['C'] = k.a6
				self.joint_position['U'] = 0
				self.joint_position['V'] = 0
				self.joint_position['W'] = 0

		# kukaframe
		elif kval.valeur.type == KUKA_FRAME:
			#print "kuka frame"
			kval.valeur.kukaVal_u = kukaFrame_t()
			kval.valeur.kukaVal_u.x = self.unpacker.unpack_float()
			kval.valeur.kukaVal_u.y = self.unpacker.unpack_float()
			kval.valeur.kukaVal_u.z = self.unpacker.unpack_float()
			kval.valeur.kukaVal_u.a = self.unpacker.unpack_float()
			kval.valeur.kukaVal_u.b = self.unpacker.unpack_float()
			kval.valeur.kukaVal_u.c = self.unpacker.unpack_float()

			k = kval.valeur.kukaVal_u

			if kval.nom == "$POS_ACT":
				self.position['X'] = k.x
				self.position['Y'] = k.y
				self.position['Z'] = k.z
				self.position['A'] = k.a
				self.position['B'] = k.b
				self.position['C'] = k.c
				self.position['U'] = 0
				self.position['V'] = 0
				self.position['W'] = 0
				self.position_updated = True


			#print "frame :  x:%f , y:%f , z%f , a:%f ; b:%f , c:%f"%(k.x,k.y,k.z,k.a,k.b,k.c)

		# kukapos
		elif kval.valeur.type == KUKA_POS:
			print "kuka pos"
			kval.valeur.kukaVal_u = kukaPos_t()
			kval.valeur.kukaVal_u.x = self.unpacker.unpack_float()
			kval.valeur.kukaVal_u.y = self.unpacker.unpack_float()
			kval.valeur.kukaVal_u.z = self.unpacker.unpack_float()
			kval.valeur.kukaVal_u.a = self.unpacker.unpack_float()
			kval.valeur.kukaVal_u.b = self.unpacker.unpack_float()
			kval.valeur.kukaVal_u.c = self.unpacker.unpack_float()
			kval.valeur.kukaVal_u.s = self.unpacker.unpack_int()
			kval.valeur.kukaVal_u.t = self.unpacker.unpack_int()

		# kukaerror
		elif kval.valeur.type == KUKA_ERROR:
			print "kuka error"
			kval.valeur.kukaVal_u = kukaError_t()
			kval.valeur.kukaVal_u.type = self.unpacker.unpack_int()
			print "type : "+get_kuka_error_desc(kval.valeur.kukaVal_u.type)
			kval.valeur.kukaVal_u.no = self.unpacker.unpack_int()
			print "no : %d"%(kval.valeur.kukaVal_u.no)
			kval.valeur.kukaVal_u.desc = self.unpacker.unpack_fstring(KUKA_STRING_LEN*4)
			print "desc : "+kval.valeur.kukaVal_u.desc

		# unknown
		else:
			print "kuka unknown"

		#print "pos : %d"%(self.unpacker.pos)
		#print "buf : %d"%(len(self.unpacker.buf))

		while self.unpacker.pos < len(self.unpacker.buf)>0:
			i = self.unpacker.unpack_uint()


		#print "end extract"

		return kval



##/*************************************************/
##void kuka_displayVar(kukaVar_t *var)
##{
##  if( var==NULL ){
##    printf("Le param?tre de kuka_displayVar ne peut etre NULL.\n");
##    return ;
##  }
##
##  printf("nom    = \"%s\"\n",var->nom);
##  switch(var->valeur.type){
##  case KUKA_INT:
##    printf("type   = KUKA_INT\n");
##    printf("valeur = %d\n",var->valeur.kukaVal_u.kukaInt);
##    break;
##  case KUKA_REAL:
##    printf("type   = KUKA_REAL\n");
##    printf("valeur = %f\n",var->valeur.kukaVal_u.kukaReal);
##    break;
##  case KUKA_BOOL:
##    printf("type   = KUKA_BOOL\n");
##    printf("valeur = %d\n",var->valeur.kukaVal_u.kukaBool);
##    break;
##  case KUKA_CHAR:
##    printf("type   = KUKA_CHAR\n");
##    printf("valeur = '%c'\n",var->valeur.kukaVal_u.kukaChar);
##    break;
##  case KUKA_STRING:
##    printf("type   = KUKA_CHAR\n");
##    printf("valeur = \"%s\"\n",var->valeur.kukaVal_u.kukaString);
##    break;
##  case KUKA_AXIS:
##    printf("type   = KUKA_AXIS\n");
##    printf("valeur = { A1 %f, A2 %f, A3 %f, A4 %f, A5 %f, A6 %f}\n",\
##	   var->valeur.kukaVal_u.kukaAxis.a1,\
##	   var->valeur.kukaVal_u.kukaAxis.a2,\
##	   var->valeur.kukaVal_u.kukaAxis.a3,\
##	   var->valeur.kukaVal_u.kukaAxis.a4,\
##	   var->valeur.kukaVal_u.kukaAxis.a5,\
##	   var->valeur.kukaVal_u.kukaAxis.a6);
##    break;
##  case KUKA_FRAME:
##    printf("type   = KUKA_FRAME\n");
##    printf("valeur = { X %f, Y %f, Z %f, A %f, B %f, C %f}\n",\
##	   var->valeur.kukaVal_u.kukaFrame.x,\
##	   var->valeur.kukaVal_u.kukaFrame.y,\
##	   var->valeur.kukaVal_u.kukaFrame.z,\
##	   var->valeur.kukaVal_u.kukaFrame.a,\
##	   var->valeur.kukaVal_u.kukaFrame.b,\
##	   var->valeur.kukaVal_u.kukaFrame.c);
##    break;
##  case KUKA_POS:
##    printf("type   = KUKA_POS\n");
##    printf("valeur = { X %f, Y %f, Z %f, A %f, B %f, C%f, S %d, T %d}\n",\
##	   var->valeur.kukaVal_u.kukaPos.x,\
##	   var->valeur.kukaVal_u.kukaPos.y,\
##	   var->valeur.kukaVal_u.kukaPos.z,\
##	   var->valeur.kukaVal_u.kukaPos.a,\
##	   var->valeur.kukaVal_u.kukaPos.b,\
##	   var->valeur.kukaVal_u.kukaPos.c,\
##	   var->valeur.kukaVal_u.kukaPos.s,\
##	   var->valeur.kukaVal_u.kukaPos.t);
##    break;
##  case KUKA_ERROR:
##    printf("type   = KUKA_ERROR\n");
##    printf("valeur = \n");
##    printf("\t type = ");
##    switch(var->valeur.kukaVal_u.kukaError.type){
##    case KUKA_API_E:
##      printf("KUKA_API_E ");
##      break;
##    case KUKA_RPC_E:
##      printf("KUKA_RPC_E ");
##      break;
##    case KUKA_CROSS_E:
##      printf("KUKA_CROSS_E ");
##      break;
##    case KUKA_KUKA_E:
##      printf("KUKA_KUKA_E ");
##      break;
##    }
##    printf("\n\t n? = %ld\n\t description = %s\n",
##	   var->valeur.kukaVal_u.kukaError.no,
##	   var->valeur.kukaVal_u.kukaError.desc);
##    break;
##
##  case KUKA_UNKNOWN :
##  default :
##    printf("type   = KUKA_UNKNOWN\n");
##    break;
##  }
##  return;
##}


##/*************************************************/
##/*  FONCTIONS PRIVEES                            */
##/*************************************************/
##/*! Ces fonctions permettent de manager les erreurs
## *  ? partir d'un reultat.
## *  Cette fonction devrait etre la seule ? ecrire
## *  dans kukaError pour simplifier la maintenance
## *  du code.
## */
##int setKukaErrorFromArg(void *arg)
##{
##  if( clnt==NULL ){
##    kukaError.valeur.type=KUKA_ERROR;
##    kukaError.valeur.kukaVal_u.kukaError.type=KUKA_API_E;
##    kukaError.valeur.kukaVal_u.kukaError.no=0;
##    strncpy(kukaError.valeur.kukaVal_u.kukaError.desc,
##	    "The api should be initialized. Call kuka_initialize().",
##	    KUKA_STRING_LEN);
##    return 1;
##  }
##
##  if( arg==NULL ){
##    kukaError.valeur.type=KUKA_ERROR;
##    kukaError.valeur.kukaVal_u.kukaError.type=KUKA_API_E;
##    kukaError.valeur.kukaVal_u.kukaError.no=0;
##    strncpy(kukaError.valeur.kukaVal_u.kukaError.desc,
##	    "Argument can't be NULL.",
##	    KUKA_STRING_LEN);
##    return 1;
##  }
##
##  return 0;
##}
##

def get_kuka_error_desc(num):
	if num == 0x001a:
		return "KUKA_KUKA_E"
	elif num == 0x002a:
		return "KUKA_CROSS_E"
	elif num == 0x003a:
		return "KUKA_RPC_E"
	elif num == 0x004a:
		return "KUKA_API_E"
	else:
		return "unknown error"










