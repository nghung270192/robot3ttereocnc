# -*- coding: utf-8 -*-



import pygtk
pygtk.require("2.0")
import gtk
import gobject

import os
import os.path
import sys

import copy

import tereoRobot



class robot_library_window(gtk.Window):


	def __init__(self,tereo,args=None):

		gtk.Window.__init__(self)

		self.tereo = tereo
		self.args = args

		self.set_default_size(640, 480)
		self.set_position(gtk.WIN_POS_CENTER)

		hpaned2 = gtk.HPaned()
		hpaned2.show()
		self.add(hpaned2)
		hpaned2.set_position(300)

		#todo : get info_win tmpdb and tmprobot
		self.tmpdb = None
		self.tmprobot = None

		vbox6 = gtk.VBox()
		vbox6.show()
		hpaned2.pack1(vbox6, False, True)
		#border_width2

		scrolledwindow7 = gtk.ScrolledWindow()
		scrolledwindow7.show()
		vbox6.pack_start(scrolledwindow7,True,True,0)

		self.robotstv = gtk.TreeStore(gobject.TYPE_STRING,gobject.TYPE_STRING)

		tvRobots = gtk.TreeView(self.robotstv)
		tvRobots.show()
		tvRobots.set_headers_visible(False)
		tvRobots.set_enable_tree_lines(True)
		tvRobots.connect("cursor_changed",self.on_robotsmodelwin_select)

		scrolledwindow7.add(tvRobots)

		tvrobotscolimage = gtk.TreeViewColumn()
		tvRobots.append_column(tvrobotscolimage)
		tvrobotscellpix = gtk.CellRendererPixbuf()
		tvrobotscellimage = gtk.CellRendererText()
		tvrobotscolimage.pack_start(tvrobotscellimage,True)
		tvrobotscolimage.add_attribute(tvrobotscellimage,'text',0)


		tvrobotscolname = gtk.TreeViewColumn()
		tvRobots.append_column(tvrobotscolname)
		tvrobotscellname = gtk.CellRendererText()
		tvrobotscolname.pack_start(tvrobotscellname,True)
		tvrobotscolname.add_attribute(tvrobotscellname,'text',1)



		robottoolbar = gtk.Toolbar()
		robottoolbar.show()
		robottoolbar.set_style(gtk.TOOLBAR_BOTH)
		robottoolbar.set_icon_size(4)

		img = gtk.Image()
		img.set_from_file('./media/configure.png')
		img.show()
		robotbt1 = gtk.ToolButton(img,"Config")
		robotbt1.show()
		robotbt1.connect("clicked", self.on_robotsmodelwin_config)
		robottoolbar.insert(robotbt1,-1)

		img = gtk.Image()
		img.set_from_file('./media/add.png')
		img.show()
		robotbt2 = gtk.ToolButton(img,"New")
		robotbt2.show()
		robotbt2.connect("clicked",self.on_robotsmodelwin_new)
		robottoolbar.insert(robotbt2,-1)

		img = gtk.Image()
		img.set_from_file('./media/dialog-no.png')
		img.show()
		robotbt3 = gtk.ToolButton(img,"Delete")
		robotbt3.show()
		robotbt3.connect("clicked",self.on_robotsmodelwin_delete)
		robottoolbar.insert(robotbt3,-1)

		img = gtk.Image()
		img.set_from_file('./media/clone.png')
		img.show()
		robotbt4 = gtk.ToolButton(img,"Clone")
		robotbt4.show()
		robotbt4.connect("clicked", self.on_robotsmodelwin_clone)
		robottoolbar.insert(robotbt4,-1)

		img = gtk.Image()
		img.set_from_file('./media/delete.png')
		img.show()
		robotbt5 = gtk.ToolButton(img,"Cancel")
		robotbt5.show()
		robotbt5.connect("clicked", self.on_robotsmodelwin_cancel)
		robottoolbar.insert(robotbt5,-1)

		img = gtk.Image()
		img.set_from_file('./media/dialog-ok.png')
		img.show()
		robotbt6 = gtk.ToolButton(img,"Ok")
		robotbt6.show()
		robotbt6.connect("clicked", self.on_robotsmodelwin_ok)
		robottoolbar.insert(robotbt6,-1)


		vbox6.pack_start(robottoolbar,False,False,1)


		vpaned1 = gtk.VPaned()
		vpaned1.show()

		robotThumbnailFrame = gtk.Frame()
		robotThumbnailFrame.show()
		vpaned1.pack1(robotThumbnailFrame, False, True)

		self.robotThumbnail = gtk.Image()
		self.robotThumbnail.show()
		robotThumbnailFrame.add(self.robotThumbnail)

		self.robotwintext = gtk.TextBuffer()

		textrobot = gtk.TextView(self.robotwintext)
		textrobot.set_size_request(-1,300)
		textrobot.show()

		vpaned1.pack2(textrobot,False, True)

		hpaned2.pack2(vpaned1,True,True)

		self.populate()

		self.show()

		return


	def populate(self):

		self.robotstv.clear()

		myiter = None

		for groupname in os.listdir(self.tereo.homedir+'/robots'):
			group = self.robotstv.append(None,('',groupname))
			for robotname in os.listdir(self.tereo.homedir+'/robots/' + groupname):
				okiter = self.robotstv.append(group,('',robotname))
				if self.tmprobot != None:
					if self.tmprobot.model == robotname and self.tmprobot.constructor == groupname:
						myiter = okiter


		if myiter != None:
			self.robotstv.expand_row(configlv.get_path(self.robotstv.iter_parent(myiter)),True)
			self.robotstv.get_selection().select_iter(myiter)
			self.on_robotsmodelwin_select(self.robotstv)

		return




	def on_robotsmodelwin_new(self,widget):
		dlg = self.builder.get_object("dlgRobots")
		dlg.show()
		return

	def on_robotsmodelwin_select(self,widget):
		#print "on robotmodelwin select"
		modele, iter = widget.get_selection().get_selected()
		if not iter:
			return
		if modele.iter_parent(iter) == None:
			return

		model = modele.get_value(iter,1)
		constructor = modele.get_value(modele.iter_parent(iter),1)
		#print name
		#print group
		readme = ''
		try:
			with open(self.tereo.homedir + '/robots/' + constructor + '/' + model + '/README', 'r') as f:
				readmefile = f.readlines()
			for line in readmefile:
				line = line.strip('\n')
				line = line.strip('\r')
				line = line.strip('\n')
				readme = readme + line + '\r'
		except:
			self.tereo.applog("Robot models",0,"no robot description readme file")
		self.robotwintext.set_text(readme)

		if self.tmprobot:
			del self.tmprobot

		self.tmprobot = tereoRobot.robotobj()
		self.tmprobot.constructor = constructor
		self.tmprobot.model = model
		self.tmprobot.homedir = self.tereo.homedir + '/robots'


		# load ini file
		if self.tmpdb != None:
			del self.tmpdb
		self.tmpdb = tereoDB.db()
		self.tmpdb.load_ini_file(self.tereo.homedir + '/robots/' + constructor + '/' + model + '/robot.ini')
		self.tmpdb.load_ini_file(self.tereo.homedir + '/robots/' + constructor + '/' + model + '/robot.ter')

		print "constructor: "+constructor
		print "model: "+model

		# showimage
		pixbuf = None
		try:
			pixbuf = gtk.gdk.pixbuf_new_from_file(self.tereo.homedir + '/robots/' + constructor + '/' + model + '/thumbnail.jpg')
			w = pixbuf.get_width()
			h = pixbuf.get_height()
			print "h : %f, w : %f"%(w,h)
			if h > 0:
				f = 150.0/h
				scaled_buf = pixbuf.scale_simple(int(w * f),150,gtk.gdk.INTERP_BILINEAR)
				print scaled_buf
				self.robotThumbnail.set_from_pixbuf(scaled_buf)
				print "c"
				del scaled_buf
			del pixbuf

		except:
			print "error"
			self.tereo.applog("Robot models",0,"No robot thumbnail")


		return

	def on_robotsmodelwin_cancel(self,widget):
		self.hide()
		return

	def on_robotsmodelwin_ok(self,widget):
		# todo
		self.robotsmodelwin.hide()
		if self.robotsmodelwinrethandler != None:
			self.robotsmodelwinrethandler()
		return

	def on_robotsmodelwin_config(self,widget):
		if not self.tmprobot:
			return
		if self.tmprobot.model == '':
			return
		self.robotsconfigwin.maximize()
		self.robotsconfigwin.show()
		#screen = self.robotsconfigwin.get_screen()
		#self.robotsconfigwin.resize(screen.get_width(),screen.get_height())
		self.robotsconfig_init()
		return

	def on_robotsmodelwin_clone(self,widget):
		if not self.tmprobot:
			return
		if self.tmprobot.model == '':
			return
		#todo
		return

	def on_robotsmodelwin_delete(self,widget):
		if not self.tmprobot:
			return
		if self.tmprobot.model == '':
			return
		#todo
		return




