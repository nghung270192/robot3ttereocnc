# -*- coding: utf-8 -*-


import os.path


pluginName = "Robot library"
version = "0.1"
author = "Olivier Martinez"


def register(parent):

	mod = imp.load_source('robot_library', './plugins/robot_library/robot_library.py')
	parent.add_function("robot.library","Robot library",mod.on_configwin_robotsmodel)
	return

