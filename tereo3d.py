import sys
import math




def vectorMatrix(t):
	zx = t.xy * t.yz - t.xz * t.yy
	zy = t.xz * t.yx - t.xx * t.yz
	zz = t.xx * t.yy - t.xy * t.yx
	rot = [ t.xx, t.xy, t.xz, 0.0,
			t.yx, t.yy, t.yz, 0.0,
			zx, zy, zz, 0.0,
			0.0, 0.0, 0.0, 1.0 ]
	return rot #glMultMatrixf(rot)
