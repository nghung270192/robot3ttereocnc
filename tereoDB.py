#!/usr/bin/python
import os
import shutil

class section:
	def __init__(self,n=''):
		self.name = n
		self.attributes = []
		return

	def set_property(self, p):
		for t in self.attributes:
			if t[0] == p[0]:
				t[1] = p[1]
				return

		self.attributes.append(p)
		return

	def get_property(self, n):
		for t in self.attributes:
			if t[0] == n:
				return t[1]
		return None

	def del_property(self, n):
		for a in self.attributes:
			if a[0] == n:
				self.attributes.remove(a)
				del a
		return


class db:
	def __init__(self):
		self.sections = []
		self.current_section = None
		return

	def load_ini_file(self, filename):
		print "load ini file : "+filename

		try:
			with open(filename, 'r') as f:
				toolfile = f.readlines()
				for line in toolfile:
					#print line
					line = line.rstrip('\r')
					line = line.rstrip('\n')
					line = line.rstrip('\r')
					#print line
					word_list = line.split(' ',1)
					#print word_list
					word = word_list[0]
					word = word.strip(' \t\n\r=')
					if len(word_list) > 1:
						arg = word_list[1]
						arg = arg.strip(' \t\n\r=')
					else:
						arg = ''

					#print "arg:-" + arg + "-"

					if word[:1] == '[':
						self.current_section = section(word)
						self.sections.append(self.current_section)
					elif word[:1] != '#' and word[:1] != ';' and word != '':
						if self.current_section:
							self.current_section.set_property([word,arg])

				f.close()

		except (UnicodeError, LookupError, IOError):
			print "could not decode file "+filename
		pass

		return

	def save_ini_file(self, filename):
		print "save"
		# if bak not found save a old and a bak
		if os.path.exists(filename+'.bak'):
			os.remove(filename+'.bak')
		else:
			shutil.copy(filename,filename+'.1st')

		shutil.copy(filename,filename+'.bak')

		try:
			with open(filename, 'w') as f:
				for s in self.sections:
					#print "section:"+s.name
					f.write(s.name+'\n')
					for a in s.attributes:
						f.write(a[0] + ' = ' + a[1]+'\n')
					f.write('\n')
				f.close()

		except (UnicodeError, LookupError, IOError):
			print "could not save file " + filename
		pass

		return

	def save_filtered_ini_file(self, sec, filename):

		# if bak not found save a old and a bak
		if os.path.exists(filename+'.bak'):
			os.remove(filename+'.bak')
		else:
			shutil.copy(filename,filename+'.1st')

		shutil.copy(filename,filename+'.bak')

		try:
			with open(filename, 'w') as f:
				for s in self.sections:
					if s.n in sec:
						f.writeline(s.name)
						for a in s.attributes:
							writeline(a[0] + ' ' + a[1])
				close(f)

		except (UnicodeError, LookupError, IOError):
			print "could not save file "+filename
		pass

		return

	def extract_section(self, n, db):
		for s in self.sections:
			if s.name == n:
				ds = section()
				for p in s.attributes:
					ds.set_property(p)
				db.sections.append(ds)
		return

	def get_section(self, n):
		for s in self.sections:
			if s.name == n:
				return s
		return None

	def set_section(self, n):
		for s in self.sections:
			if s.name == n:
				#print "section found"
				return s
		ns = section(n)
		self.sections.append(ns)
		return ns

	def del_section(self, n):
		for s in self.sections:
			if s.name == n:
				for a in s.attributes:
					s.del_property(a)
				self.sections.remove(s)
				del s
		return

	def merge(self, db):
		#print "merge"
		# merge sections and properties
		for s in db.sections:
			#print "dest:"+s.name
			sd = self.set_section(s.name)
			for a in s.attributes:
				sd.set_property(a)
		return
