# -*- coding: utf-8 -*-


import os.path

pluginName = "Tool config"
version = "0.1"
author = "Olivier Martinez"


def register(parent):

	mod = imp.load_source('tool_config', './plugins/tool_config/tool_config.py')
	parent.add_function('tool.config',"Tool config",mod.on_configwin_tools)

	return
