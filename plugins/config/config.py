# -*- coding: utf-8 -*-


from plugins.config import config_window


def on_config_activate(widget,parent):

	f = parent.object_search_path("config")

	if f == []:
		f = config_window.config_window(parent)
		parent.add_object("config","Config",f)
	else:
		f=f[0][1]

	f.show()

	return


