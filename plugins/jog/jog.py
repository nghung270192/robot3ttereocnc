# -*- coding: utf-8 -*-



from plugins.jog import jog_view


def on_jog_activate(widget,parent):
	wj = parent.builder.get_object("winJog")
	wj.show()
	wj.unmaximize()
	return