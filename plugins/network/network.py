# -*- coding: utf-8 -*-



from plugins.network.network_view import network_view



def on_network_toggle(widget,parent):

	f = parent.object_search_path("network.panel")

	if f == []:
		f = network_view(parent)
		parent.add_object("network.panel","NetWork panel",f)
	else:
		f=f[0][1]

	parent.detach_panel(f)

	f.show()

	if widget.get_active() == True:

		parent.attach_panel(f)
		f.populate 


	return
