# -*- coding: utf-8 -*-


import os.path

from plugins.scene.scene_view import scene_view




def new_scene_frame(parent):

	f = parent.get_object('frame','Scene')
	if f == None:
		f = scene_view(parent)
		parent.add_object('gui.frame','Scene',f)

	#f = scene_view.scene_view(parent,scene_icon)
	#parent.add_object('frame','Scene',f)

	return f
