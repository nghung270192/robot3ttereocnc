# -*- coding: utf-8 -*-

from plugins.flow_panel.flow_panel_view import flow_panel_view



def on_flow_toggle(widget,parent):

	f = parent.object_search_path("flow.panel")

	if f == []:
		f = flow_panel_view(parent)
		parent.add_object("flow.panel","Flow panel",f)
	else:
		f=f[0]
		f=f[1]

	parent.detach_panel(f)
	f.show()

	if widget.get_active() == True:

		parent.attach_panel(f)
		f.populate()


	return

