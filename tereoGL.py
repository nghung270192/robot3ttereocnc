#!/usr/bin/python
###############################################################
# 3D function from vismach, opengltk, ...
###############################################################

#from vismach import *
from OpenGL.GL import *




def drawLine(x0, y0, z0, x1, y1, z1):
	glBegin (GL_LINES)
	glVertex ( x0, y0, z0)
	glVertex ( x1, y1, z1)
	glEnd ()
	return

# from vismach
def drawCube(x1, y1, z1, x2, y2, z2):

	if x1 > x2:
		tmp = x1
		x1 = x2
		x2 = tmp
	if y1 > y2:
		tmp = y1
		y1 = y2
		y2 = tmp
	if z1 > z2:
		tmp = z1
		z1 = z2
		z2 = tmp
	glBegin(GL_QUADS)
	# bottom face
	glNormal3f(0.0,0.0,-1.0)
	glVertex3f(x2, y1, z1)
	glVertex3f(x1, y1, z1)
	glVertex3f(x1, y2, z1)
	glVertex3f(x2, y2, z1)
	# positive X face
	glNormal3f(1.0,0.0,0.0)
	glVertex3f(x2, y1, z1)
	glVertex3f(x2, y2, z1)
	glVertex3f(x2, y2, z2)
	glVertex3f(x2, y1, z2)
	# positive Y face
	glNormal3f(0,1,0)
	glVertex3f(x1, y2, z1)
	glVertex3f(x1, y2, z2)
	glVertex3f(x2, y2, z2)
	glVertex3f(x2, y2, z1)
	# negative Y face
	glNormal3f(0,-1,0)
	glVertex3f(x2, y1, z2)
	glVertex3f(x1, y1, z2)
	glVertex3f(x1, y1, z1)
	glVertex3f(x2, y1, z1)
	# negative X face
	glNormal3f(-1,0,0)
	glVertex3f(x1, y1, z1)
	glVertex3f(x1, y1, z2)
	glVertex3f(x1, y2, z2)
	glVertex3f(x1, y2, z1)
	# top face
	glNormal3f(0,0,1)
	glVertex3f(x1, y2, z2)
	glVertex3f(x1, y1, z2)
	glVertex3f(x2, y1, z2)
	glVertex3f(x2, y2, z2)
	glEnd()

	return

def drawWorldAxis():


	#self.drawCube(0,0,0,1,1,1)
	glDisable(GL_LIGHTING)

	# x
	glColor4f(1.0,0.0,0.0, 0.6)
	glBegin (GL_LINES)
	glVertex ( 0.0, 0.0, 0.0)
	glVertex ( 0.8, 0.0, 0.0)
	glVertex ( 1.1, 0.0, 0.1)
	glVertex ( 1.3, 0.0, -0.1)
	glVertex ( 1.1, 0.0, -0.1)
	glVertex ( 1.3, 0.0, 0.1)
	glEnd ()
	glBegin (GL_TRIANGLES)
	glVertex ( 0.8, 0.0, 0.1)
	glVertex ( 1.0, 0.0, 0.0)
	glVertex ( 0.8, 0.0, -0.1)
	glEnd ()

	# y
	glColor4f(0.0,1.0,0.0,0.6)
	glBegin (GL_LINES)
	glVertex ( 0.0, 0.0, 0.0)
	glVertex ( 0.0, 0.8, 0.0)
	glVertex ( 0.0, 1.1, 0.0)
	glVertex ( 0.0, 1.2, 0.0)
	glVertex ( 0.0, 1.2, 0.0)
	glVertex ( -0.0707, 1.3, 0.0707)
	glVertex ( 0.0, 1.2, 0.0)
	glVertex ( 0.0707, 1.3, -0.0707)
	glEnd ()

	glBegin (GL_TRIANGLES)
	glVertex ( 0.0707, 0.8, -0.0707)
	glVertex ( 0.0, 1.0, 0.0)
	glVertex ( -0.0707, 0.8, 0.0707)
	glEnd ()

	# z
	glColor4f(0.0,0.0,1.0,0.6)
	glBegin (GL_LINES)
	glVertex ( 0.0, 0.0, 0.0)
	glVertex ( 0.0, 0.0, 0.8)
	glVertex ( 0.1, 0.0, 1.1)
	glVertex ( -0.1, 0.0, 1.1)
	glVertex ( -0.1, 0.0, 1.3)
	glVertex ( 0.1, 0.0, 1.1)
	glVertex ( -0.1, 0.0, 1.3)
	glVertex ( 0.1, 0.0, 1.3)
	glEnd ()

	glBegin (GL_TRIANGLES)
	glVertex ( 0.0, 0.0, 1.0)
	glVertex ( 0.1, 0.0, 0.8)
	glVertex ( -0.1, 0.0, 0.8)
	glEnd ()



	return

def snap(a):
	m = a%90
	if m < 3:
		return a-m
	elif m > 87:
		return a-m+90
	else:
		return a
	return

# basic lighting
def basicLighting():

	glLightfv(GL_LIGHT0, GL_POSITION, (-8, 10, 5, 0))
	glLightfv(GL_LIGHT0, GL_AMBIENT, (0.5,0.5,0.5, 0))
	glLightfv(GL_LIGHT0, GL_DIFFUSE, (.7,.7,.6, 0))
	glLightfv(GL_LIGHT0+1, GL_POSITION, (-8, 10, 5, 0))
	glLightfv(GL_LIGHT0+1, GL_AMBIENT, (.1,.1,.0,1))
	glLightfv(GL_LIGHT0+1, GL_DIFFUSE, (.0,.6,.6,0))
	glEnable(GL_CULL_FACE)
	glCullFace(GL_BACK)
	glEnable(GL_LIGHTING)
	glEnable(GL_LIGHT0)
	#glEnable(GL_LIGHT0+1)
	glShadeModel(GL_FLAT)
	glEnable(GL_DEPTH_TEST)
	glDepthFunc(GL_LESS)
	#glEnable(GL_COLOR_MATERIAL)

	# end basic lighting
	return

