# -*- coding: utf-8 -*-


def on_configwin_controller(widget,parent):
	parent.controllerwin.show()
	screen = parent.controllerwin.get_screen()
	parent.controllerwin.resize(screen.get_width(),screen.get_height())
	return

