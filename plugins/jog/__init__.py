# -*- coding: utf-8 -*-


import os.path


pluginName = "Jog"
version = "0.1"
author = "Olivier Martinez"
jog_icon = None


def register(parent):

	mod = imp.load_source('jog', './plugins/jog/jog.py')

	# log icon
	img = gtk.Image()
	img.set_from_file('./plugins/jog/jog.png')
	img.show()
	jog_icon = gtk.ToolButton (img, "Jog")
	jog_icon.connect("clicked", mod.on_jog_activate, parent)
	parent.add_icon(jog_icon)

	return


