var net = require('net');

var mqtt = require('mqtt')
var exec = require('child_process').exec;

var parentDir = process.cwd();

const RETRY_INTERVAL = 10000;
//var timeout1;
var timeout2;
//var connected1 = false;
var connected2 = false;


var client;

console.log("trying to connect to cloud box...");
try {
	mqttclient = mqtt.createClient(1883, '127.0.0.1');
	mqttclient.subscribe('/RoboticCloud');
	//clearTimeout(timeout1);
} catch(e) { console.log(e); }



mqttclient.on('error', function(error) {
	
});
	


var retryConnectOnFailure2 = function(retryInMilliseconds) {
	setTimeout(function() {
		
		console.log(new Date().getTime()+": trying to connect to tereocnc...");
		if (!connected2) {
			try {
				client = new net.Socket();
				client.connect(4444, '127.0.0.1', function() {
					console.log('Connected to tereocnc');
					connected2 = true;
				});


				client.on('error', function(error) {
					console.log("Tereocnc error:"+error);
				});
	
	
				client.on('data', function(data) {

					data = data.toString();
					console.log('Tereocnc received: ' + data);
					mqttclient.publish("/RoboticCloud",data);
					//client.destroy(); // kill client after server's response


		
				});
				 
				client.on('close', function() {
					console.log('Tereocnc connection closed');
					connected2 = false;
					retryConnectOnFailure2(RETRY_INTERVAL);
				});




			} catch(e) { console.log(e); }
		}
	}, retryInMilliseconds);
}
  



mqttclient.on('message', function (topic, msg) {

	if (topic == "/RoboticCloud") {
	
		console.log(msg);
		
		// get uid
		var pos = msg.indexOf("@rc@");
		var message = msg;
		
		if (pos>-1) {
			uid = msg.substr(0,pos);
			message = msg.substr(pos+4,msg.length-pos);
		}

		var pos2=0
		
		try {
			pos2 = message.indexOf("@arg@");
		} catch(e) { console.log(e); }

		if (pos2>0) {
			arg = message.substr(pos2+5,message.length-pos2);
			message = message.substr(0,pos2);
		}

		//console.log("uid:"+uid);
		//console.log("message:"+message);

		try {
		
			switch (message) {
				case "CAM_UP":
					client.write("CAM_UP\r\n");
					break;
				case "CAM_DOWN":
					client.write("CAM_DOWN\r\n");
					break;
				case "CAM_ON":
					client.write("CAM_ON\r\n");
					break;
				case "CAM_OFF":
					client.write("CAM_OFF\r\n");
					break;
				case "POW_ON":
					client.write("POW_ON\r\n");
					break;
				case "POW_OFF":
					client.write("POW_OFF\r\n");
					break;
				case "MOVE_LEFT":
					client.write("MOVE_LEFT\r\n");
					break;
				case "MOVE_UP":
					client.write("MOVE_UP\r\n");
					break;
				case "MOVE_STOP":
					client.write("MOVE_STOP\r\n");
					break;
				case "JOG_AXIS":
					client.write(msg+"\r\n");
					break;
				case "MOVE_RIGHT":
					client.write("MOVE_RIGHT\r\n");
					break;
				case "MOVE_DOWN":
					client.write("MOVE_DOWN\r\n");
					break;
				case "SPEED_UP":
					client.write("SPEED_UP\r\n");
					break;
				case "SPEED_NORMAL":
					client.write("SPEED_NORMAL\r\n");
					break;
				case "SPEED_DOWN":
					client.write("SPEED_DOWN\r\n");
					break;
				case "E-STOP-RESET":
					client.write("E-STOP-RESET\r\n");
					break;
				case "E-STOP":
					client.write("E-STOP\r\n");
					break;
				case "MOVE_TO":
					client.write(msg+"\r\n");
					break;	
				case "ROBOT_HOME":
					client.write(msg+"\r\n");
					break;
				case "get_calibration_list":
					client.write(msg+"\r\n");
					break;
				case "get_config_list":
					client.write(msg+"\r\n");
					break;
				case "load_config":
					client.write(msg+"\r\n");
					break;
				case "get_robot_list":
					client.write(msg+"\r\n");
					break;
				case "get_network_list":
					client.write(msg+"\r\n");
					break;
				case "get_toolcalibration_list":
					client.write(msg+"\r\n");	
					break;
				case "get_robot_kinematic":
					client.write(msg+"\r\n");
					break;
				case "get_robot_mesh":
					client.write(msg+"\r\n");
					break;
				case "get_robot_mesh_list":
					client.write(msg+"\r\n");
					break;
				default:
			
			}
		
		} catch(e) { console.log(e); }
	}
	
});

//retryConnectOnFailure1(RETRY_INTERVAL);
try {
	retryConnectOnFailure2(RETRY_INTERVAL);
} catch(e) { console.log(e); }


mqttclient.options.reconnectPeriod = 10; 
