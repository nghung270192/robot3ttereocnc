# -*- coding: utf-8 -*-


import gtk
import os.path

pluginName = "Log"
version = "0.1"
author = "Olivier Martinez"
log_icon = None


def register(parent):


	mod = imp.load_source('log', './plugins/log/log.py')

	parent.add_object('gui.frame','Log',mod.new_log_frame)


	# log icon
	img = gtk.Image()
	img.set_from_file('./plugins/log/log.png')
	img.show()
	log_icon = gtk.ToolButton (img, "Log")
	log_icon.connect("clicked", mod.on_show_log, parent)
	parent.add_icon(log_icon)


