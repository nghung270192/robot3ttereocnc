# -*- coding: utf-8 -*-


import os.path
import imp


pluginName = "Controller config"
version = "0.1"
author = "Olivier Martinez"


def register(parent):

	mod = imp.load_source('controller_config', './plugins/controller_config/controller_config.py')
	parent.add_function("robot.controller.config","Controller config",mod.on_configwin_controller)
	return


