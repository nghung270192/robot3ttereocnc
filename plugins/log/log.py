# -*- coding: utf-8 -*-


from plugins.log.log_view import log_window
from plugins.log.log_view import log_view


def on_show_log(widget,parent):

	f = parent.object_search_path("log_window")

	if f == []:
		f = log_window(parent)
		parent.add_object("log_window","Log window",f)
	else:
		f=f[0]

	f.show()

	return


def new_log_frame(parent):

	f = parent.get_object('frame','Log')

	if f == None:
		f = log_view(parent)
		parent.add_object('gui.frame','Log',f)

	#f = scene_view.scene_view(parent,scene_icon)
	#parent.add_object('frame','Scene',f)

	return f
