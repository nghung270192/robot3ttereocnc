import pygtk
pygtk.require("2.0")
import gtk


class linuxcnc_controller_panel_general(gtk.Frame):


	def __init__(self,tereo,args=None):

		gtk.Frame.__init__(self)

		self.tereo = tereo
		self.tmpdb = args

		panelframe = gtk.ScrolledWindow()
		panelframe.show()
		self.add(panelframe)

		robotgeneraltab = gtk.Table(7,3)
		robotgeneraltab.show()
		panelframe.add_with_viewport(robotgeneraltab)

		label1b = gtk.Label("Linear units")
		label1b.show()
		robotgeneraltab.attach(label1b,0,1,0,1)

		label2b = gtk.Label("Angular units")
		label2b.show()
		robotgeneraltab.attach(label2b,0,1,1,2)

		label3b = gtk.Label("Cycle time")
		label3b.show()
		robotgeneraltab.attach(label3b,0,1,2,3)

		label4b = gtk.Label("Default velocity")
		label4b.show()
		robotgeneraltab.attach(label4b,0,1,3,4)

		label5b = gtk.Label("Max velocity")
		label5b.show()
		robotgeneraltab.attach(label5b,0,1,4,5)

		label6b = gtk.Label("Default acceleration")
		label6b.show()
		robotgeneraltab.attach(label6b,0,1,5,6)

		label7b = gtk.Label("Max acceleration")
		label7b.show()
		robotgeneraltab.attach(label7b,0,1,6,7)

		#-

		self.radiobutton1 = gtk.RadioButton(None,"Inch")
		self.radiobutton1.show()
		robotgeneraltab.attach(self.radiobutton1,1,2,0,1)

		self.radiobutton2 = gtk.RadioButton(self.radiobutton1,"MM")
		self.radiobutton2.show()
		robotgeneraltab.attach(self.radiobutton2,2,3,0,1)

		self.radiobutton3 = gtk.RadioButton(None,"Degrees")
		self.radiobutton3.show()
		robotgeneraltab.attach(self.radiobutton3,1,2,1,2)

		self.radiobutton4 = gtk.RadioButton(self.radiobutton3,"MM")
		self.radiobutton4.show()
		robotgeneraltab.attach(self.radiobutton4,2,3,1,2)

		self.cycletime = gtk.Entry()
		self.cycletime.show()
		robotgeneraltab.attach(self.cycletime,1,2,2,3)

		self.defaultvelocity = gtk.Entry()
		self.defaultvelocity.show()
		robotgeneraltab.attach(self.defaultvelocity,1,2,3,4)

		self.maxvelocity = gtk.Entry()
		self.maxvelocity.show()
		robotgeneraltab.attach(self.maxvelocity,1,2,4,5)

		self.defaultacceleration = gtk.Entry()
		self.defaultacceleration.show()
		robotgeneraltab.attach(self.defaultacceleration,1,2,5,6)

		self.maxacceleration = gtk.Entry()
		self.maxacceleration.show()
		robotgeneraltab.attach(self.maxacceleration,1,2,6,7)

		self.show()

		return


	def save_gene_prop(self):

		t = self.tmpdb.set_section("[TRAJ]")
		if t:
			#t.get_property('AXES', v)
			#t.get_property('COORDINATES',


			t.set_property(['CYCLE_TIME',self.cycletime.get_text()])
			t.set_property(['DEFAULT_VELOCITY',self.defaultvelocity.get_text()])
			t.set_property(['MAX_VELOCITY',self.maxvelocity.get_text()])
			t.set_property(['DEFAULT_ACCELERATION',self.defaultacceleration.get_text()])
			t.set_property(['MAX_ACCELERATION',self.maxacceleration.get_text()])

			# inch, in
			if self.radiobutton1.get_active() == True:
				t.set_property(["LINEAR_UNITS", "inch"])
			else:
				t.set_property(["LINEAR_UNITS", "mm"])

			v = t.get_property('ANGULAR_UNITS')

			if self.radiobutton3.get_active() == True:
				t.set_property(["LINEAR_UNITS", "deg"])
			else:
				t.set_property(["LINEAR_UNITS", "rad"])

		return



	def load_gene_prop(self):

		print "linuxcnc geneprop"

		t = self.tmpdb.get_section("[TRAJ]")
		if t:
			#t.get_property('AXES', v)
			#t.get_property('COORDINATES',

			v = t.get_property('CYCLE_TIME')
			self.cycletime.set_text(v)

			v = t.get_property('DEFAULT_VELOCITY')
			self.defaultvelocity.set_text(v)

			v = t.get_property('MAX_VELOCITY')
			self.maxvelocity.set_text(v)

			v = t.get_property('DEFAULT_ACCELERATION')
			self.defaultacceleration.set_text(v)

			v = t.get_property('MAX_ACCELERATION')
			self.maxacceleration.set_text(v)

			v = t.get_property('LINEAR_UNITS')
			if v == 'inch' or v == 'in':
				self.radiobutton1.set_active(True)
			else:
				self.radiobutton2.set_active(True)

			v = t.get_property('ANGULAR_UNITS')
			if v == 'deg' or v == 'degree':
				self.radiobutton3.set_active(True)
			else:
				self.radiobutton4.set_active(True)

		return


class linuxcnc_controller_panel_axis(gtk.Frame):


	def __init__(self,tereo,args=None):

		gtk.Frame.__init__(self)

		self.tereo = tereo
		self.tmpdb = args
		self.oldaxisindex = -1

		panelframe = gtk.ScrolledWindow()
		panelframe.show()
		self.add(panelframe)

		robotaxistab = gtk.Table(20,3)
		robotaxistab.show()
		panelframe.add_with_viewport(robotaxistab)


		#<property name="homogeneous">True</property>

		self.axisenabled = gtk.CheckButton("Enabled")
		#<property name="width_request">120</property>
		self.axisenabled.show()
		robotaxistab.attach(self.axisenabled,0,1,0,1)

		label8 = gtk.Label("Type")
		label8.show()
		robotaxistab.attach(label8,0,1,1,2)

		self.radiobutton5 = gtk.CheckButton("Angular")
		#<property name="width_request">120</property>
		self.radiobutton5.show()
		robotaxistab.attach(self.radiobutton5,1,2,1,2)

		self.radiobutton6 = gtk.CheckButton("Linear")
		#<property name="width_request">120</property>
		self.radiobutton6.show()
		robotaxistab.attach(self.radiobutton6,2,3,1,2)

		label16 = gtk.Label("Max limit")
		label16.show()
		robotaxistab.attach(label16,0,1,10,11)

		label15 = gtk.Label("Min limit")
		label15.show()
		robotaxistab.attach(label15,0,1,9,10)

		label4 = gtk.Label("Output scale")
		label4.show()
		robotaxistab.attach(label4,0,1,8,9)

		label3 = gtk.Label("Input scale")
		label3.show()
		robotaxistab.attach(label3,0,1,7,8)

		label12 = gtk.Label("Backlash")
		label12.show()
		robotaxistab.attach(label12,0,1,6,7)

		label1 = gtk.Label("Max acceleration")
		label1.show()
		robotaxistab.attach(label1,0,1,5,6)

		label10 = gtk.Label("Max velocity")
		label10.show()
		robotaxistab.attach(label10,0,1,4,5)

		label9 = gtk.Label("Home")
		label9.show()
		robotaxistab.attach(label9,0,1,3,4)

		label17 = gtk.Label("Ferror")
		label17.show()
		robotaxistab.attach(label17,0,1,11,12)

		label18 = gtk.Label("Min ferror")
		label18.show()
		robotaxistab.attach(label18,0,1,12,13)

		label19 = gtk.Label("Home offset")
		label19.show()
		robotaxistab.attach(label19,0,1,13,14)

		label20 = gtk.Label("Home search vel")
		label20.show()
		robotaxistab.attach(label20,0,1,14,15)

		label16 = gtk.Label("Home latch vel")
		label16.show()
		robotaxistab.attach(label16,0,1,15,16)

		label24 = gtk.Label("Home sequence")
		label24.show()
		robotaxistab.attach(label24,0,1,18,19)

		self.robotaxishome = gtk.Entry()
		self.robotaxishome.show()
		robotaxistab.attach(self.robotaxishome,1,2,3,4)

		self.robotaxismaxvelocity = gtk.Entry()
		self.robotaxismaxvelocity.show()
		robotaxistab.attach(self.robotaxismaxvelocity,1,2,4,5)

		self.robotaxismaxacceleration = gtk.Entry()
		self.robotaxismaxacceleration.show()
		robotaxistab.attach(self.robotaxismaxacceleration,1,2,5,6)

		self.robotaxisbacklash = gtk.Entry()
		self.robotaxisbacklash.show()
		robotaxistab.attach(self.robotaxisbacklash,1,2,6,7)

		self.robotaxisinputscale = gtk.Entry()
		self.robotaxisinputscale.show()
		robotaxistab.attach(self.robotaxisinputscale,1,2,7,8)

		self.robotaxisoutputscale = gtk.Entry()
		self.robotaxisoutputscale.show()
		robotaxistab.attach(self.robotaxisoutputscale,1,2,8,9)

		self.robotaxisminlimit = gtk.Entry()
		self.robotaxisminlimit.show()
		robotaxistab.attach(self.robotaxisminlimit,1,2,9,10)

		self.robotaxismaxlimit = gtk.Entry()
		self.robotaxismaxlimit.show()
		robotaxistab.attach(self.robotaxismaxlimit,1,2,10,11)

		self.robotaxisferror = gtk.Entry()
		self.robotaxisferror.show()
		robotaxistab.attach(self.robotaxisferror,1,2,11,12)

		self.robotaxisminferror = gtk.Entry()
		self.robotaxisminferror.show()
		robotaxistab.attach(self.robotaxisminferror,1,2,12,13)

		self.robotaxishomeoffset = gtk.Entry()
		self.robotaxishomeoffset.show()
		robotaxistab.attach(self.robotaxishomeoffset,1,2,13,14)

		self.robotaxishomesearchvel = gtk.Entry()
		self.robotaxishomesearchvel.show()
		robotaxistab.attach(self.robotaxishomesearchvel,1,2,14,15)

		self.robotaxishomelatchvel = gtk.Entry()
		self.robotaxishomelatchvel.show()
		robotaxistab.attach(self.robotaxishomelatchvel,1,2,15,16)

		self.robotaxishomeuseindex = gtk.CheckButton("Home use index")
		self.robotaxishomeuseindex.show()
		robotaxistab.attach(self.robotaxishomeuseindex,0,1,16,17)

		self.robotaxishomeignorelimits = gtk.CheckButton("Home ignore limits")
		self.robotaxishomeignorelimits.show()
		robotaxistab.attach(self.robotaxishomeignorelimits,0,1,17,18)

		hbox7 = gtk.HBox()
		hbox7.show()
		self.robotaxisoriginx = gtk.Entry()
		self.robotaxisoriginx.show()
		hbox7.pack_start(self.robotaxisoriginx,False,False,0)

		self.robotaxisoriginy = gtk.Entry()
		self.robotaxisoriginy.show()
		hbox7.pack_start(self.robotaxisoriginy,False,False,1)

		self.robotaxisoriginz = gtk.Entry()
		self.robotaxisoriginz.show()
		hbox7.pack_start(self.robotaxisoriginz,False,False,2)

		robotaxistab.attach(hbox7,1,2,19,20)

		label31 = gtk.Label("Parent joint position (x,y,z)")
		label31.show()
		robotaxistab.attach(label31,0,1,19,20)

		self.show()

		return



	def load_axis_prop(self,idx):


		t = self.tmpdb.get_section("[AXIS_%d"%(idx+1)+"]")
		if t:
			v = t.get_property('HOME')
			if v: self.robotaxishome.set_text(v)
			v = t.get_property('MAX_VELOCITY')
			if v: self.robotaxismaxvelocity.set_text(v)
			v  = t.get_property('MAX_ACCELERATION')
			if v: self.robotaxismaxacceleration.set_text(v)
			v = t.get_property('BACKLASH')
			if v: self.robotaxisbacklash.set_text(v)
			v = t.get_property('INPUT_SCALE')
			if v: self.robotaxisinputscale.set_text(v)
			v = t.get_property('OUTPUT_SCALE')
			if v: self.robotaxisoutputscale.set_text(v)
			v = t.get_property('MIN_LIMIT')
			if v: self.robotaxisminlimit.set_text(v)
			v = t.get_property('MAX_LIMIT')
			if v: self.robotaxismaxlimit.set_text(v)
			v = t.get_property('FERROR')
			if v: self.robotaxisferror.set_text(v)
			v = t.get_property('MIN_FERROR')
			if v: self.robotaxisminferror.set_text(v)
			v = t.get_property('HOME_OFFSET')
			if v: self.robotaxishomeoffset.set_text(v)
			v = t.get_property('HOME_SEARCH')
			if v: self.robotaxishomesearchvel.set_text(v)
			v = t.get_property('HOME_LATCH')
			if v: self.robotaxishomelatchvel.set_text(v)
			v = t.get_property('HOME_USE_INDEX')
			#self.home_use_index = 'NO' #NO
			v = t.get_property('HOME_IGNORE_LIMITS')
			#self.home_ignore_limits = 'NO' #NO
			v = t.get_property('HOME_SEQUENCE')
			#self.builder.get_object("robotaxishomesequence").set_text(str(ap.home_sequence))

			#self.type = 'ANGULAR' #ANGULAR

			self.robotaxisoriginx.set_text("0.0")
			self.robotaxisoriginy.set_text("0.0")
			self.robotaxisoriginz.set_text("0.0")

		self.oldaxisindex = idx

		return


	def save_old_axis(self):

		if self.oldaxisindex == -1:
			return

		t = self.tmpdb.set_section("[AXIS_%d"%(self.oldaxisindex+1)+"]")
		if t:
			t.set_property(['HOME',self.robotaxishome.get_text()])
			t.set_property(['MAX_VELOCITY',self.robotaxismaxvelocity.get_text()])
			t.set_property(['MAX_ACCELERATION',self.robotaxismaxacceleration.get_text()])
			t.set_property(['BACKLASH',self.robotaxisbacklash.get_text()])
			t.set_property(['INPUT_SCALE',self.robotaxisinputscale.get_text()])
			t.set_property(['OUTPUT_SCALE',self.robotaxisoutputscale.get_text()])
			t.set_property(['MIN_LIMIT',self.robotaxisminlimit.get_text()])
			t.set_property(['MAX_LIMIT',self.robotaxismaxlimit.get_text()])
			t.set_property(['FERROR',self.robotaxisferror.get_text()])
			t.set_property(['MIN_FERROR',self.robotaxisminferror.get_text()])
			t.set_property(['HOME_OFFSET',self.robotaxishomeoffset.get_text()])
			t.set_property(['HOME_SEARCH',self.robotaxishomesearchvel.get_text()])
			t.set_property(['HOME_LATCH',self.robotaxishomelatchvel.get_text()])
			#t.set_property('HOME_USE_INDEX',
			#self.home_use_index = 'NO' #NO
			#t.set_property('HOME_IGNORE_LIMITS')
			#self.home_ignore_limits = 'NO' #NO
			#t.set_property('HOME_SEQUENCE')
			#t.set_property('HOME', self.axis_prop[i].home)
			#self.type = 'ANGULAR' #ANGULAR


		return



def new_linuxcnc_controller_panel_general(tereo,arg=None):
	return linuxcnc_controller_panel_general(tereo,arg)


def new_linuxcnc_controller_panel_axis(tereo,arg=None):
	return linuxcnc_controller_panel_axis(tereo,arg)